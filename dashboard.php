<?php
ob_start();
include_once("include/check_login_status.php");
$u = "";
$id = "";

if (!$log_username) {
    echo "<script type=\"text/javascript\">" .
    "alert('please log in');" .
    "</script>";
    header("location: http://www.Rewrapped.ca/login.php");
    exit();
}
// Make sure the _GET username is set, and sanitize it
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE username='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else if ($log_username != $u) {
    echo "<script type=\"text/javascript\">" .
    "alert('please log in');" .
    "</script>";

    $d = "SELECT id FROM users WHERE username='$log_username' AND activated='1'";
    $query = mysqli_query($db_conx, $d);
    $rou = mysqli_fetch_row($query);
    $id = $rou[0];

    header("location: dashboard.php?id=$id");
    exit();
} else {
    $d = "SELECT id FROM users WHERE username='$log_username' AND activated='1'";
    $query = mysqli_query($db_conx, $d);
    $rou = mysqli_fetch_row($query);
    $id = $rou[0];
    $u = $log_username;
//    header("location: index.php");
//    exit();
}
$news = '';
$boxes = '';
$sql2 = "SELECT * FROM users WHERE id='$id' AND activated='1'";
$user_query2 = mysqli_query($db_conx, $sql2);
$row2 = mysqli_fetch_array($user_query2, MYSQLI_ASSOC);
$fname = $row2["firstname"];
$lname = $row2["lastname"];
$avatar = $row2["avatar"];
$limit = $row2["bidlimit"];
//$lname = $row2["lastname"];
// It is important for any file that includes this file, to have
// check_login_status.php included at its very top.
//$envelope = ''; //<img src="images/note_dead.png" width="32" height="32" alt="Notes" title="Notifications for registered members">
//$loginLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text"><a href="login.php"><span class="glyphicon glyphicon-user"></span> Log In</a></p>';
//$signupLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text"><a href="signup.php"><span class="glyphicon glyphicon-log-in"></span> Sign Up</a></p>';
if (isset($user_ok) && $user_ok == true) {
    $sql = "SELECT notescheck FROM users WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    $notescheck = $row[0];
    $sql = "SELECT id FROM notifications WHERE username='$log_username' AND date_time > '$notescheck' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $numrows = mysqli_num_rows($query);

    $fsql = "SELECT * FROM friends WHERE user2='$log_username' AND accepted='0' AND datemade > '$notescheck' LIMIT 1";
    $fquery = mysqli_query($db_conx, $fsql);
    $fnumrows = mysqli_num_rows($fquery);

    $sql = "SELECT * FROM users WHERE username='$log_username' AND activated='1' LIMIT 1";
    $user_query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_array($user_query, MYSQLI_ASSOC);
    $userlevel = $row["userlevel"];
    $usid = $row["id"];
    $rnumrows = "";
    if ($userlevel == "a") {
        $rsql = "SELECT id FROM bidnotes WHERE owner='$log_username' AND app='bid placed' AND date_time > '$notescheck'"; //Limit 1
        $rquery = mysqli_query($db_conx, $rsql);
        $rnumrows = mysqli_num_rows($rquery);
    } else if ($userlevel == "b") {
        $rsql = "SELECT id FROM bidnotes WHERE agent='$log_username' AND app='info request' AND date_time > '$notescheck'"; //Limit 1
        $rquery = mysqli_query($db_conx, $rsql);
        $rnumrows = mysqli_num_rows($rquery);
    }

    $asql = "SELECT * FROM private_messages WHERE to_id='$id' AND time_sent > '$notescheck' AND opened='0'";
    $aquery = mysqli_query($db_conx, $asql);
    $anumrows = mysqli_num_rows($aquery);

    $num_note = $numrows + $fnumrows + $rnumrows; // + $anumrows;
//    if ($numrows == 0 && $fnumrows == 0 && $rnumrows == 0 && $anumrows == 0) {
//        $envelope = '<a href="notifications.php" title="Your notifications and friend requests" class="btn btn-primary" style="margin-top: 6px;" role="button">Notifications <span class="badge">' . $num_note . '</span></a>'; //<img src="images/note_still.png" width="32" height="32" alt="Notes"></a>';
//    } else {
//        $envelope = '<a href="notifications.php" title="Your notifications and friend requests" class="btn btn-primary" style="margin-top: 6px;" role="button">Notifications <span class="badge">' . $num_note . '</span></a>'; //'<a href="notifications.php" title="You have new notifications"><img src="images/red_bl.gif" width="32" height="32" alt="Notes"></a>';
//    }
//    $loginLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text">Signed in as <a href="user.php?u=' . $log_username . '"><span class="glyphicon glyphicon-user"></span> ' . $log_username . '</a></p>';
//    $signupLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text"><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Log Out</a></p>';
}
?>
<?php
// AJAX CALLS THIS LOGIN CODE TO EXECUTE
if (isset($_POST["e"])) {
    // CONNECT TO THE DATABASE
    include_once("include/mysql_connect.php");
    // GATHER THE POSTED DATA INTO LOCAL VARIABLES AND SANITIZE
    $e = mysqli_real_escape_string($db_conx, $_POST['e']);
    $p = md5($_POST['p']);
    // GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
    // FORM DATA ERROR HANDLING
    if ($e == "" || $p == "") {
        echo "login_failed";
        exit();
    } else {
        // END FORM DATA ERROR HANDLING
        $sql = "SELECT id, username, password FROM users WHERE email='$e' AND activated='1' LIMIT 1";
        $query = mysqli_query($db_conx, $sql);
        $row = mysqli_fetch_row($query);
        $db_id = $row[0];
        $db_username = $row[1];
        $db_pass_str = $row[2];
        if ($p != $db_pass_str) {
            echo "login_failed";
            exit();
        } else {
            // CREATE THEIR SESSIONS AND COOKIES
            $_SESSION['userid'] = $db_id;
            $_SESSION['username'] = $db_username;
            $_SESSION['password'] = $db_pass_str;
            setcookie("id", $db_id, strtotime('+30 days'), "/", "", "", TRUE);
            setcookie("user", $db_username, strtotime('+30 days'), "/", "", "", TRUE);
            setcookie("pass", $db_pass_str, strtotime('+30 days'), "/", "", "", TRUE);
            // UPDATE THEIR "IP" AND "LASTLOGIN" FIELDS
            $sql = "UPDATE users SET ip='$ip', lastlogin=now() WHERE username='$db_username' LIMIT 1";
            $query = mysqli_query($db_conx, $sql);
            echo $db_username;
            exit();
        }
    }
    exit();
}
ob_end_flush();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <!--<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">-->

        <title>Rewrapped - Dashboard</title>

        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    

        <!-- Custom styles for this template -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/style-responsive.css" rel="stylesheet">

        <script src="assets/js/chart-master/Chart.js"></script>

        <!-- taken from user page's -->
        <!--        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        
                <link rel="stylesheet" href="css/bootstrap.min.css">
        
                <link rel="stylesheet" href="css/bootstrap-theme.min.css">
                <link rel="stylesheet" href="css/main.css">
        
                <link rel="apple-touch-icon" href="apple-touch-icon.png">
        
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        
                <link rel="stylesheet" href="css/bootstrap-theme.min.css">
                <link rel="stylesheet" href="css/main.css">
        
-->                <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
                <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script><!--
                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        
        
                [if lte IE 8]>
                  
                    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
                  
                <![endif]
                [if gt IE 8]><!
        
                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">
        
                <![endif]
        
        
        
                <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
        
                <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
                <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>-->


        <!-- end of stuff taken from user page -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script>
            function reqHandler(action, reqid, user1, pid, elem) {
                var conf = confirm("Press OK to '" + action + "' request and send your info.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = "processing ...";
                var ajax = ajaxObj("POST", "php_parsers/request_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "accept_ok") {
                            _(elem).innerHTML = "<b>Request Accepted!</b><br />Your info has been sentf";
                        } else if (ajax.responseText == "reject_ok") {
                            _(elem).innerHTML = "<b>Request Rejected</b><br />You chose to deny the request";
                        } else {
                            _(elem).innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.send("action=" + action + "&reqid=" + reqid + "&user1=" + user1 + "&postid=" + pid);
            }

            function friendReqHandler(action, reqid, user1, elem) {
                var conf = confirm("Press OK to '" + action + "' this friend request.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = "processing ...";
                var ajax = ajaxObj("POST", "php_parsers/friend_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "accept_ok") {
                            _(elem).innerHTML = "<b>Request Accepted!</b><br />Your are now friends";
                        } else if (ajax.responseText == "reject_ok") {
                            _(elem).innerHTML = "<b>Request Rejected</b><br />You chose to reject friendship with this user";
                        } else {
                            _(elem).innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.send("action=" + action + "&reqid=" + reqid + "&user1=" + user1);
            }
        </script>

    </head>

    <body>

        <section id="container" >
            <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
            *********************************************************************************************************************************************************** -->
            <!--header start-->
            <header class="header black-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                <!--logo start-->
                <a href="dashboard.php" class="logo"><b>Your Dashboard</b></a>
                <!--logo end-->
                <div class="nav notify-row" id="top_menu">
                    <!--  notification start -->
                    <ul class="nav top-menu">
                        <!-- settings start -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                                <i class="fa fa-tasks"></i>
                                <span class="badge bg-theme">4</span>
                            </a>
                            <ul class="dropdown-menu extended tasks-bar">
                                <div class="notify-arrow notify-arrow-green"></div>
                                <li>
                                    <p class="green">You have 4 pending tasks</p>
                                </li>
                                <li>
                                    <a href="index.html#">
                                        <div class="task-info">
                                            <div class="desc">DashGum Admin Panel</div>
                                            <div class="percent">40%</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.html#">
                                        <div class="task-info">
                                            <div class="desc">Database Update</div>
                                            <div class="percent">60%</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.html#">
                                        <div class="task-info">
                                            <div class="desc">Product Development</div>
                                            <div class="percent">80%</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                <span class="sr-only">80% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.html#">
                                        <div class="task-info">
                                            <div class="desc">Payments Sent</div>
                                            <div class="percent">70%</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                <span class="sr-only">70% Complete (Important)</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="external">
                                    <a href="#">See All Tasks</a>
                                </li>
                            </ul>
                        </li>
                        <!-- settings end -->
                        <!-- inbox dropdown start-->
                        <li id="header_inbox_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-theme"><?php echo $anumrows; ?></span>
                            </a>
                            <ul class="dropdown-menu extended inbox">
                                <div class="notify-arrow notify-arrow-green"></div>
                                <li>
                                    <p class="green">You have <?php echo $anumrows; ?> new messages</p>
                                </li>
                                <?php
///////////End take away///////////////////////
// SQL to gather their entire PM list
                                $sql = mysqli_query($db_conx, "SELECT * FROM private_messages WHERE to_id='$id' AND opened='0' AND recipientDelete='0' AND time_sent > '$notescheck' ORDER BY id DESC LIMIT 4");

                                while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {

                                    $date = strftime("%b %d, %Y", strtotime($row['time_sent']));
                                    if ($row['opened'] == "0") {
                                        $textWeight = 'msgDefault';
                                    } else {
                                        $textWeight = 'msgRead';
                                    }
                                    $fr_id = $row['from_id'];
                                    // SQL - Collect username for sender inside loop
                                    $ret = mysqli_query($db_conx, "SELECT id, username, firstname, lastname, avatar FROM users WHERE id='$fr_id' LIMIT 1");
                                    while ($raw = mysqli_fetch_array($ret, MYSQLI_ASSOC)) {
                                        $Sid = $raw['id'];
                                        $Sname = $raw['firstname'];
                                        $Suname = $raw['username'];
                                        $Slname = $raw['lastname'];
                                        $senderav = $raw['avatar'];
                                    }
                                    ?>
                                    <li>
                                        <a href="pm_inbox.php">
                                            <span class="photo"><img alt="avatar" src="user/<?php echo $Suname; ?>/<?php echo $senderav; ?>"></span>
                                            <span class="subject">
                                                <span class="from"><?php echo $Sname; ?> <?php echo $Slname; ?></span>
                                                <span class="time">Just now</span>
                                            </span>
                                            <span class="message">
                                                <?php echo $row['subject']; ?>...
                                            </span>
                                        </a>
                                    </li>
                                    <?php
                                }// Close Main while loop
                                ?>
                                <li>
                                    <a href="pm_inbox.php">See all messages</a>
                                </li>
                            </ul>
                        </li>
                        <!-- inbox dropdown end -->
                    </ul>
                    <!--  notification end -->
                </div>
                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li><a class="logout" href="logout.php">Logout</a></li>
                    </ul>
                    <ul class="nav pull-right top-menu">
                        <li><p style="font-size: 16px; padding: 0%;" class="navbar-text">Signed in as <a href="user.php?u=<?php echo $log_username; ?>"><span class="glyphicon glyphicon-user"></span><?php echo $log_username; ?></a></p></li>
                    </ul>

                    <ul class="nav pull-right top-menu">
                        <li class="dropdown navbar-text">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Visit<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="dashboard.php">Dashboard</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="allposts.php">All Posts</a></li>
                                <li><a href="allagents.php">All Agents</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="contactus.php">Contact Us</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="aboutus.php">About Us</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </header>
            <!--header end-->

            <!-- **********************************************************************************************************************************************************
            MAIN SIDEBAR MENU
            *********************************************************************************************************************************************************** -->
            <!--sidebar start-->
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu" id="nav-accordion">

                        <p class="centered"><a href="user.php?id=<?php echo $id ?>"><img src="user/<?php echo $u; ?>/<?php echo $avatar; ?>" class="img-circle" width="60"></a></p>
                        <h5 class="centered"><?php echo $fname; ?> <?php echo $lname; ?></h5>

                        <li class="mt">
                            <a class="active" href="user.php?id=<?php echo $id ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>User Page</span>
                            </a>
                        </li>

                        <li class="sub-menu">
                            <a href="javascript:;" >
                                <i class="fa fa-desktop"></i>
                                <span>Videos</span>
                            </a>
                            <ul class="sub">
                                <li><a  href="general.html">General</a></li>
                                <li><a  href="buttons.html">Buttons</a></li>
                                <!--<li><a  href="panels.html">Panels</a></li>-->
                            </ul>
                        </li>

                        <!--                        <li class="sub-menu">
                                                    <a href="javascript:;" >
                                                        <i class="fa fa-cogs"></i>
                                                        <span>Components</span>
                                                    </a>
                                                    <ul class="sub">
                                                        <li><a  href="calendar.html">Calendar</a></li>
                                                        <li><a  href="gallery.html">Gallery</a></li>
                                                        <li><a  href="todo_list.html">Todo List</a></li>
                                                    </ul>
                                                </li>-->
                        <li class="sub-menu">
                            <a href="javascript:;" >
                                <i class="fa fa-book"></i>
                                <span>Extra Pages</span>
                            </a>
                            <ul class="sub">
                                <li><a  href="blank.html">Blank Page</a></li>
                                <li><a  href="login.html">Login</a></li>
                                <li><a  href="lock_screen.html">Lock Screen</a></li>
                            </ul>
                        </li>
                        <!--                        <li class="sub-menu">
                                                    <a href="javascript:;" >
                                                        <i class="fa fa-tasks"></i>
                                                        <span>Forms</span>
                                                    </a>
                                                    <ul class="sub">
                                                        <li><a  href="form_component.html">Form Components</a></li>
                                                    </ul>
                                                </li>
                                                <li class="sub-menu">
                                                    <a href="javascript:;" >
                                                        <i class="fa fa-th"></i>
                                                        <span>Data Tables</span>
                                                    </a>
                                                    <ul class="sub">
                                                        <li><a  href="basic_table.html">Basic Table</a></li>
                                                        <li><a  href="responsive_table.html">Responsive Table</a></li>
                                                    </ul>
                                                </li>
                                                <li class="sub-menu">
                                                    <a href="javascript:;" >
                                                        <i class=" fa fa-bar-chart-o"></i>
                                                        <span>Charts</span>
                                                    </a>
                                                    <ul class="sub">
                                                        <li><a  href="morris.html">Morris</a></li>
                                                        <li><a  href="chartjs.html">Chartjs</a></li>
                                                    </ul>-->
                        </li>

                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->

            <!-- **********************************************************************************************************************************************************
            MAIN CONTENT
            *********************************************************************************************************************************************************** -->
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">

                    <div class="row">
                        <div class="col-lg-9 main-chart">

                            <div class="row mtbox">
                                <div class="col-md-2 col-sm-2 col-md-offset-1 box0">
                                    <div class="box1">
                                        <span class="li_heart"></span>
                                        <h3>933</h3>
                                    </div>
                                    <p>933 People liked your page the last 24hs. Whoohoo!</p>
                                </div>
                                <div class="col-md-2 col-sm-2 box0">
                                    <div class="box1">
                                        <span class="li_cloud"></span>
                                        <h3>+48</h3>
                                    </div>
                                    <p>48 New files were added in your cloud storage.</p>
                                </div>
                                <div class="col-md-2 col-sm-2 box0">
                                    <div class="box1">
                                        <span class="li_stack"></span>
                                        <h3><?php echo $anumrows; ?></h3>
                                    </div>
                                    <p>You have <?php echo $anumrows; ?> unread messages in your inbox.</p>
                                </div>
                                <div class="col-md-2 col-sm-2 box0">
                                    <?php
                                    if ($userlevel == 'b') {
                                        $news .= '<div class="box1">
                                        <span class="li_news"></span>
                                        <h3>+' . $limit . '</h3>
                                    </div>
                                    <p>You have ' . $limit . ' bids remaining on your account.</p>';
                                    } else {
                                        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM bids WHERE ownerid='$id'");
                                        $countrow = mysqli_fetch_row($countquery);
                                        $count = $countrow[0];
                                        $news .= '<div class="box1">
                                        <span class="li_news"></span>
                                        <h3>+' . $count . '</h3>
                                    </div>
                                    <p>You have ' . $count . ' active bids on your posts.</p>';
                                    }
                                    ?>
                                    <?php echo $news; ?>
                                </div>
                                <div class="col-md-2 col-sm-2 box0">
                                    <div class="box1">
                                        <span class="li_data"></span>
                                        <h3>OK!</h3>
                                    </div>
                                    <p>Your server is working perfectly. Relax & enjoy.</p>
                                </div>

                            </div><!-- /row mt -->	

                            <?php
                            if ($userlevel == 'b') {
                                $boxes .= '
                                <!-- SERVER STATUS PANELS -->
                                <div class="col-md-4 col-sm-4 mb">
                                    <div class="white-panel pn donut-chart">
                                        <div class="white-header">
                                            <h5>SERVER LOAD</h5>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 goleft">
                                                <p><i class="fa fa-database"></i> 70%</p>
                                            </div>
                                        </div>
                                        <canvas id="serverstatus01" height="120" width="120"></canvas>
                                        <script>
                                            var doughnutData = [
                                                {
                                                    value: 70,
                                                    color: "#68dff0"
                                                },
                                                {
                                                    value: 30,
                                                    color: "#fdfdfd"
                                                }
                                            ];
                                            var myDoughnut = new Chart(document.getElementById("serverstatus01").getContext("2d")).Doughnut(doughnutData);</script>
                                    </div><! --/grey-panel -->
                                </div><!-- /col-md-4-->


                                <div class="col-md-4 col-sm-4 mb">
                                    <div class="white-panel pn">
                                        <div class="white-header">
                                            <h5>TOP PRODUCT</h5>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 goleft">
                                                <p><i class="fa fa-heart"></i> 122</p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6"></div>
                                        </div>
                                        <div class="centered">
                                            <img src="assets/img/product.png" width="120">
                                        </div>
                                    </div>
                                </div><!-- /col-md-4 -->

                                <div class="col-md-4 mb">
                                    <!-- WHITE PANEL - TOP USER -->
                                    <div class="white-panel pn">
                                        <div class="white-header">
                                            <h5>TOP USER</h5>
                                        </div>
                                        <p><img src="assets/img/ui-zac.jpg" class="img-circle" width="80"></p>
                                        <p><b>Zac Snider</b></p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="small mt">MEMBER SINCE</p>
                                                <p>2012</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="small mt">TOTAL SPEND</p>
                                                <p>$ 47,60</p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /col-md-4 -->


                            </div><!-- /row -->


                            <div class="row">
                                <!-- TWITTER PANEL -->
                                <div class="col-md-4 mb">
                                    <div class="darkblue-panel pn">
                                        <div class="darkblue-header">
                                            <h5>DROPBOX STATICS</h5>
                                        </div>
                                        <canvas id="serverstatus02" height="120" width="120"></canvas>
                                        <script>
                                            var doughnutData = [
                                                {
                                                    value: 60,
                                                    color: "#68dff0"
                                                },
                                                {
                                                    value: 40,
                                                    color: "#444c57"
                                                }
                                            ];
                                            var myDoughnut = new Chart(document.getElementById("serverstatus02").getContext("2d")).Doughnut(doughnutData);</script>
                                        <p>April 17, 2014</p>
                                        <footer>
                                            <div class="pull-left">
                                                <h5><i class="fa fa-hdd-o"></i> 17 GB</h5>
                                            </div>
                                            <div class="pull-right">
                                                <h5>60% Used</h5>
                                            </div>
                                        </footer>
                                    </div><! -- /darkblue panel -->
                                </div><!-- /col-md-4 -->


                                <div class="col-md-4 mb">
                                    <!-- INSTAGRAM PANEL -->
                                    <div class="instagram-panel pn">
                                        <i class="fa fa-instagram fa-4x"></i>
                                        <p>@THISISYOU<br/>
                                            5 min. ago
                                        </p>
                                        <p><i class="fa fa-comment"></i> 18 | <i class="fa fa-heart"></i> 49</p>
                                    </div>
                                </div><!-- /col-md-4 -->

                                <div class="col-md-4 col-sm-4 mb">
                                    <!-- REVENUE PANEL -->
                                    <div class="darkblue-panel pn">
                                        <div class="darkblue-header">
                                            <h5>REVENUE</h5>
                                        </div>
                                        <div class="chart mt">
                                            <div class="sparkline" data-type="line" data-resize="true" data-height="75" data-width="90%" data-line-width="1" data-line-color="#fff" data-spot-color="#fff" data-fill-color="" data-highlight-line-color="#fff" data-spot-radius="4" data-data="[200,135,667,333,526,996,564,123,890,464,655]"></div>
                                        </div>
                                        <p class="mt"><b>$ 17,980</b><br/>Month Income</p>
                                    </div></div>';
                            } else {
                                $boxes .='';
                            }
                            ?>
                            <div class="row mt">
                                <?php echo $boxes; ?>
                            </div>
                            <!--</div> /row -->

                            <!--                            <div class="row mt">
                                                            CUSTOM CHART START 
                                                            <div class="border-head">
                                                                <h3>VISITS</h3>
                                                            </div>
                                                            <div class="custom-bar-chart">
                                                                <ul class="y-axis">
                                                                    <li><span>10.000</span></li>
                                                                    <li><span>8.000</span></li>
                                                                    <li><span>6.000</span></li>
                                                                    <li><span>4.000</span></li>
                                                                    <li><span>2.000</span></li>
                                                                    <li><span>0</span></li>
                                                                </ul>
                                                                <div class="bar">
                                                                    <div class="title">JAN</div>
                                                                    <div class="value tooltips" data-original-title="8.500" data-toggle="tooltip" data-placement="top">85%</div>
                                                                </div>
                                                                <div class="bar ">
                                                                    <div class="title">FEB</div>
                                                                    <div class="value tooltips" data-original-title="5.000" data-toggle="tooltip" data-placement="top">50%</div>
                                                                </div>
                                                                <div class="bar ">
                                                                    <div class="title">MAR</div>
                                                                    <div class="value tooltips" data-original-title="6.000" data-toggle="tooltip" data-placement="top">60%</div>
                                                                </div>
                                                                <div class="bar ">
                                                                    <div class="title">APR</div>
                                                                    <div class="value tooltips" data-original-title="4.500" data-toggle="tooltip" data-placement="top">45%</div>
                                                                </div>
                                                                <div class="bar">
                                                                    <div class="title">MAY</div>
                                                                    <div class="value tooltips" data-original-title="3.200" data-toggle="tooltip" data-placement="top">32%</div>
                                                                </div>
                                                                <div class="bar ">
                                                                    <div class="title">JUN</div>
                                                                    <div class="value tooltips" data-original-title="6.200" data-toggle="tooltip" data-placement="top">62%</div>
                                                                </div>
                                                                <div class="bar">
                                                                    <div class="title">JUL</div>
                                                                    <div class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top">75%</div>
                                                                </div>
                                                            </div>
                                                            custom chart end
                                                        </div> /row 	-->

                        </div> <!--/col-lg-9 END SECTION MIDDLE  -->


                        <!-- **********************************************************************************************************************************************************
                        RIGHT SIDEBAR CONTENT
                        *********************************************************************************************************************************************************** -->                  

                        <div class="col-lg-3 ds">
                            <!--COMPLETED ACTIONS DONUTS CHART-->
                            <h3><a href="notifications.php"> NOTIFICATIONS (<?php echo $num_note; ?>)</a></h3>
                            <?php
                            $friend_requests = "";
                            $sql = "SELECT * FROM friends WHERE user2='$log_username' AND accepted='0' ORDER BY datemade ASC";
                            $query = mysqli_query($db_conx, $sql);
                            $numrows = mysqli_num_rows($query);
                            if ($numrows < 1) {
                                $friend_requests = '<div class="desc">
                                                            <div class="thumb">
                                                                <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                            <div class="details">
                                                                <p>No Friends Requests received...
                                                                </p>
                                                            </div>
                                                        </div>';
//                                $friend_requests = 'No friend requests';
                            } else {
                                while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                                    $reqID = $row["id"];
                                    $user1 = $row["user1"];
                                    $datemade = $row["datemade"];
                                    $datemade = strftime("%B %d", strtotime($datemade));
                                    $thumbquery = mysqli_query($db_conx, "SELECT id, avatar, firstname, lastname FROM users WHERE username='$user1' LIMIT 1");
                                    $thumbrow = mysqli_fetch_row($thumbquery);
                                    $userid = $thumbrow[0];
                                    $user1avatar = $thumbrow[1];
                                    $usern = $thumbrow[2] . ' ' . $thumbrow[3];
                                    ;
                                    //$userln = $thumbrow[2];
                                    $user1pic = '<img src="user/' . $user1 . '/' . $user1avatar . '" alt="' . $user1 . '" class="img-circle" width="35px" height="35px" align="">';
                                    if ($user1avatar == NULL) {
                                        $user1pic = '<img src="images/avatardefault.jpg" alt="' . $user1 . '" class="img-circle" width="35px" height="35px" align="">';
                                    }
                                    $friend_requests .= '<div class="desc" id="friendreq_' . $reqID . '">
                                                            <div class="thumb">
                                                                <a href="user.php?id=' . $userid . '">' . $user1pic . '</a>
                                                            </div>
                                                            <div class="details" id="user_info_' . $reqID . '">
                                                                <p><muted>' . $datemade . '</muted><br/>
                                                                <a href="user.php?id=' . $userid . '">' . $usern . '</a> Requests to connect with you.<br/>
                                                                </p>
                                                                <button onclick="friendReqHandler(\'accept\',\'' . $reqID . '\',\'' . $user1 . '\',\'user_info_' . $reqID . '\')">accept</button> or 
                                                                    <button onclick="friendReqHandler(\'reject\',\'' . $reqID . '\',\'' . $user1 . '\',\'user_info_' . $reqID . '\')">reject</button>
                                                            </div>
                                                        </div>';
//                                    $friend_requests .= '<div id="friendreq_' . $reqID . '" class="friendrequests">';
//                                    $friend_requests .= '<a href="user.php?u=' . $user1 . '">' . $user1pic . '</a>';
//                                    $friend_requests .= '<div class="user_info" id="user_info_' . $reqID . '">' . $datemade . ' <a href="user.php?id=' . $userid . '">' . $user1 . '</a> requests friendship<br /><br />';
//                                    $friend_requests .= '<button onclick="friendReqHandler(\'accept\',\'' . $reqID . '\',\'' . $user1 . '\',\'user_info_' . $reqID . '\')">accept</button> or ';
//                                    $friend_requests .= '<button onclick="friendReqHandler(\'reject\',\'' . $reqID . '\',\'' . $user1 . '\',\'user_info_' . $reqID . '\')">reject</button>';
//                                    $friend_requests .= '</div>';
//                                    $friend_requests .= '</div>';
                                }
                            }
                            ?>
                            <?php echo $friend_requests; ?>
                            <!-- First Action -->
                            <!--                                <div class="desc">
                                                                <div class="thumb">
                                                                    <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                                                                </div>
                                                                <div class="details">
                                                                    <p><muted>2 Minutes Ago</muted><br/>
                                                                    <a href="#">James Brown</a> subscribed to your newsletter.<br/>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            Second Action 
                                                            <div class="desc">
                                                                <div class="thumb">
                                                                    <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                                                                </div>
                                                                <div class="details">
                                                                    <p><muted>3 Hours Ago</muted><br/>
                                                                    <a href="#">Diana Kennedy</a> purchased a year subscription.<br/>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            Third Action 
                                                            <div class="desc">
                                                                <div class="thumb">
                                                                    <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                                                                </div>
                                                                <div class="details">
                                                                    <p><muted>7 Hours Ago</muted><br/>
                                                                    <a href="#">Brandon Page</a> purchased a year subscription.<br/>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            Fourth Action 
                                                            <div class="desc">
                                                                <div class="thumb">
                                                                    <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                                                                </div>
                                                                <div class="details">
                                                                    <p><muted>11 Hours Ago</muted><br/>
                                                                    <a href="#">Mark Twain</a> commented your post.<br/>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            Fifth Action 
                                                            <div class="desc">
                                                                <div class="thumb">
                                                                    <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                                                                </div>
                                                                <div class="details">
                                                                    <p><muted>18 Hours Ago</muted><br/>
                                                                    <a href="#">Daniel Pratt</a> purchased a wallet in your store.<br/>
                                                                    </p>
                                                                </div>
                                                            </div>-->

                            <!-- USERS ONLINE SECTION -->
                            <h3>TEAM MEMBERS</h3>
                            <!-- First Member -->
                            <div class="desc">
                                <div class="thumb">
                                    <img class="img-circle" src="assets/img/ui-divya.jpg" width="35px" height="35px" align="">
                                </div>
                                <div class="details">
                                    <p><a href="#">DIVYA MANIAN</a><br/>
                                    <muted>Available</muted>
                                    </p>
                                </div>
                            </div>
                            <!-- Second Member -->
                            <div class="desc">
                                <div class="thumb">
                                    <img class="img-circle" src="assets/img/ui-sherman.jpg" width="35px" height="35px" align="">
                                </div>
                                <div class="details">
                                    <p><a href="#">DJ SHERMAN</a><br/>
                                    <muted>I am Busy</muted>
                                    </p>
                                </div>
                            </div>
                            <!-- Third Member -->
                            <div class="desc">
                                <div class="thumb">
                                    <img class="img-circle" src="assets/img/ui-danro.jpg" width="35px" height="35px" align="">
                                </div>
                                <div class="details">
                                    <p><a href="#">DAN ROGERS</a><br/>
                                    <muted>Available</muted>
                                    </p>
                                </div>
                            </div>
                            <!-- Fourth Member -->
                            <div class="desc">
                                <div class="thumb">
                                    <img class="img-circle" src="assets/img/ui-zac.jpg" width="35px" height="35px" align="">
                                </div>
                                <div class="details">
                                    <p><a href="#">Zac Sniders</a><br/>
                                    <muted>Available</muted>
                                    </p>
                                </div>
                            </div>
                            <!-- Fifth Member -->
                            <div class="desc">
                                <div class="thumb">
                                    <img class="img-circle" src="assets/img/ui-sam.jpg" width="35px" height="35px" align="">
                                </div>
                                <div class="details">
                                    <p><a href="#">Marcel Newman</a><br/>
                                    <muted>Available</muted>
                                    </p>
                                </div>
                            </div>

                            <!-- CALENDAR-->
<!--                            <div id="calendar" class="mb">
                                <div class="panel green-panel no-margin">
                                    <div class="panel-body">
                                        <div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                                            <div class="arrow"></div>
                                            <h3 class="popover-title" style="disadding: none;"></h3>
                                            <div id="date-popover-content" class="popover-content"></div>
                                        </div>
                                        <div id="my-calendar"></div>
                                    </div>
                                </div>
                            </div>-->
                            <!-- / calendar -->

                        </div><!-- /col-lg-3 -->
                    </div> <!--/row -->
                </section>
            </section>

            <!--main content end-->
            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 - Rewrapped
                    <a href="dashboard.php" class="go-top">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery-1.8.3.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="assets/js/jquery.sparkline.js"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
        <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

        <!--script for this page-->
        <script src="assets/js/sparkline-chart.js"></script>    
        <script src="assets/js/zabuto_calendar.js"></script>	

        <script type="text/javascript">
            $(document).ready(function () {
                var unique_id = $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Welcome to Rewrapped!',
                    // (string | mandatory) the text inside the notification
                    text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
                    // (string | optional) the image to display on the left
                    image: 'assets/img/ui-sam.jpg',
                    // (bool | optional) if you want it to fade out on its own or just sit there
                    sticky: true,
                    // (int | optional) the time you want it to be alive for before fading out
                    time: '',
                    // (string | optional) the class name you want to apply to that specific message
                    class_name: 'my-sticky-class'
                });
                return false;
            });

            $(document).ready(function () {
                var unique_id = $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Welcome to Rewrapped!',
                    // (string | mandatory) the text inside the notification
                    text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
                    // (string | optional) the image to display on the left
                    image: 'assets/img/ui-sam.jpg',
                    // (bool | optional) if you want it to fade out on its own or just sit there
                    sticky: true,
                    // (int | optional) the time you want it to be alive for before fading out
                    time: '1',
                    // (string | optional) the class name you want to apply to that specific message
                    class_name: 'my-sticky-class'
                });
                return false;
            });
        </script>

        <script type="text/javascript">
//            $.noConflict();
            $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
            $(this).hide();
            });

            $("#my-calendar").zabuto_calendar({
            action: function () {
            return myDateFunction(this.id, false);
            },
            action_nav: function () {
            return myNavFunction(this.id);
            },
            ajax: {
            url: "show_data.php?action=1",
            modal: true
            },
            legend: [
            {type: "text", label: "Special event", badge: "00"},
            {type: "block", label: "Regular event", }
            ]
            });
            });


            function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
            }
        </script>


    </body>
</html>
