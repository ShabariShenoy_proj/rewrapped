<?php
include_once("include/check_login_status.php");
// Initialize any variables that the page might echo
$u = "";
$id = "";
$l_id = "";
$sex = "Male";
$userlevel = "";
$profile_pic = "";
$profile_pic_btn = "";
$avatar_form = "";
$country = "";
$joindate = "";
$lastsession = "";
$creatPost = " ";
$usertype = "";
$usertype2 = "";
$name = "";
$interactionBox = "";
$photobox = "";
$editBox = "";
$changeBox = "";
$location = "";
$fname = "";
$lname = "";
$soldmap = '';
$soldTitle = '';
$ratingBox = '';
$bidBox = '';
$avbid = '';
$rate = '';
$current = '';
$rated = 'false';
$isagent = "false";
$logUserAgent = 'false';
$mess = '';
// ------- ESTABLISH THE PROFILE INTERACTION TOKEN ---------
$thisRandNum = rand(9999999999999, 999999999999999999);
$_SESSION['wipit'] = base64_encode($thisRandNum); // Will always overwrite itself each time this script runs
// ------- END ESTABLISH THE PROFILE INTERACTION TOKEN ---------
// 
// Make sure the _GET username is set, and sanitize it
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE username='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else {
    header("location: index.php");
    exit();
}


// get the average bid percentage for the current user if they are an agent
$bidperc = mysqli_query($db_conx, "SELECT AVG(bid) FROM bidnotes WHERE agent='$u'");
while ($avgrow = mysqli_fetch_row($bidperc)) {
    $avbid = round($avgrow[0]);
}

// Select the member from the users table
$sql = "SELECT * FROM users WHERE username='$u' AND activated='1' LIMIT 1";
$user_query = mysqli_query($db_conx, $sql);

$sql1 = "SELECT * FROM users WHERE username='$u'";
$user_query1 = mysqli_query($db_conx, $sql1);
$row = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
$id = $row["id"];
$fname = $row["firstname"];
$lname = $row["lastname"];
$usrlvl = $row["userlevel"];
if ($usrlvl == "b") {
    $isagent = "true";
}
//if ($userlevel == "a") {
//    $usertype = "Home Owner";
//} else if ($userlevel == "b") {
//    $usertype = "Agent";
//} else if ($userlevel == "c") {
//    $usertype = "Buyer";
//} else if ($userlevel == "d") {
//    $usertype = "Administrator";
//}
$sql2 = "SELECT * FROM users WHERE username='$log_username' AND activated='1'";
$user_query2 = mysqli_query($db_conx, $sql2);
$row2 = mysqli_fetch_array($user_query2, MYSQLI_ASSOC);
$l_id = $row2["id"];
$ulvl = $row2["userlevel"];
if ($ulvl == "b") {
    $logUserAgent = "true";
}

//$check = mysqli_query($db_conx, "SELECT AVG(rating) FROM ratings WHERE agentid=$id");

$check = "SELECT COUNT(id) FROM ratings WHERE agentid='$id' AND userid='$l_id'";
$chkquery = mysqli_query($db_conx, $check);
$chkquery_count = mysqli_fetch_row($chkquery);
$rate_count = $chkquery_count[0];
if ($rate_count < 1) {
    //$rated = '';
    $q = mysqli_query($db_conx, "SELECT AVG(rating) FROM ratings WHERE agentid=$id");
    while ($trow = mysqli_fetch_row($q)) {
        $rate = round($trow[0]);
    }
} else {
    $rated = 'true';
    $q = mysqli_query($db_conx, "SELECT AVG(rating) FROM ratings WHERE agentid=$id");
    while ($trow = mysqli_fetch_row($q)) {
        $rate = round($trow[0]);
    }
}

// Now make sure that user exists in the table
$numrows = mysqli_num_rows($user_query);
if ($numrows < 1) {
    echo "That user does not exist or is not yet activated, press back";
    exit();
}

$name = "$fname $lname";
$you = "";

// Check to see if the viewer is the account owner
$isOwner = "no";
$logStatus = "<font color='red'>offline</font>";
if ($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    $logStatus = "<font color='#00FF7F'>online</font>";
    $profile_pic_btn = '<a href="#" onclick="return false;" onmousedown="toggleElement(\'avatar_form\')">Toggle Avatar Form</a>';
    $avatar_form = '<form id="avatar_form" enctype="multipart/form-data" method="post" action="php_parsers/photo_system.php">';
    $avatar_form .= '<p>Change your avatar</p>';
    $avatar_form .= '<input type="file" name="avatar" required>';
    $avatar_form .= '<p><input type="submit" value="Upload"></p>';
    $avatar_form .= '</form>';
    //$name = "Your";

    $you = "Your";
    if ($isagent == "true") {
        $editBox = '<br /><div class="btn btn-primary">
                            <a style="color: white; padding-right: 6px;" href="edtuser.php?u=' . $log_username . '&id=' . $id . '">Edit Profile</a>
                            </div><br />'; //&l='.$userlevel.'

        $photobox = '<div class="btn btn-primary">
                            <a style="color: white;" href="photos.php?u=' . $log_username . '&id=' . $id . '">Add Photos</a>
                            </div>';
        $changeBox = '<br /><div class="btn btn-primary">
                            <a style="color: white; padding-right: 6px;" href="change_pass.php?u=' . $log_username . '&id=' . $id . '">Change Password</a>
                            </div><br />'; //&l='.$userlevel.'

        if ($rate == 0) {
            $mess = 'This agent has not yet been rated.';
            $current = '<span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
        } else if ($rate == 1) {
            $mess = $rate . '/5';
            $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
        } else if ($rate == 2) {
            $mess = $rate . '/5';
            $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
        } else if ($rate == 3) {
            $mess = $rate . '/5';
            $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
        } else if ($rate == 4) {
            $mess = $rate . '/5';
            $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
        } else if ($rate == 5) {
            $mess = $rate . '/5';
            $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                    ';
        }
//        $bidBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
//            <h2>Your Average Bid</h2>
//                    <div>' . $avbid . '</div></div><br/>';

        $ratingBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
            <h2>Your Current Rating</h2>
                    <div class="rates">' . $mess . '<br/>' . $current . '</div></div><br/>';
//        $ratingBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
//            <p>Current Rating</p><br/>
//                    <div>x/5</div></div><br/>';
    } else {
//        $bidBox = '';
        $changeBox = '<br /><div class="btn btn-primary">
                            <a style="color: white; padding-right: 6px;" href="change_pass.php?u=' . $log_username . '&id=' . $id . '">Change Password</a>
                            </div><br />'; //&l='.$userlevel.'
    }
//    if ($usertype == "agent") {
//        $ratingBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
//            <p>Rating</p><br/>
//                    <div>x/5</div></div><br/>';
//    }
    //$createPost = '<p><a href="post.php?u='.$u.'"> Create a Posting </a></p>';
} else {
    if ($log_username) {
        $interactionBox = '<br /><div class="interactionLinksDiv btn btn-primary">
           <a href="#" style="text-align: center; color: white;" onclick="return false" onmousedown="javascript:toggleInteractContainers(\'private_message\');">Private Message</a> 
          </div><br />';  // padding-right: 24px; padding-left: 24px;

        if ($isagent == "true") {
            if ($rate == 0) {
                $mess = 'This agent has not yet been rated.';
                $current = '<span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 1) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 2) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 3) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 4) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 5) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                    ';
            }
//
//            $bidBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
//            <h2>' . $fname . '\'s Average Bid</h2>
//                    <div>' . $avbid . '%</div></div><br/>';

            $ratingBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
            <p>Current Rating</p>
                    <div class="rates">' . $mess . '<br/>' . $current . '</div>';

            if ($logUserAgent == 'false') {
                if ($rated == 'false') {
                    $ratingBox .= '<div class="rating"><p>Rate This Agent</p>
                    <span onclick="window.location = \'php_parsers/rate.php?agentid=' . $id . '&rating=1&userid=' . $l_id . '\'">☆</span>
                        <span onclick="window.location = \'php_parsers/rate.php?agentid=' . $id . '&rating=2&userid=' . $l_id . '\'">☆</span>
                        <span onclick="window.location = \'php_parsers/rate.php?agentid=' . $id . '&rating=3&userid=' . $l_id . '\'">☆</span>
                        <span onclick="window.location = \'php_parsers/rate.php?agentid=' . $id . '&rating=4&userid=' . $l_id . '\'">☆</span>
                        <span onclick="window.location = \'php_parsers/rate.php?agentid=' . $id . '&rating=5&userid=' . $l_id . '\'">☆</span>
                    
                    </div>';
//            <a href = "php_parsers/rate.php?agentid=' . $id . '&rating=1&userid=' . $l_id . '">
//                    <a href = "php_parsers/rate.php?agentid=' . $id . '&rating=2&userid=' . $l_id . '"><span>☆</span></a>
//                    <a href = "php_parsers/rate.php?agentid=' . $id . '&rating=3&userid=' . $l_id . '"><span>☆</span></a>
//                    <a href = "php_parsers/rate.php?agentid=' . $id . '&rating=4&userid=' . $l_id . '"><span>☆</span></a>
//                    <a href = "php_parsers/rate.php?agentid=' . $id . '&rating=5&userid=' . $l_id . '"><span>☆</span></a>
                } else if ($rated == 'true') {
                    $ratingBox .= '<p>You\'ve already rated this agent..</p>';
                }
            }
            $ratingBox .= '</div><br />';
            $photobox = '<div class="btn btn-primary">
                            <a style="color: white;" href="photos.php?id=' . $id . '">Achievements</a>
                            </div>';
        }
    } else {
        if ($isagent == "true") {
            if ($rate == 0) {
                $mess = 'This agent has not yet been rated.';
                $current = '<span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 1) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 2) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 3) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 4) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span style="font-size: 40px;">☆</span>
                    ';
            } else if ($rate == 5) {
                $mess = $rate . '/5';
                $current = '<span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                        <span class="rated" style="font-size: 40px;">☆</span>
                    ';
            }

//            $bidBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
//            <h2>' . $fname . '\'s Average Bid</h2>
//                    <div>' . $avbid . '</div></div><br/>';

            $ratingBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
            <p>Current Rating</p>
                    <div class="rates">' . $mess . '<br/>' . $current . '</div></div><br />';
            $photobox = '<div class="btn btn-primary">
                            <a style="color: white;" href="photos.php?id=' . $id . '">Achievements</a>
                            </div>';
        }
    }
    $you = $fname . "'s";
}
//$you = $fname . "'s";
// Fetch the user row from the query above
while ($row = mysqli_fetch_array($user_query, MYSQLI_ASSOC)) {
    $profile_id = $row["id"];
    $gender = $row["gender"];
    $country = $row["country"];
    $userlevel = $row["userlevel"];
    $avatar = $row["avatar"];
    $signup = $row["signup"];
    $lastlogin = $row["lastlogin"];
    $joindate = strftime("%b %d, %Y", strtotime($signup));
    $lastsession = strftime("%b %d, %Y", strtotime($lastlogin));
    if ($userlevel == "a") {
        $usertype = "Home Owner";
    } else if ($userlevel == "b") {
        $usertype = "Agent";
    } else if ($userlevel == "c") {
        $usertype = "Buyer";
    } else if ($userlevel == "d") {
        $usertype = "Administrator";
    }
}
if ($gender == "f") {
    $sex = "Female";
}
$profile_pic = '<img style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);" class="pure-img" src="user/' . $u . '/' . $avatar . '" alt="' . $u . '">';
if ($avatar == NULL) {
    $profile_pic = '<img style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);" class="pure-img" src="images/th_1.jpg" alt="Default Profile Picture">';
}
?><?php
$isFriend = false;
$ownerBlockViewer = false;
$viewerBlockOwner = false;
if ($u != $log_username && $user_ok == true) {
    $friend_check = "SELECT id FROM friends WHERE user1='$log_username' AND user2='$u' AND accepted='1' OR user1='$u' AND user2='$log_username' AND accepted='1' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $friend_check)) > 0) {
        $isFriend = true;
    }
    $block_check1 = "SELECT id FROM blockedusers WHERE blocker='$u' AND blockee='$log_username' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $block_check1)) > 0) {
        $ownerBlockViewer = true;
    }
    $block_check2 = "SELECT id FROM blockedusers WHERE blocker='$log_username' AND blockee='$u' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $block_check2)) > 0) {
        $viewerBlockOwner = true;
    }
}
?>
<?php
$friend_button = ''; //'<button disabled>Request As Friend</button>';
$block_button = ''; //'<button disabled>Block User</button>';
// LOGIC FOR FRIEND BUTTON
if ($isFriend == true) {
    $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'unfriend\',\'' . $u . '\',\'friendBtn\')" style="text-align: center;">Unfriend</button>';
} else if ($user_ok == true && $u != $log_username && $ownerBlockViewer == false) {
    $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'friend\',\'' . $u . '\',\'friendBtn\')" style="text-align: center;">Request As Friend</button>';
}
// LOGIC FOR BLOCK BUTTON
if ($viewerBlockOwner == true) {
    $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'unblock\',\'' . $u . '\',\'blockBtn\')">Unblock User</button>';
} else if ($user_ok == true && $u != $log_username) {
    $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'block\',\'' . $u . '\',\'blockBtn\')" style="padding-left: 10%; text-align: center;">Block User</button>';
}
?>
<?php
$arrFriendList = array();

$friendsHTML = '';
$friends_view_all_link = ''; //<a href="view_friends.php?u=' . $u . '">View all</a>
$sql = "SELECT COUNT(id) FROM friends WHERE user1='$u' AND accepted='1' OR user2='$u' AND accepted='1'";
$query = mysqli_query($db_conx, $sql);
$query_count = mysqli_fetch_row($query);
$friend_count = $query_count[0];
if ($friend_count < 1) {
    $friendsHTML = "<p>" . $u . " has no friends yet</p>";
} else {
    $max = 6;
    $all_friends = array();
    $sql = "SELECT user1 FROM friends WHERE user2='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $un = $row["user1"];

        array_push($all_friends, $row["user1"]);

        $sqla = "SELECT username, avatar FROM users WHERE username='$un'";
        $querya = mysqli_query($db_conx, $sqla);
        $rowrf = mysqli_fetch_array($querya, MYSQLI_ASSOC);
        //$friend_username = $rowrf["username"];
        $friend_avatar = $rowrf["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $un . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
//        array_push($arrFriendList, $un);
        $arrFriendList[] = $rowrf;
//        array_push($arrFriendList, $un, $friend_pic);
    }
    $sql = "SELECT user2 FROM friends WHERE user1='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $un = $row["user2"];

        array_push($all_friends, $row["user2"]);
//        array_push($arrFriendList, $un);
        $sqlb = "SELECT * FROM users WHERE username='$un'";
        $queryv = mysqli_query($db_conx, $sqlb);
        $rowf = mysqli_fetch_array($queryv, MYSQLI_ASSOC);
        $friend_username = $rowf["username"];
        $friend_avatar = $rowf["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $un . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $arrFriendList[] = $rowf;
//        array_push($arrFriendList, $un, $friend_pic);
    }
    $friendArrayCount = count($all_friends);
    if ($friendArrayCount > $max) {
        array_splice($all_friends, $max);
    }
    if ($friend_count > 0) {
        $friends_view_all_link = '<div class="btn btn-primary" style="text-align: center;"><a style="text-align: center; color: white;" href="view_friends.php?u=' . $u . '">View all</a></div>';
    }
    $orLogic = '';
    foreach ($all_friends as $key => $user) {
        $orLogic .= "username='$user' OR ";
    }
    $orLogic = chop($orLogic, "OR ");
    $sql = "SELECT * FROM users WHERE $orLogic";
    $query = mysqli_query($db_conx, $sql);
    while ($rowr = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $friend_id = $rowr["id"];
        $friend_username = $rowr["username"];
        $friend_avatar = $rowr["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $friend_username . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $friend_fname = $rowr["firstname"];
        $friend_lname = $rowr["lastname"];
        $friend_userlevel = $rowr["userlevel"];
        if ($friend_userlevel == "a") {
            $usertype2 = "Home Owner";
        } else if ($friend_userlevel == "b") {
            $usertype2 = "Agent";
        } else if ($friend_userlevel == "c") {
            $usertype2 = "Buyer";
        } else if ($friend_userlevel == "d") {
            $usertype2 = "Administrator";
        }
        $friend_country = $row["country"];

//        $friendsHTML .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // background-color: #B0E0E6\
//        $friendsHTML .= '<div class=\'pure-u-1  pure-u-sm-1-4 pure-u-md-1-4 pure-u-lg-1-4\' style=\'margin:auto;\'><div class="image" onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // was md-2-5
//        $friendsHTML .= '<img class="pure-img" src="' . $friend_pic . '" alt="cover photo">'; //id="thumbnail"        
//        $friendsHTML .= '</div></div>';
//        //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
//        //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
//        $friendsHTML .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
//                    <div class=\'pure-g\'>
//                        <div class=\'pure-u-1\'><b style="text-align: center;">' . $friend_fname . ' ' . $friend_lname . '</b></div>
//                        <div class=\'pure-u-1\'><text>Username:<span class="badge">' . $friend_username . '</span></text></div>
//                        <div class=\'pure-u-1\'>' . $friend_fname . ' is a(n)<span class="badge">' . $usertype . '</span></div>
//                            <div class=\'pure-u-1\'>Location: <span class="badge">' . $friend_country . '</span></div>
//                                <div class=\'pure-u-1\'>
//                        <p>
//                            <span id="friendBtn">
//                                ' . $friend_button . '</span>
//                        </p></div>
//                        <div class=\'pure-u-1\'><p>
//                            <span id="blockBtn">
//                                ' . $block_button . '
//                            </span>
//                        </p>
//                    </div>
//                    </div>
//                </div>';
////$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
//        $friendsHTML .= '</div><hr>';
        $friendsHTML .= '<div class="col-sm-3 col-md-4"><div class="thumbnail" style="text-align:center; background:#CCF5FF;">'; //class="thumbnail"  col-md-2 beside sm-2
        $friendsHTML .= '<a href="user.php?u=' . $friend_username . '"><img style="width:80px; height:80px;" class="friendpics" src="' . $friend_pic . '" alt="' . $friend_username . '" title="' . $friend_username . '"></a>';
        $friendsHTML .= '<div class="caption"><p style="text-align: center; font-size: 12px;">' . $friend_fname . ' ' . $friend_lname . '</p><br/><span class="badge" style="margin-left: -8px;">' . $usertype2 . '</span></div></div></div>'; //<br/><p style="text-align: center; padding-left: -1px; font-size: 18px;">(' . $friend_username . ')</p>
//        $friendsHTML .= '<div class="caption"></div></div></div>';
    }
}
//$friendsHTML = '';
//$friends_view_all_link = ''; //<a href="view_friends.php?u=' . $u . '">View all</a>
//$sql = "SELECT COUNT(id) FROM friends WHERE user1='$u' AND accepted='1' OR user2='$u' AND accepted='1'";
//$query = mysqli_query($db_conx, $sql);
//$query_count = mysqli_fetch_row($query);
//$friend_count = $query_count[0];
//if ($friend_count < 1) {
//    $friendsHTML = "<p>" . $u . " has no friends yet</p>";
//} else {
//    $max = 6;
//    $all_friends = array();
//    $sql = "SELECT user1 FROM friends WHERE user2='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
//    $query = mysqli_query($db_conx, $sql);
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        array_push($all_friends, $row["user1"]);
//    }
//    $sql = "SELECT user2 FROM friends WHERE user1='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
//    $query = mysqli_query($db_conx, $sql);
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        array_push($all_friends, $row["user2"]);
//    }
//    $friendArrayCount = count($all_friends);
//    if ($friendArrayCount > $max) {
//        array_splice($all_friends, $max);
//    }
//    if ($friend_count > 0) {
//        $friends_view_all_link = '<div class="btn btn-primary" style="text-align: center;"><a style="text-align: center; color: white;" href="view_friends.php?u=' . $u . '">View all</a></div>';
//    }
//    $orLogic = '';
//    foreach ($all_friends as $key => $user) {
//        $orLogic .= "username='$user' OR ";
//    }
//    $orLogic = chop($orLogic, "OR ");
//    $sql = "SELECT * FROM users WHERE $orLogic";
//    $query = mysqli_query($db_conx, $sql);
//    while ($rowr = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        $friend_id = $rowr["id"];
//        $friend_username = $rowr["username"];
//        $friend_avatar = $rowr["avatar"];
//        if ($friend_avatar != "") {
//            $friend_pic = 'user/' . $friend_username . '/' . $friend_avatar . '';
//        } else {
//            $friend_pic = 'images/avatardefault.jpg';
//        }
//        $friend_fname = $rowr["firstname"];
//        $friend_lname = $rowr["lastname"];
//        $friend_userlevel = $rowr["userlevel"];
//        if ($friend_userlevel == "a") {
//            $usertype2 = "Home Owner";
//        } else if ($friend_userlevel == "b") {
//            $usertype2 = "Agent";
//        } else if ($friend_userlevel == "c") {
//            $usertype2 = "Buyer";
//        } else if ($friend_userlevel == "d") {
//            $usertype2 = "Administrator";
//        }
//        $friend_country = $row["country"];
//
////        $friendsHTML .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // background-color: #B0E0E6\
////        $friendsHTML .= '<div class=\'pure-u-1  pure-u-sm-1-4 pure-u-md-1-4 pure-u-lg-1-4\' style=\'margin:auto;\'><div class="image" onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // was md-2-5
////        $friendsHTML .= '<img class="pure-img" src="' . $friend_pic . '" alt="cover photo">'; //id="thumbnail"        
////        $friendsHTML .= '</div></div>';
////        //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
////        //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
////        $friendsHTML .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
////                    <div class=\'pure-g\'>
////                        <div class=\'pure-u-1\'><b style="text-align: center;">' . $friend_fname . ' ' . $friend_lname . '</b></div>
////                        <div class=\'pure-u-1\'><text>Username:<span class="badge">' . $friend_username . '</span></text></div>
////                        <div class=\'pure-u-1\'>' . $friend_fname . ' is a(n)<span class="badge">' . $usertype . '</span></div>
////                            <div class=\'pure-u-1\'>Location: <span class="badge">' . $friend_country . '</span></div>
////                                <div class=\'pure-u-1\'>
////                        <p>
////                            <span id="friendBtn">
////                                ' . $friend_button . '</span>
////                        </p></div>
////                        <div class=\'pure-u-1\'><p>
////                            <span id="blockBtn">
////                                ' . $block_button . '
////                            </span>
////                        </p>
////                    </div>
////                    </div>
////                </div>';
//////$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
////        $friendsHTML .= '</div><hr>';
//        $friendsHTML .= '<div class="col-sm-3 col-md-4"><div class="thumbnail" style="text-align:center; background:#CCF5FF;">'; //class="thumbnail"  col-md-2 beside sm-2
//        $friendsHTML .= '<a href="user.php?u=' . $friend_username . '"><img style="width:80px; height:80px;" class="friendpics" src="' . $friend_pic . '" alt="' . $friend_username . '" title="' . $friend_username . '"></a>';
//        $friendsHTML .= '<div class="caption"><p style="text-align: center; font-size: 12px;">' . $friend_fname . ' ' . $friend_lname . '</p><br/><span class="badge" style="margin-left: -8px;">' . $usertype2 . '</span></div></div></div>'; //<br/><p style="text-align: center; padding-left: -1px; font-size: 18px;">(' . $friend_username . ')</p>
////        $friendsHTML .= '<div class="caption"></div></div></div>';
//    }
//}
?>
<!--?php
//$coverpic = "";
//$sql = "SELECT filename FROM photos WHERE user='$u' ORDER BY RAND() LIMIT 1";
//$query = mysqli_query($db_conx, $sql);
//if (mysqli_num_rows($query) > 0) {
//    $row = mysqli_fetch_row($query);
//    $filename = $row[0];
//    $coverpic = '<img src="user/' . $u . '/' . $filename . '" alt="pic">';
//}
?>-->
<?php
$coverpic2 = '<img src="images/scroll-feather2.jpg" alt="pic" class="img-responsive">';
$json1 = array();
$result = mysqli_query($db_conx, "SELECT address, location, id, user, askingPrice, postName FROM posts WHERE location='Mississauga' ORDER BY postdate LIMIT 3");
//$json = mysqli_fetch_all($result, MYSQLI_ASSOC);
while (($brow = mysqli_fetch_row($result))) {
    $json1[] = $brow;
}
//echo json_encode($json);
//echo json_encode(array($json1));
//Select the user galleries
$gallery_list = "";
$sql = "SELECT DISTINCT postName, id FROM posts WHERE user='$u'";
$query = mysqli_query($db_conx, $sql);
if (mysqli_num_rows($query) < 1) {
    $gallery_list = "This user has not uploaded any posts yet.";
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $pn = $row["postName"];
        $pid = $row["id"];
        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$pid'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];
        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$pid' ORDER BY RAND() LIMIT 1");
        $filerow = mysqli_fetch_row($filequery);
        $file = $filerow[0];
        $otherquery = mysqli_query($db_conx, "SELECT location FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
        $otherrow = mysqli_fetch_row($otherquery);
        $loc = $otherrow[0];
        $postalquery = mysqli_query($db_conx, "SELECT postal FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
        $postalrow = mysqli_fetch_row($postalquery);
        $postal = $postalrow[0];
        $gallery_list .= '<div class="1-box pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-5">';
        //$gallery_list .= '<div onclick="showGallery(\'' . $pn . '\',\'' . $u . '\')">';
        //&postn=<?php echo $p;?//>
        $gallery_list .= '<div class="image" style="margin-left: 30px;" onclick="window.location = \'postphotos.php?postn=' . $pn . '&u=' . $u . '&location=' . $loc . '&postal=' . $postal . '&postid=' . $pid . '\'">';
        if ($filerow == NULL) {
            $gallery_list .= '<img src="images/altposts.png" alt="cover photo">';
        } else {
            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:160px; height:120px;">';
        }
        $gallery_list .= '</div>';
        $gallery_list .= '<b style="display: inline-block; width: 180px; white-space:nowrap; margin-left: 28px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' . $pn . ' (' . $count . ')</b>';
        $gallery_list .= '</div>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $u; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <link rel="stylesheet" href="css/demo.css">
        <link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">

        <!--<![endif]-->

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhPNo9ESa69mJXBUvzKdGMMMDdWx2wwA4&sensor=false&extension=.js"></script>


        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script type="text/javascript">
            $('.avatar').click(function (e) {
                $('.card').toggleClass('active');
            });

// Ripple effect
            var target, ink, d, x, y;
            $(".social").click(function (e) {
                target = $(this);
                //create .ink element if it doesn't exist
                if (target.find(".ink").length === 0)
                    target.prepend("<span class='ink'></span>");

                ink = target.find(".ink");
                //incase of quick double clicks stop the previous animation
                ink.removeClass("animate");

                //set size of .ink
                if (!ink.height() && !ink.width()) {
                    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    d = Math.max(target.outerWidth(), target.outerHeight());
                    ink.css({
                        height: d,
                        width: d
                    });
                }

                //get click coordinates
                //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
                x = e.pageX - target.offset().left - ink.width() / 2;
                y = e.pageY - target.offset().top - ink.height() / 2;

                //set the position and add class .animate
                ink.css({
                    top: y + 'px',
                    left: x + 'px'
                }).addClass("animate");
            });
        </script>
        <style type="text/css">
            form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px;}
            div#galleries{}
            div#galleries > div{float:left; margin:20px; text-align:center; cursor:pointer;}
            div#galleries > div > div {height:100px; overflow:hidden;}
            div#galleries > div > div > img{width:150px; cursor:pointer;}
            div#photos{display:none; border:#666 1px solid; padding:20px;}
            div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
            div#photos > div > img{width:125px; cursor:pointer;}
            div#picbox{display:none; padding-top:36px;}
            div#picbox > img{max-width:800px; display:block; margin:0px auto;}
            div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
        </style>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <script src="js/ProfilePage.js"></script>

        <style type="text/css">
            @import url("https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css");
            body {
                background: #64b5f6;
                text-align: center;
            }

            .card {
                height: 400px;
                width: 300px;
                background: #fff;
                font-family: Roboto;
                display: block;
                position: relative;
                margin: 50px auto;
                border-radius: 5px;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
                transition: all 0.2s ease-in-out;
            }

            .card:hover {
                box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            }

            .fab {
                position: absolute;
                width: 60px;
                height: 60px;
                margin-top: 0;
                margin-left: 50px;
                visibility: hidden;
                background-color: #ffd54f;
                border-radius: 50%;
                transform: scale(0);
                box-shadow: 0 2px 3px rgba(0, 0, 0, 0.22), 0 1px 2px rgba(0, 0, 0, 0.24);
                transition: margin-top 0.6s 0.0s ease-in-out, margin-left 0.6s 0.1s ease-in-out, transform 0.6s 0.0s ease-in-out, visibility 0.6s ease-in-out;
            }

            .active .fab {
                margin-top: 50px;
                margin-left: 120px;
                transform: scale(12);
                visibility: visible;
                transition: margin-top 0.5s ease-in-out, margin-left 0.6s ease-in-out, transform 0.4s 0.3s ease-in-out, visibility 0.4s ease-in-out;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            }

            .avatar {
                margin-top: 5px;
                margin-left: -30px;
                width: 60px;
                height: 60px;
                font-size:2em;
                line-height:60px;
                color: #37474f;
                border-radius: 50%;
                background-color: #ffd54f;
                position: absolute;
                transition: 0.6s ease-in-out;
                cursor: pointer;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            }

            .active .avatar {
                transform: scale(2);
                margin-top: 50px;
                margin-left: -30px;
                transition: 0.6s ease-in-out;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            }

            .active .avatar:hover {
                box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            }

            .fabs {
                position: absolute;
                margin-top: -30px;
                margin-left: 260px;
                overflow: hidden;
                width: 80px;
                height: 80px;
                border-radius: 5px;
                transition: 1s ease-in-out;
                border-radius: 50%;
            }

            .active .fabs {
                margin-top: 0px;
                margin-left: 0px;
                width: 300px;
                height: 400px;
                transition: 0.4s ease-in-out;
                border-radius: 0;
            }

            .content {
                position: absolute;
                width: 280px;
                height: 380px;
                margin: 10px;
                text-align: center;
                overflow-y: auto;
                transition: 0.5s 0.3s cubic-bezier(.55, 0, .1, 1);
            }

            .active .content {
                transform: scale(0.2);
                opacity: 0;
                transition: 0.2s 0.3s cubic-bezier(.55, 0, .1, 1);
            }

            .post {
                position: relative;
                display: inline-block;
                height: 100px;
                margin: 10px auto 0;
                background: #eceff1;
                border-radius: 3px;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
                transition: all 0.2s ease-in-out;
            }

            .post:hover{
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
            }

            .counter {
                border-radius: 50%;
                background: #3E50B4;
                cursor: pointer;
            }

            .detail {
                border: 1px solid #607d8b;
            }

            .main {
                width: 99%;
                height: 130px;
            }

            .main>.preview {
                width: 50%;
                height: 100%;
                background: #ef5350;
            }

            .main>.counter {
                width: 40px;
                height: 40px;
                margin-left: 220px;
                margin-top: -120px;
            }

            .main>.detail{
                width:50px;
                margin-top: -10px;
                margin-left: 150px;
            }

            .main>.details{
                border: 1px solid #607d8b;
                width:100px;
                margin-top: 30px;
                margin-left: 150px;
            }

            .sec {
                width: 49%;
            }

            .sec>.preview {
                width: 100%;
                height: 70%;
                background: #42a5f5;
            }

            .sec>.counter {
                width: 20px;
                height: 20px;
                margin-left: 110px;
                margin-top: -10px;
            }

            .sec>.detail{
                width:50px;
                margin-left: 10px;
            }

            .ter {
                width: 32%;
            }

            .ter>.preview {
                width: 100%;
                height: 80%;
                background: #d4e157;
            }

            .ter>.counter {
                width: 15px;
                height: 15px;
                margin-left: 65px;
                margin-top: -7px;
            }

            .ter>.detail{
                width:40px;
                margin-left: 10px;
            }

            .user {
                position: absolute;
                width: 280px;
                height: 200px;
                margin: 150px 10px 0 10px;
                text-align: center;
                visibility: hidden;
                transition: 0.5s cubic-bezier(.55, 0, .1, 1);
            }

            .active .user {
                visibility: visible;
            }

            .socials {
                display: inline-block;
            }

            .social {
                width: 40px;
                height: 40px;
                border-radius: 50%;
                transform: translate(0px, -10px);
                opacity: 0;
                float: left;
                margin: 0 auto;
                overflow: hidden;
                z-index: 2;
                transition: 0.2s cubic-bezier(.55, 0, .1, 1);
                cursor: pointer;
            }

            .social>i {
                line-height: 40px;
                font-size: 2em;
                color: #37474f;
            }

            .active .social {
                transform: translate(0px, 0px);
                opacity: 1;
                transition: 0.3s 0.5s cubic-bezier(.55, 0, .1, 1);
            }

            .profiles {
                display: inline-block;
            }

            .profile {
                width: 50%;
                height: auto;
                transform: translate(0px, -10px);
                opacity: 0;
                float: left;
                margin: 0 auto;
                overflow: hidden;
                z-index: 2;
                transition: 0.2s cubic-bezier(.55, 0, .1, 1);
                color: #37474f;
            }

            .profile>span {
                line-height: 40px;
                font-size: 1.2em;
                font-weight: 600;
                display: block;
                font-style: none;
                color: #37474f;
            }

            .active .profile {
                transform: translate(0px, 0px);
                opacity: 1;
                transition: 0.3s 0.8s cubic-bezier(.55, 0, .1, 1);
            }
            /* Ripple */

            .ink {
                display: block;
                position: absolute;
                background: rgba(38, 50, 56, 0.4);
                border-radius: 100%;
                -moz-transform: scale(0);
                -ms-transform: scale(0);
                webkit-transform: scale(0);
                transform: scale(0);
            }
            /*animation effect*/

            .ink.animate {
                animation: ripple 0.5s ease-in-out;
            }

            @keyframes ripple {
                /*scale the element to 250% to safely cover the entire link and fade it out*/

                100% {
                    opacity: 0;
                    -moz-transform: scale(5);
                    -ms-transform: scale(5);
                    webkit-transform: scale(5);
                    transform: scale(5);
                }
            }
            /*Scrollbar*/

            ::-webkit-scrollbar {
                width: 6px;
            }

            ::-webkit-scrollbar-track {
                border-radius: 0;
            }

            ::-webkit-scrollbar-thumb {
                margin: 2px;
                border-radius: 10px;
                background: rgba(0, 0, 0, 0.2);
            }
        </style>

        <style>
            * {
                font-family : tahoma, verdana, arial, sans-serif;
            }
            .outer-container {
                width: 100%;
                border: 1px solid #C4CDE0;
                border-radius: 2px;
                margin: 0 auto;
                height: auto;
                overflow: hidden;
            }
            .fb-search-container {
                background: #F6F7F8;
                border-bottom: 1px solid #C4CDE0;
                height: 40px;
            }
            .heading {
                font-size: 16px;
                font-weight: bold;
                width: 70%;
                color: #333333;
                margin-left: 20px;
            }
            .heading img {
                background-position:left;
                margin-top:10px;
                float:left;
            }
            .float {
                float: left;
                line-height: 44px;
            }
            .textbox {
                margin-top: 5px;
            }
            .textbox input {
                border: 1px solid #C4CDE0;
                outline: medium none;
                padding: 2px;
                width: 190px;
                font-size: 11px;
                height: 22px;
            }
            .fb-friends-list {
                margin: 20px 25px;
            }
            .inner_profile {
                border: 1px solid #CCC;
                width: 46%;
                height: 80px;
                margin-bottom:10px;
                margin-top: 2px;
                margin-right: 10px;
                float:left;
            }
            .name {
                margin-left: 10px;
                margin-top:0px;
            }
            .name a {
                font-size: 12px;
                font-weight: bold;
                color: #3B5998;
                cursor: pointer;
                text-decoration: none;
            }
            .name a:hover {
                text-decoration: underline;
            }

            #result { font-size:12px; font-family:Verdana, Geneva, sans-serif; margin:10px; } 
            .friend_span{
                font-size:20px;
                padding-left:5px;
            }
        </style>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".friend_name").keyup(function () {
                    var str = $(".friend_name").val();
                    var count = 0;
                    $(".fb-friends-list .inner_profile").each(function (index) {
                        if ($(this).attr("id")) {
                            //case insenstive search
                            if (!$(this).attr("id").match(new RegExp(str, "i"))) {
                                $(this).fadeOut("fast");
                            } else {
                                $(this).fadeIn("slow");
                                count++;
                            }
                        }
                    });

                    if (str == '') {
                        $("#result").hide();
                    } else {
                        $("#result").show();
                    }
                    //display no of results found
                    if (count < 1) {
                        $("#result").text("No results for :" + str);
                    } else {
                        $("#result").text("Top " + count + " results for " + str);
                    }
                });
            });
        </script>

        <style type="text/css">

            // star rating system
            /*            .rating {
                            unicode-bidi: bidi-override;
                            direction: rtl;
                        }
                        .rating > span {
                            display: inline-block;
                            position: relative;
                            width: 1.1em;
                        }
                        .rating > span:hover:before,
                        .rating > span:hover ~ span:before {
                            content: "\2605";
                            position: absolute;
                        }*/
            // end star rating system


            div#profile_pic_box{margin:20px 30px 0px 0px; overflow-y:hidden; overflow-x: hidden;}//border:#999 2px solid;
            div#profile_pic_box > img{z-index:2000; width:200px;}
            div#profile_pic_box > a {
                display: none;
                position:absolute; 
                margin:140px 0px 0px 120px;
                z-index:4000;
                background:#D8F08E;
                border:#81A332 1px solid;
                border-radius:3px;
                padding:5px;
                font-size:12px;
                text-decoration:none;
                color:#60750B;
            }
            div#profile_pic_box > form{
                display:none;
                position:absolute; 
                z-index:3000;
                padding:10px;
                opacity:.8;
                background:#F0FEC2;
                width:180px;
                height:180px;
            }
            div#profile_pic_box:hover a {
                display: block;
            }
            div#photo_showcase{float:right; background:url(images/image.jpg) no-repeat; width:110px; height:110px; margin:20px 30px 0px 0px; cursor:pointer;}
            div#photo_showcase > img{width:74px; height:74px; margin:37px 0px 0px 9px;}
            div#photo_showcase2 > img{width:74px; height:74px; margin:37px 0px 0px 9px;}

            /* ------- Interaction Links Class -------- */
            /*            .interactionLinksDiv a {
                            border:#B9B9B9 1px solid; padding:5px; color:#060; font-size:11px; background-image:url(style/headerBtnsBG.jpg); text-decoration:none;
                        }
                        .interactionLinksDiv a:hover {
                            border:#090 1px solid; padding:5px; color:#060; font-size:11px; background-image:url(style/headerBtnsBGover.jpg);
                        }*/
            .interactContainers {
                padding:8px;
                //background-color:#BDF;
                border:#999 1px solid;
                display:none;
            }
            /*img.friendpics{border:#000 1px solid; width:80px; height:80px; margin:2px;}*/
        </style>
        <style type="text/css">
            textarea#statustext{width:80%; height:80px; padding:8px; border:#999 1px solid; font-size:16px;}
            div.status_boxes{padding:12px; line-height:1.5em;}
            div.status_boxes > div{width: 70%; padding:8px; border:#99C20C 1px solid; background: #F4FDDF;}
            div.status_boxes > div > b{font-size:12px;}
            div.status_boxes > button{padding:5px; font-size:12px;}
            textarea.replytext{width:70%; height:40px; padding:1%; border:#999 1px solid;}
            div.reply_boxes{padding:12px; border:#999 1px solid; background:#F5F5F5;}
            div.reply_boxes > div > b{font-size:12px;}
        </style>
        <style>
            body {
                background-image: url("images/NYC.jpg");
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: 100%;
                //height: 100%;
                /*opacity: 0.6;*/
                //background: #F1F0D1;
                font-family: Verdana, Tahoma, Arial, sans-serif;
                font-size: 18px;
                overflow: auto;
            }
            h1, h2, h3 {
                text-align: center;
                padding-left: 5%;
                color: #878E63;
            }
            p {
                padding: 2%;
                //color: #white; //#878E63
            }
            img {
                text-align: center;
                max-width: 100%;
                height: auto;
                width: auto;
            }
            .friendpics {
                width:80px; 
                height:80px;
            }
            #wrapper {
                margin: auto ;
                max-width: 1020px;
                width: 98%;
                background: white;//#e1e1e1;//whitesmoke;//#FEF8E8;
                border: 1px solid #878E63;
                border-radius: 2px;
                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
            }
            #container {
                /*                margin: auto ;
                                max-width: 1020px;
                                width: 98%;*/
                background: grey;//#FEF8E8;
                border: 1px solid #878E63;
                border-radius: 2px;
                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
            }
            #callout {
                width: 100%;
                height: auto;
                background: #878E63;
                overflow: hidden;
            }
            #callout p {
                text-align: right;
                font-size: 13px;
                padding: 0.1% 5px 0 0;
                color: #F1F0D1;
            }
            #callout p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            header {
                width: 96%;
                min-height: 125px;
                padding: 5px;
                text-align: center;
            }
            /*            nav ul {
                            list-style: none;
                            margin: 0;
                            padding-left: 50px;
                        }
                        nav ul li {
                            float: left;
                            border: 1px solid #878E63;
                            width: 15%;
                        }
                        nav ul li a {
                            background: #F1F0D1;
                            display: block;
                            padding: 5% 12%;
                            font-weight: bold;
                            font-size: 18px;
                            color: #878E63;
                            text-decoration: none;
                            text-align: center;
                        }
                        nav ul li a:hover, nav ul li.active a{
                            background-color: #878E63;
                            color: #F1F0D1;
                        }*/
            .iframe {
                width: 400px; 
                height: 300px;
            }
            .banner img {
                width: 100%;
                border-top: 1px solid #878E63;
                border-bottom: 1px solid #878E63;
            }
            .caption{
                font-size: 1px;
            }
            .clearfix {
                clear: both;
            }
            .left-col {
                width: 55%;
                float: left;
                //margin: -2% 1% 1% 1%; // top, right, bottom, left
                //width: 40%;
                //float: right;
                margin: 1%;
                //text-align: center;
            }
            #statusui {
                float: left;
                margin: 0 auto;
                width: 100%;
                height: auto;
                padding: 1%;
            }
            .sidebar {
                width: 40%;
                float: right;
                margin: 1%;
                text-align: center;
            }
            #profile_pic_box
            {
                //margin-top: 100px;
                left: 15%;
                width:auto; 
                height:auto;
            }
            .bidbar {
                width: 40%;
                //float: center;
                margin: 1%;
                text-align: center;
            }
            .therapy {
                float: left;
                margin: 0 auto;
                width: 100%;
                height: auto;
                padding: 1%;
            }
            .section {
                width: 29%;
                float: left;
                margin: 2% 2%;
                text-align: center;
            }
            .clock{
                margin-left: 30%;
                width: 40%;
                //float: center;
                //margin: 1%;
                text-align: center;
            }
            footer {
                background: #878E63;
                width: 100%;
                overflow: hidden;
            }
            footer p, footer h3 {
                color: #F1F0D1;
            }
            footer p a {
                color: #F1F0D1;
                text-decoration: none;
            }

            #map_canvas
            {
                width: 100%;
                height: 600px;
            }
            /*            photo_showcase2{
                            right: 30%;
                        }*/
            /*            ul{
                            list-style-type: none;
                            margin: 0;
                            padding: 0;
                        }
                        li{
                            display: inline;
                        }
                        ul li img{
                            height: 50px;
                        }*/
            /* adding media queries tags */
            @media screen and (max-width: 478px){
                body{
                    font-size: 13px;
                }
                #map_canvas
                {
                    width: 100%;
                    height: 200px;
                }

            }

            @media screen and (max-width: 740px){
                /*                nav {
                                    width: 100%;
                                    margin-bottom: 10px;
                                }
                                nav ul{
                                    list-style: none;
                                    margin: 0 auto;
                                    padding-left: 0;
                                }
                                nav ul li{
                                    text-align: center;
                                    margin-left: 0 auto;
                                    width: 100%;
                                    border-top: 1px solid #878E63;
                                    border-right: 0px solid #878E63;
                                    border-bottom: 1px solid #878E63;
                                    border-left: 0px solid #878E63;
                                }
                                nav ul li a{
                                    padding: 8px 0;
                                    font-size: 16px;
                                }*/
                .friendpics {
                    width:80px; 
                    height:80px;
                }
                .left-col{
                    width: 100%;
                }
                .sidebar{
                    width: 100%;
                }
                .section{
                    float: left;
                    width: 100%;
                    margin: 0;
                }
                td {
                    font-size: 10px;
                }
                th {
                    font-size: 12px;
                }
                .iframe {
                    width: 270px; 
                    height: 230px;
                }
                .clock{
                    margin-left: 20%;
                    //width: 20px;;
                    float: left;
                    //margin: 1%;
                    text-align: center;
                    width: 200px; 
                    text-align: center;
                }
                #map_canvas
                {
                    width: 100%;
                    height: 300px;
                }
            }
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <!--<link rel="stylesheet" href="style/style.css">-->
        <script type="text/javascript">

            $(document).ready(function () {
                var map;
                var bounds = new google.maps.LatLngBounds();
                var infowindow = new google.maps.InfoWindow();
//                var elevator;
                var myOptions = {
                    zoom: 1,
                    center: new google.maps.LatLng(0, 0), //43.547981, -79.3895494
                    mapTypeId: 'terrain'
                };
                map = new google.maps.Map($('#map_canvas')[0], myOptions);

//                var addresses = [['Mississauga', 'Great house here', '$400,000'], ['Brampton', 'Great house here', '$400,000'], ['Etobicoke', 'Great house here', '$400,000'], ['Toronto', 'Great house here', '$400,000'], ['3816 Tacc Drive', 'Great house here', '$400,000'], ['645 albert street, Waterloo ON', 'Great house here', '$400,000']];

                var addresses = <?php echo json_encode($json1); ?>;
                //console.log()
                //addresses.toString();
//                document.getElementById("soldTitle").innerHTML = addresses;

                for (var x = 0; x < addresses.length; x++) {
                    var id = addresses[x][2];
                    var price = addresses[x][4];
                    var location = addresses[x][1];
                    var user = addresses[x][3];
                    var pname = addresses[x][5];
                    $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + addresses[x] + '&sensor=false', null, function (data) {
                        var p = data.results[0].geometry.location
                        var latlng = new google.maps.LatLng(p.lat, p.lng);
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            title: 'rock'
                        });

                        var contentString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h1 id="firstHeading" class="firstHeading">' + pname + '</h1>' +
                                '<div id="bodyContent">' +
                                '<p><b>Location: ' + location + '</b><br/>Sold By: ' + <?php echo json_encode($name); ?> + '<br/> Listed Price: $' + price +
                                '<br/>Seller: <a href="user.php?u=' + pname + '">' + user + '</a><br/>Listing Id: ' + id + '</p><br/>' + '<p>Visit the post for pictures and other information.. <a href="postphotos.php?postid=' + id + '&u=' + user + '">Go to listing</a></p>' +
                                '</div>' +
                                '</div>';

                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });
                        //extend the bounds to include each marker's position
                        bounds.extend(marker.position);

                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(map, marker);
                        });

                    });
                }

                //now fit the map to the newly inclusive bounds
                map.fitBounds(bounds);

//(optional) restore the zoom level after the map is done scaling
                var listener = google.maps.event.addListener(map, "idle", function () {
                    map.setZoom(9);
                    google.maps.event.removeListener(listener);
                });
//                var bounds = new google.maps.LatLngBounds();
////                bounds.extend(myPlace);
////                bounds.extend(Item_1);
//                map.fitBounds(bounds);
                map.setCenter(new google.maps.LatLng('43.4935891', '-79.8600753'));

                $('#star').raty({
                    score: 3, //default score
                    starOn: "http://wbotelhos.com/raty/lib/img/star-on.png",
                    starOff: "http://wbotelhos.com/raty/lib/img/star-off.png",
                    readOnly: true                                               //read only
                });

                $("#star > img").click(function () {
                    var score = $(this).attr("alt");                             //record clicked
                    alert(score);                                                // value of the
                    //save to database                                              star
                });
            });

            function friendToggle(type, user, elem) {
                var conf = confirm("Press OK to confirm the '" + type + "' action for user <?php echo $u; ?>.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = 'please wait ...';
                var ajax = ajaxObj("POST", "php_parsers/friend_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "friend_request_sent") {
                            _(elem).innerHTML = 'OK Friend Request Sent';
                        } else if (ajax.responseText == "unfriend_ok") {
                            _(elem).innerHTML = '<button onclick="friendToggle(\'friend\',\'<?php echo $u; ?>\',\'friendBtn\')">Request As Friend</button>';
                        } else {
                            alert(ajax.responseText);
                            _(elem).innerHTML = 'Try again later';
                        }
                    }
                }
                ajax.send("type=" + type + "&user=" + user);
            }
            function blockToggle(type, blockee, elem) {
                var conf = confirm("Press OK to confirm the '" + type + "' action on user <?php echo $u; ?>.");
                if (conf != true) {
                    return false;
                }
                var elem = document.getElementById(elem);
                elem.innerHTML = 'please wait ...';
                var ajax = ajaxObj("POST", "php_parsers/block_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "blocked_ok") {
                            elem.innerHTML = '<button style="text-align: center; color: white; padding-right: 24px; padding-left: 24px;" onclick="blockToggle(\'unblock\',\'<?php echo $u; ?>\',\'blockBtn\')"><b>Unblock User</b></button>';
                        } else if (ajax.responseText == "unblocked_ok") {
                            elem.innerHTML = '<button style="text-align: center; color: white; padding-right: 24px; padding-left: 24px;" onclick="blockToggle(\'block\',\'<?php echo $u; ?>\',\'blockBtn\')"><b>Block User</b></button>';
                        } else {
                            alert(ajax.responseText);
                            elem.innerHTML = 'Try again later';
                        }
                    }
                }
                ajax.send("type=" + type + "&blockee=" + blockee);
            }

            // jQuery functionality for toggling member interaction containers
            $(document).ready(function () {

                $('.interactContainers').hide();
            });
            function toggleInteractContainers(x) {
                if ($('#' + x).is(":hidden")) {
                    $('#' + x).slideDown(200);
                } else {
                    $('#' + x).hide();
                }
//                $('.interactContainers').hide();
            }
// Start Private Messaging stuff
            $('#pmForm').submit(function () {
                $('input[type=submit]', this).attr('disabled', 'disabled');
            });
            function sendPM() {
                var pmSubject = $("#pmSubject");
                var pmTextArea = $("#pmTextArea");
                var sendername = $("#pm_sender_name");
                var senderid = $("#pm_sender_id");
                var recName = $("#pm_rec_name");
                var recID = $("#pm_rec_id");
                var pm_wipit = $("#pmWipit");
                //var url = "php_parsers/private_msg_parse.php";
                var ajax = ajaxObj("POST", "php_parsers/private_msg_parse.php");
                if (pmSubject.val() == "") {
                    $("#interactionResults").html('<img src="images/round_error.png" alt="Error" width="31" height="30" /> &nbsp; Please type a subject.').show().fadeOut(6000);
                } else if (pmTextArea.val() == "") {
                    $("#interactionResults").html('<img src="images/round_error.png" alt="Error" width="31" height="30" /> &nbsp; Please type in your message.').show().fadeOut(6000);
                } else {
                    $("#pmFormProcessGif").show();
//                    $.post(url, {subject: pmSubject.val(), message: pmTextArea.val(), senderName: sendername.val(), senderID: senderid.val(), rcpntName: recName.val(), rcpntID: recID.val(), thisWipit: pm_wipit.val()}, function (data) {
//                        $('#private_message').slideUp("fast");
//                        $("#interactionResults").html(data).show().fadeOut(10000);
//                        document.pmForm.pmTextArea.value = '';
//                        document.pmForm.pmSubject.value = '';
//                        $("#pmFormProcessGif").hide();
//                    });
                    ajax.send("pm_sender_id=" + senderid.val() + "&pm_sender_name=" + sendername.val() + "&pm_rec_id=" + recID.val() + "&pm_rec_name=" + recName.val() + "&pmSubject=" + pmSubject.val() + "&pmTextArea=" + pmTextArea.val());
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText == "private_message_sent") {
                                $("#interactionResults").html('<img src="images/round_success.png" alt="Success" width="31" height="30" /> &nbsp;&nbsp;&nbsp;<strong>Message sent successfully</strong>').show().fadeOut(10000); //= 'OK Request Sent';
                                $('#private_message').slideUp("fast");
                                //$("#interactionResults").html(data).show().fadeOut(10000);                      
                            } else {
                                alert(ajax.responseText);
                                $("#interactionResults").innerHTML = 'Try again later';
                            }
                        }
                    }
                    document.pmForm.pmTextArea.value = '';
                    document.pmForm.pmSubject.value = '';
                    $("#pmFormProcessGif").hide();
                }
            }
// End Private Messaging stuff
        </script>
    </head>
    <body>
        <?php include_once("include/template_pageTop.php"); ?>
        <div>
            <div id="wrapper">
                <br/> <!-- below the background color was:#c7b6b6 #d9d0f7 (purps)-->

                <?php
                if ($usertype == "Agent") {
                    $soldTitle = '<div id="soldTitle" class="pureu-1" style="text-align: center;"><h1>Homes Sold</h1></div>';
                    $soldmap = '<div class="container" id="map_canvas"></div>';
                    $bidBox = '<br/><div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
            <h2>' . $fname . '\'s Average Bid</h2>
                    <div>' . $avbid . '%</div></div><br/>';
                }
                ?>

                <div class='pure-g' style='background: url("images/ugh.png"); border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);'> <!-- #B0E0E6 -->
                    <div class='pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-3' style='margin:auto;'>
                        <center><div class='pure-g'> <!-- style='margin: auto;'-->
                                <div class='pure-u-1'><center><div id="profile_pic_box" style='margin: auto;'><?php echo $profile_pic_btn; ?><?php echo $avatar_form; ?><?php echo $profile_pic; ?></div></center></div> <!-- class="col-xs-6 col-md-4" background: black; border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);-->
                            </div></center>
                    </div> <!-- <i class="fa fa-html5 fa-3x"></i> -->
                    <div class='pure-u-1 pure-u-sm-2-3 pure-u-md-2-3 pure-u-lg-2-3'>
                        <div class='pure-u-1' style="margin-top: 2%; border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);"><center><div id="profile_pic_box" class="jumbotron" style='margin: auto; font-size: 130%; padding-top: 10%; font-size: 40px;'><?php echo $name; ?></div></center></div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="pure-g">
                    <div class='pure-u-1 pure-u-md-2-5 pure-u-lg-3-5' style='margin:auto; background-color: #c7b6b6;border-radius: 2px; box-shadow: 0 0 20px 0px rgba(12, 3, 25, 0.8);'>
                        <center><div id="interactionResults" style="font-size:15px;"></div>
                            <!-- START DIV that contains the Private Message form -->
                            <div class="interactContainers" id="private_message" style="margin: auto;">
                                <form name="pmForm" id="pmForm" action="javascript:sendPM();" method="post"><!-- action="javascript:sendPM();" php_parsers/private_msg_parse.php-->
                                    <font size="+1"><text>Message <strong><em><?php echo "$name"; ?></em></strong></text></font><br /><br />
                                    Subject:
                                    <input name="pmSubject" id="pmSubject" type="text" maxlength="64" style="width:98%;" />
                                    Message:
                                    <textarea name="pmTextArea" id="pmTextArea" rows="8" style="width:98%;"></textarea>
                                    <input name="pm_sender_id" id="pm_sender_id" type="hidden" value="<?php echo $l_id; ?>" />
                                    <input name="pm_sender_name" id="pm_sender_name" type="hidden" value="<?php echo $log_username; ?>" />
                                    <input name="pm_rec_id" id="pm_rec_id" type="hidden" value="<?php echo $id; ?>" />
                                    <input name="pm_rec_name" id="pm_rec_name" type="hidden" value="<?php echo $u; ?>" />
                                    <input name="pmWipit" id="pmWipit" type="hidden" value="<?php echo $thisRandNum; ?>" />
                                    <span id="PMStatus" style="color:#F00;"></span>
                                    <br /><input name="pmSubmit" type="submit" value="Submit"/> or <a href="#" onclick="return false" onmousedown="javascript:toggleInteractContainers('private_message');">Close</a>
                                    <span id="pmFormProcessGif" style="display:none;"><img src="images/loading.gif" width="28" height="10" alt="Loading" /></span></form>
                            </div>
                        </center>
                        <!-- END DIV that contains the Private Message form -->
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="sidebar">
                    <div style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);"> <!-- was a left-col -->
                        <div class='pure-g' style="padding-left: 10px;">
                            <div class='pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-3' style='margin:auto;'>
                                <center>
                                    <div class='pure-u-1'>
                                        <?php echo $interactionBox; ?>
                                        <?php echo $editBox; ?>
                                        <?php echo $photobox; ?>
                                        <?php echo $changeBox; ?>
                                    </div>
                                    <div class='pure-u-1'>
                                        <p>
                                            <span id="friendBtn">
                                                <?php echo $friend_button; ?>
                                            </span>
                                        </p>
                                    </div>
                                    <div class='pure-u-1'>
                                        <p>
                                            <span id="blockBtn">
                                                <?php echo $block_button; ?>
                                            </span>
                                        </p>
                                    </div>
                                </center>
                            </div>
                            <div class='pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-3' style='margin:auto;'>
                                <div class='pure-u-1'><span class="badge"><?php echo $logStatus; ?></span></div>
                                <div class='pure-u-1'><span class="badge"><?php echo $usertype; ?></span></div>
                                <div class='pure-u-1'><span class="badge"><?php echo $country; ?></span></div>
                                <div class='pure-u-1'><span class="badge"><?php echo $location; ?></span></div>                        
                            </div>
                        </div>

                        <!--                <div class='pure-u-1 pure-u-sm-1-3 pure-u-md-2-5 pure-u-lg-2-5'>
                                            <center>
                                        </div>-->
                    </div>
                    <?php echo $bidBox; ?>
                    <style type="text/css">

                        // star rating system RATED
                        .rates ~ span {
                            color: transparent; 
                            content: "\2605"; 
                            position: absolute;
                            left: 0; 
                            color: gold;
                            font-size: 40px;
                        }

                        .rates span.rated{
                            color: gold;//#A8C471;
                        }
                        // star rating system
                        .rating {
                            text-align: center;

                        }
                        .rating > span {
                            display: inline-block;
                            position: relative;
                            width: 1.1em;
                            font-size: 40px;
                        }
                        .rating:hover > span {
                            color: transparent;
                        }
                        .rating > span:hover ~ span {
                            color: black;
                            font-size: 40px;
                        }
                        .rating:hover > span:before {
                            content: "\2605";
                            position: absolute;
                            left: 0; 
                            color: gold;
                            font-size: 40px;
                        }
                        .rating > span:hover ~ span:before {
                            color: transparent;
                        }
                    </style>
                    <?php echo $ratingBox; ?>
                    <br/>
                    <div class="pure-u-1" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
                        <p><h2><?php echo $you; ?> Network</h2><?php echo $friends_view_all_link; ?><br><?php echo $friend_count . " connections"; ?></p>
                        <!--<div class="container">--><hr>
                        <div class="therapy" style="margin-top: -40px;">
                            <p><?php echo $friendsHTML; ?></p>
                        </div>
                    </div>
                </div>
                &nbsp;
                <!-- this is where the php block was placed before-->
                <div class="left-col" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
                    <?php echo $soldTitle; ?>
                    <?php echo $soldmap; ?>
                </div>
                <div id="star"></div>

                <div id="photo_showcase2" class="left-col" style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
                    <h2 style="text-align: center;">View <?php echo $you; ?> Posts</h2>
                    <!--<div class="wrap">
                        <style scoped>
                            .image {
                                position: relative;
                                margin: 20px auto 50px;
                                padding-bottom: 40px; /* Needed to make room for the shadow */
                                height: 300px;
                                text-align: center;
                            }
                            /* Floating Shadow Pseudoelement */
                            .image a:after { 
                                content:'';
                                display: block;
                                position: absolute;
                                left: 10%;
                                bottom: 0px;
                                width: 80%;
                                max-width: auto;
                                height: 10px;
                                background: transparent;
                                border-radius: 100px/50px;
                                box-shadow: 0 50px 40px rgba(0,0,0,0.5);
                            }

                            .image a {
                                display: block;
                                position: relative;
                                margin: 0 auto;
                                width: 95%;
                                -webkit-transition: all .2s linear;
                                -moz-transition: all .2s linear;
                                -ms-transition: all .2s linear;
                                -o-transition: all .2s linear;
                                transition: all .2s linear;
                            }

                            .image a:hover,
                            .image a:focus {
                                width: 100%;
                            }

                            img {
                                height: auto;
                                max-width: 100%;
                            }


                            /* Boring container/type styles */
                            * {
                                -moz-box-sizing: border-box;
                                -webkit-box-sizing: border-box;
                                box-sizing: border-box;
                            }
                            .wrap {
                                margin: 0 auto;
                                padding: 0 15px;
                                width: 100%;
                                max-width: 300px;
                            }

                            @media screen and (max-width: 478px){
                                .image {
                                    position: relative;
                                    margin: 20px auto 50px;
                                    padding-bottom: 40px; /* Needed to make room for the shadow */
                                    height: 50px;
                                    text-align: center;
                                }
                                /* Floating Shadow Pseudoelement */
                                .image a:after { 
                                    content:'';
                                    display: block;
                                    position: absolute;
                                    left: 12%;
                                    bottom: 0px;
                                    width: 80%;
                                    max-width: 10px;
                                    height: 10px;
                                    background: transparent;
                                    border-radius: 100px/50px;
                                    box-shadow: 0 50px 40px rgba(0,0,0,0.5);
                                }

                                .image a {
                                    display: block;
                                    position: relative;
                                    margin: 0 auto;
                                    width: 55%;
                                    -webkit-transition: all .2s linear;
                                    -moz-transition: all .2s linear;
                                    -ms-transition: all .2s linear;
                                    -o-transition: all .2s linear;
                                    transition: all .2s linear;
                                }

                                .image a:hover,
                                .image a:focus {
                                    width: 50%;
                                }

                                img {
                                    height: auto;
                                    max-width: 50%;
                                }


                                /* Boring container/type styles */
                                * {
                                    -moz-box-sizing: border-box;
                                    -webkit-box-sizing: border-box;
                                    box-sizing: border-box;
                                }
                                .wrap {
                                    margin: 0 auto;
                                    padding: 0 15px;
                                    width: 100%;
                                    max-width: 300px;
                                }

                            }

                            @media screen and (max-width: 740px){
                            }
                        </style>
                        <div class="banner" onclick="window.location = 'post.php?u=<?php echo $u; ?>';">
                            <div class="image">
                                <a><?php echo $coverpic2; ?></a>
                            </div>
                        </div>
                    </div>-->
                    <div class="card">
                        <div class="content">
                            <div class="post main">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                                <div class="detail"></div>
                                <div class="details"></div>
                                <div class="details"></div>
                            </div>
                            <div class="post sec">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post sec">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post ter">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post ter">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post ter">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post ter">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post ter">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                            <div class="post ter">
                                <div class="preview"></div>
                                <div class="counter"></div>
                                <div class="detail"></div>
                            </div>
                        </div>
                        <div class="fabs">
                            <div class="fab"></div>
                            <i class="avatar zmdi zmdi-account"></i>
                        </div>
                        <div class="user">
                            <div class="socials">
                                <div class="social"><i class="zmdi zmdi-twitter"></i></div>
                                <div class="social"><i class="zmdi zmdi-github"></i></div>
                                <div class="social"><i class="zmdi zmdi-google-plus"></i></div>
                                <div class="social"><i class="zmdi zmdi-codepen"></i></div>
                            </div>
                            <div class="profiles">
                                <div class="profile"><span>51</span>Upvoted</div>
                                <div class="profile"><span>9</span>Created</div>
                                <div class="profile"><span>9</span>Showcased</div>
                                <div class="profile"><span>1</span>Collections</div>
                                <div class="profile"><span>2</span>Followers</div>
                                <div class="profile"><span>5</span>Following</div>
                            </div>
                        </div>
                    </div>
                    <div id="wrapper">
                        <div id="galleries" style="text-align: center;"><?php echo $gallery_list; ?></div>
                        <div id="photos" style="text-align: center;"></div>
                        <div id="picbox" style="text-align: center;"></div>
                        <p style="clear:left;">These photos belong to <a href="user.php?u=<?php echo $u; ?>"><?php echo $u; ?></a></p>
                        <!--  <form action="$" class="dropzone" id="avatar-dropzone">
                              <input
                              
                          </form>
                        -->
                    </div>
                </div>
                <!--</div>-->
                <hr />
                <!--<p></?ph/p //include/_/o/n/c/e//(/"/i/nclude/template_status.php");                                                     ?></p>-->
                <div class="clearfix"></div>
                &nbsp;
                <hr/>
                <!--           <div class="banner">
        <div class="container">
    <div class="wrapper2">
    </?php include_once("status.php"); ?>
    </div>
    </div>
    </div>-->
                <!-- start from twitter bootstrap-->
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
                <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

                <script src="js/vendor/bootstrap.min.js"></script>

<!--        <script src="js/main.js"></script>-->

                <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
                <script>
                                        (function (b, o, i, l, e, r) {
                                            b.GoogleAnalyticsObject = l;
                                            b[l] || (b[l] =
                                                    function () {
                                                        (b[l].q = b[l].q || []).push(arguments)
                                                    });
                                            b[l].l = +new Date;
                                            e = o.createElement(i);
                                            r = o.getElementsByTagName(i)[0];
                                            e.src = '//www.google-analytics.com/analytics.js';
                                            r.parentNode.insertBefore(e, r)
                                        }(window, document, 'script', 'ga'));
                                        ga('create', 'UA-XXXXX-X', 'auto');
                                        ga('send', 'pageview');
                </script>

            </div>
            <!-- end of bootstrap -->
            <div class="footer"><?php include_once("include/template_pageBottom.php"); ?></div>
        </div>
    </body>
</html>