/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
geocoder = new google.maps.Geocoder();

function getCoordinates(address, callback){
    var coordinates;
    geocoder.geocode({address: address}, function(results, status){
        coords_obj = results[0].geometry.location;
        coordinates = [coords_obj.nb, coords_obj.ob];
        callback(coordinates);
    })
}