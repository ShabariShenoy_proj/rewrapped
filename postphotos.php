<?php
include_once("include/check_login_status.php");
include_once("classes/develop_php_library.php");
// Make sure the _GET "u" is set, and sanitize it
$id = $_GET["postid"];
$pname = "";
if (isset($_GET["postn"])) {
    $pname = $_GET["postn"];
} else {
    $q1 = "select postName FROM posts WHERE id='$id'";
    $r1 = mysqli_query($db_conx, $q1);
    $rw1 = mysqli_fetch_row($r1);
    $pname = $rw1[0];
}
$u = "";
$u_id = "";
$img = "";
$visitor_id = "";
$location = "";
$desc = "";
$postal = "";
$bedroom = "";
$bathroom = "";
$basement = "";
$sqfootage = "";
$views = "";
$postD = "";
$expiry = "";
$expired = "";
$views = "";
//
$thisRandNum = rand(9999999999999, 999999999999999999);
$_SESSION['wipit'] = base64_encode($thisRandNum); // Will always overwrite itself each time this script runs

$bids = "";
$bidtable = "";
$sql1 = "SELECT * FROM users WHERE username='$log_username' AND activated='1' LIMIT 1";
$user_query = mysqli_query($db_conx, $sql1);
$r = mysqli_fetch_array($user_query, MYSQLI_ASSOC);
$userlevel = $r["userlevel"];
$visitor_id = $r["id"];
$limit = $r["bidlimit"];

//get the asking price
$sq1 = "SELECT * FROM posts WHERE id='$id'";
$price_query = mysqli_query($db_conx, $sq1);
$e = mysqli_fetch_array($price_query, MYSQLI_ASSOC);
$askPrice = $e["askingPrice"];
$expired = $e["expired"];
$desc = $e["description"];

//// view counter
//mysqli_query($db_conx, "SELECT views FROM viewcounter WHERE postid='$id'");
$count = mysqli_query($db_conx, "SELECT * FROM viewcounter WHERE postid='$id'");
while ($row3 = mysqli_fetch_array($count)) {
    $views = $row3["views"];
}
//
//
if (isset($_GET["location"])) {
    $location = $_GET["location"];
}
if (isset($_GET["postal"])) {
    $postal = $_GET["postal"];
}
//$postal = $_GET["postal"];
if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE username='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $u_id = $ro[0];
} else {
    header("location: index.php");
    exit();
}

// get the bedroom and bathroom and other stuff from posts.DB
$query1 = "select location, postal, bedroom, bathroom, sqfootage, basement FROM posts WHERE id='$id'";
$res1 = mysqli_query($db_conx, $query1);
$row1 = mysqli_fetch_row($res1);
$location = $row1[0];
$postal = $row1[1];
$bedroom = $row1[2];
$bathroom = $row1[3];
$sqfootage = $row1[4];
$basement = $row1[5];

// didnt work
//if ($res1 = mysqli_prepare($db_conx, $query1)) {
//    mysqli_stmt_execute($res1);
//    mysqli_stmt_bind_result($res1, $location, $postal, $bedroom, $bathroom, $sqfootage, $basement);
//    mysqli_stmt_close($res1);
//}
//
//
//// start of trial carousel
$query = "select * from postpictures WHERE usr='$u' AND postid='$id' order by id";
$res = mysqli_query($db_conx, $query);
$count = mysqli_num_rows($res);
$slides = '';
$Indicators = '';
$counter = 0;
$banner = "";
$addMore = "";
$banner1 = "";
$banner2 = "";

// end of trial carousel

$photo_form = "";
$add_photos = "";
// Check to see if the viewer is the account owner
$isOwner = "no";
if ($u == $log_username && $user_ok == true) {
//    // start of trial carousel
//    $query = "select * from postpictures WHERE usr='$u' AND pname='$pname' order by id";
//    $res = mysqli_query($db_conx, $query);
//    $count = mysqli_num_rows($res);
//    $slides = '';
//    $Indicators = '';
//    $counter = 0;
//    $banner = "";
//    $addMore = "";
//// end of trial carousel
    $banner1 = "Your Post";
    $banner2 = "Your Album";
    $isOwner = "yes";
    $photo_form = '<form id="photo_form" class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="php_parsers/photo_system_1.php?p=' . $pname . '&pid=' . $id . '">';
    $photo_form .= '<p>Hi ' . $u . ', Upload pictures to your post </p>';
    $photo_form .= '<div class="form-group"><label style="font-size: 20px;" class="control-label col-sm-3" for="postname">Title:</label><div class="col-sm-7"><input name="postname" class="form-control" type="text" placeholder="' . $pname . '" disabled id="postname" maxlength="88"></div></div>';
    $photo_form .= '<div class="form-group"><label style="font-size: 20px;" class="control-label col-sm-3" for="description">Description:</label><div class="col-sm-7"><textarea class="form-control" rows="5" cols="50" id="description" name="description" disabled>' . $desc . '</textarea></div></div>';
    $photo_form .= '<div class="form-group"><label style="font-size: 20px;" class="control-label col-sm-3" style="font-size: 20px;" for="location">Location:</label><span style="font-size: 20px;" id="location" class="badge">' . $location . '</span></div>';
    //$photo_form .= '<div>Location:</div><input name="location" type="text" value="' . $location . '" disabled id="location" maxlength="88">';//name="location" id="location"
    //$photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="postal">Postal Code:</label><div><span id="postal" style="font-size: 20px;" class="badge">' . $postal . '</span></div></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="bedroom">Bedrooms:</label><span style="font-size: 20px;" id="bedroom" class="badge">' . $bedroom . '</span></div>';
    //$photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="bathroom">Bathrooms:</label><span style="font-size: 20px;" id="bathroom" class="badge">' . $bathroom . '</span></div>';
    //$photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="sqfootage">Square Footage:</label><span style="font-size: 25px;" id="sqfootage" class="badge">' . $sqfootage . ' Sq Ft</span></div>';
    //$photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="basement">Basement:</label><span style="font-size: 20px;" id="basement" class="badge">' . $basement . '</span></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="askPrice">Asking Price:</label><span style="font-size: 20px;" id="askPrice" class="badge">$' . number_format($askPrice) . '</span></div></form>';
    $photo_form .= '</form>';
    $photo_form .= '<p>There are ' . $views . ' Views on this page!</p>';
    $add_photos = '<div id="addphotos" class="container" style="text-align: center; width: 85%; border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">';
    $add_photos .= '<form id="add_form" class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="php_parsers/photo_system_1.php?p=' . $pname . '&pid=' . $id . '">';
    $add_photos .= '<h3>Add a new photo into one of your galleries</h3>';
    $add_photos .= '<div class="form-group"><label class="control-label col-sm-3"><b>Choose Gallery:</b></label>';
    $add_photos .= '<select class="btn btn-primary dropdown-toggle" name="gallery" required>';
    $add_photos .= '<option value=""></option>';
    $add_photos .= '<option value="Exterior">Exterior</option>';
    $add_photos .= '<option value="Family">Family Room</option>';
    $add_photos .= '<option value="Bedrooms">Bedrooms</option>';
    $add_photos .= '<option value="Washrooms">Washrooms</option>';
    $add_photos .= '<option value="Basement">Basement</option>';
    $add_photos .= '<option value="Other">Other</option>';
    $add_photos .= '</select></div>';
    $add_photos .= '<div class="form-group"><label class="control-label col-sm-3"><b>Choose Photo:</b></label>';
    $add_photos .= '<input type="file" style="width: 60%;" class="btn btn-primary" name="photo" accept="image/*" required></div>';
//	$photo_form .=   '<form id="my-dropzone" name="photo" action="#" class="dropzone"></form>';	
    $add_photos .= '<div style="text-align: center;"><input type="submit" style="position: relative;" class="btn btn-primary" value="Save & Upload Photos"></div>';
    $add_photos .= '</form>';
    $add_photos .= '<br/></div>';

    $banner .= '<h3> Your Home Photo Gallery</h3>';
//    $addMore .= '<span style="float: right;margin-top: -30px;"><a href="photos.php?u=' . $log_username . '">Add More Images</a></span>';
    while ($row = mysqli_fetch_array($res)) {
        $title = $row['usr'];
        $desc = $row['gallery'];
        $image = $row['filename'];
        if ($counter == 0) {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '" class="active"></li>';
            $slides .= '<div class="item active">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" />
            <div class="carousel-caption">
              <h3 style="color: white; font-size: 20px;">' . $title . '</h3>
              <p style="color: white; font-size: 20px;">' . $desc . '.</p>
              </div>
          </div>';
        } else {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '"></li>';
            $slides .= '<div class="item">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" />
            <div class="carousel-caption" style="font-size: 20px;">
              <p style="color: white; font-size: 30px;">' . $title . '</p>
              <p style="color: white; font-size: 20px;">' . $desc . '.</p>
              </div>
          </div>';
        }
        $counter++;
    }
} else {
    // view counter
    mysqli_query($db_conx, "UPDATE viewcounter SET `views` = `views`+'1' WHERE postid='$id'");
    $count = mysqli_query($db_conx, "SELECT * FROM viewcounter WHERE postid='$id'");
    while ($row3 = mysqli_fetch_array($count)) {
        $views = $row3["views"];
    }
//    $id = $_GET["postid"];
    //Select the user galleries
    $banner1 = '' . $u . '\'s Post';
    $banner2 = '' . $u . '\'s Album';
    $banner .= '<h3>' . $u . '\'s Home Picture Gallery</h3>';
    $gallery_list = "";
    $sql = "SELECT DISTINCT gallery FROM postpictures WHERE usr='$u' AND postid='$id' AND pname='$pname'";
    $query = mysqli_query($db_conx, $sql);
    $photo_form .= '<form class="form-horizontal" role="form">';
    $photo_form .= '<div class="form-group"><label style="font-size: 20px;" class="control-label col-sm-3" for="postname">Title:</label><div class="col-sm-7"><input name="postname" class="form-control" type="text" placeholder="' . $pname . '" disabled id="postname" maxlength="88"></div></div>';
    $photo_form .= '<div class="form-group"><label style="font-size: 20px;" class="control-label col-sm-3" for="description">Description:</label><div class="col-sm-7"><textarea class="form-control" rows="5" cols="50" id="description" name="description" disabled>' . $desc . '</textarea></div></div>';
    $photo_form .= '<div class="form-group"><label style="font-size: 20px;" class="control-label col-sm-3" style="font-size: 20px;" for="location">Location:</label><span style="font-size: 20px;" id="location" class="badge">' . $location . '</span></div>';
    //$photo_form .= '<div>Location:</div><input name="location" type="text" value="' . $location . '" disabled id="location" maxlength="88">';//name="location" id="location"
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="postal">Postal Code:</label><div><span id="postal" style="font-size: 20px;" class="badge">' . $postal . '</span></div></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="bedroom">Bedrooms:</label><span style="font-size: 20px;" id="bedroom" class="badge">' . $bedroom . '</span></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="bathroom">Bathrooms:</label><span style="font-size: 20px;" id="bathroom" class="badge">' . $bathroom . '</span></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="sqfootage">Square Footage:</label><span style="font-size: 20px;" id="sqfootage" class="badge">' . $sqfootage . ' Sq Ft</span></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="basement">Basement:</label><span style="font-size: 20px;" id="basement" class="badge">' . $basement . '</span></div>';
    $photo_form .= '<div class="form-group"><label class="control-label col-sm-3" style="font-size: 20px;" for="askPrice">Asking Price:</label><span style="font-size: 20px;" id="askPrice" class="badge">' . $askPrice . '</span></div></form>';
//    $photo_form .= '<div>Title:</div><input name="postname" type="text" value="' . $pname . '" disabled id="postname" maxlength="88">';
//    $photo_form .= '<div>Location:</div><span class="badge">' . $location . '</span>';
//    $photo_form .= '<div>Postal Code:</div><span class="badge">' . $postal . '</span>';
//    $photo_form .= '<div>Bedrooms:</div><span class="badge">' . $bedroom . '</span>';
//    $photo_form .= '<div>Bathrooms:</div><span class="badge">' . $bathroom . '</span>';
//    $photo_form .= '<div>Square Footage:</div><span class="badge">' . $sqfootage . '</span>';
//    $photo_form .= '<div>Basement:</div><span class="badge">' . $basement . '</span>';
//    $photo_form .= '<div>Asking Price:</div><span class="badge">' . $askPrice . '</span>';
//    $photo_form .= '<p>There are ' . $views . ' Views on this page!</p>';
    if (mysqli_num_rows($query) < 1) {
        $gallery_list = "This user has not uploaded any photos yet.";
    } else {
        while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
            $gallery = $row["gallery"];
            $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND gallery='$gallery' AND postid='$id'");
            $countrow = mysqli_fetch_row($countquery);
            $count = $countrow[0];
            $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND gallery='$gallery' AND postid='$id' ORDER BY RAND() LIMIT 1");
            $filerow = mysqli_fetch_row($filequery);
            $file = $filerow[0];
            $gallery_list .= '<div>';
            $gallery_list .= '<div onclick="showGallery(\'' . $gallery . '\',\'' . $u . '\',\'' . $pname . '\')">';
            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo">';
            $gallery_list .= '</div>';
            $gallery_list .= '<b>' . $gallery . '</b> (' . $count . ')';
            $gallery_list .= '</div>';

            //new shit
            //$addMore .= '<span style="float: right;margin-top: -30px;"><a href="photos.php?u=' . $log_username . '"></a></span>';
            while ($row = mysqli_fetch_array($res)) {
                $title = $row['usr'];
                $desc = $row['gallery'];
                $image = $row['filename'];
                if ($counter == 0) {
                    $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '" class="active"></li>';
                    $slides .= '<div class="item active">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" class="img-responsive" />
            <div class="carousel-caption">
              <h4 style="color: white;">' . $title . '</h4>
              <p style="color: white;";>' . $desc . '.</p>
              </div>
          </div>';
                } else {
                    $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '"></li>';
                    $slides .= '<div class="item">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" class="img-responsive" />
            <div class="carousel-caption">
              <h4 style="color: white;">' . $title . '</h4>
              <p style="color: white; font-size: 20px;">' . $desc . '.</p>
              </div>
          </div>';
                }
                $counter++;
            }
        }
    }
}

//Select the user galleries
$gallery_list = "";
$sql = "SELECT DISTINCT gallery FROM postpictures WHERE usr='$u' AND postid='$id'";
$query = mysqli_query($db_conx, $sql);
if (mysqli_num_rows($query) < 1) {
    $gallery_list = "This user has not uploaded any photos yet.";
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $gallery = $row["gallery"];
        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND gallery='$gallery' AND postid='$id'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];
        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND gallery='$gallery' AND postid='$id' ORDER BY RAND() LIMIT 1");
        $filerow = mysqli_fetch_row($filequery);
        $file = $filerow[0];
        $gallery_list .= '<div>';
        $gallery_list .= '<div onclick="showGallery(\'' . $gallery . '\',\'' . $u . '\',\'' . $pname . '\')">';
        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:150px; height:90px;">';
        $gallery_list .= '</div>';
        $gallery_list .= '<b>' . $gallery . '</b> (' . $count . ')';
        $gallery_list .= '</div>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $u; ?> Photos</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <link rel = "icon" href = "images/altposts.png" type = "image/x-icon">
        <link rel = "stylesheet" href = "css/bootstrap.min.css" />
        <link rel = "stylesheet" href = "css/bootstrap-theme.min.css" />

        <link rel = "stylesheet" href = "http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src = "js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />

        <link rel="stylesheet" href="css/demo.css">
        <link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">



        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <!--<link rel="stylesheet" href="css/layouts/marketing.css" />-->
        <!--<![endif]-->
        <!--<link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />-->
        <!--        <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
        <link rel="stylesheet" href="css/main.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <!-- starting of old code-->
        <link rel="stylesheet" href="style/style.css">
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/demo.css">
        <link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

        <style>
            body {
                background: whitesmoke;//#F1F0D1;
                font-family: Verdana, Tahoma, Arial, sans-serif;
                font-size: 18px;
                overflow: auto;
            }
            h1, h2, h3 {
                text-align: center;
                padding-left: 5%;
                color: #878E63;
            }
            h4{
                color: white;
            }           
            p {
                padding: 2%;
                color: #878E63;
            }
            img {
                text-align: center;
                max-width: 100%;
                height: auto;
                width: auto;
            }
            #wrapper {
                margin: 0 auto;
                max-width: 920px;
                width: 98%;
                background: white;//#FEF8E8;
                border: 1px solid #878E63;
                border-radius: 2px;
                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
            }
            #callout {
                width: 100%;
                height: auto;
                background: #878E63;
                overflow: hidden;
            }
            #callout p {
                text-align: right;
                font-size: 13px;
                padding: 0.1% 5px 0 0;
                color: #F1F0D1;
            }
            #callout p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            header {
                width: 96%;
                min-height: 125px;
                padding: 5px;
                text-align: center;
            }
            /*            nav ul {
                            list-style: none;
                            margin: 0;
                            padding-left: 50px;
                        }
                        nav ul li {
                            float: left;
                            border: 1px solid #878E63;
                            width: 15%;
                        }
                        nav ul li a {
                            background: #F1F0D1;
                            display: block;
                            padding: 5% 12%;
                            font-weight: bold;
                            font-size: 18px;
                            color: #878E63;
                            text-decoration: none;
                            text-align: center;
                        }
                        nav ul li a:hover, nav ul li.active a{
                            background-color: #878E63;
                            color: #F1F0D1;
                        }*/
            /*            .iframe {
                            width: 360px; 
                            height: 300px;
                        }*/
            .googleMap {
                position: relative;
                padding-bottom: 95%; // This is the aspect ratio
                height: 0;
                overflow: hidden;
            }
            .googleMap iframe {
                width: 360px; 
                height: 300px;
                position: absolute;
                top: 0;
                left: 0;
                /*                width: 100% !important;
                                height: 100% !important;*/
            }
            .banner img {
                width: 100%;
                border-top: 1px solid #878E63;
                border-bottom: 1px solid #878E63;
            }
            .clearfix {
                clear: both;
            }
            .left-col {
                width: 55%;
                float: left;
                margin: -2% 1% 1% 1%; // top, right, bottom, left
            }
            .sidebar {
                width: 40%;
                float: right;
                margin: 1%;
                text-align: center;
            }
            .bidbar {
                width: 40%;
                //float: center;
                margin: 1%;
                text-align: center;
            }
            .therapy {
                float: left;
                margin: 0 auto;
                width: 100%;
                height: auto;
                padding: 1%;
            }
            .section {
                width: 29%;
                float: left;
                margin: 2% 2%;
                text-align: center;
            }
            .clock{
                //position: relative;
                margin-left: 12%;
                width: 76%;
                /*alignment-adjust: middle;*/
                //float: center;
                //margin: 1%;
                text-align: center;
            }
            footer {
                background: #878E63;
                width: 100%;
                overflow: hidden;
            }
            footer p, footer h3 {
                color: #F1F0D1;
            }
            footer p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            /*            ul{
                            list-style-type: none;
                            margin: 0;
                            padding: 0;
                        }
                        li{
                            display: inline;
                        }
                        ul li img{
                            height: 50px;
                        }*/
            /* adding media queries tags */
            @media screen and (max-width: 478px){
                body{
                    font-size: 13px;
                }
                .google-maps {
                    position: relative;
                    padding-bottom: 95%; // This is the aspect ratio
                    height: 0;
                    overflow: hidden;
                }
                .google-maps iframe {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100% !important;
                    height: 100% !important;
                }

            }

            @media screen and (max-width: 740px){
                /*                nav {
                                    width: 100%;
                                    margin-bottom: 10px;
                                }
                                nav ul{
                                    list-style: none;
                                    margin: 0 auto;
                                    padding-left: 0;
                                }
                                nav ul li{
                                    text-align: center;
                                    margin-left: 0 auto;
                                    width: 100%;
                                    border-top: 1px solid #878E63;
                                    border-right: 0px solid #878E63;
                                    border-bottom: 1px solid #878E63;
                                    border-left: 0px solid #878E63;
                                }
                                nav ul li a{
                                    padding: 8px 0;
                                    font-size: 16px;
                                }*/
                .left-col{
                    width: 100%;
                }
                .sidebar{
                    width: 100%;
                }
                .section{
                    float: left;
                    width: 100%;
                    margin: 0;
                }
                td {
                    font-size: 10px;
                }
                th {
                    font-size: 12px;
                }
                .google-maps {
                    position: relative;
                    padding-bottom: 95%; // This is the aspect ratio
                    height: 0;
                    overflow: hidden;
                }
                .google-maps iframe {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100% !important;
                    height: 100% !important;
                }
                /*                .iframe {
                                    width: 270px; 
                                    height: 230px;
                                }*/
                .clock{
                    margin-left: 20%;
                    //width: 20px;;
                    float: left;
                    //margin: 1%;
                    text-align: center;
                    width: 200px; 
                    text-align: center;
                }
            }
        </style>

        <style type="text/css"> 
            .button { width: 150px; padding: 10px; background-color: #FF8C00; box-shadow: -8px 8px 10px 3px rgba(0,0,0,0.2); font-weight:bold; text-decoration:none; } 
            #cover{ position:fixed; top:0; left:0; background:rgba(0,0,0,0.6); z-index:5; width:100%; height:100%; display:none; } 
            #loginScreen { height:auto; width:auto; margin:0 auto; position:relative; z-index:10; display:none; background: url(login.png) no-repeat; border:5px solid #cccccc; border-radius:10px; } 
            #loginScreen:target, #loginScreen:target + #cover{ display:block; opacity:2; } 
            .cancel { display:block; position:absolute; top:3px; right:2px; background:rgb(245,245,245); color:black; height:30px; width:35px; font-size:30px; text-decoration:none; text-align:center; font-weight:bold; } 
        </style>

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style type="text/css">
            form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px;}
            div#galleries{}
            div#galleries > div{float:left; margin:20px; text-align:center; cursor:pointer;}
            div#galleries > div > div {height:100px; overflow:hidden;}
            div#galleries > div > div > img{width:150px; cursor:pointer;}
            div#photos{display:none; border:#666 1px solid; padding:20px;}
            div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
            div#photos > div > img{width:125px; cursor:pointer;}
            div#picbox{display:none; padding-top:36px;}
            div#picbox > img{max-width:800px; display:block; margin:0px auto;}
            div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <!--<script src="js/flipclock.js.js"></script>-->
        <link rel="stylesheet" href="style/style.css">
<!--        <script type="text/javascript">

        </script>-->

        <script>
            function restrict(elem) {
                var tf = _(elem);
                var rx = new RegExp;
                if (elem == "com") {
                    rx = /[^0-9.]/gi;
                }
                tf.value = tf.value.replace(rx, "");
            }

            function sendPM() {
                var pmSubject = $("#pmSubject");
                var pmTextArea = $("#pmTextArea");
                var sendername = $("#pm_sender_name");
                var senderid = $("#pm_sender_id");
                var recName = $("#pm_rec_name");
                var recID = $("#pm_rec_id");
                var pm_wipit = $("#pmWipit");
                //var url = "php_parsers/private_msg_parse.php";
                var ajax = ajaxObj("POST", "php_parsers/private_msg_parse.php");
                if (pmSubject.val() == "") {
                    $("#interactionResults").html('<img src="images/round_error.png" alt="Error" width="31" height="30" /> &nbsp; Please type a subject.').show().fadeOut(6000);
                } else if (pmTextArea.val() == "") {
                    $("#interactionResults").html('<img src="images/round_error.png" alt="Error" width="31" height="30" /> &nbsp; Please type in your message.').show().fadeOut(6000);
                } else {
                    $("#pmFormProcessGif").show();
//                    $.post(url, {subject: pmSubject.val(), message: pmTextArea.val(), senderName: sendername.val(), senderID: senderid.val(), rcpntName: recName.val(), rcpntID: recID.val(), thisWipit: pm_wipit.val()}, function (data) {
//                        $('#private_message').slideUp("fast");
//                        $("#interactionResults").html(data).show().fadeOut(10000);
//                        document.pmForm.pmTextArea.value = '';
//                        document.pmForm.pmSubject.value = '';
//                        $("#pmFormProcessGif").hide();
//                    });
                    ajax.send("pm_sender_id=" + senderid.val() + "&pm_sender_name=" + sendername.val() + "&pm_rec_id=" + recID.val() + "&pm_rec_name=" + recName.val() + "&pmSubject=" + pmSubject.val() + "&pmTextArea=" + pmTextArea.val());
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText == "private_message_sent") {
                                $("#interactionResults").html('<img src="images/round_success.png" alt="Success" width="31" height="30" /> &nbsp;&nbsp;&nbsp;<strong>Message sent successfully</strong>').show().fadeOut(10000);  //= 'OK Request Sent';
                                $('#private_message').slideUp("fast");
                                //$("#interactionResults").html(data).show().fadeOut(10000);                      
                            }
                            else {
                                alert(ajax.responseText);
                                $("#interactionResults").innerHTML = 'Try again later';
                            }
                        }
                    }
                    document.pmForm.pmTextArea.value = '';
                    document.pmForm.pmSubject.value = '';
                    $("#pmFormProcessGif").hide();
                }
            }

            function showGallery(gallery, user, pname) {
                _("galleries").style.display = "none";
                _("section_title").innerHTML = user + '&#39;s ' + gallery + ' Gallery &nbsp; <button onclick="backToGalleries()"><p>Go back to all galleries</p></button>';
                _("photos").style.display = "block";
                _("photos").innerHTML = 'loading photos ...';
                var ajax = ajaxObj("POST", "php_parsers/photo_system_1.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        _("photos").innerHTML = '';
                        var pics = ajax.responseText.split("|||");
                        for (var i = 0; i < pics.length; i++) {
                            var pic = pics[i].split("|");
                            _("photos").innerHTML += '<div><img onclick="photoShowcase(\'' + pics[i] + '\')" src="user/' + user + '/' + pic[1] + '" alt="pic"><div>';
                        }
                        _("photos").innerHTML += '<p style="clear:left;"></p>';
                    }
                }
                ajax.send("show=galpics&gallery=" + gallery + "&user=" + user + "&pname=" + pname);
            }
            function backToGalleries() {
                _("photos").style.display = "none";
                _("section_title").innerHTML = "<?php echo $u; ?>&#39;s Home Picture Galleries";
                _("galleries").style.display = "block";
            }
            function photoShowcase(picdata) {
                var data = picdata.split("|");
                _("section_title").style.display = "none";
                _("photos").style.display = "none";
                _("picbox").style.display = "block";
                _("picbox").innerHTML = '<button onclick="closePhoto()">x</button>';
                _("picbox").innerHTML += '<img src="user/<?php echo $u; ?>/' + data[1] + '" alt="photo">';
                if ("<?php echo $isOwner ?>" == "yes") {
                    _("picbox").innerHTML += '<p id="deletelink"><a href="#" onclick="return false;" onmousedown="deletePhoto(\'' + data[0] + '\')">Delete this Photo <?php echo $u; ?></a></p>';
                }
            }
            function closePhoto() {
                _("picbox").innerHTML = '';
                _("picbox").style.display = "none";
                _("photos").style.display = "block";
                _("section_title").style.display = "block";
            }
            function deletePhoto(id) {
                var conf = confirm("Press OK to confirm the delete action on this photo.");
                if (conf != true) {
                    return false;
                }
                _("deletelink").style.visibility = "hidden";
                var ajax = ajaxObj("POST", "php_parsers/photo_system_1.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "deleted_ok") {
                            alert("This picture has been deleted successfully. We will now refresh the page for you.");
                            window.location = "post.php?u=<?php echo $u; ?>";
                            //                              window.location = "index.php?u=<?php echo $u; ?>";
                        }
                    }
                }
                ajax.send("delete=photo&id=" + id);
            }

            function requestToggle(type, bidid, user, pid, elem) {
                var conf = confirm("Press OK to confirm the '" + type + "' action for user '" + user + "' on '" + bidid + "' and '" + pid + "'.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = 'please wait ...';
                var ajax = ajaxObj("POST", "php_parsers/request_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "request_info_sent") {
                            _(elem).innerHTML = 'OK Request Sent';
                        }
//                        else if (ajax.responseText == "unfriend_ok") {
//                            _(elem).innerHTML = '<button onclick="friendToggle(\'request\',\'<\?php echo $u; ?>\',\'friendBtn\')">Request As Friend</button>';
//                        } 
                        else {
                            alert(ajax.responseText);
                            _(elem).innerHTML = 'Try again later';
                        }
                    }
                }
                ajax.send("type=" + type + "&user=" + user + "&bidid=" + bidid + "&postid=" + pid);
            }
        </script>
    </head>
    <body>
        <?php include_once("include/template_pageTop.php"); ?>
        <div id="wrapper">
            <div class="banner"><p><h1><?php echo $banner1 ?></h1></p></div>
            <div id="photo_form" class="left-col" style="padding-left: 2%; text-align: center; border: 1px solid #878E63;
                 border-radius: 2px;
                 box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);"><?php echo $photo_form; ?>      
            </div>
            <aside class="sidebar">
                <div class="therapy" style="padding-top: -10px;">
                    <?php
                    $loco = "";
                    $address = "";
                    $sql3 = "SELECT address FROM posts WHERE id = $id";
                    $address_query = mysqli_query($db_conx, $sql3);
                    if (mysqli_num_rows($address_query) < 1) {
                        $loco = $location;
                    } else {
                        $ad_row = mysqli_fetch_row($address_query);
                        $address = $ad_row[0];
                        $loco = str_replace(' ', '%20', $address);
                        $loco .= "%2C%20$location";
                    }
                    ?>
                    <!-- 3816%20Tacc%20Drive%2C%20Mississauga%2C%20ON%2C%20Canada-->

                    <style>
                        /*                        .google-maps {
                                                    position: relative;
                                                    padding-bottom: 95%; // This is the aspect ratio
                                                    height: 0;
                                                    overflow: hidden;
                                                }
                                                .google-maps iframe {
                                                    position: absolute;
                                                    top: 0;
                                                    left: 0;
                                                    width: 100% !important;
                                                    height: 100% !important;
                                                }*/
                    </style>

                    <div id="googleMap">
                        <iframe class="iframe" frameborder="0" style="border:0; border: 1px solid #878E63;
                                border-radius: 2px;
                                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $loco; ?>&key=AIzaSyDhPNo9ESa69mJXBUvzKdGMMMDdWx2wwA4">
                        </iframe>   <!-- 3816%20Tacc%20Drive%2C%20Mississauga%2C%20ON%2C%20Canada-->             
                    </div>
                </div>
                <?php
                $q1 = "select postdate, expiry FROM posts WHERE id='$id'";
                $r1 = mysqli_query($db_conx, $q1);
                $ro1 = mysqli_fetch_row($r1);
                $postD = $ro1[0];
                $expiry = $ro1[1]; //str_replace('-', '/', 
                //echo "<script>cdtd(\'$expiry\')</script>";
//            $datetime1 = date_create(date('Y-m-d H:i:s'));
//            $datetime2 = date_create($expiry);
//            $interval = date_diff($datetime1, $datetime2);
//            echo $interval->format('%R%a days');
                $now = time(); // or your date as well
                $your_date = strtotime($expiry);
                $datediff = $now - $your_date;
//            echo floor($datediff / (60 * 60 * 24));
//            if (floor($datediff / (60 * 60 * 24)) == '0') {
//                $l = "UPDATE posts SET expired='1' WHERE id='$id' LIMIT 1";
//                $quer = mysqli_query($db_conx, $l);
//            }
                ?>
                <div class="clock">
                    <script>
                        function cdtd() {
                            var postid = <?php echo json_encode($id); ?>;
                            var js_var = <?php echo json_encode($expiry); ?>;
                            var t = js_var.split(/[- :]/);
                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                            var xmas = new Date(d);
                            var now = new Date();
                            var timeDiff = xmas.getTime() - now.getTime();
                            if (timeDiff <= 0) {
                                clearTimeout(timer);
                                //                            document.write("Christmas is here!");
                                $.ajax({
                                    url: 'php_expired.php',
                                    type: 'post',
                                    data: {postid: postid},
                                    success: function (output)
                                    {
                                        document.getElementById("daysBox").innerHTML = 0;
                                        document.getElementById("hoursBox").innerHTML = 0;
                                        document.getElementById("minsBox").innerHTML = 0;
                                        document.getElementById("secsBox").innerHTML = 0;
                                        alert('This Post has Expired!' + output);
                                    }, error: function ()
                                    {
                                        alert('something went wrong, rating failed');
                                    }
                                });
                                // Run any code needed for countdown completion here
                            }
                            else {
                                var seconds = Math.floor(timeDiff / 1000);
                                var minutes = Math.floor(seconds / 60);
                                var hours = Math.floor(minutes / 60);
                                var days = Math.floor(hours / 24);
                                hours %= 24;
                                minutes %= 60;
                                seconds %= 60;
                                document.getElementById("daysBox").innerHTML = days;
                                document.getElementById("hoursBox").innerHTML = hours;
                                document.getElementById("minsBox").innerHTML = minutes;
                                document.getElementById("secsBox").innerHTML = seconds;
                                var timer = setTimeout('cdtd()', 1000);
                            }
                            //                        var seconds = Math.floor(timeDiff / 1000);
                            //                        var minutes = Math.floor(seconds / 60);
                            //                        var hours = Math.floor(minutes / 60);
                            //                        var days = Math.floor(hours / 24);
                            //                        hours %= 24;
                            //                        minutes %= 60;
                            //                        seconds %= 60;
                            //                        document.getElementById("daysBox").innerHTML = days;
                            //                        document.getElementById("hoursBox").innerHTML = hours;
                            //                        document.getElementById("minsBox").innerHTML = minutes;
                            //                        document.getElementById("secsBox").innerHTML = seconds;
                            //                        var timer = setTimeout('cdtd()', 1000);
                        }
                    </script>
                    <div>
                        <div><p style="font-size: 25px;"><b>Countdown</b></p>
                            <div class="row" style="background: whitesmoke; text-align: center; border: 1px solid #878E63;
                                 border-radius: 2px;
                                 box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
                                <div class="col-xs-3" style="text-aligh: center;">
                                    <p style="color: red;"><b>Days</b></p>
                                    <div id="daysBox"></div>
                                </div>
                                <div class="col-xs-3">
                                    <p style="color: red;"><b>Hours</b></p>
                                    <div id="hoursBox"></div>
                                </div>
                                <div class="col-xs-3">
                                    <p style="color: red;"><b>Mins</b></p>
                                    <div id="minsBox"></div>
                                </div>
                                <div class="col-xs-3">
                                    <p style="color: red;"><b>Sec</b></p>
                                    <div id="secsBox"></div>
                                </div>
                            </div>
                        </div>
                    </div>                
                    <script>cdtd();</script>
                </div> <br/>
                <?php
//            while ($row = mysqli_fetch_array($user_query, MYSQLI_ASSOC)) {
//                $userlevel = $row["userlevel"];
////                $avatar = $row["avatar"];

                if ($log_username && $userlevel == "b" && $expired == 0 && $limit > 0) {

                    $bids .= '<div id="bids" class="container" style="text-align: center; width: 85%; border: 1px solid #878E63;
                     border-radius: 2px;
                     box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">';
                    $bids .= '<h1>Bid on this Listing:</h1>';
                    $bids .= '<form role="form" id="photo_form" class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="php_parsers/bid_system.php?p=' . $pname . '&pid=' . $id . '&u=' . $u . '&logu=' . $log_username . '&u_id=' . $u_id . '&a_id=' . $visitor_id . '"><fieldset>'; //enctype="multipart/form-data" method="post" action="php_parsers/bid_system.php?p=' . $pname . '&pid=' . $id . '&u=' . $u . '&logu=' . $log_username . '"
                    $bids .= '<div>Commision:</div><input name="com" type="text" id="com" onkeyup="restrict(\'com\')" maxlength="4">';
                    $bids .= '<div>Extra Info:</div><textarea name="desc" type="text" id="desc"></textarea>';
                    $bids .= '<div class="info"><img src="images/Knob Info.ico" alt="info">Describe if selling for asking or higher/lower or if you will include services like staging, etc.. </div>';
                    $bids .= '<p><input type="submit" value="Bid" class="btn btn-primary"></p>';
                    $bids .= '<span id="status"></span>';
                    $bids .= '</fieldset></form>';
                    $bids .= '</div><br/>';
                } else if ($log_username && $userlevel == "b" && $expired == 0 && $limit <= 0) {
                    $message = "You've Run out of bids visit the store to buy more";
                    $bids .= '<div id="bids" class="container" style="text-align: center; width: 85%; border: 1px solid #878E63;
                     border-radius: 2px;
                     box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">';
                    $bids .= '<h1>Bid on this Listing:</h1>';
                    $bids .= '<form role="form" id="photo_form" class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="php_parsers/bid_system.php?p=' . $pname . '&pid=' . $id . '&u=' . $u . '&logu=' . $log_username . '&u_id=' . $u_id . '&a_id=' . $visitor_id . '">'; //enctype="multipart/form-data" method="post" action="php_parsers/bid_system.php?p=' . $pname . '&pid=' . $id . '&u=' . $u . '&logu=' . $log_username . '"
                    $bids .= '<fieldset disabled><div class="form-group"><label class="control-label col-sm-2" for="com">Commision:</label><br/><input placeholder="Commision rate.." class="form-control" name="com" type="text" id="com" onkeyup="restrict(\'com\')" maxlength="4"></div>'; //<div class="input-group-addon" style="width: 85%;">%</div>
                    $bids .= '<div class="form-group"><label class="control-label col-sm-2" for="desc">Extra Info:</label><textarea class="form-control" rows="4" cols="50" name="desc" type="text" id="desc" placeholder="Write a small statement to the seller..." required></textarea></div>';
                    $bids .= '<div class="info"><img src="images/round_error.png" alt="info">' . $message . '</div>';
                    $bids .= '<p><input disabled type="submit" value="Bid" class="btn btn-disabled"></p>';
                    $bids .= '<span id="status"></span>';
                    $bids .= '</fieldset></form>';
                    $bids .= '</div>';
//                $message = "You've Run out of bids visit the store to buy more";
//                echo "<script type='text/javascript'>alert('$message');</script>";
                }
//            }
                ?>
                <style scoped>
                    .info, .success, .warning, .error, .validation {
                        border: 1px solid;
                        margin: 10px 0px;
                        padding:15px 10px 15px 50px;
                        background-repeat: no-repeat;
                        background-position: 10px center;
                    }
                    .info {
                        color: #00529B;
                        background-color: #BDE5F8;
                        /*background-image: url('info.png');*/
                    }
                    .success {
                        color: #4F8A10;
                        background-color: #DFF2BF;
                        background-image:url('success.png');
                    }
                    .warning {
                        color: #9F6000;
                        background-color: #FEEFB3;
                        background-image: url('warning.png');
                    }
                    .error {
                        color: #D8000C;
                        background-color: #FFBABA;
                        background-image: url('error.png');
                    }
                </style>
                <div class="clearfix"></div>
                <?php echo $bids; ?>


                <?php echo $add_photos; ?>
                <!--                <br/>
                                </div>-->
            </aside>
            <div class="clearfix"></div>


            <?php
            //$bids = "";
            $timeAgoObject = new convertToAgo;
            $bidtable = "";
            $sql2 = "SELECT * FROM bids WHERE owner='$u' AND postid='$id' ORDER BY datemade DESC";
            $bid_query = mysqli_query($db_conx, $sql2);
            $n = mysqli_num_rows($bid_query);
            if ($n > 0) {
                if ($userlevel == 'b' || $u == $log_username) {
                    $bidtable .= '<h2>Bids..</h2>
                <p>View all of the bids on this post..</p>
                ';
                    $bidtable .= '<div id="bidtable" class="table-responsive">';
                    $bidtable .= '<table class="table table-hover">';
                    $bidtable .= '<thead>';
                    $bidtable .= '<tr>';
                    $bidtable .= '<th style="font-size: 20px;"></th>';
                    $bidtable .= '<th style="font-size: 20px;">Agent</th>';
                    $bidtable .= '<th style="font-size: 20px;">Commission</th>';
                    $bidtable .= '<th style="font-size: 20px;">Date</th>';
                    if ($u == $log_username && $user_ok == true) {
                        $bidtable .= '<th style="font-size: 20px;">Agent Say\'s</th>';
                        $bidtable .= '<th style="font-size: 20px;">Agent Gets</th>';
                        $bidtable .= '<th style="font-size: 20px;">Hit em up!</th>';
                    }
                    $bidtable .= '</tr>';
                    $bidtable .= '</thead >';
                    $bidtable .= '<tbody>';

                    while ($row = mysqli_fetch_array($bid_query, MYSQLI_ASSOC)) {
                        $bidid = $row["id"];
                        $agent = $row["agent"];
                        $agent_id = $row["agentid"];
                        $owner_id = $row["ownerid"];
                        $rate = $row["bid"];
                        $date = $row["datemade"];
                        $convertedTime = ($timeAgoObject->convert_datetime($date)); // convert date time
                        $when = ($timeAgoObject->makeAgo($convertedTime));
                        $descr = $row["description"];
                        $agent_name = "";
                        $owner_name = "";
                        $av = "";

                        // get the agents name for the private message
                        $pa = "SELECT firstname, lastname, avatar, username FROM users WHERE id='$agent_id' AND activated='1'";
                        $query = mysqli_query($db_conx, $pa);
                        $ro = mysqli_fetch_row($query);
                        $afn = $ro[0];
                        $aln = $ro[1];
                        $img = $ro[2];
                        $ausername = $ro[3];
                        if ($img == NULL) {
                            $av .= '<img class="pure-img" src="images/th.jpg" alt="cover photo" style="width:68px; height:68px;">'; //id="thumbnail"
                        } else {
                            $av .= '<img class="pure-img" src="user/' . $ausername . '/' . $img . '" alt="cover photo" style="width:68px; height:68px;">'; //style="width:160px; height:120px;"id="thumbnail"
                        }
                        $agent_name = $ro[0] . ' ' . $ro[1];
                        // get the user's name for the private messgae
                        $pax = "SELECT firstname, lastname FROM users WHERE id='$owner_id' AND activated='1'";
                        $query = mysqli_query($db_conx, $pax);
                        $roa = mysqli_fetch_row($query);
                        $owner_name = $roa[0] . ' ' . $roa[1];

//                $bidtable .= '<table class="table">';
//                $bidtable .= '<thead>';
//                $bidtable .= '<tr>';
//                $bidtable .= '<th>Agent</th>';
//                $bidtable .= '<th>Commission</th>';
//                $bidtable .= '<th>Date</th>';
//                $bidtable .= '</tr>';
//                $bidtable .= '</thead >';
                        if ($u == $log_username && $user_ok == true) { //($userlevel == "b" || $userlevel == "a")                            
                            $cost = floatval($askPrice);
                            $commision_rate = floatval(($rate) / 100);
                            $savings = floatval($cost * $commision_rate);
                            $bidtable .= '<tr><td style="font-size: 20px; width: 12%;"><a href="user.php?id=' . $agent_id . '">' . $av . '</a></td><td style="font-size: 20px;">' . $agent_name . '</td><td style="font-size: 20px;">' . $rate . '%</td><td style="font-size: 20px;">' . $when . '</td><td style="font-size: 20px;">' . $descr . '</td><td style="font-size: 20px;">$' . number_format($savings) . '</td><td style="font-size: 20px;"><span id="reqBtn"><a href="#loginScreen" style="font-size: 20px;" class="btn btn-primary">Request Info</a></span></td>'; //<button class="btn btn-primary" onclick="requestToggle(\'request\',\'' . $bidid . '\',\'' . $agent . '\',\'' . $id . '\',\'reqBtn\')">Request Info</button>
                            $bidtable .= '</tr>';
                        } else {
                            $bidtable .= '<tr><td style="font-size: 20px; width: 12%;"><a href="user.php?id=' . $agent_id . '">' . $av . '</a></td><td style="font-size: 20px;">' . $agent_name . '</td><td style="font-size: 20px;">' . $rate . '%</td><td style="font-size: 20px;">' . $when . '</td>';
                            $bidtable .= '</tr>';
                        }
                    }
                    $bidtable .= '<tbody>';
                    $bidtable .= '</table>';
                    $bidtable .= '</div><hr/>';
                    $bidtable .= '<div id="loginScreen">
                <center><div id="interactionResults" style="font-size:15px;"></div>
                    <!-- START DIV that contains the Private Message form -->
                    <div class="interactContainers" id="private_message" style="margin: auto;">
                        <form name="pmForm" id="pmForm" action="javascript:sendPM();" method="post">
                            <font size="+1"><text>Message <strong><em>' . $agent_name . '</em></strong></text></font><br /><br />
                            Subject:
                            <input name="pmSubject" id="pmSubject" type="text" maxlength="64" style="width:98%;" />
                            Message:
                            <textarea name="pmTextArea" id="pmTextArea" rows="8" style="width:68%;"></textarea>
                            <input name="pm_sender_id" id="pm_sender_id" type="hidden" value=' . $owner_id . ' />
                            <input name="pm_sender_name" id="pm_sender_name" type="hidden" value=' . $owner_name . ' />
                            <input name="pm_rec_id" id="pm_rec_id" type="hidden" value=' . $agent_id . ' />
                            <input name="pm_rec_name" id="pm_rec_name" type="hidden" value=' . $agent_name . ' />
                            <input name="pmWipit" id="pmWipit" type="hidden" value=' . $thisRandNum . ' />
                            <span id="PMStatus" style="color:#F00;"></span>
                            <br /><input name="pmSubmit" type="submit" value="Submit"/>
                            <span id="pmFormProcessGif" style="display:none;"><img src="images/loading.gif" width="28" height="10" alt="Loading" /></span></form>
                    </div>
                </center><a href="#" class="cancel">&times;</a> 
            </div>
            <div id="cover" ></div>';
                }
            }
//            else {
//                $bidtable .= "<b>No bids have been placed yet...</b><hr/>";
//            }
            ?>

            <!--            <div align="center"> 
                            <br><br><br><br> 
                            <a href="#loginScreen" class="button">Click here to Log In</a> 
                        </div>-->
            <div id="bidtable" style="border: 1px solid #878E63;
                 border-radius: 2px;
                 box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);">
                 <?php echo $bidtable; ?>
            </div>

            <div>
                <h2><?php echo $banner2; ?></h2>
                <div id="galleries"><?php echo $gallery_list; ?></div>
                <div id="photos"></div>
                <div id="picbox"></div>            
                <p style="clear:left;">These photos belong to <a href="user.php?u=<?php echo $u; ?>"><?php echo $u; ?></a></p>
            </div>
            <div class="banner">
                <div id="myCarousel">  <!--the class was originally container but changed to make it n...nvm -->
                    <h2><?php echo $banner; ?></h2><div id="link"><?php echo $addMore; ?></div>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!--Indicators--> 
                        <ol class="carousel-indicators">
                            <?php echo $Indicators; ?>
                        </ol>

                        <!--Wrapper for slides--> 
                        <div class="carousel-inner">
                            <?php echo $slides; ?>  
                        </div>

                        <!--Controls--> 
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
            &nbsp;
        </div>
        <?php include_once("include/template_pageBottom.php"); ?>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

<!--        <script src="js/main.js"></script>-->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                        (function (b, o, i, l, e, r) {
                            b.GoogleAnalyticsObject = l;
                            b[l] || (b[l] =
                                    function () {
                                        (b[l].q = b[l].q || []).push(arguments)
                                    });
                            b[l].l = +new Date;
                            e = o.createElement(i);
                            r = o.getElementsByTagName(i)[0];
                            e.src = '//www.google-analytics.com/analytics.js';
                            r.parentNode.insertBefore(e, r)
                        }(window, document, 'script', 'ga'));
                        ga('create', 'UA-XXXXX-X', 'auto');
                        ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
    </body>
</html>