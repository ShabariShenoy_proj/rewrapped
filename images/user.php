<?php
include_once("include/check_login_status.php");
// Initialize any variables that the page might echo
$u = "";
$id = "";
$l_id = "";
$sex = "Male";
$userlevel = "";
$profile_pic = "";
$profile_pic_btn = "";
$avatar_form = "";
$country = "";
$joindate = "";
$lastsession = "";
$creatPost = " ";
$usertype = "";
$name = "";
$interactionBox = "";
$photobox = "";
$editBox = "";
$location = "";
$fname = "";
$lname = "";
// ------- ESTABLISH THE PROFILE INTERACTION TOKEN ---------
$thisRandNum = rand(9999999999999, 999999999999999999);
$_SESSION['wipit'] = base64_encode($thisRandNum); // Will always overwrite itself each time this script runs
// ------- END ESTABLISH THE PROFILE INTERACTION TOKEN ---------
// 
// Make sure the _GET username is set, and sanitize it
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE id='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else {
    header("location: index.php");
    exit();
}
// Select the member from the users table
$sql = "SELECT * FROM users WHERE username='$u' AND activated='1' LIMIT 1";
$user_query = mysqli_query($db_conx, $sql);

$sql1 = "SELECT * FROM users WHERE username='$u'";
$user_query1 = mysqli_query($db_conx, $sql1);
$row = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
$id = $row["id"];
$fname = $row["firstname"];
$lname = $row["lastname"];
$usertype = $row["userlevel"];

$sql2 = "SELECT * FROM users WHERE username='$log_username' AND activated='1'";
$user_query2 = mysqli_query($db_conx, $sql2);
$row2 = mysqli_fetch_array($user_query2, MYSQLI_ASSOC);
$l_id = $row2["id"];

// Now make sure that user exists in the table
$numrows = mysqli_num_rows($user_query);
if ($numrows < 1) {
    echo "That user does not exist or is not yet activated, press back";
    exit();
}

$name = "$fname $lname";
$you = "";

// Check to see if the viewer is the account owner
$isOwner = "no";
$logStatus = "<font color='red'>offline</font>";
if ($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    $logStatus = "<font color='#00FF7F'>online</font>";
    $profile_pic_btn = '<a href="#" onclick="return false;" onmousedown="toggleElement(\'avatar_form\')">Toggle Avatar Form</a>';
    $avatar_form = '<form id="avatar_form" enctype="multipart/form-data" method="post" action="php_parsers/photo_system.php">';
    $avatar_form .= '<p>Change your avatar</p>';
    $avatar_form .= '<input type="file" name="avatar" required>';
    $avatar_form .= '<p><input type="submit" value="Upload"></p>';
    $avatar_form .= '</form>';
    //$name = "Your";
    $editBox = '<br /><div class="btn btn-link">
                            <a  href="edtuser.php?u=' . $log_username . '&id=' . $id . '">Edit Profile</a>
                            </div><br />';
    $you = "Your";
    //$createPost = '<p><a href="post.php?u='.$u.'"> Create a Posting </a></p>';
} else {
    $you = $fname . "'s";
    if ($log_username) {
        $interactionBox = '<br /><div class="interactionLinksDiv">
           <a href="#" onclick="return false" onmousedown="javascript:toggleInteractContainers(\'private_message\');">Private Message</a>
          </div><br />';
    }
}
$you = $fname."'s";
// Fetch the user row from the query above
while ($row = mysqli_fetch_array($user_query, MYSQLI_ASSOC)) {
    $profile_id = $row["id"];
    $gender = $row["gender"];
    $country = $row["country"];
    $userlevel = $row["userlevel"];
    $avatar = $row["avatar"];
    $signup = $row["signup"];
    $lastlogin = $row["lastlogin"];
    $joindate = strftime("%b %d, %Y", strtotime($signup));
    $lastsession = strftime("%b %d, %Y", strtotime($lastlogin));
    if ($userlevel == "a") {
        $usertype = "Home Owner";
    } else if ($userlevel == "b") {
        $usertype = "Agent";
    } else if ($userlevel == "c") {
        $usertype = "Buyer";
    } else if ($userlevel == "d") {
        $usertype = "Administrator";
    }
}
if ($gender == "f") {
    $sex = "Female";
}
$profile_pic = '<img src="user/' . $u . '/' . $avatar . '" alt="' . $u . '">';
if ($avatar == NULL) {
    $profile_pic = '<img src="images/th_1.jpg" alt="Default Profile Picture">';
}
?><?php
$isFriend = false;
$ownerBlockViewer = false;
$viewerBlockOwner = false;
if ($u != $log_username && $user_ok == true) {
    $friend_check = "SELECT id FROM friends WHERE user1='$log_username' AND user2='$u' AND accepted='1' OR user1='$u' AND user2='$log_username' AND accepted='1' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $friend_check)) > 0) {
        $isFriend = true;
    }
    $block_check1 = "SELECT id FROM blockedusers WHERE blocker='$u' AND blockee='$log_username' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $block_check1)) > 0) {
        $ownerBlockViewer = true;
    }
    $block_check2 = "SELECT id FROM blockedusers WHERE blocker='$log_username' AND blockee='$u' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $block_check2)) > 0) {
        $viewerBlockOwner = true;
    }
}
?>
<?php
$friend_button = ''; //'<button disabled>Request As Friend</button>';
$block_button = ''; //'<button disabled>Block User</button>';
// LOGIC FOR FRIEND BUTTON
if ($isFriend == true) {
    $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'unfriend\',\'' . $u . '\',\'friendBtn\')">Unfriend</button>';
} else if ($user_ok == true && $u != $log_username && $ownerBlockViewer == false) {
    $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'friend\',\'' . $u . '\',\'friendBtn\')" style="">Request As Friend</button>';
}
// LOGIC FOR BLOCK BUTTON
if ($viewerBlockOwner == true) {
    $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'unblock\',\'' . $u . '\',\'blockBtn\')">Unblock User</button>';
} else if ($user_ok == true && $u != $log_username) {
    $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'block\',\'' . $u . '\',\'blockBtn\')">Block User</button>';
}
?>
<?php
$friendsHTML = '';
$friends_view_all_link = '';
$sql = "SELECT COUNT(id) FROM friends WHERE user1='$u' AND accepted='1' OR user2='$u' AND accepted='1'";
$query = mysqli_query($db_conx, $sql);
$query_count = mysqli_fetch_row($query);
$friend_count = $query_count[0];
if ($friend_count < 1) {
    $friendsHTML = "<p>" . $u . " has no friends yet</p>";
} else {
    $max = 6;
    $all_friends = array();
    $sql = "SELECT user1 FROM friends WHERE user2='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($all_friends, $row["user1"]);
    }
    $sql = "SELECT user2 FROM friends WHERE user1='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($all_friends, $row["user2"]);
    }
    $friendArrayCount = count($all_friends);
    if ($friendArrayCount > $max) {
        array_splice($all_friends, $max);
    }
    if ($friend_count > $max) {
        $friends_view_all_link = '<a href="view_friends.php?u=' . $u . '">View all</a>';
    }
    $orLogic = '';
    foreach ($all_friends as $key => $user) {
        $orLogic .= "username='$user' OR ";
    }
    $orLogic = chop($orLogic, "OR ");
    $sql = "SELECT username, avatar FROM users WHERE $orLogic";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $friend_username = $row["username"];
        $friend_avatar = $row["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $friend_username . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $friendsHTML .= '<div class="col-sm-2 col-md-2"><div class="thumbnail" style="text-align:center; background:#CCF5FF;">'; //class="thumbnail"  col-md-2 beside sm-2
        $friendsHTML .= '<a href="user.php?u=' . $friend_username . '"><img style="width:80px; height:80px;" class="friendpics" src="' . $friend_pic . '" alt="' . $friend_username . '" title="' . $friend_username . '"></a>';
        $friendsHTML .= '<div class="caption"><p style="text-align: center; font-size: 18px;">' . $friend_username . '</p></div></div></div>';
    }
}
?>
<!--?php
//$coverpic = "";
//$sql = "SELECT filename FROM photos WHERE user='$u' ORDER BY RAND() LIMIT 1";
//$query = mysqli_query($db_conx, $sql);
//if (mysqli_num_rows($query) > 0) {
//    $row = mysqli_fetch_row($query);
//    $filename = $row[0];
//    $coverpic = '<img src="user/' . $u . '/' . $filename . '" alt="pic">';
//}
?>-->
<?php
$coverpic2 = '<img src="images/scroll-feather2.jpg" alt="pic" class="img-responsive">';
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $u; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <style type="text/css">
            div#profile_pic_box{margin:20px 30px 0px 0px; overflow-y:hidden; overflow-x: hidden;}//border:#999 2px solid;
            div#profile_pic_box > img{z-index:2000; width:200px;}
            div#profile_pic_box > a {
                display: none;
                position:absolute; 
                margin:140px 0px 0px 120px;
                z-index:4000;
                background:#D8F08E;
                border:#81A332 1px solid;
                border-radius:3px;
                padding:5px;
                font-size:12px;
                text-decoration:none;
                color:#60750B;
            }
            div#profile_pic_box > form{
                display:none;
                position:absolute; 
                z-index:3000;
                padding:10px;
                opacity:.8;
                background:#F0FEC2;
                width:180px;
                height:180px;
            }
            div#profile_pic_box:hover a {
                display: block;
            }
            div#photo_showcase{float:right; background:url(images/image.jpg) no-repeat; width:110px; height:110px; margin:20px 30px 0px 0px; cursor:pointer;}
            div#photo_showcase > img{width:74px; height:74px; margin:37px 0px 0px 9px;}
            div#photo_showcase2 > img{width:74px; height:74px; margin:37px 0px 0px 9px;}

            /* ------- Interaction Links Class -------- */
            /*            .interactionLinksDiv a {
                            border:#B9B9B9 1px solid; padding:5px; color:#060; font-size:11px; background-image:url(style/headerBtnsBG.jpg); text-decoration:none;
                        }
                        .interactionLinksDiv a:hover {
                            border:#090 1px solid; padding:5px; color:#060; font-size:11px; background-image:url(style/headerBtnsBGover.jpg);
                        }
                        .interactContainers {
                            padding:8px;
                            background-color:#BDF;
                            border:#999 1px solid;
                            display:none;
                        }*/
            /*img.friendpics{border:#000 1px solid; width:80px; height:80px; margin:2px;}*/
        </style>
        <style type="text/css">
            textarea#statustext{width:80%; height:80px; padding:8px; border:#999 1px solid; font-size:16px;}
            div.status_boxes{padding:12px; line-height:1.5em;}
            div.status_boxes > div{width: 70%; padding:8px; border:#99C20C 1px solid; background: #F4FDDF;}
            div.status_boxes > div > b{font-size:12px;}
            div.status_boxes > button{padding:5px; font-size:12px;}
            textarea.replytext{width:70%; height:40px; padding:1%; border:#999 1px solid;}
            div.reply_boxes{padding:12px; border:#999 1px solid; background:#F5F5F5;}
            div.reply_boxes > div > b{font-size:12px;}
        </style>
        <style>
            body {
                background-image: url("images/NYC.jpg");
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: 100%;
                //height: 100%;
                /*opacity: 0.6;*/
                //background: #F1F0D1;
                font-family: Verdana, Tahoma, Arial, sans-serif;
                font-size: 18px;
                overflow: auto;
            }
            h1, h2, h3 {
                text-align: center;
                padding-left: 5%;
                color: #878E63;
            }
            p {
                padding: 2%;
                //color: #white; //#878E63
            }
            img {
                text-align: center;
                max-width: 100%;
                height: auto;
                width: auto;
            }
            .friendpics {
                width:80px; 
                height:80px;
            }
            #wrapper {
                margin: auto ;
                max-width: 1020px;
                width: 98%;
                background: #e1e1e1;//whitesmoke;//#FEF8E8;
                border: 1px solid #878E63;
                border-radius: 2px;
                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
            }
            #container {
                /*                margin: auto ;
                                max-width: 1020px;
                                width: 98%;*/
                background: grey;//#FEF8E8;
                border: 1px solid #878E63;
                border-radius: 2px;
                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
            }
            #callout {
                width: 100%;
                height: auto;
                background: #878E63;
                overflow: hidden;
            }
            #callout p {
                text-align: right;
                font-size: 13px;
                padding: 0.1% 5px 0 0;
                color: #F1F0D1;
            }
            #callout p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            header {
                width: 96%;
                min-height: 125px;
                padding: 5px;
                text-align: center;
            }
            /*            nav ul {
                            list-style: none;
                            margin: 0;
                            padding-left: 50px;
                        }
                        nav ul li {
                            float: left;
                            border: 1px solid #878E63;
                            width: 15%;
                        }
                        nav ul li a {
                            background: #F1F0D1;
                            display: block;
                            padding: 5% 12%;
                            font-weight: bold;
                            font-size: 18px;
                            color: #878E63;
                            text-decoration: none;
                            text-align: center;
                        }
                        nav ul li a:hover, nav ul li.active a{
                            background-color: #878E63;
                            color: #F1F0D1;
                        }*/
            .iframe {
                width: 400px; 
                height: 300px;
            }
            .banner img {
                width: 100%;
                border-top: 1px solid #878E63;
                border-bottom: 1px solid #878E63;
            }
            .caption{
                font-size: 1px;
            }
            .clearfix {
                clear: both;
            }
            .left-col {
                width: 55%;
                float: left;
                margin: -2% 1% 1% 1%; // top, right, bottom, left
            }
            #statusui {
                float: left;
                margin: 0 auto;
                width: 100%;
                height: auto;
                padding: 1%;
            }
            .sidebar {
                width: 40%;
                float: right;
                margin: 1%;
                text-align: center;
            }
            #profile_pic_box
            {
                //margin-top: 100px;
                left: 15%;
                width:auto; 
                height:auto;
            }
            .bidbar {
                width: 40%;
                //float: center;
                margin: 1%;
                text-align: center;
            }
            .therapy {
                float: left;
                margin: 0 auto;
                width: 100%;
                height: auto;
                padding: 1%;
            }
            .section {
                width: 29%;
                float: left;
                margin: 2% 2%;
                text-align: center;
            }
            .clock{
                margin-left: 30%;
                width: 40%;
                //float: center;
                //margin: 1%;
                text-align: center;
            }
            footer {
                background: #878E63;
                width: 100%;
                overflow: hidden;
            }
            footer p, footer h3 {
                color: #F1F0D1;
            }
            footer p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            /*            photo_showcase2{
                            right: 30%;
                        }*/
            /*            ul{
                            list-style-type: none;
                            margin: 0;
                            padding: 0;
                        }
                        li{
                            display: inline;
                        }
                        ul li img{
                            height: 50px;
                        }*/
            /* adding media queries tags */
            @media screen and (max-width: 478px){
                body{
                    font-size: 13px;
                }

            }

            @media screen and (max-width: 740px){
                /*                nav {
                                    width: 100%;
                                    margin-bottom: 10px;
                                }
                                nav ul{
                                    list-style: none;
                                    margin: 0 auto;
                                    padding-left: 0;
                                }
                                nav ul li{
                                    text-align: center;
                                    margin-left: 0 auto;
                                    width: 100%;
                                    border-top: 1px solid #878E63;
                                    border-right: 0px solid #878E63;
                                    border-bottom: 1px solid #878E63;
                                    border-left: 0px solid #878E63;
                                }
                                nav ul li a{
                                    padding: 8px 0;
                                    font-size: 16px;
                                }*/
                .friendpics {
                    width:80px; 
                    height:80px;
                }
                .left-col{
                    width: 100%;
                }
                .sidebar{
                    width: 100%;
                }
                .section{
                    float: left;
                    width: 100%;
                    margin: 0;
                }
                td {
                    font-size: 10px;
                }
                th {
                    font-size: 12px;
                }
                .iframe {
                    width: 270px; 
                    height: 230px;
                }
                .clock{
                    margin-left: 20%;
                    //width: 20px;;
                    float: left;
                    //margin: 1%;
                    text-align: center;
                    width: 200px; 
                    text-align: center;
                }
            }
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <!--<link rel="stylesheet" href="style/style.css">-->
        <script type="text/javascript">
            function friendToggle(type, user, elem) {
                var conf = confirm("Press OK to confirm the '" + type + "' action for user <?php echo $u; ?>.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = 'please wait ...';
                var ajax = ajaxObj("POST", "php_parsers/friend_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "friend_request_sent") {
                            _(elem).innerHTML = 'OK Friend Request Sent';
                        } else if (ajax.responseText == "unfriend_ok") {
                            _(elem).innerHTML = '<button onclick="friendToggle(\'friend\',\'<?php echo $u; ?>\',\'friendBtn\')">Request As Friend</button>';
                        } else {
                            alert(ajax.responseText);
                            _(elem).innerHTML = 'Try again later';
                        }
                    }
                }
                ajax.send("type=" + type + "&user=" + user);
            }
            function blockToggle(type, blockee, elem) {
                var conf = confirm("Press OK to confirm the '" + type + "' action on user <?php echo $u; ?>.");
                if (conf != true) {
                    return false;
                }
                var elem = document.getElementById(elem);
                elem.innerHTML = 'please wait ...';
                var ajax = ajaxObj("POST", "php_parsers/block_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "blocked_ok") {
                            elem.innerHTML = '<button onclick="blockToggle(\'unblock\',\'<?php echo $u; ?>\',\'blockBtn\')">Unblock User</button>';
                        } else if (ajax.responseText == "unblocked_ok") {
                            elem.innerHTML = '<button onclick="blockToggle(\'block\',\'<?php echo $u; ?>\',\'blockBtn\')">Block User</button>';
                        } else {
                            alert(ajax.responseText);
                            elem.innerHTML = 'Try again later';
                        }
                    }
                }
                ajax.send("type=" + type + "&blockee=" + blockee);
            }

            // jQuery functionality for toggling member interaction containers
            $(document).ready(function () {

                $('.interactContainers').hide();
            });
            function toggleInteractContainers(x) {
                if ($('#' + x).is(":hidden")) {
                    $('#' + x).slideDown(200);
                } else {
                    $('#' + x).hide();
                }
//                $('.interactContainers').hide();
            }
// Start Private Messaging stuff
            $('#pmForm').submit(function () {
                $('input[type=submit]', this).attr('disabled', 'disabled');
            });
            function sendPM() {
                var pmSubject = $("#pmSubject");
                var pmTextArea = $("#pmTextArea");
                var sendername = $("#pm_sender_name");
                var senderid = $("#pm_sender_id");
                var recName = $("#pm_rec_name");
                var recID = $("#pm_rec_id");
                var pm_wipit = $("#pmWipit");
                //var url = "php_parsers/private_msg_parse.php";
                var ajax = ajaxObj("POST", "php_parsers/private_msg_parse.php");
                if (pmSubject.val() == "") {
                    $("#interactionResults").html('<img src="images/round_error.png" alt="Error" width="31" height="30" /> &nbsp; Please type a subject.').show().fadeOut(6000);
                } else if (pmTextArea.val() == "") {
                    $("#interactionResults").html('<img src="images/round_error.png" alt="Error" width="31" height="30" /> &nbsp; Please type in your message.').show().fadeOut(6000);
                } else {
                    $("#pmFormProcessGif").show();
//                    $.post(url, {subject: pmSubject.val(), message: pmTextArea.val(), senderName: sendername.val(), senderID: senderid.val(), rcpntName: recName.val(), rcpntID: recID.val(), thisWipit: pm_wipit.val()}, function (data) {
//                        $('#private_message').slideUp("fast");
//                        $("#interactionResults").html(data).show().fadeOut(10000);
//                        document.pmForm.pmTextArea.value = '';
//                        document.pmForm.pmSubject.value = '';
//                        $("#pmFormProcessGif").hide();
//                    });
                    ajax.send("pm_sender_id=" + senderid.val() + "&pm_sender_name=" + sendername.val() + "&pm_rec_id=" + recID.val() + "&pm_rec_name=" + recName.val() + "&pmSubject=" + pmSubject.val() + "&pmTextArea=" + pmTextArea.val());
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText == "private_message_sent") {
                                $("#interactionResults").html('<img src="images/round_success.png" alt="Success" width="31" height="30" /> &nbsp;&nbsp;&nbsp;<strong>Message sent successfully</strong>').show().fadeOut(10000);  //= 'OK Request Sent';
                                $('#private_message').slideUp("fast");
                                //$("#interactionResults").html(data).show().fadeOut(10000);                      
                            }
                            else {
                                alert(ajax.responseText);
                                $("#interactionResults").innerHTML = 'Try again later';
                            }
                        }
                    }
                    document.pmForm.pmTextArea.value = '';
                    document.pmForm.pmSubject.value = '';
                    $("#pmFormProcessGif").hide();
                }
            }
// End Private Messaging stuff
        </script>
    </head>
    <body>
        <!--<img src="images/NYC.jpg" id="bg2" alt="" style="">-->
        <?php include_once("include/template_pageTop.php"); ?>
        <div id="wrapper">
            <!--&nbsp;<br/>-->
            <!--&nbsp;-->
            <br/> <!-- below the background color was: #d9d0f7 (purps)-->
            <div class='pure-g' style='background-color: #c7b6b6; border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);'> <!-- #B0E0E6 -->
                <div class='pure-u-1 pure-u-md-1-5 pure-u-lg-1-5' style='margin:auto;'>
                    <center><div class='pure-g' style="padding-left: 10px;">
                            <div class='pure-u-1'><span class="badge"><?php echo $logStatus; ?></span></div>
                            <div class='pure-u-1'><span class="badge"><?php echo $usertype; ?></span></div>
                            <div class='pure-u-1'><span class="badge"><?php echo $country; ?></span></div>
                            <div class='pure-u-1'><span class="badge"><?php echo $location; ?></span></div>
                        </div></center>
                    <center></center>
                </div> <!-- <i class="fa fa-html5 fa-3x"></i> -->
                <div class='pure-u-1 pure-u-md-3-5 pure-u-lg-3-5'>
                    <center><div class='pure-g'> <!-- style='margin: auto;'-->
                            <div class='pure-u-1'><center><div id="profile_pic_box" style='background: black; margin: auto; border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);'><?php echo $profile_pic_btn; ?><?php echo $avatar_form; ?><?php echo $profile_pic; ?></div></center></div> <!-- class="col-xs-6 col-md-4" -->
                            <div class='pure-u-1'><center><div id="profile_pic_box" style='margin: auto; font-size: 130%'><?php echo $name; ?></div></div>
                        </div></center>
                </div>
                <div class='pure-u-1 pure-u-md-1-5 pure-u-lg-1-5' style='margin:auto;'>
                    <div class='pure-u-1'><center>
                            <?php echo $interactionBox; ?>
                        </center></div>
                    <div class='pure-u-1'>
                        <center>
                            <?php echo $editBox; ?>
                        </center>
                    </div>
                    <center>
                        <?php
                        //$coverpic = "";
                        if ($usertype == "Agent") {
                            $photobox = '<div class="btn btn-primary">
                            <a style="color: white;" href="photos.php?u=' . $log_username . '&id=' . $id . '">Add Photos</a>
                            </div>';
                        }
                        ?>
                        <?php echo $photobox; ?>
                    </center>
                    <center>
                        <p>
                            <span id="friendBtn">
                                <?php echo $friend_button; ?></span><?php echo $friends_view_all_link; ?>
                        </p>
                        <p>
                            <?php echo $friend_count . " connections"; ?>
                        </p>
                        <p>
                            <span id="blockBtn">
                                <?php echo $block_button; ?>
                            </span>
                        </p>
                    </center>
                </div> <!-- <i class="fa fa-youtube-square fa-3x"></i> -->

            </div>
            <div class="pure-g">
                <div class='pure-u-1 pure-u-md-2-5 pure-u-lg-3-5' style='margin:auto; background-color: #c7b6b6;'>
                    <center><div id="interactionResults" style="font-size:15px;"></div>
                        <!-- START DIV that contains the Private Message form -->
                        <div class="interactContainers" id="private_message" style="margin: auto;">
                            <form name="pmForm" id="pmForm" action="javascript:sendPM();" method="post"><!-- action="javascript:sendPM();" php_parsers/private_msg_parse.php-->
                                <font size="+1"><text>Message <strong><em><?php echo "$name"; ?></em></strong></text></font><br /><br />
                                Subject:
                                <input name="pmSubject" id="pmSubject" type="text" maxlength="64" style="width:98%;" />
                                Message:
                                <textarea name="pmTextArea" id="pmTextArea" rows="8" style="width:98%;"></textarea>
                                <input name="pm_sender_id" id="pm_sender_id" type="hidden" value="<?php echo $l_id; ?>" />
                                <input name="pm_sender_name" id="pm_sender_name" type="hidden" value="<?php echo $log_username; ?>" />
                                <input name="pm_rec_id" id="pm_rec_id" type="hidden" value="<?php echo $id; ?>" />
                                <input name="pm_rec_name" id="pm_rec_name" type="hidden" value="<?php echo $u; ?>" />
                                <input name="pmWipit" id="pmWipit" type="hidden" value="<?php echo $thisRandNum; ?>" />
                                <span id="PMStatus" style="color:#F00;"></span>
                                <br /><input name="pmSubmit" type="submit" value="Submit"/> or <a href="#" onclick="return false" onmousedown="javascript:toggleInteractContainers('private_message');">Close</a>
                                <span id="pmFormProcessGif" style="display:none;"><img src="images/loading.gif" width="28" height="10" alt="Loading" /></span></form>
                        </div>
                    </center>
                    <!-- END DIV that contains the Private Message form -->
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="banner">
                <h2><?php echo $you; ?> Network</h2>
                <!--<div class="container">-->
                <div class="row">
                    <p><?php echo $friendsHTML; ?></p>
                </div>
            </div>
            <!--</div>-->
            <hr />
            <p><?php //include_once("include/template_status.php");                                                 ?></p>
            <div id="photo_showcase2" onclick="window.location = 'post.php?u=<?php echo $u; ?>';" >
                <h2 style="text-align: center;">View <?php echo $you; ?> Posts</h2>
                <div class="wrap">
                    <style scoped>
                        .image {
                            position: relative;
                            margin: 20px auto 50px;
                            padding-bottom: 40px; /* Needed to make room for the shadow */
                            height: 300px;
                            text-align: center;
                        }
                        /* Floating Shadow Pseudoelement */
                        .image a:after { 
                            content:'';
                            display: block;
                            position: absolute;
                            left: 10%;
                            bottom: 0px;
                            width: 80%;
                            max-width: auto;
                            height: 10px;
                            background: transparent;
                            border-radius: 100px/50px;
                            box-shadow: 0 50px 40px rgba(0,0,0,0.5);
                        }

                        .image a {
                            display: block;
                            position: relative;
                            margin: 0 auto;
                            width: 95%;
                            -webkit-transition: all .2s linear;
                            -moz-transition: all .2s linear;
                            -ms-transition: all .2s linear;
                            -o-transition: all .2s linear;
                            transition: all .2s linear;
                        }

                        .image a:hover,
                        .image a:focus {
                            width: 100%;
                        }

                        img {
                            height: auto;
                            max-width: 100%;
                        }


                        /* Boring container/type styles */
                        * {
                            -moz-box-sizing: border-box;
                            -webkit-box-sizing: border-box;
                            box-sizing: border-box;
                        }
                        .wrap {
                            margin: 0 auto;
                            padding: 0 15px;
                            width: 100%;
                            max-width: 300px;
                        }

                        @media screen and (max-width: 478px){
                            .image {
                                position: relative;
                                margin: 20px auto 50px;
                                padding-bottom: 40px; /* Needed to make room for the shadow */
                                height: 50px;
                                text-align: center;
                            }
                            /* Floating Shadow Pseudoelement */
                            .image a:after { 
                                content:'';
                                display: block;
                                position: absolute;
                                left: 12%;
                                bottom: 0px;
                                width: 80%;
                                max-width: 10px;
                                height: 10px;
                                background: transparent;
                                border-radius: 100px/50px;
                                box-shadow: 0 50px 40px rgba(0,0,0,0.5);
                            }

                            .image a {
                                display: block;
                                position: relative;
                                margin: 0 auto;
                                width: 55%;
                                -webkit-transition: all .2s linear;
                                -moz-transition: all .2s linear;
                                -ms-transition: all .2s linear;
                                -o-transition: all .2s linear;
                                transition: all .2s linear;
                            }

                            .image a:hover,
                            .image a:focus {
                                width: 50%;
                            }

                            img {
                                height: auto;
                                max-width: 50%;
                            }


                            /* Boring container/type styles */
                            * {
                                -moz-box-sizing: border-box;
                                -webkit-box-sizing: border-box;
                                box-sizing: border-box;
                            }
                            .wrap {
                                margin: 0 auto;
                                padding: 0 15px;
                                width: 100%;
                                max-width: 300px;
                            }

                        }

                        @media screen and (max-width: 740px){
                        }
                    </style>
                    <div class="banner">
                        <div class="image">
                            <a><?php echo $coverpic2; ?></a>
                        </div>
                    </div>
                </div>
            </div>
            &nbsp;
            <hr/>
            <div class="banner">
                <div class="container">
                    <!--<div class="wrapper2">-->
                    <!--</?php include_once("status.php"); ?>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

<!--        <script src="js/main.js"></script>-->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                (function (b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                            function () {
                                (b[l].q = b[l].q || []).push(arguments)
                            });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = '//www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
                }(window, document, 'script', 'ga'));
                ga('create', 'UA-XXXXX-X', 'auto');
                ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
        <!--<footer><?php include_once("include/template_pageBottom.php"); ?></footer>-->
    </body>
</html>