<?php
// This file is www.developphp.com curriculum material
// Written by Adam Khoury January 01, 2011
// http://www.youtube.com/view_play_list?p=442E340A42191003
// Script Error Reporting
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php
// Check to see the URL variable is set and that it exists in the database
if (isset($_GET['id'])) {
    // Connect to the MySQL database  
    include "include/check_login_status.php";
    $id = preg_replace('#[^0-9]#i', '', $_GET['id']);
    // Use this var to check to see if this ID exists, if yes then get the product 
    // details, if no then exit this script and give message why
    $sql = mysqli_query($db_conx, "SELECT * FROM products WHERE id='$id' LIMIT 1");
    $productCount = mysqli_num_rows($sql); // count the output amount
    if ($productCount > 0) {
        // get all the product details
        while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
            $product_name = $row["product_name"];
            $price = $row["price"];
            $details = $row["details"];
            $category = $row["category"];
            $subcategory = $row["subcategory"];
            $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
        }
    } else {
        echo "That item does not exist.";
        exit();
    }
} else {
    echo "Data to render this page is missing.";
    exit();
}
//mysqli_close($db_conx);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $product_name; ?></title>
        <link rel="stylesheet" href="style/style.css" type="text/css" media="screen" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <link rel="icon" href="images/altposts.png" type="image/x-icon">
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" />

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css" />
        <!--<![endif]-->
        <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
        <!--        <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
        <link rel="stylesheet" href="css/main.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <!-- starting of old code-->
        <link rel="stylesheet" href="style/style.css">
        
    </head>
    <body>
<?php include_once("include/template_pageTop.php"); ?>
        &nbsp;
        <div align="center" id="mainWrapper">
            <div id="pageContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="15">
                    <tr>
                        <td width="19%" valign="top"><img src="inventory_images/<?php echo $id; ?>.jpg" width="142" height="188" alt="<?php echo $product_name; ?>" /><br />
                            <a href="inventory_images/<?php echo $id; ?>.jpg">View Full Size Image</a></td>
                        <td width="81%" valign="top"><h3><?php echo $product_name; ?></h3>
                            <p><?php echo "$" . $price; ?><br />
                                <br />
<?php echo "$subcategory $category"; ?> <br />
                                <br />
                                <?php echo $details; ?>
                                <br />
                            </p>
                            <form id="form1" name="form1" method="post" action="cart.php">
                                <input type="hidden" name="pid" id="pid" value="<?php echo $id; ?>" />
                                <input type="submit" name="button" id="button" value="Add to Shopping Cart" />
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

<!--        <script src="js/main.js"></script>-->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                (function (b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                            function () {
                                (b[l].q = b[l].q || []).push(arguments)
                            });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = '//www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
                }(window, document, 'script', 'ga'));
                ga('create', 'UA-XXXXX-X', 'auto');
                ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
<?php include_once("include/template_pageBottom.php"); ?>
    </body>
</html>