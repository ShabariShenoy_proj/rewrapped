<?php
include_once("include/check_login_status.php");
// Make sure the _GET "u" is set, and sanitize it
$u = "";
$gal = "";
$addMore = "";
if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $gal = preg_replace('#[^a-z0=9]#i', '', $_GET['gal']);
} else {
    header("location: index.php");
    exit();
}
// start of trial carousel
$query = "select * from photos WHERE user='$u' AND gallery='$gal' order by id desc limit 6";
$res = mysqli_query($db_conx, $query);
$count = mysqli_num_rows($res);
$slides = '';
$Indicators = '';
$counter = 0;
$banner = "";
// end of trial carousel
$photo_form = "";
// Check to see if the viewer is the account owner
$isOwner = "no";
if ($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    $banner .= '<h3> Your '.$gal.' gallery</h3>';
    $addMore .= '<span style="float: right;margin-top: -30px;"><a href="photos.php?u=' . $log_username . '">Add More Images</a></span>';
    while ($row = mysqli_fetch_array($res)) {
        $title = $row['user'];
        $desc = $row['gallery'];
        $image = $row['filename'];
        if ($counter == 0) {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '" class="active"></li>';
            $slides .= '<div class="item active">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" />
            <div class="carousel-caption">
              <h3>' . $title . '</h3>
              <p>' . $desc . '.</p>
              </div>
          </div>';
        } else {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '"></li>';
            $slides .= '<div class="item">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" />
            <div class="carousel-caption">
              <h3>' . $title . '</h3>
              <p>' . $desc . '.</p>
              </div>
          </div>';
        }
        $counter++;
    }
} else {
    $banner .= '<h3>' . $u . '\'s '.$gal.' gallery</h3>';
    //$addMore .= '<span style="float: right;margin-top: -30px;"><a href="photos.php?u=' . $log_username . '"></a></span>';
    while ($row = mysqli_fetch_array($res)) {
        $title = $row['user'];
        $desc = $row['gallery'];
        $image = $row['filename'];
        if ($counter == 0) {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '" class="active"></li>';
            $slides .= '<div class="item active">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" />
            <div class="carousel-caption">
              <h3>' . $title . '</h3>
              <p>' . $desc . '.</p>
              </div>
          </div>';
        } else {
            $Indicators .='<li data-target="#carousel-example-generic" data-slide-to="' . $counter . '"></li>';
            $slides .= '<div class="item">
            <img src="user/' . $u . '/' . $image . '" alt="' . $title . '" />
            <div class="carousel-caption">
              <h3>' . $title . '</h3>
              <p>' . $desc . '.</p>
              </div>
          </div>';
        }
        $counter++;
    }
}
// Select the user galleries
//$gallery_list = "";
//$sql = "SELECT DISTINCT gallery FROM photos WHERE user='$u'";
//$query = mysqli_query($db_conx, $sql);
//if (mysqli_num_rows($query) < 1) {
//    $gallery_list = "This user has not uploaded any photos yet.";
//} else {
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        $gallery = $row["gallery"];
//        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM photos WHERE user='$u' AND gallery='$gallery'");
//        $countrow = mysqli_fetch_row($countquery);
//        $count = $countrow[0];
//        $filequery = mysqli_query($db_conx, "SELECT filename FROM photos WHERE user='$u' AND gallery='$gallery' ORDER BY RAND() LIMIT 1");
//        $filerow = mysqli_fetch_row($filequery);
//        $file = $filerow[0];
//        $gallery_list .= '<div>';
//        $gallery_list .= '<div onclick="showGallery(\'' . $gallery . '\',\'' . $u . '\')">';
//        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo">';
//        $gallery_list .= '</div>';
//        $gallery_list .= '<b>' . $gallery . '</b> (' . $count . ')';
//        $gallery_list .= '</div>';
//    }
//}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $u; ?> Photos</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <style type="text/css">
            form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px;}
            div#galleries{}
            div#galleries > div{float:left; margin:20px; text-align:center; cursor:pointer;}
            div#galleries > div > div {height:100px; overflow:hidden;}
            div#galleries > div > div > img{width:150px; cursor:pointer;}
            div#photos{display:none; border:#666 1px solid; padding:20px;}
            div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
            div#photos > div > img{width:125px; cursor:pointer;}
            div#picbox{display:none; padding-top:36px;}
            div#picbox > img{max-width:800px; display:block; margin:0px auto;}
            div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <script>
            function showGallery(gallery, user) {
                _("galleries").style.display = "none";
                _("section_title").innerHTML = user + '&#39;s ' + gallery + ' Gallery &nbsp; <button onclick="backToGalleries()">Go back to all galleries</button>';
                _("photos").style.display = "block";
                _("photos").innerHTML = 'loading photos ...';
                var ajax = ajaxObj("POST", "php_parsers/photo_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        _("photos").innerHTML = '';
                        var pics = ajax.responseText.split("|||");
                        var length = pics.length;
                        for (var i = 0; i < pics.length; i++) {
                            var pic = pics[i].split("|");
                            _("photos").innerHTML += '<div><img onclick="photoShowcase(\'' + pics[i] + '\')" src="user/' + user + '/' + pic[1] + '" alt="pic"></div>';
                        }
                        _("photos").innerHTML += '<p style="clear:left;"></p>';
                    }
                }
                ajax.send("show=galpics&gallery=" + gallery + "&user=" + user);
            }
            function backToGalleries() {
                _("photos").style.display = "none";
                _("section_title").innerHTML = "<?php echo $u; ?>&#39;s Photo Galleries";
                _("galleries").style.display = "block";
            }
            function photoShowcase(picdata) {
                var data = picdata;
                _("section_title").style.display = "none";
                _("picbox").style.display = "block";
                _("picbox").innerHTML = '<button onclick="closePhoto()">x</button>';
                _("picbox").innerHTML += '<img src="user/<?php echo $u; ?>/' + data[1] + '" alt="photo">';
                if ("<?php echo $isOwner ?>" == "yes") {
                    _("picbox").innerHTML += '<p id="deletelink"><a href="#" onclick="return false;" onmousedown="deletePhoto(\'' + data[0] + '\')">Delete this Photo <?php echo $u; ?></a></p>';
                }
            }
            function closePhoto() {
                _("picbox").innerHTML = '';
                _("picbox").style.display = "none";
                _("photos").style.display = "block";
                _("section_title").style.display = "block";
            }
            function deletePhoto(id) {
                var conf = confirm("Press OK to confirm the delete action on this photo.");
                if (conf != true) {
                    return false;
                }
                _("deletelink").style.visibility = "hidden";
                var ajax = ajaxObj("POST", "php_parsers/photo_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "deleted_ok") {
                            alert("This picture has been deleted successfully. We will now refresh the page for you.");
                            window.location = "photos.php?u=<?php echo $u; ?>";
                        }
                    }
                }
                ajax.send("delete=photo&id=" + id);
            }
        </script>
    </head>
    <body>
        <?php include_once("include/template_pageTop.php"); ?>
        <div id="myCarousel" class="container" style="width: 730px;">
            <h2><?php echo $banner; ?></h2><div id="link"><?php echo $addMore; ?></div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php echo $Indicators; ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php echo $slides; ?>  
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>


        <!--<?php include_once("include/template_pageBottom.php"); ?>-->
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
    </body>
</html>