<?php
include_once("include/check_login_status.php");
// Make sure the _GET "u" is set, and sanitize it
$u = "";
$id = '';
$li = '';
$fname = "";
$lname = "";
$usrlvl = "";
$name = "";
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE id='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else {
    header("location: index.php");
    exit();
}

$sql = "SELECT * FROM users WHERE username='$u' AND activated='1' LIMIT 1";
$user_query = mysqli_query($db_conx, $sql);

$sql1 = "SELECT * FROM users WHERE username='$u'";
$user_query1 = mysqli_query($db_conx, $sql1);
$row = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
//$id = $row["id"];
$fname = $row["firstname"];
$lname = $row["lastname"];
$usrlvl = $row["userlevel"];

$photo_form = "";
// Check to see if the viewer is the account owner
$isOwner = "no";
if ($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    $photo_form = '<form id="photo_form" enctype="multipart/form-data" method="post" action="php_parsers/photo_system.php">';
    $photo_form .= '<h3>Hi ' . $u . ', add a new photo into one of your galleries</h3>';
    $photo_form .= '<b>Choose Gallery:</b> ';
    $photo_form .= '<select name="gallery" required>';
    $photo_form .= '<option value=""></option>';
    $photo_form .= '<option value="Achievements">Achievements & Awards</option>';
    $photo_form .= '<option value="Sold">Homes Sold</option>';
    $photo_form .= '<option value="Staging">Staging Snapshots</option>';
//    $photo_form .= '<option value="Rand">Other</option>';
    $photo_form .= '<option value="Random">Random</option>';
    $photo_form .= '</select><br/>';
    $photo_form .= '<p style="display: inline;"><b>Choose Photo:</b> '; // &nbsp; &nbsp; &nbsp;
    $photo_form .= '<input style=" cursor:pointer;" type="file" name="photo" accept="image/*" required></p>';
    $photo_form .= '<p><input type="submit" value="Upload Photo Now"></p>';
    $photo_form .= '</form>';
    $name = "Your ";
} else {
    $name = "$fname $lname&#39;s ";
}
// Select the user galleries
$gallery_list = "";
$sql = "SELECT DISTINCT gallery FROM photos WHERE user='$u'";
$query = mysqli_query($db_conx, $sql);
if (mysqli_num_rows($query) < 1) {
    $gallery_list = "This user has not uploaded any photos yet.";
} else {
    $gallery_list .= '<div id="imageSet">';
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $gallery = $row["gallery"];
        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM photos WHERE user='$u' AND gallery='$gallery'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];

        $gallery_list .= '<div id="wrapper">';
        $gallery_list .= '<b>' . $gallery . '</b> (' . $count . ')<br/>';
//        $gallery_list .= '<section class="grid-wrap">';
//        $gallery_list .= '<ul class="grid swipe-right" id="grid">';

        $filequery = mysqli_query($db_conx, "SELECT filename FROM photos WHERE user='$u' AND gallery='$gallery'"); //ORDER BY RAND() LIMIT 1
        while ($filerow = mysqli_fetch_array($filequery, MYSQLI_ASSOC)) {// was fetch_row
            $file = $filerow["filename"];
            $gallery_list .= '<a href="user/' . $u . '/' . $file . '" class="lightbox35" onclick="#" style="display: inline; margin:20px; text-align:center; cursor:pointer;" class="lightbox_trigger">'; //onclick="window.location = \'photo.php?gal=' . $gallery . '&u=' . $u . '\'" //onclick="showGallery(\''.$gallery.'\',\''.$u.'\')"
            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" class="thumb">';
            $gallery_list .= '</a>';
        }

//        $gallery_list .= '</section>';
//        $gallery_list .= '<hr/>';
        //$gallery_list .= '<div onclick="showGallery\'' . $gallery . '\',\'' . $u . '\')">';

        $gallery_list .= '</div><hr/>';

        $li .= '<li><a href="#"><img src="user/' . $u . '/' . $file . '" alt="gallery photo"><h3><b>' . $gallery . '</b> (' . $count . ')</h3></a></li>';
    }
    $gallery_list .= '</div>';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $u; ?> Photos</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <script src="https://code.jquery.com/jquery-1.6.2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="js/lightbox35.js"></script>
        <script src="js/modernizr.custom.js"></script>
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
                margin:0; 
                padding:0; 
                background:#efefef;
                /*                text-align:center;  used to center div in IE */
            }
            #wrapper {
                width: 60%;//auto;//600px; 
                margin:0 auto; /*centers the div horizontally in all browsers (except IE)*/
                background:#fff; 
                text-align:left; /*resets text alignment from body tag */
                border:1px solid #ccc;
                border-top:none; 
                padding:25px; 
                /*Let's add some CSS3 styles, these will degrade gracefully in older browser and IE*/
                border-radius:0 0 5px 5px;
                -moz-border-radius:0 0 5px 5px;
                -webkit-border-radius: 0 0 5px 5px; 
                box-shadow:0 0 5px #ccc;
                -moz-box-shadow:0 0 5px #ccc;
                -webkit-box-shadow:0 0 5px #ccc;
            }
            #lightbox {
                position:fixed; /* keeps the lightbox window in the current viewport */
                top:0; 
                left:0; 
                width:100%; 
                height:100%; 
                background:url('http://thevectorlab.net/flatlab/assets/fancybox/source/fancybox_overlay.png') repeat; //url(overlay.png)
                text-align:center;
            }
            #lightbox p {
                text-align:right; 
                color:#fff; 
                margin-right:20px; 
                font-size:12px; 
            }
            #lightbox img {
                box-shadow:0 0 25px #111;
                -webkit-box-shadow:0 0 25px #111;
                -moz-box-shadow:0 0 25px #111;
                max-width:940px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <style type="text/css">
            div#photo_form {display: inline; text-align: left; margin:20px;}
            form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px; margin: 0 auto; display: table;}//width: 80%; 
            div#galleries{}

            /*            div#galleries > div > div#galleries2 {height:100px; overflow:hidden;}*/
            div#galleries > div > div > a > img{width:150px; cursor:pointer;}
            @media only screen and (max-width: 320px) {
                div#galleries > div > div > a > img{width:100px; cursor:pointer;}
            }
            div#photos{display:none; border:#666 1px solid; padding:20px;}
            div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
            div#photos > div > img{width:125px; cursor:pointer;}
            div#picbox{display:none; padding-top:36px;}
            div#picbox > img{max-width:800px; display:block; margin:0px auto;}
            div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <link rel="stylesheet" href="style/style.css">

        <script>
            function showGallery(gallery, user) {
                _("galleries").style.display = "none";
                _("section_title").innerHTML = user + '&#39;s ' + gallery + ' Gallery &nbsp; <button onclick="backToGalleries()">Go back to all galleries</button>';
                _("photos").style.display = "block";
                _("photos").innerHTML = 'loading photos ...';
                var ajax = ajaxObj("POST", "php_parsers/photo_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        _("photos").innerHTML = '';
                        var pics = ajax.responseText.split("|||");
                        var length = pics.length;
                        for (var i = 0; i < pics.length; i++) {
                            var pic = pics[i].split("|");
                            _("photos").innerHTML += '<div><img onclick="photoShowcase(\'' + pics[i] + '\')" src="user/' + user + '/' + pic[1] + '" alt="pic"></div>';
                        }
                        _("photos").innerHTML += '<p style="clear:left;"></p>';
                    }
                }
                ajax.send("show=galpics&gallery=" + gallery + "&user=" + user);
            }
            function backToGalleries() {
                _("photos").style.display = "none";
                _("section_title").innerHTML = "<?php echo $u; ?>&#39;s Photo Galleries";
                _("galleries").style.display = "block";
            }
            function photoShowcase(picdata) {
                var data = picdata.split("|");
                _("section_title").style.display = "none";
                _("photos").style.display = "none";
                _("picbox").style.display = "block";
                _("picbox").innerHTML = '<button onclick="closePhoto()">x</button>';
                _("picbox").innerHTML += '<img src="user/<?php echo $u; ?>/' + data[1] + '" alt="photo">';
                if ("<?php echo $isOwner ?>" == "yes") {
                    _("picbox").innerHTML += '<p id="deletelink"><a href="#" onclick="return false;" onmousedown="deletePhoto(\'' + data[0] + '\')">Delete this Photo <?php echo $u; ?></a></p>';
                }
            }
            function closePhoto() {
                _("picbox").innerHTML = '';
                _("picbox").style.display = "none";
                _("photos").style.display = "block";
                _("section_title").style.display = "block";
            }
            function deletePhoto(id) {
                var conf = confirm("Press OK to confirm the delete action on this photo.");
                if (conf != true) {
                    return false;
                }
                _("deletelink").style.visibility = "hidden";
                var ajax = ajaxObj("POST", "php_parsers/photo_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "deleted_ok") {
                            alert("This picture has been deleted successfully. We will now refresh the page for you.");
                            window.location = "photos.php?u=<?php echo $u; ?>";
                        }
                    }
                }
                ajax.send("delete=photo&id=" + id);
            }
        </script>
    </head>
    <body>
        <?php include_once("include/template_pageTop.php"); ?>
        <div id="pageMiddle">
            <div class="container"><div style="margin: 0 auto;"><?php echo $photo_form; ?></div></div> <!-- id="photo_form" -->
            <h2 id="section_title" style="text-align: center;"><?php echo $name; ?> Personal Gallery</h2>
            <div id="galleries"><?php echo $gallery_list; ?></div> <!--  id="galleries" -->
            <div id="photos"></div>
            <div id="picbox"></div>
            <p style="clear:left;">These photos belong to <a href="user.php?u=<?php echo $u; ?>"><?php echo $u; ?></a></p>
            <!--            <div id="lightbox">
                            <p>Click to close</p>
                            <div id="content">
                                <img src="#" />
                            </div>
                        </div>-->
            <div class="footer"><?php include_once("include/template_pageBottom.php"); ?></div>
        </div>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/colorfinder-1.1.js"></script>
        <script src="js/gridScrollFx.js"></script>
        <script>
            new GridScrollFx(document.getElementById('grid'), {
                viewportFactor: 0.4
            });
        </script>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
    </body>
</html>