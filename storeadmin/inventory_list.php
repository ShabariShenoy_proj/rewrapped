<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
if (!$_SESSION["manager"]) {
    header("location: admin_login.php");
    exit();
}
// be sure to check the manager session value is actually in the Database
$managerID = preg_replace('#[^0-9]#i', '', $_SESSION["id"]);
$manager = preg_replace('#[^A-Za-z0-9]#i', '', $_SESSION["manager"]);
$password = preg_replace('#[^A-Za-z0-9]#i', '', $_SESSION["password"]);

include"../include/mysql_connect.php";
$sql = mysqli_query($db_conx, "SELECT * FROM admin WHERE username='$manager' AND password='$password' LIMIT 1"); //query the person loging in id='$managerID' AND 
// ensure this person exists in the database
$existCount = mysqli_num_rows($sql);
if ($existCount == 1) {
    $row = mysqli_fetch_array($sql, MYSQLI_ASSOC);
    $id = $row["id"];
    $_SESSION["id"] = $id;
} else if ($existCount == 0) {
    echo "$managerID your login session data is not on our record" . $_SESSION["id"];
    //header("location: ../index.php");
    exit();
}
?>
<?php
// Script Error Reporting
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php
// Delete Item Question to Admin, and Delete Product if they choose
if (isset($_GET['deleteid'])) {
    echo 'Do you really want to delete product with ID of ' . $_GET['deleteid'] . '? <a href="inventory_list.php?yesdelete=' . $_GET['deleteid'] . '">Yes</a> | <a href="inventory_list.php">No</a>';
    exit();
}
if (isset($_GET['yesdelete'])) {
    // remove item from system and delete its picture
    // delete from database
    $id_to_delete = $_GET['yesdelete'];
    $sql = mysqli_query($db_conx, "DELETE FROM products WHERE id='$id_to_delete' LIMIT 1") or die(mysql_error());
    // unlink the image from server
    // Remove The Pic -------------------------------------------
    $pictodelete = ("../inventory_images/$id_to_delete.jpg");
    if (file_exists($pictodelete)) {
        unlink($pictodelete);
    }
    header("location: inventory_list.php");
    exit();
}
?>
<?php
// Parse the form data and add inventory item to the system
if (isset($_POST['product_name'])) {

    $product_name = mysqli_real_escape_string($db_conx, $_POST['product_name']);
    $price = mysqli_real_escape_string($db_conx, $_POST['price']);
    $category = mysqli_real_escape_string($db_conx, $_POST['category']);
    $subcategory = mysqli_real_escape_string($db_conx, $_POST['subcategory']);
    $details = mysqli_real_escape_string($db_conx, $_POST['details']);
    // See if that product name is an identical match to another product in the system
    $sql = mysqli_query($db_conx, "SELECT id FROM products WHERE product_name='$product_name' LIMIT 1");
    $productMatch = mysqli_num_rows($sql); // count the output amount
    if ($productMatch > 0) {
        echo 'Sorry you tried to place a duplicate "Product Name" into the system, <a href="inventory_list.php">click here</a>';
        exit();
    }
    // Add this product into the database now
    $sql = mysqli_query($db_conx, "INSERT INTO products (product_name, price, details, category, subcategory, date_added) 
        VALUES('$product_name','$price','$details','$category','$subcategory',now())") or die(mysql_error());
    $pid = mysqli_insert_id($db_conx);
    // Place image in the folder 
    $newname = "$pid.jpg";
    move_uploaded_file($_FILES['fileField']['tmp_name'], "../inventory_images/$newname");
    // the below header code is here so that after loading the item it doesnt try to reload it when refreshed after form submissionedit
    header("location: inventory_list.php");
    exit();
}
?>
<?php
// This block grabs the whole list for viewing
$product_list = "";
$sql = mysqli_query($db_conx, "SELECT * FROM products ORDER BY date_added DESC");
$productCount = mysqli_num_rows($sql); // count the output amount
if ($productCount > 0) {
    while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
        $id = $row["id"];
        $product_name = $row["product_name"];
        $price = $row["price"];
        $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
        $product_list .= "Product ID: $id - <strong>$product_name</strong> - $$price - <em>Added $date_added</em> &nbsp; &nbsp; &nbsp; <a href='inventory_edit.php?pid=$id'>edit</a> &bull; <a href='inventory_list.php?deleteid=$id'>delete</a><br />";
    }
} else {
    $product_list = "You have no products listed in your store yet";
}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A layout example that shows off a responsive product landing page.">

        <title>Store Home Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <link rel="icon" href="../images/altposts.png" type="image/x-icon">
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css" />

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="../css/layouts/marketing.css" />
        <!--<![endif]-->
        <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
        <!--        <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
        <link rel="stylesheet" href="../css/main.css"/>
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <!-- starting of old code-->
        <link rel="stylesheet" href="../style/style.css">
        
        <!-- Pasted from Index.php-->
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css" />
        <!--<meta name="keywords" content="footer, address, phone, icons" />-->
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <link rel="stylesheet" href="../css/demo.css">
        <link rel="stylesheet" href="../css/footer-distributed-with-address-and-phones.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="../css/layouts/marketing.css" />
        <!--<![endif]-->
        <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/main.css"/>
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <!-- starting of old code-->
        <link rel="stylesheet" href="../style/style.css">
        <!-- pasted from index.php -->
        
    </head>
    <body>
        <?php include_once ("../include/template_pageTop.php"); ?>
        &nbsp;
        <p>hello <?php echo $manager; ?> <?php echo $managerID; ?> <?php echo $_SESSION["id"]; ?></p>
        <div id="pageContent"><br/>
            <a href="indexs.php">Admin Home</a><br/>
            <a href="logout.php">Log Out</a>
            <div align="right" style="margin-right: 32px;"><a href="inventory_list.php#inventoryForm"> + add new item </a></div>
            <div align="left" style="margin-left: 24px;">
                <h1> Inventory List </h1>
                <?php echo $product_list; ?>
            </div>           

            <!--<h3>Add new inventory item form</h3>-->
            <a name="inventoryForm" id="inventoryForm"></a>
            <h3>
                &darr; Add New Inventory Item Form &darr;
            </h3>
            <form action="inventory_list.php" enctype="multipart/form-data" name="myForm" id="myform" method="post">
                <table width="90%" border="0" cellspacing="0" cellpadding="6">
                    <tr>
                        <td width="20%" align="right">Product Name</td>
                        <td width="80%"><label>
                                <input name="product_name" type="text" id="product_name" size="64" />
                            </label></td>
                    </tr>
                    <tr>
                        <td align="right">Product Price</td>
                        <td><label>
                                $
                                <input name="price" type="text" id="price" size="12" />
                            </label></td>
                    </tr>
                    <tr>
                        <td align="right">Category</td>
                        <td><label>
                                <select name="category" id="category">
                                    <option value="Clothing">Clothing</option>
                                </select>
                            </label></td>
                    </tr>
                    <tr>
                        <td align="right">Subcategory</td>
                        <td><select name="subcategory" id="subcategory">
                                <option value=""></option>
                                <option value="Hats">Hats</option>
                                <option value="Pants">Pants</option>
                                <option value="Shirts">Shirts</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td align="right">Product Details</td>
                        <td><label>
                                <textarea name="details" id="details" cols="64" rows="5"></textarea>
                            </label></td>
                    </tr>
                    <tr>
                        <td align="right">Product Image</td>
                        <td><label>
                                <input type="file" name="fileField" id="fileField" />
                            </label></td>
                    </tr>      
                    <tr>
                        <td>&nbsp;</td>
                        <td><label>
                                <input type="submit" name="button" id="button" value="Add This Item Now" />
                            </label></td>
                    </tr>
                </table>
            </form>
        </div>

        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
        <?php include_once ("../include/template_pageBottom.php"); ?>
    </body>
</html>
