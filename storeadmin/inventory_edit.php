<?php
// This file is www.developphp.com curriculum material
// Written by Adam Khoury January 01, 2011
// http://www.youtube.com/view_play_list?p=442E340A42191003
session_start();
if (!isset($_SESSION["manager"])) {
    header("location: admin_login.php");
    exit();
}
// Be sure to check that this manager SESSION value is in fact in the database
$managerID = preg_replace('#[^0-9]#i', '', $_SESSION["id"]); // filter everything but numbers and letters
$manager = preg_replace('#[^A-Za-z0-9]#i', '', $_SESSION["manager"]); // filter everything but numbers and letters
$password = preg_replace('#[^A-Za-z0-9]#i', '', $_SESSION["password"]); // filter everything but numbers and letters
// Run mySQL query to be sure that this person is an admin and that their password session var equals the database information
// Connect to the MySQL database  
include "../include/mysql_connect.php";
$sql = mysqli_query($db_conx, "SELECT * FROM admin WHERE username='$manager' AND password='$password' LIMIT 1"); // query the person id='$managerID' AND 
// ------- MAKE SURE PERSON EXISTS IN DATABASE ---------
$existCount = mysqli_num_rows($sql); // count the row nums
if ($existCount == 1) {
    $row = mysqli_fetch_array($sql, MYSQLI_ASSOC);
    $id = $row["id"];
    $_SESSION["id"] = $id;
} else if ($existCount == 0) { // evaluate the count
    echo $_SESSION["id"] . " $managerID Your login session data is not on record in the database.";
    exit();
}
?>
<?php
// Script Error Reporting
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php
// Parse the form data and add inventory item to the system
if (isset($_POST['product_name'])) {

    $pid = mysqli_real_escape_string($db_conx, $_POST['thisID']);
    $product_name = mysqli_real_escape_string($db_conx, $_POST['product_name']);
    $price = mysqli_real_escape_string($db_conx, $_POST['price']);
    $category = mysqli_real_escape_string($db_conx, $_POST['category']);
    $subcategory = mysqli_real_escape_string($db_conx, $_POST['subcategory']);
    $details = mysqli_real_escape_string($db_conx, $_POST['details']);
    // See if that product name is an identical match to another product in the system
    $sql = mysqli_query($db_conx, "UPDATE products SET product_name='$product_name', price='$price', details='$details', category='$category', subcategory='$subcategory' WHERE id='$pid'");
    if ($_FILES['fileField']['tmp_name'] != "") {
        // Place image in the folder 
        $newname = "$pid.jpg";
        move_uploaded_file($_FILES['fileField']['tmp_name'], "../inventory_images/$newname");
    }
    header("location: inventory_list.php");
    exit();
}
?>
<?php
// Gather this product's full information for inserting automatically into the edit form below on page
if (isset($_GET['pid'])) {
    $targetID = $_GET['pid'];
    $sql = mysqli_query($db_conx, "SELECT * FROM products WHERE id='$targetID' LIMIT 1");
    $productCount = mysqli_num_rows($sql); // count the output amount
    if ($productCount > 0) {
        while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {

            $product_name = $row["product_name"];
            $price = $row["price"];
            $category = $row["category"];
            $subcategory = $row["subcategory"];
            $details = $row["details"];
            $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
        }
    } else {
        echo "Sorry dude that crap dont exist.";
        exit();
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Inventory List</title>
        <link rel="stylesheet" href="../style/style.css" type="text/css" media="screen" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
            <link rel="icon" href="../images/altposts.png" type="image/x-icon">
                <link rel="stylesheet" href="../css/bootstrap.min.css" />
                <link rel="stylesheet" href="../css/bootstrap-theme.min.css" />

                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
                <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


                <!--[if lte IE 8]>
                  
                    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
                  
                <![endif]-->
                <!--[if gt IE 8]><!-->

                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

                <!--<![endif]-->



                <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




                <!--[if lte IE 8]>
                    <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
                <![endif]-->
                <!--[if gt IE 8]><!-->
                <link rel="stylesheet" href="../css/layouts/marketing.css" />
                <!--<![endif]-->
                <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
                <!--        <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
                <link rel="stylesheet" href="../css/main.css"/>
                <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
                <link rel="icon" href="favicon.ico" type="image/x-icon"/>
                <!-- starting of old code-->
                <link rel="stylesheet" href="../style/style.css"/>


                <!-- Pasted from Index.php-->
                <link rel="stylesheet" href="../css/bootstrap.min.css" />
                <link rel="stylesheet" href="../css/bootstrap-theme.min.css" />
                <!--<meta name="keywords" content="footer, address, phone, icons" />-->
                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
                <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

                <link rel="stylesheet" href="../css/demo.css">
                    <link rel="stylesheet" href="../css/footer-distributed-with-address-and-phones.css">

                        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

                            <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
                                <!--[if lte IE 8]>
                                  
                                    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
                                  
                                <![endif]-->
                                <!--[if gt IE 8]><!-->

                                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

                                <!--<![endif]-->



                                <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




                                <!--[if lte IE 8]>
                                    <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
                                <![endif]-->
                                <!--[if gt IE 8]><!-->
                                <link rel="stylesheet" href="../css/layouts/marketing.css" />
                                <!--<![endif]-->
                                <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
                                <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
                                    <link rel="stylesheet" href="../css/main.css"/>
                                    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

                                    <!-- starting of old code-->
                                    <link rel="stylesheet" href="../style/style.css">
                                        <!-- pasted from index.php -->

                                        </head>

                                        <body>
                                            <?php include_once("../include/template_pageTop.php"); ?>
                                            &nbsp;
                                            <p>hello <?php echo $manager; ?> <?php echo $managerID; ?> <?php echo $_SESSION["id"]; ?></p>
                                            <div align="center" id="mainWrapper">
                                                <div id="pageContent"><br />
                                                    <div align="left" style="margin-right:32px;">
                                                        <a href="indexs.php">Admin Home</a> | <a href="logout.php">Log Out</a> | <a href="inventory_list.php">Back to Inventory</a>
                                                    </div>
                                                    <div align="right" style="margin-right:32px;"><a href="inventory_list.php#inventoryForm">+ Add New Inventory Item</a></div>
                                                    <!--                <div align="left" style="margin-left:24px;">
                                                                        <h2>Inventory list</h2>
                                                                        </?php echo $product_list; ?>
                                                                    </div>-->
                                                    <hr />
                                                    <a name="inventoryForm" id="inventoryForm"></a>
                                                    <h3>
                                                        &darr; Edit Inventory Item Form &darr;
                                                    </h3>
                                                    <form action="inventory_edit.php" enctype="multipart/form-data" name="myForm" id="myform" method="post">
                                                        <table width="90%" border="0" cellspacing="0" cellpadding="6">
                                                            <tr>
                                                                <td width="20%" align="right">Product Name</td>
                                                                <td width="80%"><label>
                                                                        <input name="product_name" type="text" id="product_name" size="64" value="<?php echo $product_name; ?>" />
                                                                    </label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Product Price</td>
                                                                <td><label>
                                                                        $
                                                                        <input name="price" type="text" id="price" size="12" value="<?php echo $price; ?>" />
                                                                    </label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Category</td>
                                                                <td><label>
                                                                        <select name="category" id="category">
                                                                            <option value="Clothing">Clothing</option>
                                                                        </select>
                                                                    </label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Subcategory</td>
                                                                <td><select name="subcategory" id="subcategory">
                                                                        <option value="<?php echo $subcategory; ?>"><?php echo $subcategory; ?></option>
                                                                        <option value="Hats">Hats</option>
                                                                        <option value="Pants">Pants</option>
                                                                        <option value="Shirts">Shirts</option>
                                                                    </select></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Product Details</td>
                                                                <td><label>
                                                                        <textarea name="details" id="details" cols="64" rows="5"><?php echo $details; ?></textarea>
                                                                    </label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Product Image</td>
                                                                <td><label>
                                                                        <input type="file" name="fileField" id="fileField" />
                                                                    </label></td>
                                                            </tr>      
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td><label>
                                                                        <input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
                                                                        <input type="submit" name="button" id="button" value="Make Changes" />
                                                                    </label></td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                    <br />
                                                    <br />
                                                </div>
                                                <?php include_once("../include/template_pageBottom.php"); ?>
                                            </div>
                                        </body>
                                        </html>