<?php
include_once("include/check_login_status.php");
// If the page requestor is not logged in, usher them away
if ($user_ok != true || $log_username == "") {
    header("location: index.php");
    exit();
}
$notification_list = "";
$sql = "SELECT * FROM notifications WHERE username LIKE BINARY '$log_username' ORDER BY date_time DESC";
$query = mysqli_query($db_conx, $sql);
$numrows = mysqli_num_rows($query);
if ($numrows < 1) {
    $notification_list = "You do not have any notifications";
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $noteid = $row["id"];
        $initiator = $row["initiator"];
        $app = $row["app"];
        $note = $row["note"];
        $date_time = $row["date_time"];
        $date_time = strftime("%b %d, %Y", strtotime($date_time));
        $notification_list .= "<p><a href='user.php?u=$initiator'>$initiator</a> | $app<br />$note</p>";
    }
}
mysqli_query($db_conx, "UPDATE users SET notescheck=now() WHERE username='$log_username' LIMIT 1");
?><?php
$friend_requests = "";
$sql = "SELECT * FROM friends WHERE user2='$log_username' AND accepted='0' ORDER BY datemade ASC";
$query = mysqli_query($db_conx, $sql);
$numrows = mysqli_num_rows($query);
if ($numrows < 1) {
    $friend_requests = 'No friend requests';
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $reqID = $row["id"];
        $user1 = $row["user1"];
        $datemade = $row["datemade"];
        $datemade = strftime("%B %d", strtotime($datemade));
        $thumbquery = mysqli_query($db_conx, "SELECT avatar FROM users WHERE username='$user1' LIMIT 1");
        $thumbrow = mysqli_fetch_row($thumbquery);
        $user1avatar = $thumbrow[0];
        $user1pic = '<img src="user/' . $user1 . '/' . $user1avatar . '" alt="' . $user1 . '" class="user_pic">';
        if ($user1avatar == NULL) {
            $user1pic = '<img src="images/avatardefault.jpg" alt="' . $user1 . '" class="user_pic">';
        }
        $friend_requests .= '<div id="friendreq_' . $reqID . '" class="friendrequests">';
        $friend_requests .= '<a href="user.php?u=' . $user1 . '">' . $user1pic . '</a>';
        $friend_requests .= '<div class="user_info" id="user_info_' . $reqID . '">' . $datemade . ' <a href="user.php?u=' . $user1 . '">' . $user1 . '</a> requests friendship<br /><br />';
        $friend_requests .= '<button onclick="friendReqHandler(\'accept\',\'' . $reqID . '\',\'' . $user1 . '\',\'user_info_' . $reqID . '\')">accept</button> or ';
        $friend_requests .= '<button onclick="friendReqHandler(\'reject\',\'' . $reqID . '\',\'' . $user1 . '\',\'user_info_' . $reqID . '\')">reject</button>';
        $friend_requests .= '</div>';
        $friend_requests .= '</div>';
    }
}
?><?php
$usql = "SELECT * FROM users WHERE username='$log_username'";
$uquery = mysqli_query($db_conx, $usql);
$urow = mysqli_fetch_array($uquery, MYSQLI_ASSOC);
$userlevel = $urow["userlevel"];
$request = "";
if ($userlevel == 'b') {
    $request.= "<div id=\"ReqBox\"><h2>Info Requests</h2>";
    $reqsql = "SELECT * FROM bids WHERE agent='$log_username' AND requested='1' ORDER BY datemade ASC";
    $requery = mysqli_query($db_conx, $reqsql);
    $rnumrows = mysqli_num_rows($requery);
    if ($rnumrows < 1) {
        $request = 'No bid info requests';
    } else {
        while ($row = mysqli_fetch_array($requery, MYSQLI_ASSOC)) {
            $reqID = $row["id"];
            $postid = $row["postid"];
            $user1 = $row["owner"];
            $reqdatemade = $row["reqdate"];
            $reqdatemade = strftime("%B %d", strtotime($reqdatemade));
            $rthumbquery = mysqli_query($db_conx, "SELECT avatar FROM users WHERE username='$user1' LIMIT 1");
            $rthumbrow = mysqli_fetch_row($rthumbquery);
            $user1avatar = $rthumbrow[0];
            $user1pic = '<img src="user/' . $user1 . '/' . $user1avatar . '" alt="' . $user1 . '" class="user_pic">';
            if ($user1avatar == NULL) {
                $user1pic = '<img src="images/avatardefault.jpg" alt="' . $user1 . '" class="user_pic">';
            }

            //$sql = "SELECT * FROM notifications WHERE username LIKE BINARY '$log_username' ORDER BY date_time DESC";
            $rquery = mysqli_query($db_conx, "SELECT note FROM bidnotes WHERE agent='$log_username' AND app='Info request' AND owner='$user1' AND bidid='$reqID' AND postid='$postid' LIMIT 1");
            $rrow = mysqli_fetch_row($rquery);
            $notes = $rrow[0];

            $request .= '<div id="req_' . $reqID . '" class="friendrequests">';
            $request .= '<a href="user.php?u=' . $user1 . '">' . $user1pic . '</a>';
            $request .= '<div class="user_info" id="user_req_' . $reqID . '">' . $reqdatemade . ' <a href="user.php?u=' . $user1 . '">' . $user1 . '</a> requests more info'; //<br/> at the emd
            $request .= '<p>' . $notes . '</p><br>';
            $request .= '<button onclick="reqHandler(\'accept\',\'' . $reqID . '\',\'' . $user1 . '\',\'' . $postid . '\',\'user_req_' . $reqID . '\')">accept</button> or ';
            $request .= '<button onclick="reqHandler(\'reject\',\'' . $reqID . '\',\'' . $user1 . '\',\'' . $postid . '\',\'user_req_' . $reqID . '\')">reject</button>';
            $request .= '</div>';
            $request .= '</div>';
        }
    }
    $request .= '</div>';
} else if ($userlevel == 'a') {
    $request.= "<div id=\"ReqBox\"><h2>Bids Notifications</h2>";
    $reqsql = "SELECT * FROM bids WHERE owner='$log_username' ORDER BY datemade ASC";
    $rsql = "SELECT * FROM bidnotes WHERE owner='$log_username' AND app = 'bid placed'";
    $requery = mysqli_query($db_conx, $reqsql);
    // get the previous bids if they changed their bids
    $query = mysqli_query($db_conx, $rsql);
    // left off here-----------
    //$numrows = mysqli_num_rows($rquery);
    $rnumrows = mysqli_num_rows($requery);
    if ($rnumrows < 1) {
        $request = 'No bid placed yet';
    } else {
        while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
            $id = $row["id"];
            $reqID = $row["bidid"];
            $postid = $row["postid"];
            $user1 = $row["agent"];
            $bid = $row["bid"];
            $notes = $row["note"];
            $reqdatemade = $row["date_time"];
            $reqdatemade = strftime("%B %d", strtotime($reqdatemade));
            $rthumbquery = mysqli_query($db_conx, "SELECT avatar FROM users WHERE username='$user1' LIMIT 1");
            $rthumbrow = mysqli_fetch_row($rthumbquery);
            $user1avatar = $rthumbrow[0];
            $user1pic = '<img src="user/' . $user1 . '/' . $user1avatar . '" alt="' . $user1 . '" class="user_pic">';
            if ($user1avatar == NULL) {
                $user1pic = '<img src="images/avatardefault.jpg" alt="' . $user1 . '" class="user_pic">';
            }

            //$sql = "SELECT * FROM notifications WHERE username LIKE BINARY '$log_username' ORDER BY date_time DESC";
//            $rquery = mysqli_query($db_conx, "SELECT bid, note FROM bidnotes WHERE agent='$user1' AND owner='$log_username' AND bidid='$reqID' AND postid='$postid' LIMIT 1");
//            $rrow = mysqli_fetch_row($rquery);
//            $bid = $row[""];
//            $notes = $row[""];

            $request .= '<div id="req_' . $reqID . '" class="friendrequests">';
            $request .= '<a href="user.php?u=' . $user1 . '">' . $user1pic . '</a>';
            $request .= '<div class="user_info_bid" id="agent_bid_' . $reqID . '">' . $reqdatemade . '<a href="user.php?u=' . $user1 . '"> ' . $user1 . '</a> has bid on your post:'; // <p>Bid on your post: %' . $bid . '</p><br/> at the emd
            $request .= ' <b><u>%' . $bid . '</u></b>';
            $request .= '<p>' . $notes . '</p>';
//            $request .= '<button onclick="reqHandler(\'accept\',\'' . $reqID . '\',\'' . $user1 . '\',\'' . $postid . '\',\'user_req_' . $reqID . '\')">accept</button> or ';
//            $request .= '<button onclick="reqHandler(\'reject\',\'' . $reqID . '\',\'' . $user1 . '\',\'' . $postid . '\',\'user_req_' . $reqID . '\')">reject</button>';
            $request .= '</div>';
            $request .= '</div>';
        }
    }
    $request .= '</div>';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Notifications and Friend Requests</title>
        <!-- start of bootstrap -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <!--<title>Landing Page &ndash; Layout Examples &ndash; Pure</title>-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">


        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!-- end of bootstrap -->
        <link rel="stylesheet" href="style/style.css">
        <style type="text/css">
            div#notesBox{float:left; width:430px; border:#F0F 1px dashed; margin-right:60px; padding:10px;}
            div#friendReqBox{float:left; width:430px; border:#F0F 1px dashed; margin-right: 60px; padding:10px;}
            div#ReqBox{float:left; width:auto; border:#F0F 1px dashed; padding:10px;}
            div.friendrequests{height:74px; border-bottom:#CCC 1px solid; margin-bottom:8px;}
            img.user_pic{float:left; width:68px; height:68px; margin-right:8px;}
            div.user_info{float:left; font-size:14px;}
            div.user_info_bid{float:left; font-size:16px;}
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script type="text/javascript">
            function reqHandler(action, reqid, user1, pid, elem) {
                var conf = confirm("Press OK to '" + action + "' request and send your info.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = "processing ...";
                var ajax = ajaxObj("POST", "php_parsers/request_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "accept_ok") {
                            _(elem).innerHTML = "<b>Request Accepted!</b><br />Your info has been sentf";
                        } else if (ajax.responseText == "reject_ok") {
                            _(elem).innerHTML = "<b>Request Rejected</b><br />You chose to deny the request";
                        } else {
                            _(elem).innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.send("action=" + action + "&reqid=" + reqid + "&user1=" + user1 + "&postid=" + pid);
            }

            function friendReqHandler(action, reqid, user1, elem) {
                var conf = confirm("Press OK to '" + action + "' this friend request.");
                if (conf != true) {
                    return false;
                }
                _(elem).innerHTML = "processing ...";
                var ajax = ajaxObj("POST", "php_parsers/friend_system.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "accept_ok") {
                            _(elem).innerHTML = "<b>Request Accepted!</b><br />Your are now friends";
                        } else if (ajax.responseText == "reject_ok") {
                            _(elem).innerHTML = "<b>Request Rejected</b><br />You chose to reject friendship with this user";
                        } else {
                            _(elem).innerHTML = ajax.responseText;
                        }
                    }
                }
                ajax.send("action=" + action + "&reqid=" + reqid + "&user1=" + user1);
            }
        </script>
    </head>
    <body>
<?php include_once("include/template_pageTop.php"); ?>
        <div id="" class="container">
            <!-- START Page Content -->
<!--            <div id="notesBox"><h2>Notifications</h2><//?p/h/p e/c/h/o $notification_list; ?></div>-->
            <div id="friendReqBox"><h2>Friend Requests</h2><?php echo $friend_requests; ?></div>
<?php echo $request; ?>
            <div style="clear:left;"></div>
            <!-- END Page Content -->
        </div>
<?php include_once ("include/template_pageBottom.php"); ?>

        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
    </body>
</html>