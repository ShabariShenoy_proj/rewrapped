<?php
include_once ("include/check_login_status.php");
// view counter
//mysqli_query($db_conx, "UPDATE viewcounter SET `views` = `views`+'1' WHERE id='1'");
//$count = mysqli_query($db_conx, "SELECT * FROM viewcounter WHERE id='1'");
//while ($row = mysqli_fetch_array($count)) {
//    $id = $row["id"];
//    $page = $row["pagename"];
//    $views = $row["views"];
//}
//
//Select the user galleries
$last = '';
$pagenum = '';
$page_rows = '';
$search_output = "";

$sql = "SELECT COUNT(id) FROM posts WHERE expiry >= CURDATE()"; //expired='0'"; //, usr, filename FROM postpictures";
$q1 = mysqli_query($db_conx, $sql);
$pnrow = mysqli_fetch_row($q1);
// total number of rows count
$rows = $pnrow[0];
// this is the number of results we want displayed per page
$page_rows = 10;
//this tells us the page number of our last page
$last = ceil($rows / $page_rows);
// if there are no results then only show one page
if ($last < 1) {
    $last = 1;
}
// establish pagenum variable
$pagenum = 1;
// get pagenum from URL vars if it is present, e;se it is =1
if (isset($_GET['pn'])) {
    $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
}
// ensure pagenum isnt below one or more than $last
if ($pagenum < 1) {
    $pagenum = 1;
} else if ($pagenum > $last) {
    $pagenum = $last;
}
// set the range of rows to query for the chosen $pagenum
$limit = 'LIMIT ' . ($pagenum - 1) * $page_rows . ',' . $page_rows;
$gallery_list = "";
//$sql = "SELECT * FROM posts WHERE expired='0' ORDER BY postdate DESC $limit"; //, usr, filename FROM postpictures";
$sql = "SELECT * FROM posts WHERE expiry >= CURDATE() ORDER BY postdate DESC $limit"; //, usr, filename FROM postpictures";
$query = mysqli_query($db_conx, $sql);

$line1 = "<p style='font-size: 18px;'>(<b>$rows</b>)<strong> Listings";
$line2 = "<p style='font-size: 18px;'>Page <b>$pagenum</b> of <b>$last</b></p>";
// establish the pagination controls variable
$paginationCtrls = '';
//<nav>
//                <ul class="pagination">
//                    <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
//                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
//                    <li class="disabled"><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
//                </ul>
//            </nav>
// if there is more than one page of results
if ($last != 1) {
    $paginationCtrls = '<nav><ul class="pagination">';
    // first check if were on page one if so then we dont need a link to prev page and if we only have
    // one page then no forward link
    if ($pagenum > 1) {
        $previous = $pagenum - 1;
        $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
        // render clickable number links that should appearon the left of the target page number
        for ($i = $pagenum - 4; $i < $pagenum; $i++) {
            if ($i > 0) {
                $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '<span class="sr-only"></span></a></li>'; //(current) inbetween  & spans class="active" after li
            }
        }
    }
    // render target page number but without it being a link
    $paginationCtrls .= '<li class="active"><a href="#">' . $pagenum . '<span class="sr-only">(current)</span></a></li>';
    // render right of current page above
    for ($i = $pagenum + 1; $i <= $last; $i++) {
        $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '<span class="sr-only"></span></a></li>';
        if ($i >= $pagenum + 4) {
            // keep the pages from diplaying extra numbers past the page theyre on
            break;
//$paginationCtrls .= '<li class="active"><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '<span class="sr-only"></span></a></li>'; //(current) inbetween spans
        }
    }
    // this does the same as above only checking if we are on the last page and then generating the next...
    if ($pagenum != $last) {
        $next = $pagenum + 1;
        $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $next . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
    }
    $paginationCtrls .= '</ul></nav>';
}

//$list = '';


if (mysqli_num_rows($query) < 1) {
    $gallery_list = "No posts.";
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $id = $row["id"];
        $u = $row["user"];
        $pn = $row["postName"];
        $desc = $row["description"];
        $asking = $row["askingPrice"];
        $location = $row["location"];
        $address = $row["address"];
        $bedroom = $row["bedroom"];
        $bathroom = $row["bathroom"];
        $sqfootage = $row["sqfootage"];
        $basement = $row["basement"];
        $img = "";

        $loco = '';

        if ($address == '' || $address == NULL) {
            $loco = "%2C%20$location";
        } else {
//            $ad_row = mysqli_fetch_row($address_query);
//            $address = $ad_row[0];
            $loco = str_replace(' ', '%20', $address);
            $loco .= "%2C%20$location";
        }

        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$id'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];
        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$id' ORDER BY RAND() LIMIT 1");
        $filerow = mysqli_fetch_row($filequery);
        $file = $filerow[0];



        $gallery_list .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '\'">'; // background-color: #B0E0E6\
        $gallery_list .= '<div class=\'pure-u-1 style=\'margin:auto;\'><center><div style=\'padding:5px; background-color: #ffad60;\' class="" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'"><b>' . $pn . '</b></center></div>';
        $gallery_list .= '<div class=\'pure-u-1  pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'><center><div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'"></center>'; // was md-2-5
        if ($filerow == NULL) {
            $gallery_list .= '<img class="pure-img" src="images/altposts.png" alt="cover photo">'; //id="thumbnail"
        } else {
            $gallery_list .= '<img class="pure-img" src="user/' . $u . '/' . $file . '" alt="cover photo">'; //style="width:160px; height:120px;"id="thumbnail"
        }
        $gallery_list .= '</div>';
        //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
        //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-2-5 pure-u-md-2-5 pure-u-lg-2-5\'>
                    <div class=\'pure-g\' style="padding-left: 10px;">
                        <div class=\'pure-u-1\'><b style="text-align: center;">Seller: ' . $u . '</b></div>
                        <div class=\'pure-u-1\'><text>Price:<span class="badge">$' . number_format($asking) . '</span></text></div>
                        <div class=\'pure-u-1\'><text>Square Footage: <span class="badge">' . $sqfootage . 'Sq Feet </span></text></div>
                        <div class=\'pure-u-1\'><text>Description: ' . $desc . '</text></div>
                    </div>
                </div>';
        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
                    <div class=\'pure-g\' style="padding-left: 10px;">
                        <div class=\'pure-u-1\'>Bathroom: <span class="badge">' . $bathroom . '</span></b></div>
                        <div class=\'pure-u-1\'><text>Bedroom:<span class="badge">' . $bedroom . '</span></text></div>
                        <div class=\'pure-u-1\'><text>Basement:<span class="badge">' . $basement . '</span></text></div>
                        <div class=\'pure-u-1\'><text>Location:<span class="badge">' . $location . '</span></text></div>
                    </div>
                </div>';
//        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:120px; height:100px;">';
        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'>'//<center>
                . '<div id="googleMap">
                        <iframe class="iframe" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=' . $loco . '&key=AIzaSyDhPNo9ESa69mJXBUvzKdGMMMDdWx2wwA4">
                        </iframe>             
                    </div>'
//                . '</center>'
                . '</div>';
//$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
        $gallery_list .= '</div><hr>';
    }
}


if (isset($_POST['searchquery']) && $_POST['searchquery'] != "") {
    $searchquery = preg_replace('#[^a-z 0-9?!]#i', '', $_POST['searchquery']);
    $filter_category = "";
    if ($_POST['filter1'] == "all") {
        $sqlCommand = "SELECT * FROM posts WHERE MATCH (user, description, postName, location, postal, bedroom, bathroom, basement, address) AGAINST ('$searchquery') AND expired='0'";
        $filter_category = $_POST['filter1'];
    } else if ($_POST['filter1'] == "location") {
        $sqlCommand = "SELECT * FROM posts WHERE location LIKE '%$searchquery%' AND expired='0'"; //MATCH (location) AGAINST ('$searchquery' IN BOOLEAN MODE)";
        $filter_category = $_POST['filter1'];
    } else if ($_POST['filter1'] == "postname") {
        $sqlCommand = "SELECT * FROM posts WHERE postName LIKE '%$searchquery%' AND expired='0'";
        $filter_category = $_POST['filter1'];
    } else if ($_POST['filter1'] == "seller") {
        $sqlCommand = "SELECT * FROM posts WHERE user LIKE '%$searchquery%' AND expired='0'";
        $filter_category = $_POST['filter1'];
    }
//    include_once("mysql_connect.php");
    $line1 = '';
    $line2 = '';
    $paginationCtrls = '';
    $gallery_list = "";
    $query = mysqli_query($db_conx, $sqlCommand) or die(mysqli_error($db_conx));
    $count = mysqli_num_rows($query);

//    $rows = $count;
//
//    //this tells us the page number of our last page
//    $last = ceil($rows / $page_rows);
//// if there are no results then only show one page
//    if ($last < 1) {
//        $last = 1;
//    }
//// establish pagenum variable
//    $pagenum = 1;
//// get pagenum from URL vars if it is present, e;se it is =1
//    if (isset($_GET['pn'])) {
//        $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
//    }
//// ensure pagenum isnt below one or more than $last
//    if ($pagenum < 1) {
//        $pagenum = 1;
//    } else if ($pagenum > $last) {
//        $pagenum = $last;
//    }
//// set the range of rows to query for the chosen $pagenum
//    $limit = 'LIMIT ' . ($pagenum - 1) * $page_rows . ',' . $page_rows;
//    $gallery_list = "";
////    $sql = "SELECT * FROM users WHERE activated='1' AND userlevel='b' ORDER BY signup DESC $limit"; //, usr, filename FROM postpictures";
////    $query = mysqli_query($db_conx, $sql);
////    $line1 = "<p style='font-size: 18px;'>(<b>$rows</b>)<strong> Listings";
////    $line2 = "<p style='font-size: 18px;'>Page <b>$pagenum</b> of <b>$last</b></p>";
//// establish the pagination controls variable
//    $paginationCtrls = '';
//// if there is more than one page of results
//    if ($last != 1) {
//        $paginationCtrls = '<nav><ul class="pagination">';
//        // first check if were on page one if so then we dont need a link to prev page and if we only have
//        // one page then no forward link
//        if ($pagenum > 1) {
//            $previous = $pagenum - 1;
//            $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
//            // render clickable number links that should appearon the left of the target page number
//            for ($i = $pagenum - 4; $i < $pagenum; $i++) {
//                if ($i > 0) {
//                    $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '<span class="sr-only"></span></a></li>'; //(current) inbetween  & spans class="active" after li
//                }
//            }
//        }
//        // render target page number but without it being a link
//        $paginationCtrls .= '<li class="active"><a href="#">' . $pagenum . '<span class="sr-only">(current)</span></a></li>';
//        // render right of current page above
//        for ($i = $pagenum + 1; $i <= $last; $i++) {
//            $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '<span class="sr-only"></span></a></li>';
//            if ($i >= $pagenum + 4) {
//                // keep the pages from diplaying extra numbers past the page theyre on
//                break;
////$paginationCtrls .= '<li class="active"><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '<span class="sr-only"></span></a></li>'; //(current) inbetween spans
//            }
//        }
//        // this does the same as above only checking if we are on the last page and then generating the next...
//        if ($pagenum != $last) {
//            $next = $pagenum + 1;
//            $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $next . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
//        }
//        $paginationCtrls .= '</ul></nav>';
//    }

    if ($count > 0) {
        $search_output .= "<hr /><p style='font-size: 18px;'><span class='glyphicon glyphicon-search'></span> $count results for <strong><u><i style='color: blue;'>$searchquery</i></u></strong> in <strong><u><i style='color: blue;'>$filter_category</i></u> category</strong></p><hr />"; //$sqlCommand";
        while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//            $id = $row["id"];
//            $title = $row["title"];
//            $search_output .= "Item ID: $id - $title<br />";

            $id = $row["id"];
            $u = $row["user"];
            $pn = $row["postName"];
            $desc = $row["description"];
            $asking = $row["askingPrice"];
            $location = $row["location"];
            $address = $row["address"];
            $bedroom = $row["bedroom"];
            $bathroom = $row["bathroom"];
            $sqfootage = $row["sqfootage"];
            $basement = $row["basement"];
            $img = "";

            $loco = '';

            if ($address == '' || $address == NULL) {
                $loco = "%2C%20$location";
            } else {
//            $ad_row = mysqli_fetch_row($address_query);
//            $address = $ad_row[0];
                $loco = str_replace(' ', '%20', $address);
                $loco .= "%2C%20$location";
            }

            $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$id'");
            $countrow = mysqli_fetch_row($countquery);
            $count = $countrow[0];
            $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$id' ORDER BY RAND() LIMIT 1");
            $filerow = mysqli_fetch_row($filequery);
            $file = $filerow[0];



            $gallery_list .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '\'">'; // background-color: #B0E0E6\
            $gallery_list .= '<div class=\'pure-u-1  pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'><center><div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'"></center>'; // was md-2-5
            if ($filerow == NULL) {
                $gallery_list .= '<img class="pure-img" src="images/altposts.png" alt="cover photo">'; //id="thumbnail"
            } else {
                $gallery_list .= '<img class="pure-img" src="user/' . $u . '/' . $file . '" alt="cover photo">'; //style="width:160px; height:120px;"id="thumbnail"
            }
            $gallery_list .= '</div>';
            //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
            //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
            $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-2-5 pure-u-md-2-5 pure-u-lg-2-5\'>
                    <div class=\'pure-g\' style="padding-left: 10px;">
                    <div class=\'pure-u-1\'><b style="text-align: center;">' . $pn . '</b></div>
                        <div class=\'pure-u-1\'><b style="text-align: center;">Seller: ' . $u . '</b></div>
                        <div class=\'pure-u-1\'><text>Price:<span class="badge">$' . number_format($asking) . '</span></text></div>
                        <div class=\'pure-u-1\'><text>Square Footage: <span class="badge">' . $sqfootage . 'Sq Feet </span></text></div>
                        <div class=\'pure-u-1\'><text>Description: ' . $desc . '</text></div>
                    </div>
                </div>';
            $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
                    <div class=\'pure-g\' style="padding-left: 10px;">
                        <div class=\'pure-u-1\'>Bathroom: <span class="badge">' . $bathroom . '</span></b></div>
                        <div class=\'pure-u-1\'><text>Bedroom:<span class="badge">' . $bedroom . '</span></text></div>
                        <div class=\'pure-u-1\'><text>Basement:<span class="badge">' . $basement . '</span></text></div>
                        <div class=\'pure-u-1\'><text>Location:<span class="badge">' . $location . '</span></text></div>
                    </div>
                </div>';
//        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:120px; height:100px;">';
            $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'>'//<center>
                    . '<div id="googleMap">
                        <iframe class="iframe" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=' . $loco . '&key=AIzaSyDhPNo9ESa69mJXBUvzKdGMMMDdWx2wwA4">
                        </iframe>             
                    </div>'
//                . '</center>'
                    . '</div>';
//$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
            $gallery_list .= '</div><hr>';
        } // close while
    } else {
        $search_output = "<hr /><p style='font-size: 18px;'><b><u>0</u></b> results when searching for <strong>$searchquery</strong> under <strong>$filter_category</strong></p><hr />"; //$sqlCommand";
    }
}

//mysqli_close($db_conx);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title> Rewrapped - All Posts </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A layout example that shows off a responsive product landing page.">

        <!--<title>Landing Page &ndash; Layout Examples &ndash; Pure</title>-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <link rel="stylesheet" href="css/demo.css"/>
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css"/>
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css">
        <!--<![endif]-->
        <!-- end of old code-->

        <style>
            .iframe {
                display: table-cell;
                width: 100%; 
                height: 100%;
            }
            .pure-g > div {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .l-box {
                padding: 1em;
            }
            /*            .pure-g:hover {
                            background: yellow;
                        }*/

            @media screen and (min-width: 35.5em){
                #thumbnail{
                    width:30px; 
                    height:20px;
                }
            }

            @media screen and (min-width: 48em){
                #thumbnail{
                    width:120px; 
                    height:80px;
                }
            }
            @media screen and (min-width: 64em){
                #thumbnail{
                    width:160px; 
                    height:120px;
                }
            }
        </style>
    </head>
    <body>
        <?php include_once ("include/template_pageTop.php"); ?>
        <!--<a href="mysql_connect.php">Search</a>-->
        <div class="jumbotron">
            <div class="container">
                <h1>View All Posts</h1>
                <p>Rewrapped lets you list your house with ease, avoiding pesky posting and other listing related fees you may incur.  Once posted Real Estate agents may bid on your property based on their commission rate.  There are other cool features such as finding Open Houses near near you! Click below for more.</p>
                <p><a class="btn btn-primary btn-lg" href="info.php" role="button">Learn more&raquo;</a></p>
            </div>
        </div>
        <div class="container">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" class="pure-form" method="post" style="margin:auto;'">
                <fieldset>
                    <!--<legend>Search for posts: </legend>-->
                    <div style='padding:5px; background-color: #ffad60;'> <!-- #B0E0E6; -->
                        <div class="pure-u-1 pure-u-md-1-6">
                            <p class="navbar-text">Search for posts:</p>
                        </div>
                        <div class="pure-u-1 pure-u-md-2-3">
                            <label for="search">Key:</label>
                            <input name="searchquery" id="search" class="pure-input-rounded" type="text"> 
            <!--                <input  type="text" maxlength="88"> -->
                            <!--                        </div> 
                                                    <div class="pure-u-1 pure-u-md-1-3">-->
                            <label for="filter1">By: </label>
                            <select id="filter1" name="filter1" class="pure-input-1-2">
                                <option value="all">All</option>
                                <option value="location">Location</option>
                                <option value="postname">Post Name</option>
                                <option value="seller">Seller Name</option>
                            </select>
                        </div>
                        <button type="submit" class="pure-button pure-button-primary"><span class="glyphicon glyphicon-search"></span> Search</button>
                    </div>
                    <br>
                </fieldset>
            </form>
            <div>
                <?php echo $search_output; ?><br/>
                <?php echo $line1; ?><br/>
                <p style="text-align: center;"><?php echo $line2; ?></p>
            </div>
            <div style="text-align: center;">
                <legend><h2 class="content-head is-center" style="text-align: center;">All Posts</h2></legend>
                <?php echo $paginationCtrls; ?>
            </div>
            <hr>
            <?php echo $gallery_list; ?>
            <div style="text-align: center;">
                <?php echo $paginationCtrls; ?>
            </div>
        </div>
        <hr>
        <?php include_once ("include/template_pageBottom.php"); ?>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->


    </body>
</html>
