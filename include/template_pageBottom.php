<link rel="stylesheet" href="css/demo.css">
<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
<footer class="footer-distributed">
    <div class="footer-left">

        <h3>Re<span>wrapped</span></h3>

        <p class="footer-links">
            <a href="index.php">Home</a>
            ·
            <a href="allposts.php">Listings</a>
            ·
            <a href="">Pricing</a>
            ·
            <a href="aboutus.php">About</a>
            ·
            <a href="faq.php">Faq</a>
            ·
            <a href="contactus.php">Contact</a>
        </p>

        <p class="footer-company-name">Rewrapped &copy; 2015</p>
    </div>

    <div class="footer-center">

        <div>
            <i class="fa fa-map-marker"></i>
            <p><span>In the Greater Toronto Area</span> Ontario, Canada</p>
        </div>

        <div>
            <i class="fa fa-phone"></i>
            <p>+1 555 123456</p>
        </div>

        <div>
            <i class="fa fa-envelope"></i>
            <p><a href="mailto:support@company.com">support@Rewrapped.ca</a></p>
        </div>
    </div>

    <div class="footer-right">

        <p class="footer-company-about">
            <span>About Rewrapped</span>
            We are a bid based listing service where the agents come to you, no more shopping around.  Sit back, relax and bid on!
        </p>

        <div class="footer-icons">

            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-youtube"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>

        </div>

    </div>
</footer>