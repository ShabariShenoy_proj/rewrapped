<?php
// This file is www.developphp.com curriculum material
// Written by Adam Khoury January 01, 2011
// http://www.youtube.com/view_play_list?p=442E340A42191003
// Connect to the MySQL database  
include_once ("mysql_connect.php"); 

$tbl_products = "CREATE TABLE products (
		 		 id int(11) NOT NULL auto_increment,
				 product_name varchar(255) NOT NULL,
		 		 price varchar(16) NOT NULL,
				 details text NOT NULL,
				 category varchar(64) NOT NULL,
				 subcategory varchar(64) NOT NULL,
		 		 date_added date NOT NULL,
		 		 PRIMARY KEY (id),
		 		 UNIQUE KEY product_name (product_name)
		 		 ) ";
$query = mysqli_query($db_conx, $tbl_products);
if ($query === TRUE) {
	echo "<h3> table created OK :) </h3>"; 
} else {
	echo "<h3> table NOT created :( </h3>"; 
}
?>