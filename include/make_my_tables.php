<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once ("mysql_connect.php");

$tbl_posts = "CREATE TABLE IF NOT EXISTS posts ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                user VARCHAR(16) NOT NULL,
                gallery VARCHAR(16) NOT NULL,
		filename VARCHAR(255) NOT NULL,
                description TEXT NULL,
                uploaddate DATETIME NOT NULL,
                postName VARCHAR(255) NOT NULL,
                askingPrice VARCHAR(255) NOT NULL,
                location VARCHAR(255) NULL,
                postdate DATETIME NOT NULL,
                expiry DATETIME NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_posts);
if ($query === TRUE) {
    echo "<h3>posts table created OK :) </h3>";
} else {
    echo "<h3>photos table NOT created :( </h3>";
}

$tbl_users = "CREATE TABLE IF NOT EXISTS users (
              id INT(11) NOT NULL AUTO_INCREMENT,
			  username VARCHAR(16) NOT NULL,
			  email VARCHAR(255) NOT NULL,
			  password VARCHAR(255) NOT NULL,
			  gender ENUM('m','f') NOT NULL,
			  website VARCHAR(255) NULL,
			  country VARCHAR(255) NULL,
			  userlevel VARCHAR(255) NOT NULL,
			  avatar VARCHAR(255) NULL,
			  ip VARCHAR(255) NOT NULL,
			  signup DATETIME NOT NULL,
			  lastlogin DATETIME NOT NULL,
			  notescheck DATETIME NOT NULL,
			  activated ENUM('0','1') NOT NULL DEFAULT '0',
              PRIMARY KEY (id),
			  UNIQUE KEY username (username,email)
             )";
$query = mysqli_query($db_conx, $tbl_users);
if ($query === TRUE) {
    echo "<h3> User table created ok :) </h3>";
} else {
    echo "<h3> User table NOT created ok :( </h3>";
}
////////////////////////////////////
/* question and answer field for enhanced security for any kind of delete actions
 * provide user with a question and save old and new question and answer for any deleting article and stuff
 */
$tbl_useroptions = "CREATE TABLE IF NOT EXISTS useroptions ( 
                id INT(11) NOT NULL,
                username VARCHAR(16) NOT NULL,
				background VARCHAR(255) NOT NULL,
				question VARCHAR(255) NULL,
				answer VARCHAR(255) NULL,
                PRIMARY KEY (id),
                UNIQUE KEY username (username) 
                )";
$query = mysqli_query($db_conx, $tbl_useroptions);
if ($query === TRUE) {
    echo "<h3>useroptions table created OK :) </h3>";
} else {
    echo "<h3>useroptions table NOT created :( </h3>";
}
////////////////////////////////////
/* user 1 person requesting friendship from USER 2
 * when they accept take in the date and time of the field
 * change the accepted fiend from 0 - 1 to 'accepted' when user 2 presses confirm
 * 
 */
$tbl_friends = "CREATE TABLE IF NOT EXISTS friends ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                user1 VARCHAR(26) NOT NULL,
                user2 VARCHAR(26) NOT NULL,
                datemade DATETIME NOT NULL,
                accepted ENUM('0','1') NOT NULL DEFAULT '0',
                PRIMARY KEY (id)
                )";
$query = mysqli_query($db_conx, $tbl_friends);
if ($query === TRUE) {
    echo "<h3>friends table created OK :) </h3>";
} else {
    echo "<h3>friends table NOT created :( </h3>";
}
////////////////////////////////////
/* have the blocker's user name and the person being blocked the blockee
 * 
 * 
 */
$tbl_blockedusers = "CREATE TABLE IF NOT EXISTS blockedusers ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                blocker VARCHAR(26) NOT NULL,
                blockee VARCHAR(26) NOT NULL,
                blockdate DATETIME NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_blockedusers);
if ($query === TRUE) {
    echo "<h3>blockedusers table created OK :) </h3>";
} else {
    echo "<h3>blockedusers table NOT created :( </h3>";
}
////////////////////////////////////
/* more status in detail later
 * 
 * 
 */
$tbl_status = "CREATE TABLE IF NOT EXISTS status ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                osid INT(11) NOT NULL,
                account_name VARCHAR(16) NOT NULL,
                author VARCHAR(16) NOT NULL,
                type ENUM('a','b','c') NOT NULL,
                data TEXT NOT NULL,
                postdate DATETIME NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_status);
if ($query === TRUE) {
    echo "<h3>status table created OK :) </h3>";
} else {
    echo "<h3>status table NOT created :( </h3>";
}
////////////////////////////////////
/* how were going to store string data for all of the photos on the site
 * connect the photo's to useres using the USER field
 * 
 */
$tbl_photos = "CREATE TABLE IF NOT EXISTS photos ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                user VARCHAR(16) NOT NULL,
                gallery VARCHAR(16) NOT NULL,
		filename VARCHAR(255) NOT NULL,
                description VARCHAR(255) NULL,
                uploaddate DATETIME NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_photos);
if ($query === TRUE) {
    echo "<h3>photos table created OK :) </h3>";
} else {
    echo "<h3>photos table NOT created :( </h3>";
}
////////////////////////////////////
/* notify friend to inform other person's activity
 * 
 * 
 */
$tbl_notifications = "CREATE TABLE IF NOT EXISTS notifications ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                username VARCHAR(16) NOT NULL,
                initiator VARCHAR(16) NOT NULL,
                app VARCHAR(255) NOT NULL,
                note VARCHAR(255) NOT NULL,
                did_read ENUM('0','1') NOT NULL DEFAULT '0',
                date_time DATETIME NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_notifications);
if ($query === TRUE) {
    echo "<h3>notifications table created OK :) </h3>";
} else {
    echo "<h3>notifications table NOT created :( </h3>";
}


$tbl_viewcounter = "CREATE TABLE IF NOT EXISTS viewcounter ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                pagename VARCHAR(255) NOT NULL,
                views int(11) NOT NULL,
                user VARCHAR(50),
                postid int(10) NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_viewcounter);
if ($query === TRUE) {
    echo "<h3>view counter table created OK :) </h3>";
} else {
    echo "<h3>view counter table NOT created :( </h3>";
}

$tbl_postpictures = "CREATE TABLE IF NOT EXISTS postpictures (
  id int(11) NOT NULL,
  usr varchar(16) NOT NULL,
  gallery varchar(16) NOT NULL,
  filename varchar(255) NOT NULL,
  uploaddate datetime NOT NULL,
  pname varchar(255) NOT NULL,
  postid int(15) NOT NULL,
        PRIMARY KEY (id) 
                )";

$query = mysqli_query($db_conx, $tbl_postpictures);
if ($query === TRUE) {
    echo "<h3>postpictures table created OK :) </h3>";
} else {
    echo "<h3>postpictures table NOT created :( </h3>";
}
?>
