<?php
include_once ("mysql_connect.php");
$tbl_bidnotes = "CREATE TABLE IF NOT EXISTS bidnotes ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                agent VARCHAR(16) NOT NULL,
                owner VARCHAR(16) NOT NULL,
                app VARCHAR(255) NOT NULL,
                note VARCHAR(255) NOT NULL,
                did_read ENUM('0','1') NOT NULL DEFAULT '0',
                date_time DATETIME NOT NULL,
                postid int(11) NOT NULL,
                bidid int(11) NOT NULL,
                bid FLOAT NOT NULL,
                PRIMARY KEY (id) 
                )";
$query = mysqli_query($db_conx, $tbl_bidnotes);
if ($query === TRUE) {
    echo "<h3>bid notifications table created OK :) </h3>";
} else {
    echo "<h3>bid notifications table NOT created :( </h3>";
}

?>