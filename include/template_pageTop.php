<?php
// It is important for any file that includes this file, to have
// check_login_status.php included at its very top.
$envelope = ''; //<img src="images/note_dead.png" width="32" height="32" alt="Notes" title="Notifications for registered members">
$pm = '';
$loginLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text"><a href="login.php"><span class="glyphicon glyphicon-user"></span> Log In</a></p>';
$signupLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text"><a href="signup.php"><span class="glyphicon glyphicon-log-in"></span> Register</a></p>';
if (isset($user_ok) && $user_ok == true) {
    $sql = "SELECT notescheck FROM users WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    $notescheck = $row[0];
    $sql = "SELECT id FROM notifications WHERE username='$log_username' AND date_time > '$notescheck' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $numrows = mysqli_num_rows($query);

    $fsql = "SELECT * FROM friends WHERE user2='$log_username' AND accepted='0' AND datemade > '$notescheck' LIMIT 1";
    $fquery = mysqli_query($db_conx, $fsql);
    $fnumrows = mysqli_num_rows($fquery);

    $sql = "SELECT * FROM users WHERE username='$log_username' AND activated='1' LIMIT 1";
    $user_query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_array($user_query, MYSQLI_ASSOC);
    $userlevel = $row["userlevel"];
    $usid = $row["id"];
    if ($userlevel == "a") {
        $rsql = "SELECT id FROM bidnotes WHERE owner='$log_username' AND app='bid placed' AND date_time > '$notescheck'";//Limit 1
        $rquery = mysqli_query($db_conx, $rsql);
        $rnumrows = mysqli_num_rows($rquery);
    } else if ($userlevel == "b") {
        $rsql = "SELECT id FROM bidnotes WHERE agent='$log_username' AND app='info request' AND date_time > '$notescheck'";//Limit 1
        $rquery = mysqli_query($db_conx, $rsql);
        $rnumrows = mysqli_num_rows($rquery);
    }

    $asql = "SELECT * FROM private_messages WHERE to_id='$usid' AND time_sent > '$notescheck' AND opened='0'";
    $aquery = mysqli_query($db_conx, $asql);
    $anumrows = mysqli_num_rows($aquery);
    
    $num_note = $numrows + $fnumrows + $rnumrows;
    if ($numrows == 0 && $fnumrows == 0 && $rnumrows == 0) {
        $envelope = '<a href="notifications.php" title="Your notifications and friend requests" class="btn btn-primary" style="margin-top: 6px;" role="button">Notifications <span class="badge">' . $num_note . '</span></a>'; //<img src="images/note_still.png" width="32" height="32" alt="Notes"></a>';
    } else {
        $envelope = '<a href="notifications.php" title="Your notifications and friend requests" class="btn btn-primary" style="margin-top: 6px;" role="button">Notifications <span class="badge">' . $num_note . '</span></a>'; //'<a href="notifications.php" title="You have new notifications"><img src="images/red_bl.gif" width="32" height="32" alt="Notes"></a>';
    }
    //$anumrows
    if ($anumrows == 0) {
        $pm = '<a href="pm_inbox.php" title="Your private messages"><i class="fa fa-envelope-o fa-2x" style="margin-top: 6px;"><span class="badge bg-theme" role="button">'.$anumrows.'</span></i></a>'; //<img src="images/note_still.png" width="32" height="32" alt="Notes"></a>';
    } else {
        $pm = '<a href="pm_inbox.php" title="Your private messages"><i class="fa fa-envelope-o fa-2x" style="margin-top: 6px;"><span class="badge bg-theme" role="button">'.$anumrows.'</span></i></a>';
    }
//    $pm = '';
    $loginLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text">Signed in as <a href="user.php?u=' . $log_username . '"><span class="glyphicon glyphicon-user"></span> ' . $log_username . '</a></p>';
    $signupLink = '<p style="font-size: 16px; padding: 0%;" class="navbar-text"><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Log Out</a></p>';
}
?>
<?php
// AJAX CALLS THIS LOGIN CODE TO EXECUTE
if (isset($_POST["e"])) {
    // CONNECT TO THE DATABASE
    include_once("include/mysql_connect.php");
    // GATHER THE POSTED DATA INTO LOCAL VARIABLES AND SANITIZE
    $e = mysqli_real_escape_string($db_conx, $_POST['e']);
    $p = md5($_POST['p']);
    // GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
    // FORM DATA ERROR HANDLING
    if ($e == "" || $p == "") {
        echo "login_failed";
        exit();
    } else {
        // END FORM DATA ERROR HANDLING
        $sql = "SELECT id, username, password FROM users WHERE email='$e' AND activated='1' LIMIT 1";
        $query = mysqli_query($db_conx, $sql);
        $row = mysqli_fetch_row($query);
        $db_id = $row[0];
        $db_username = $row[1];
        $db_pass_str = $row[2];
        if ($p != $db_pass_str) {
            echo "login_failed";
            exit();
        } else {
            // CREATE THEIR SESSIONS AND COOKIES
            $_SESSION['userid'] = $db_id;
            $_SESSION['username'] = $db_username;
            $_SESSION['password'] = $db_pass_str;
            setcookie("id", $db_id, strtotime('+30 days'), "/", "", "", TRUE);
            setcookie("user", $db_username, strtotime('+30 days'), "/", "", "", TRUE);
            setcookie("pass", $db_pass_str, strtotime('+30 days'), "/", "", "", TRUE);
            // UPDATE THEIR "IP" AND "LASTLOGIN" FIELDS
            $sql = "UPDATE users SET ip='$ip', lastlogin=now() WHERE username='$db_username' LIMIT 1";
            $query = mysqli_query($db_conx, $sql);
            echo $db_username;
            exit();
        }
    }
    exit();
}
?>
<style>
    body {
        padding-top: 50px;
        padding-bottom: 20px;
    }
    a{
        color: deepPink;
        text-decoration: none;
        font-weight: bold;
    }
</style>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <p style="font-size: 18px; padding: 0%;"><img src="images/bb.png" style="width:35px; height:35px;">Rewrapped</p>
            </a>
            <!--            <a class="navbar-brand" href="index.php">
                            <a href="index.php"><img src="../images/home.jpg" alt="Rewrapped" /></a>
                        </a>-->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" role="form">
                <!--                <div class="form-group">
                                    <input type="text" placeholder="Email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="password" placeholder="Password" class="form-control">
                                </div>
                                <button type="submit" class="btn btn-success">Sign in</button>-->
                <div id="form-group">
                    <?php echo $pm; ?>
                </div>                
            </form>
            <form class="navbar-form navbar-right" role="form">
                <!--                <div class="form-group">
                                    <input type="text" placeholder="Email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="password" placeholder="Password" class="form-control">
                                </div>
                                <button type="submit" class="btn btn-success">Sign in</button>-->
                <div id="form-group">
                    <p style="font-size: 10px"><?php echo $envelope; ?></p>
                </div>                
            </form>
            <form class="navbar-form navbar-right" role="form">

                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Visit<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="dashboard.php">Dashboard</a></li>
                            <li><a href="allposts.php">All Posts</a></li>
                            <li><a href="allagents.php">All Agents</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="contactus.php">Contact Us</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="aboutus.php">About Us</a></li>
                        </ul>
                    </li>
                </ul>
            </form>
            <form class="navbar-form navbar-right" role="form">
                <div id="form-group">
                    <?php echo $loginLink; ?>
                </div>
            </form>
            <form class="navbar-form navbar-right" role="form">
                <div id="form-group">
                    <?php echo $signupLink; ?>
                </div>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>