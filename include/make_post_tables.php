<?php
include_once ("mysql_connect.php");

$tbl_postphotos = "CREATE TABLE IF NOT EXISTS postphotos ( 
                id INT(11) NOT NULL AUTO_INCREMENT,
                user VARCHAR(16) NOT NULL,
                gallery VARCHAR(16) NOT NULL,
		filename VARCHAR(255) NOT NULL,
                description VARCHAR(255) NULL,
                uploaddate DATETIME NOT NULL,
                postname VARCHAR(255) NOT NULL,
                askingPrice VARCHAR(255) NOT NULL,
                location VARCHAR(255) NULL,
                PRIMARY KEY (id) 
                )"; 
$query = mysqli_query($db_conx, $tbl_postphotos); 
if ($query === TRUE) {
	echo "<h3>postphotos table created OK :) </h3>"; 
} else {
	echo "<h3>postphotos table NOT created :( </h3>"; 
}
?>