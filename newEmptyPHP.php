<?php
/* Web Intersect Social Network Template System and CMS v1.34
 * Copyright (c) 2011 Adam Khoury
 * Licensed under the GNU General Public License version 3.0 (GPLv3)
 * http://www.webintersect.com/license.php
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 * See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: February 9, 2010
 * ------------------------------------------------------------------------------------------------ */
// Start_session, check if user is logged in or not, and connect to the database all in one included file
include_once("include/check_login_status.php");
?>
<?php
$type = "";
if (isset($_POST["type"])) {
    $type = $_POST["type"];
}
///////////End take away///////////////////////
// SQL to gather their entire PM list

$boxlist = "";
$my_id = "";
$u = "";
$my_uname = ""; // Put user's first name into a local variable
$sql = "SELECT id, username, avatar, firstname FROM users WHERE username='$log_username'";
$query = mysqli_query($db_conx, $sql);
$userlist = "";
while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    $my_id = $row["id"];
    $u = $row["username"];
    $avatar = $row["avatar"];
    $my_uname = $row["firstname"];
}
//if (!isset($_SESSION['idx'])) {
//echo  '<br /><br /><font color="#FF0000">Your session has timed out</font>
//<p><a href="pm_inbox.php.php">Please Click Here</a></p>';
//exit(); 
//}
//// Decode the Session IDX variable and extract the user's ID from it
//$decryptedID = base64_decode($_SESSION['idx']);
//$id_array = explode("p3h9xfn8sq03hs2234", $decryptedID);
//$my_uname = $_SESSION['username']; // Put user's first name into a local variable
// ------- ESTABLISH THE INTERACTION TOKEN ---------
$thisRandNum = rand(9999999999999, 999999999999999999);
$_SESSION['wipit'] = base64_encode($thisRandNum); // Will always overwrite itself each time this script runs
// ------- END ESTABLISH THE INTERACTION TOKEN ---------
?>
<?php
// Mailbox Parsing for deleting inbox messages
if (isset($_POST['deleteBtn'])) {
    foreach ($_POST as $key => $value) {
        $value = urlencode(stripslashes($value));
        if ($key != "deleteBtn") {
            $sql = mysqli_query($db_conx, "UPDATE private_messages SET recipientDelete='1', opened='1' WHERE id='$value' AND to_id='$my_id' LIMIT 1");
            // Check to see if sender also removed from sent box, then it is safe to remove completely from system
        }
    }
    header("location: pm_inbox.php");
}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A layout example that shows off a responsive email layout.">

        <title>Email &ndash; Layout Examples &ndash; Pure</title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

        <script language="javascript" type="text/javascript">
            function toggleChecks(field) {
                if (document.myform.toggleAll.checked == true) {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = true;
                    }
                } else {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = false;
                    }
                }

            }
            $(document).ready(function () {
                $(".toggle").click(function () {
                    if ($(this).next().is(":hidden")) {
                        $(".hiddenDiv").hide();
                        $(this).next().slideDown("fast");
                    } else {
                        $(this).next().hide();
                    }
                });
            });
            function markAsRead(msgID) {
                $.post("php_parsers/markAsRead.php", {messageid: msgID, ownerid:<?php echo $my_id; ?>}, function (data) {
                    $('#subj_line_' + msgID).addClass('msgRead');
                    // alert(data); // This line was just for testing returned data from the PHP file, it is not required for marking messages as read
                });
            }
            function toggleReplyBox(subject, sendername, senderid, recName, recID, replyWipit) {
                $("#subjectShow").text(subject);
                $("#recipientShow").text(recName);
                document.replyForm.pmSubject.value = subject;
                document.replyForm.pm_sender_name.value = sendername;
                document.replyForm.pmWipit.value = replyWipit;
                document.replyForm.pm_sender_id.value = senderid;
                document.replyForm.pm_rec_name.value = recName;
                document.replyForm.pm_rec_id.value = recID;
                document.replyForm.replyBtn.value = "Send reply to " + recName;
                if ($('#replyBox').is(":hidden")) {
                    $('#replyBox').fadeIn(1000);
                } else {
                    $('#replyBox').hide();
                }
            }
            function processReply() {

                var pmSubject = $("#pmSubject");
                var pmTextArea = $("#pmTextArea");
                var sendername = $("#pm_sender_name");
                var senderid = $("#pm_sender_id");
                var recName = $("#pm_rec_name");
                var recID = $("#pm_rec_id");
                var pm_wipit = $("#pmWipit");
                var url = "php_parsers/private_msg_parse.php";
                if (pmTextArea.val() == "") {
                    $("#PMStatus").text("Please type in your message.").show().fadeOut(6000);
                } else {
                    $("#pmFormProcessGif").show();
                    $.post(url, {subject: pmSubject.val(), message: pmTextArea.val(), senderName: sendername.val(), senderID: senderid.val(), rcpntName: recName.val(), rcpntID: recID.val(), thisWipit: pm_wipit.val()}, function (data) {
                        document.replyForm.pmTextArea.value = "";
                        $("#pmFormProcessGif").hide();
                        $('#replyBox').slideUp("fast");
                        $("#PMFinal").html("&nbsp; &nbsp;" + data).show().fadeOut(8000);
                    });
                }
            }
        </script>
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/email-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/email.css">
        <!--<![endif]-->

        <style type="text/css"> 
            .hiddenDiv{display:none}
            #pmFormProcessGif{display:none}
            .msgDefault {font-weight:bold;}
            .msgRead {font-weight:100;color:#666;}
        </style>
    </head>
    <body>
        <div id="layout" class="content pure-g">
            <div id="nav" class="pure-u">
                <a href="#" class="nav-menu-button">Menu</a>

                <div class="nav-inner">
                    <button class="primary-button pure-button">Compose</button>

                    <div class="pure-menu">
                        <ul class="pure-menu-list">
                            <li class="pure-menu-item"><a href="newEmptyPHP.php?" class="pure-menu-link">Inbox <span class="email-count">(2)</span></a></li>
                            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Sent</a></li>
                            <!--                            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Trash</a></li>
                                                        <li class="pure-menu-heading">Labels</li>
                                                        <li class="pure-menu-item"><a href="#" class="pure-menu-link"><span class="email-label-personal"></span>Personal</a></li>
                                                        <li class="pure-menu-item"><a href="#" class="pure-menu-link"><span class="email-label-work"></span>Work</a></li>
                                                        <li class="pure-menu-item"><a href="#" class="pure-menu-link"><span class="email-label-travel"></span>Travel</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>



            <div id="list" class="pure-u-1">
                <?php
                $pm_id = "";
                $sql = mysqli_query($db_conx, "SELECT * FROM private_messages WHERE to_id='$my_id' AND recipientDelete='0' ORDER BY id DESC LIMIT 100");
                while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
                    $pm_id = $row["id"];
                    $date = strftime("%b %d, %Y", strtotime($row['time_sent']));
                    if ($row['opened'] == "0") {
                        $textWeight = 'msgDefault';
                    } else {
                        $textWeight = 'msgRead';
                    }
                    $fr_id = $row['from_id'];
                    // SQL - Collect username for sender inside loop
                    $ret = mysqli_query($db_conx, "SELECT * FROM users WHERE id='$fr_id' LIMIT 1");
                    while ($raw = mysqli_fetch_array($ret, MYSQLI_ASSOC)) {
                        $Sid = $raw['id'];
                        $avatar = $raw["avatar"];
                        $Sname = $raw['firstname'];
                        $lname = $raw["lastname"];
                        $Suname = $raw["username"];

                        $boxlist .= '<div class="email-item email-item-selected pure-g" onclick="markAsRead(<?php echo $row[\'id\']; ?>)">
                                    <div class="pure-u">
                                    <input type="checkbox" name="cb<?php echo $row[\'id\'];?>" id="cb" value="<?php echo $row[\'id\']; ?>" />
                                    </div>
                                    <div class="pure-u">
                                        <a href="user.php?u=' . $Suname . '"><img class="email-avatar" alt="' . $Suname . '#x27s avatar" height="64" width="64" src="user/' . $Suname . '/' . $avatar . '"></a>
                                    </div>
                                    <div class="pure-u-3-5">
                                        <h5 class="email-name"><a href="user.php?u= \'.$Suname.\'">' . $Sname . '</a></h5>
                                        <h4 class="email-subject" id="subj_line_<?php echo $row[\'id\']; ?>" style="cursor:pointer;">stripslashes('.$row['subject'].')</h4>
                                        <h5 class="email-name">' . $date . '</h5>
                                    </div>
                                </div>';
                    }
                }
                ?>                
<!--<input name="toggleAll" id="toggleAll" type="checkbox" onclick="toggleChecks(document.myform.cb)" />-->
                <?php echo $boxlist; ?>
            </div>

            <div id="main" class="pure-u-1">
                <div class="email-content">
                    <div class="email-content-header pure-g">
                        <div class="pure-u-1-2">
                            <h1 class="email-content-title">Hello from Toronto</h1>
                            <p class="email-content-subtitle">
                                From <a>Tilo Mitra</a> at <span>3:56pm, April 3, 2012</span>
                            </p>
                        </div>

                        <div class="email-content-controls pure-u-1-2">
                            <button class="secondary-button pure-button">Reply</button>
                            <button class="secondary-button pure-button">Forward</button>
                            <button class="secondary-button pure-button">Move to</button>
                        </div>
                    </div>

                    <div class="email-content-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                        <p>
                            Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                            Aliquam ac feugiat dolor. Proin mattis massa sit amet enim iaculis tincidunt. Mauris tempor mi vitae sem aliquet pharetra. Fusce in dui purus, nec malesuada mauris. Curabitur ornare arcu quis mi blandit laoreet. Vivamus imperdiet fermentum mauris, ac posuere urna tempor at. Duis pellentesque justo ac sapien aliquet egestas. Morbi enim mi, porta eget ullamcorper at, pharetra id lorem.
                        </p>
                        <p>
                            Donec sagittis dolor ut quam pharetra pretium varius in nibh. Suspendisse potenti. Donec imperdiet, velit vel adipiscing bibendum, leo eros tristique augue, eu rutrum lacus sapien vel quam. Nam orci arcu, luctus quis vestibulum ut, ullamcorper ut enim. Morbi semper erat quis orci aliquet condimentum. Nam interdum mauris sed massa dignissim rhoncus.
                        </p>
                        <p>
                            Regards,<br>
                            Tilo
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <script src="http://yui.yahooapis.com/3.17.2/build/yui/yui-min.js"></script>

        <script>
            YUI().use('node-base', 'node-event-delegate', function (Y) {

                var menuButton = Y.one('.nav-menu-button'),
                        nav = Y.one('#nav');

                // Setting the active class name expands the menu vertically on small screens.
                menuButton.on('click', function (e) {
                    nav.toggleClass('active');
                });

                // Your application code goes here...

            });
        </script>






    </body>
</html>
