<?php
include_once("include/check_login_status.php");
// Make sure the _GET "u" is set, and sanitize it
$u = "";
//$p = "";
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE username='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else {
    header("location: index.php");
    exit();
}

$nameBar = "";

$sys = '';

$sql1 = "SELECT * FROM users WHERE username='$u'";
$user_query1 = mysqli_query($db_conx, $sql1);
$row = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
//$id = $row["id"];
//$fname = $row["firstname"];
//$lname = $row["lastname"];
$location = $row["location"];

$photo_form = "";
// Check to see if the viewer is the account owner
$isOwner = "no";
if ($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    $nameBar = "Your";
    $userlevel = $row["userlevel"];
    if ($userlevel == 'b') {
        $sys = 'php_parsers/photo_system_2.php';
    } else {
        $sys = 'php_parsers/photo_system_1.php';
    }
    $photo_form = '<div class="jumbotron">';
    $photo_form .= '<form role="form" class="form-horiozontal" id="photo_form" enctype="multipart/form-data" method="post" action="'.$sys.'">';//php_parsers/photo_system_1.php
    $photo_form .= '<h2>Hi ' . $u . ', Create a posting </h2>';
    $photo_form .= '<div class="form-group"><label for="postname" class="control-label col-sm-2">Title:</label><div class="col-sm-10"><input name="postname" type="text" id="postname" maxlength="88" class="form-control" style="width: 70%;"></div></div>';
    $photo_form .= '<div class="form-group"><label for="postal" class="control-label col-sm-2">Description:</label><div class="col-sm-10"><textarea rows="4" cols="50" id="description" name="description" placeholder="Write a small description..." required></textarea></div></div>';
    $photo_form .= '<div class="form-group"><label for="askingPrice" class="control-label col-sm-2">Asking Price: ($)</label><div class="col-sm-10"><input class="form-control" style="width: 20%;" name="askingPrice" type="text" id="askingPrice" onkeyup="restrict(\'askingPrice\')" maxlength="8"></div></div>';
//    $photo_form .= '<select name="askingPrice" required>';
//    $photo_form .= '<option value="100K">$100K</option>';
//    $photo_form .= '<option value="125K">125K</option>';
//    $photo_form .= '<option value="150K">150K</option>';
//    $photo_form .= '<option value="175K">175K</option>';
//    $photo_form .= '<option value="200K">200K</option>';
//    $photo_form .= '<option value="225K">225K</option>';
//    $photo_form .= '<option value="250K">250K</option>';
//    $photo_form .= '</select>';
//    $photo_form .= '<div class="form-group"><label for="sqfootage" class="control-label col-sm-2">Square Footage:</label><div class="col-sm-10"><input class="form-control" style="width: 20%;" name="sqfootage" type="text" id="sqfootage" maxlength="7" onkeyup="restrict(\'sqfootage\')"></div></div>';
    $photo_form .= '<div class="form-group"><label for="bedroom" class="control-label col-sm-2">Bedrooms:</label>';
    $photo_form .= '<div class="col-sm-10"><select id="bedoom" name="bedroom" required class="form-control col-sm-13" style="width: 25%;">';
    //$photo_form .= '<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example';
    $photo_form .= '<option value="1" class="col-sm-6">1 Bedroom</option>';
    $photo_form .= '<option value="1+1" class="col-sm-6">1 Bedroom + Den</option>';
    $photo_form .= '<option value="2" class="col-sm-6">2 Bedrooms</option>';
    $photo_form .= '<option value="2+1" class="col-sm-6">2 Bedrooms + Den</option>';
    $photo_form .= '<option value="3" class="col-sm-6">3 Bedrooms</option>';
    $photo_form .= '<option value="3+1" class="col-sm-6">3 Bedrooms + Den</option>';
    $photo_form .= '<option value="4" class="col-sm-6">4 Bedrooms</option>';
    $photo_form .= '</select></div></div>';

 /*   $photo_form .= '<div class="form-group"><label for="bathroom" class="control-label col-sm-2">Bathrooms:</label>';
    $photo_form .= '<div class="col-sm-10"><select id="bathroom" name="bathroom" required class="form-control col-sm-13" style="width: 25%;">';
    $photo_form .= '<option value="1" class="col-sm-6">1 Full Bathroom</option>';
    $photo_form .= '<option value="1+1" class="col-sm-6">1 Full Bathroom + Powder</option>';
    $photo_form .= '<option value="2" class="col-sm-6">2 Full Bathrooms</option>';
    $photo_form .= '<option value="2+1" class="col-sm-6">2 FullBathrooms + Powder</option>';
    $photo_form .= '<option value="3" class="col-sm-6">3 Full Bathrooms</option>';
    $photo_form .= '<option value="3+1" class="col-sm-6">3 Bathrooms + Powder</option>';
    $photo_form .= '<option value="3+" class="col-sm-6">3+ Bathrooms</option>';
    $photo_form .= '</select></div></div>';*/

    /*$photo_form .= '<div class="form-group"><label for="basement" class="control-label col-sm-2">Basement:</label>';
    $photo_form .= '<div class="col-sm-10"><select id="basement" name="basement" required class="form-control col-sm-13" style="width: 25%;">';
    $photo_form .= '<option value="finished">Finished</option>';
    $photo_form .= '<option value="unfinished">Unfinished</option>';
    $photo_form .= '<option value="framed">Framed</option>';
    $photo_form .= '<option value="walkout">Walkout</option>';
    $photo_form .= '<option value="none">No basement (appartment)</option>';
    $photo_form .= '</select></div></div>';*/

    $photo_form .= '<div class="form-group"><label for="address" class="control-label col-sm-2">Street Address:</label><div class="col-sm-10"><input class="form-control" style="width:20%;" name="address" type="text" id="address" maxlength="100"></div></div>';
    $photo_form .= '<div class="form-group"><label for="location" class="control-label col-sm-2">City:</label><div class="col-sm-10"><input class="form-control" style="width:20%;" name="location" value="'.$location.'" type="text" id="location" maxlength="88"></div></div>';
    $photo_form .= '<div class="form-group"><label for="postal" class="control-label col-sm-2">Postal Code:</label><div class="col-sm-10"><input class="form-control" style="width:20%;" name="postal" type="text" id="postal" maxlength="7"></div></div>';
    $photo_form .= '<div class="form-group"><label for="duration" class="control-label col-sm-2">Duration:</label>';
    $photo_form .= '<div class="col-sm-10"><select id="duration" name="duration" required class="form-control col-sm-13" style="width: 25%;">';
    $photo_form .= '<option value="7">1 Week</option>';
    $photo_form .= '<option value="10">10 Days</option>';
    $photo_form .= '<option value="14">2 Weeks</option>';
    $photo_form .= '</select></div></div>';
    $photo_form .= '<p><input class="btn btn-primary" type="submit" value="Save & Upload Photos"></p>';
    $photo_form .= '</form>';
    $photo_form .= '</div>';
} else {
    //Select the user galleries
    $nameBar = $u."'s";
    $gallery_list = "";
    $sql = "SELECT DISTINCT pname, postid FROM postpictures WHERE usr='$u'";
    $query = mysqli_query($db_conx, $sql);
    if (mysqli_num_rows($query) < 1) {
        $gallery_list = "This user has not uploaded any posts yet.";
    } else {
        while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
            $pn = $row["pname"];
            $pid = $row["postid"];
            $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$pid'");
            $countrow = mysqli_fetch_row($countquery);
            $count = $countrow[0];
            $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$pid' ORDER BY RAND() LIMIT 1");
            $filerow = mysqli_fetch_row($filequery);
            $file = $filerow[0];
            $otherquery = mysqli_query($db_conx, "SELECT location FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
            $otherrow = mysqli_fetch_row($otherquery);
            $loc = $otherrow[0];
            $postalquery = mysqli_query($db_conx, "SELECT postal FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
            $postalrow = mysqli_fetch_row($postalquery);
            $postal = $postalrow[0];
            $gallery_list .= '<div>';
            //$gallery_list .= '<div onclick="showGallery(\'' . $pn . '\',\'' . $u . '\')">';
            //&postn=<?php echo $p;?//>
            $gallery_list .= '<div onclick="window.location = \'postphotos.php?postn=' . $pn . '&u=' . $u . '&location=' . $loc . '&postal=' . $postal . '&postid=' . $pid . '\'">';
            if ($filerow == NULL) {
                $gallery_list .= '<img src="images/altposts.png" alt="cover photo">';
            } else {
                $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo">';
            }
//            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo">';
            $gallery_list .= '</div>';
            $gallery_list .= '<b>' . $pn . '</b> (' . $count . ')';
            $gallery_list .= '</div>';
        }
    }
}
//Select the user galleries
$gallery_list = "";
$sql = "SELECT DISTINCT pname, postid FROM postpictures WHERE usr='$u'";
$query = mysqli_query($db_conx, $sql);
if (mysqli_num_rows($query) < 1) {
    $gallery_list = "This user has not uploaded any posts yet.";
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $pn = $row["pname"];
        $pid = $row["postid"];
        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$pid'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];
        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$pid' ORDER BY RAND() LIMIT 1");
        $filerow = mysqli_fetch_row($filequery);
        $file = $filerow[0];
        $otherquery = mysqli_query($db_conx, "SELECT location FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
        $otherrow = mysqli_fetch_row($otherquery);
        $loc = $otherrow[0];
        $postalquery = mysqli_query($db_conx, "SELECT postal FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
        $postalrow = mysqli_fetch_row($postalquery);
        $postal = $postalrow[0];
        $gallery_list .= '<div class="1-box pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-5">';
        //$gallery_list .= '<div onclick="showGallery(\'' . $pn . '\',\'' . $u . '\')">';
        //&postn=<?php echo $p;?//>
        $gallery_list .= '<div class="image" style="margin-left: 30px;" onclick="window.location = \'postphotos.php?postn=' . $pn . '&u=' . $u . '&location=' . $loc . '&postal=' . $postal . '&postid=' . $pid . '\'">';
        if ($filerow == NULL) {
            $gallery_list .= '<img src="images/altposts.png" alt="cover photo">';
        } else {
            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:160px; height:120px;">';
        }
        $gallery_list .= '</div>';
        $gallery_list .= '<b style="display: inline-block; width: 180px; white-space:nowrap; margin-left: 28px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' . $pn . ' (' . $count . ')</b>';
        $gallery_list .= '</div>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>

        <title><?php echo $u; ?> Posts</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                background: #F1F0D1;
                font-family: Verdana, Tahoma, Arial, sans-serif;
                font-size: 18px;
                overflow: auto;
            }
            h1, h2, h3 {
                text-align: center;
                padding-left: 5%;
                color: #878E63;
            }
            p {
                padding: 2%;
                color: #878E63;
            }
            img {
                text-align: center;
                max-width: 100%;
                height: auto;
                width: auto;
            }
            #wrapper {
                margin: 0 auto;
                max-width: 1020px;
                width: 98%;
                background: #FEF8E8;
                border: 1px solid #878E63;
                border-radius: 2px;
                box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
            }
            #callout {
                width: 100%;
                height: auto;
                background: #878E63;
                overflow: hidden;
            }
            #callout p {
                text-align: right;
                font-size: 13px;
                padding: 0.1% 5px 0 0;
                color: #F1F0D1;
            }
            #callout p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            header {
                width: 96%;
                min-height: 125px;
                padding: 5px;
                text-align: center;
            }
            /*            nav ul {
                            list-style: none;
                            margin: 0;
                            padding-left: 50px;
                        }
                        nav ul li {
                            float: left;
                            border: 1px solid #878E63;
                            width: 15%;
                        }
                        nav ul li a {
                            background: #F1F0D1;
                            display: block;
                            padding: 5% 12%;
                            font-weight: bold;
                            font-size: 18px;
                            color: #878E63;
                            text-decoration: none;
                            text-align: center;
                        }
                        nav ul li a:hover, nav ul li.active a{
                            background-color: #878E63;
                            color: #F1F0D1;
                        }*/
            .iframe {
                width: 400px; 
                height: 300px;
            }
            .banner img {
                width: 100%;
                border-top: 1px solid #878E63;
                border-bottom: 1px solid #878E63;
            }
            .clearfix {
                clear: both;
            }
            .left-col {
                width: 55%;
                float: left;
                margin: -2% 1% 1% 1%; // top, right, bottom, left
            }
            .sidebar {
                width: 40%;
                float: right;
                margin: 1%;
                text-align: center;
            }
            .bidbar {
                width: 40%;
                //float: center;
                margin: 1%;
                text-align: center;
            }
            .therapy {
                float: left;
                margin: 0 auto;
                width: 100%;
                height: auto;
                padding: 1%;
            }
            .section {
                width: 29%;
                float: left;
                margin: 2% 2%;
                text-align: center;
            }
            .clock{
                margin-left: 30%;
                width: 40%;
                //float: center;
                //margin: 1%;
                text-align: center;
            }
            footer {
                background: #878E63;
                width: 100%;
                overflow: hidden;
            }
            footer p, footer h3 {
                color: #F1F0D1;
            }
            footer p a {
                color: #F1F0D1;
                text-decoration: none;
            }
            /*            ul{
                            list-style-type: none;
                            margin: 0;
                            padding: 0;
                        }
                        li{
                            display: inline;
                        }
                        ul li img{
                            height: 50px;
                        }*/
            /* adding media queries tags */
            @media screen and (max-width: 478px){
                body{
                    font-size: 13px;
                }

            }

            @media screen and (max-width: 740px){
                /*                nav {
                                    width: 100%;
                                    margin-bottom: 10px;
                                }
                                nav ul{
                                    list-style: none;
                                    margin: 0 auto;
                                    padding-left: 0;
                                }
                                nav ul li{
                                    text-align: center;
                                    margin-left: 0 auto;
                                    width: 100%;
                                    border-top: 1px solid #878E63;
                                    border-right: 0px solid #878E63;
                                    border-bottom: 1px solid #878E63;
                                    border-left: 0px solid #878E63;
                                }
                                nav ul li a{
                                    padding: 8px 0;
                                    font-size: 16px;
                                }*/
                .left-col{
                    width: 100%;
                }
                .sidebar{
                    width: 100%;
                }
                .section{
                    float: left;
                    width: 100%;
                    margin: 0;
                }
                td {
                    font-size: 10px;
                }
                th {
                    font-size: 12px;
                }
                .iframe {
                    width: 270px; 
                    height: 230px;
                }
                .clock{
                    margin-left: 20%;
                    //width: 20px;;
                    float: left;
                    //margin: 1%;
                    text-align: center;
                    width: 200px; 
                    text-align: center;
                }
            }
        </style></style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <style type="text/css">
        form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px;}
        div#galleries{}
        div#galleries > div{float:left; margin:20px; text-align:center; cursor:pointer;}
        div#galleries > div > div {height:100px; overflow:hidden;}
        div#galleries > div > div > img{width:150px; cursor:pointer;}
        div#photos{display:none; border:#666 1px solid; padding:20px;}
        div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
        div#photos > div > img{width:125px; cursor:pointer;}
        div#picbox{display:none; padding-top:36px;}
        div#picbox > img{max-width:800px; display:block; margin:0px auto;}
        div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
    </style>
    <script src="js/main.js"></script>
    <script src="js/ajax.js"></script>
    <link rel="stylesheet" href="style/style.css">
    <script>
        function restrict(elem) {
            var tf = _(elem);
            var rx = new RegExp;
            if (elem == "sqfootage") {
                rx = /[^0-9]/gi;
            } else if (elem == "askingPrice") {
                rx = /[^0-9]/gi;
            }
            tf.value = tf.value.replace(rx, "");
        }
        function showGallery(gallery, user) {
            _("galleries").style.display = "none";
            _("section_title").innerHTML = user + '&#39;s ' + gallery + ' Gallery &nbsp; <button onclick="backToGalleries()">Go back to all galleries</button>';
            _("photos").style.display = "block";
            _("photos").innerHTML = 'loading photos ...';
            var ajax = ajaxObj("POST", "php_parsers/photo_system_1.php");
            ajax.onreadystatechange = function () {
                if (ajaxReturn(ajax) == true) {
                    _("photos").innerHTML = '';
                    var pics = ajax.responseText.split("|||");
                    for (var i = 0; i < pics.length; i++) {
                        var pic = pics[i].split("|");
                        _("photos").innerHTML += '<div><img onclick="photoShowcase(\'' + pics[i] + '\')" src="user/' + user + '/' + pic[1] + '" alt="pic"><div>';
                    }
                    _("photos").innerHTML += '<p style="clear:left;"></p>';
                }
            }
            ajax.send("show=galpics&gallery=" + gallery + "&user=" + user);
        }
        function backToGalleries() {
            _("photos").style.display = "none";
            _("section_title").innerHTML = "<?php echo $u; ?>&#39;s Photo Galleries";
            _("galleries").style.display = "block";
        }
        function photoShowcase(picdata) {
            var data = picdata.split("|");
            _("section_title").style.display = "none";
            _("photos").style.display = "none";
            _("picbox").style.display = "block";
            _("picbox").innerHTML = '<button onclick="closePhoto()">x</button>';
            _("picbox").innerHTML += '<img src="user/<?php echo $u; ?>/' + data[1] + '" alt="photo">';
            if ("<?php echo $isOwner ?>" == "yes") {
                _("picbox").innerHTML += '<p id="deletelink"><a href="#" onclick="return false;" onmousedown="deletePhoto(\'' + data[0] + '\')">Delete this Photo <?php echo $u; ?></a></p>';
            }
        }
        function closePhoto() {
            _("picbox").innerHTML = '';
            _("picbox").style.display = "none";
            _("photos").style.display = "block";
            _("section_title").style.display = "block";
        }
        function deletePhoto(id) {
            var conf = confirm("Press OK to confirm the delete action on this photo.");
            if (conf != true) {
                return false;
            }
            _("deletelink").style.visibility = "hidden";
            var ajax = ajaxObj("POST", "php_parsers/photo_system_1.php");
            ajax.onreadystatechange = function () {
                if (ajaxReturn(ajax) == true) {
                    if (ajax.responseText == "deleted_ok") {
                        alert("This picture has been deleted successfully. We will now refresh the page for you.");
                        window.location = "post.php?u=<?php echo $u; ?>";
                    }
                }
            }
            ajax.send("delete=photo&id=" + id);
        }
    </script>
</head>
<body>
    <?php include_once("include/template_pageTop.php"); ?>
    <div id="wrapper">
        <div id="photo_form"><?php echo $photo_form; ?></div>
        <h2 id="section_title" style="text-align: center;">All of <?php echo $nameBar; ?> Posts</h2>
        <div id="galleries" style="text-align: center;"><?php echo $gallery_list; ?></div>
        <div id="photos" style="text-align: center;"></div>
        <div id="picbox" style="text-align: center;"></div>
        <p style="clear:left;">These photos belong to <a href="user.php?u=<?php echo $u; ?>"><?php echo $u; ?></a></p>
        <!--  <form action="$" class="dropzone" id="avatar-dropzone">
              <input
              
          </form>
        -->
    </div>
    <?php include_once("include/template_pageBottom.php"); ?>
    <!-- start from twitter bootstrap-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

<!--        <script src="js/main.js"></script>-->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- end of bootstrap -->
</body>
</html>