<?php
session_start();
// If user is logged in, header them away
if (isset($_SESSION["username"])) {
    header("location: message.php?msg=You are already logged in... Continue browsing");
    exit();
}
?><?php
// Ajax calls this NAME CHECK code to execute
if (isset($_POST["usernamecheck"])) {
    include_once("include/mysql_connect.php");
    $username = preg_replace('#[^a-z0-9]#i', '', $_POST['usernamecheck']);
    $sql = "SELECT id FROM users WHERE username='$username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $uname_check = mysqli_num_rows($query);
    if (strlen($username) < 3 || strlen($username) > 16) {
        echo '<strong style="color:#F00;">3 - 16 characters please</strong>';
        exit();
    }
    if (is_numeric($username[0])) {
        echo '<strong style="color:#F00;">Usernames must begin with a letter</strong>';
        exit();
    }
    if ($uname_check < 1) {
        echo '<strong style="color:#009900;">' . $username . ' is OK</strong> <span class="glyphicon glyphicon-ok"></span>';
        exit();
    } else {
        echo '<strong style="color:#F00;">' . $username . ' is taken</strong> <span class="glyphicon glyphicon-remove"></span>';
        exit();
    }
}
?>
<?php
// Ajax calls this NAME CHECK code to execute
if (isset($_POST["emailcheck"])) {
    include_once("include/mysql_connect.php");
    $email = preg_replace('#[^a-z0-9_@.]#i', '', $_POST['emailcheck']); //'#[^a-z0-9]#i', '',   -->this was inside the preg_replace() around the whole thing
    $sql = "SELECT id FROM users WHERE email='$email' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $email_check = mysqli_num_rows($query);
    if (strlen($email) < 3 || strlen($email) > 100) {
        echo '<strong style="color:#F00;">3 - 100 characters please</strong>';
        exit();
    }
    if (is_numeric($email[0])) {
        echo '<strong style="color:#F00;">Usernames must begin with a letter</strong>';
        exit();
    }
    if ($email_check < 1) {
        echo '<strong style="color:#009900;">' . $email . ' is OK</strong> <span class="glyphicon glyphicon-ok"></span>';
        exit();
    } else {
        echo '</strong> <span class="glyphicon glyphicon-remove"></span><strong style="color:#F00;"><u>' . $email . '</u> already exists go to the <a href="forgot_pass.php">forgot password</a> page to recover your password';
        exit();
    }
}
?>
<?php
// Ajax calls this REGISTRATION code to execute
if (isset($_POST["u"])) {
    // CONNECT TO THE DATABASE
    include_once("include/mysql_connect.php");
    // GATHER THE POSTED DATA INTO LOCAL VARIABLES
    //$fn = preg_replace('#[^a-zA-Z]#', '', $_POST['fn']);
    //$ln = preg_replace('#[^a-zA-Z]#', '', $_POST['ln']);
    $u = preg_replace('#[^a-z0-9]#i', '', $_POST['u']);
    $e = mysqli_real_escape_string($db_conx, $_POST['e']);
    $p = $_POST['p'];
    //$g = preg_replace('#[^a-z]#', '', $_POST['g']);
    $c = preg_replace('#[^a-z ]#i', '', $_POST['c']);
    $s = preg_replace('#[^a-z ]#i', '', $_POST['s']);
    $city = preg_replace('#[^a-z ]#i', '', $_POST['city']);
    //$t = preg_replace('#[^a-z]#', '', $_POST['t']);
    // GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
    // DUPLICATE DATA CHECKS FOR USERNAME AND EMAIL
    $sql = "SELECT id FROM users WHERE username='$u' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $u_check = mysqli_num_rows($query);
    // -------------------------------------------
    $sql = "SELECT id FROM users WHERE email='$e' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $e_check = mysqli_num_rows($query);
    // FORM DATA ERROR HANDLING
    if ($u == "" || $e == "" || $p == "" || $c == "") {
        echo "The form submission is missing values.";
        exit();
    } else if ($u_check > 0) {
        echo "The username you entered is alreay taken";
        exit();
    } else if ($e_check > 0) {
        echo "That email address is already in use in the system";
        exit();
    } else if (strlen($u) < 3 || strlen($u) > 16) {
        echo "Username must be between 3 and 16 characters";
        exit();
    } else if (is_numeric($u[0])) {
        echo 'Username cannot begin with a number';
        exit();
    } else {
        // END FORM DATA ERROR HANDLING
        // Begin Insertion of data into the database
        // Hash the password and apply your own mysterious unique salt
        // the below hash is not suggested so read about hash() from php,com
        $p_hash = md5($p);
        //original hashing before the above change
//		$cryptpass = crypt($p);
//		include_once ("randStrGen.php");
//		$p_hash = randStrGen(20)."$cryptpass".randStrGen(20);
        // Add user info into the database table for the main site table
        $sql = "INSERT INTO users (username, email, password, country, state, ip, signup, lastlogin, notescheck, location)       
		        VALUES('$u','$e','$p_hash','$c','$s','$ip',now(),now(),now(), '$city')"; //, '$fn','$ln'
        $query = mysqli_query($db_conx, $sql);
        $uid = mysqli_insert_id($db_conx);
        /*if ($t == 'b') {
            $sql2 = "UPDATE users SET bidlimit='10' WHERE id='$uid' LIMIT 1";
            $query2 = mysqli_query($db_conx, $sql2);
        }*/
        // Establish their row in the useroptions table
        $sql = "INSERT INTO useroptions (id, username, background) VALUES ('$uid','$u','original')";
        $query = mysqli_query($db_conx, $sql);
        // Create directory(folder) to hold each user's files(pics, MP3s, etc.)
        if (!file_exists("user/$u")) {
            mkdir("user/$u", 0755);
        }
        // Email the user their activation link
        $to = "$e";
        $from = "auto_responder@Rewrapped.ca"; //need to change this to a hosting email change
        $subject = 'Rewrapped Account Activation';
        $message = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Rewrapped Message</title></head><body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;"><div style="padding:10px; background:#333; font-size:24px; color:#CCC;"><a href="http://www.Rewrapped.ca/"><img src="http://www.Rewrapped.ca/images/altposts.png" width="36" height="30" alt="Rewrapped" style="border:none; float:left;"></a>Rewrapped Account Activation</div><div style="padding:24px; font-size:17px;">Hello ' . $u . ',<br /><br />Click the link below to activate your account when ready:<br /><br /><a href="http://www.Rewrapped.ca/activation.php?id=' . $uid . '&u=' . $u . '&e=' . $e . '&p=' . $p_hash . '">Click here to activate your account now</a><br /><br />Login after successful activation using your:<br />* E-mail Address: <b>' . $e . '</b></div></body></html>';
        $headers = "From: $from\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
        if (mail($to, $subject, $message, $headers)) {
            echo "signup_success";
            exit();
        } else {
            echo "signup_failed";
            exit();
        }
    }
    exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Sign Up</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <!--<link rel="stylesheet" href="style/style.css">-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <link rel = "icon" href = "images/altposts.png" type = "image/x-icon">
        <link rel = "stylesheet" href = "css/bootstrap.min.css" />
        <link rel = "stylesheet" href = "css/bootstrap-theme.min.css" />

        <link rel = "stylesheet" href = "http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src = "js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <!--<link rel="stylesheet" href="css/layouts/marketing.css" />-->
        <!--<![endif]-->
        <!--<link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />-->
        <!--        <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
        <link rel="stylesheet" href="css/main.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <!-- starting of old code-->
        <link rel="stylesheet" href="style/style.css">
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script src="js/masonry.pkgd.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.0/masonry.pkgd.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/signup.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <style type="text/css">
            /*            #signupform{
                            margin-top:24px;	
                        }
                        #signupform > div {
                            margin-top: 12px;	
                        }
                        #signupform > input,select {
                            width: 200px;
                            padding: 3px;
                            background: #F3F9DD;
                        }
                        #signupbtn {
                            font-size:18px;
                            padding: 12px;
                        }*/
            #terms {
                border:#CCC 1px solid;
                background: #F5F5F5;
                padding: 12px;
            }
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script>
            function restrict(elem) {
                var tf = _(elem);
                var rx = new RegExp;
                if (elem == "email") {
                    rx = /[' "]/gi;
                } else if (elem == "username") {
                    rx = /[^a-z0-9]/gi;
                } else if (elem == "firstname") {
                    rx = /[^a-zA-Z]/gi;
                } else if (elem == "lastname") {
                    rx = /[^a-zA-Z]/gi;
                }
                tf.value = tf.value.replace(rx, "");
            }
            function emptyElement(x) {
                _(x).innerHTML = "";
            }
            function checkusername() {
                var u = _("username").value;
                if (u != "") {
                    //can put the image tag for an animated waiting gif here under neath where the ... is                     _("unamestatus").innerHTML = '<img src="images/ajax-loader.gif" alt="..loading">checking for duplicates...';
                    var ajax = ajaxObj("POST", "signup.php");
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            _("unamestatus").innerHTML = ajax.responseText;
                        }
                    }
                    ajax.send("usernamecheck=" + u);
                }
            }
            function checkemail() {
                var e = _("email").value;
                if (e != "") {
                    //can put the image tag for an animated waiting gif here under neath where the ... is
                    _("emailstatus").innerHTML = '<img src="images/ajax-loader.gif" alt="..loading">checking for duplicates...';
                    var ajax = ajaxObj("POST", "signup.php");
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            _("emailstatus").innerHTML = ajax.responseText;
                        }
                    }
                    ajax.send("emailcheck=" + e);
                }
            }
            function signup() {
                ///var fn = _("firstname").value;
                //var ln = _("lastname").value;
                var u = _("username").value;
                var e = _("email").value;
                var p1 = _("pass1").value;
                var p2 = _("pass2").value;
                var c = _("country").value;
                var s = _("state").value;
                var city = _("city").value;
                //var g = _("gender").value;
                //var t = _("userlevel").value;
                var status = _("status");
                //if (fn == "" || ln == "" || s == "" || city == "" || u == "" || e == "" || p1 == "" || p2 == "" || c == "" || g == "" || t == "") {
                if (s == "" || city == "" || u == "" || e == "" || p1 == "" || p2 == "" || c == "") {
                    status.innerHTML = "Fill out all of the form data";
                } else if (p1 != p2) {
                    status.innerHTML = "Your password fields do not match";
                } else if (_("terms").style.display == "none") {
                    status.innerHTML = "Please view the terms of use";
                } else {
                    _("signupbtn").style.display = "none";
                    status.innerHTML = 'please wait ...';
                    var ajax = ajaxObj("POST", "signup.php");
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText != "signup_success") {
                                status.innerHTML = ajax.responseText;
                                _("signupbtn").style.display = "block";
                            } else {
                                window.scrollTo(0, 0);
                                _("signupform").innerHTML = "<div class='container'><p id='note'> OK "+ u + ", check your email inbox and junk mail box at <u>" + e + "</u> in a moment to complete the sign up process by activating your account. You will not be able to do anything on the site until you successfully activate your account.</p></div>";
                            }
                        }
                    }
                    ajax.send("&u=" + u + "&e=" + e + "&p=" + p1 + "&c=" + c + "&s=" + s + "&city=" + city);//"fn=" + fn + "&ln=" + ln + 
                }
            }
            function openTerms() {
                _("terms").style.display = "block";
                emptyElement("status");
            }


            // Countries
            var country_arr = new Array("Canada");//, "USA");

// States
            var s_a = new Array();
            s_a[0] = "";
            s_a[1] = "Ontario|Alberta|British Columbia|Manitoba|New Brunswick|Newfoundland|Northwest Territories|Nova Scotia|Nunavut|Prince Edward Island|Quebec|Saskatchewan|Yukon Territory";
            //s_a[2] = "Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|District of Columbia|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming";
//           

            var s_c = new Array();
            s_c[0] = "";
            s_c[1] = "Scarborough|Toronto|Mississauga|Brampton|Etobicoke|Ajax|Halton|Peterborough|Atikokan|Halton Hills|Pickering|Barrie|Hamilton|Port Bruce|Belleville|Hamilton-Wentworth|Port Burwell|Blandford-Blenheim|Hearst|Port Colborne|Blind River|Huntsville|Port Hope|Ingersoll|Prince Edward|Brant|James|Quinte West|Brantford|Kanata|Renfrew|Brock|Kincardine|Richmond Hill|Brockville|King|Sarnia|Burlington|Kingston|Sault Ste. Marie|Caledon|Kirkland Lake|Cambridge|Kitchener|Scugog|Chatham-Kent|Larder Lake|Souix Lookout CoC Sioux Lookout|Chesterville|Leamington|Smiths Falls|Clarington|Lennox-Addington|South-West Oxford|Cobourg|Lincoln|St. Catharines|Cochrane|Lindsay|St. Thomas|Collingwood|London|Stoney Creek|Cornwall|Loyalist Township|Stratford|Cumberland|Markham|Sudbury|Deep River|Metro Toronto|Temagami|Dundas|Merrickville|Thorold|Durham|Milton|Thunder Bay|Dymond|Nepean|Tillsonburg|Ear Falls|Newmarket|Timmins|East Gwillimbury|Niagara|East Zorra-Tavistock|Niagara Falls|Uxbridge|Elgin|Niagara-on-the-Lake|Vaughan|Elliot Lake|North Bay|Wainfleet|Flamborough|North Dorchester|Wasaga Beach|Fort Erie|North Dumfries|Waterloo|Fort Frances|North York|Waterloo|Gananoque|Norwich|Welland|Georgina|Oakville|Wellesley|Glanbrook|Orangeville|West Carleton|Gloucester|Orillia|West Lincoln|Goulbourn|Osgoode|Whitby|Gravenhurst|Oshawa|Wilmot|Grimsby|Ottawa|Windsor|Guelph|Ottawa-Carleton|Woolwich|Haldimand-Norfork|Owen Sound|York";
            //s_c[2] = "Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|District of Columbia|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming";

//            array("Ontario" = > array(
//                    "Ajax|Halton|Peterborough|Atikokan|Halton Hills|Pickering|Barrie|Hamilton|Port Bruce|Belleville|Hamilton-Wentworth|Port Burwell|Blandford-Blenheim|Hearst|Port Colborne|Blind River|Huntsville|Port Hope|Brampton|Ingersoll|Prince Edward|Brant|James|Quinte West|Brantford|Kanata|Renfrew|Brock|Kincardine|Richmond Hill|Brockville|King|Sarnia|Burlington|Kingston|Sault Ste. Marie|Caledon|Kirkland Lake|Scarborough|Cambridge|Kitchener|Scugog|Chatham-Kent|Larder Lake|Souix Lookout CoC Sioux Lookout|Chesterville|Leamington|Smiths Falls|Clarington|Lennox-Addington|South-West Oxford|Cobourg|Lincoln|St. Catharines|Cochrane|Lindsay|St. Thomas|Collingwood|London|Stoney Creek|Cornwall|Loyalist Township|Stratford|Cumberland|Markham|Sudbury|Deep River|Metro Toronto|Temagami|Dundas|Merrickville|Thorold|Durham|Milton|Thunder Bay|Dymond|Nepean|Tillsonburg|Ear Falls|Newmarket|Timmins|East Gwillimbury|Niagara|Toronto|East Zorra-Tavistock|Niagara Falls|Uxbridge|Elgin|Niagara-on-the-Lake|Vaughan|Elliot Lake|North Bay|Wainfleet|Flamborough|North Dorchester|Wasaga Beach|Fort Erie|North Dumfries|Waterloo|Fort Frances|North York|Waterloo|Gananoque|Norwich|Welland|Georgina|Oakville|Wellesley|Glanbrook|Orangeville|West Carleton|Gloucester|Orillia|West Lincoln|Goulbourn|Osgoode|Whitby|Gravenhurst|Oshawa|Wilmot|Grimsby|Ottawa|Windsor|Guelph|Ottawa-Carleton|Woolwich|Haldimand-Norfork|Owen Sound|York"
//                    ));

            function populateCities(stateElementId, cityElementId) {

                var selectedStateIndex = document.getElementById(stateElementId).selectedIndex;

                var cityElement = document.getElementById(cityElementId);

                cityElement.length = 0; // Fixed by Julian Woods
                cityElement.options[0] = new Option('Select city', '');
                cityElement.selectedIndex = 0;

                var state_arr = s_c[selectedStateIndex].split("|");

                for (var i = 0; i < state_arr.length; i++) {
                    cityElement.options[cityElement.length] = new Option(state_arr[i], state_arr[i]);
                }
            }
            function populateStates(countryElementId, stateElementId, cityElementId) {

                var selectedCountryIndex = document.getElementById(countryElementId).selectedIndex;

                var stateElement = document.getElementById(stateElementId);

                stateElement.length = 0; // Fixed by Julian Woods
                stateElement.options[0] = new Option('Select State/Province', '');
                stateElement.selectedIndex = 0;

                var state_arr = s_a[selectedCountryIndex].split("|");

                for (var i = 0; i < state_arr.length; i++) {
                    stateElement.options[stateElement.length] = new Option(state_arr[i], state_arr[i]);
                }
                if (cityElementId) {
                    stateElement.onchange = function () {
                        populateCities(stateElementId, cityElementId);
                    };
                }
            }

            function populateCountries(countryElementId, stateElementId, cityElementId) {
                // given the id of the <select> tag as function argument, it inserts <option> tags
                var countryElement = document.getElementById(countryElementId);
                countryElement.length = 0;
                countryElement.options[0] = new Option('Select Country', '-1');
                countryElement.selectedIndex = 0;
                for (var i = 0; i < country_arr.length; i++) {
                    countryElement.options[countryElement.length] = new Option(country_arr[i], country_arr[i]);
                }

                // Assigned all countries. Now assign event listener for the states.

                if (stateElementId) {
                    countryElement.onchange = function () {
                        populateStates(countryElementId, stateElementId, cityElementId);
                    };
                }
            }



            /* function addEvents(){
             _("elemID").addEventListener("click", func, false);
             }
             window.onload = addEvents; */
        </script>
    </head>
    <body>
        <?php include_once("include/template_pageTop.php"); ?>
        <img src="images/bg3.jpg" id="bg2" alt="">
        <!-- there was the header nav bar file included here used to work but for whatever reason this doesnt work with it now ...
         i left a space between the ? and php on purpose or else it wont comment out</? php include_once("include/template_pageTop.php"); ?>-->
        <!--<div class="container">-->
        <div id="pageMiddle" class="container">
            <form name="signupform" id="signupform" onsubmit="return false;">
                <h1 id="pageHeader2"><span><i>Sign Up</i></span></h1>
                <div id="bbbook"><i class="fa-bbbook">Rewrapped</i><div id="connect"><a href="index.php">Go To Rewrapped Home</a></div></div>
                <div id="mainlogin2">
                    <!--<div>Firstname</div>
                    <input id="firstname" type="text" onkeyup="restrict('firstname')" maxlength="40">
                    <span id="firstnamestatus"></span>
                    <div>Lastname</div>
                    <input id="lastname" type="text" onkeyup="restrict('lastname')" maxlength="40">
                    <span id="lastnamestatus"></span>
                    --><div>Username</div>
                    <input id="username" type="text" onblur="checkusername()" onkeyup="restrict('username')" maxlength="16">
                    <span id="unamestatus"></span>
                    <div>Email Address</div>
                    <input id="email" type="text" onblur="checkemail()" onfocus="emptyElement('status')" onkeyup="restrict('email')" maxlength="88">
                    <span id="emailstatus"></span>
                    <div>Create Password</div>
                    <input id="pass1" type="password" onfocus="emptyElement('status')" maxlength="100">
                    <div>Confirm Password</div>
                    <input id="pass2" type="password" onfocus="emptyElement('status')" maxlength="100">
                    <!--<div>Gender</div>
                    <select id="gender" onfocus="emptyElement('status')">
                        <option value=""></option>
                        <option value="m">Male</option>
                        <option value="f">Female</option>
                    </select>-->
                    <!--<div>Are you a Real Estate Agent or Home owner</div>
                    <select id="userlevel" onfocus="emptyElement('status')">
                        <option value=""></option>
                        <option value="a">Home Owner</option>
                        <option value="b">Real Estate Agent</option>
                        <option value="c">Seller</option>
                    </select>-->
                    <div>Country</div>
                    <select id="country" name="country" onfocus="emptyElement('status')"></select>
                    <select id="state" name="state" onfocus="emptyElement('status')"></select>
                    <select id="city" name="city" onfocus="emptyElement('status')"></select>
                    <script language="javascript">
                        populateCountries("country", "state", "city");
//                        populateCountries("country2");
                    </script>
                    <div>
                        <a href="#" onclick="return false" onmousedown="openTerms()">
                            View the Terms Of Use
                        </a>
                    </div>
                    <div id="terms" style="display:none;">
                        <h3>BidStar Terms Of Use</h3>
                        <p>1. Play nice here.</p>
                        <p>2. Take a bath before you visit.</p>
                        <p>3. Brush your teeth before bed.</p>
                    </div>
                    <br /><br />
                    <button id="signupbtn" onclick="signup()">Create Account</button>
                    <span id="status"></span>
                </div>
            </form>
            <div id="signupform2" style="background:#333;"></div>
        </div>
        <!--</div>-->
                    <!--<footer><?php include_once("include/template_pageBottom.php"); ?></footer>-->

        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                        (function (b, o, i, l, e, r) {
                            b.GoogleAnalyticsObject = l;
                            b[l] || (b[l] =
                                    function () {
                                        (b[l].q = b[l].q || []).push(arguments)
                                    });
                            b[l].l = +new Date;
                            e = o.createElement(i);
                            r = o.getElementsByTagName(i)[0];
                            e.src = '//www.google-analytics.com/analytics.js';
                            r.parentNode.insertBefore(e, r)
                        }(window, document, 'script', 'ga'));
                        ga('create', 'UA-XXXXX-X', 'auto');
                        ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
    </body>
</html>