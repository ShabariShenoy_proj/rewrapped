<?php
include_once("include/check_login_status.php");
// Initialize any variables that the page might echo
$u = "";
$id = "";
$l_id = "";
$sex = "Male";
$userlevel = "";
$profile_pic = "";
$profile_pic_btn = "";
$avatar_form = "";
$country = "";
$joindate = "";
$lastsession = "";
$creatPost = " ";
$usertype = "";
$usertype2 = "";
$name = "";
$interactionBox = "";
$photobox = "";
$editBox = "";
$changeBox = "";
$location = "";
$fname = "";
$lname = "";
$soldmap = '';
$soldTitle = '';
$ratingBox = '';
$bidBox = '';
$avbid = '';
$rate = '';
$current = '';
$rated = 'false';
$isagent = "false";
$logUserAgent = 'false';
$mess = '';
// ------- ESTABLISH THE PROFILE INTERACTION TOKEN ---------
$thisRandNum = rand(9999999999999, 999999999999999999);
$_SESSION['wipit'] = base64_encode($thisRandNum); // Will always overwrite itself each time this script runs
// ------- END ESTABLISH THE PROFILE INTERACTION TOKEN ---------
// 
// Make sure the _GET username is set, and sanitize it
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE username='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else {
    header("location: index.php");
    exit();
}


// get the average bid percentage for the current user if they are an agent
$bidperc = mysqli_query($db_conx, "SELECT AVG(bid) FROM bidnotes WHERE agent='$u'");
while ($avgrow = mysqli_fetch_row($bidperc)) {
    $avbid = round($avgrow[0]);
}

// Select the member from the users table
$sql = "SELECT * FROM users WHERE username='$u' AND activated='1' LIMIT 1";
$user_query = mysqli_query($db_conx, $sql);

$sql1 = "SELECT * FROM users WHERE username='$u'";
$user_query1 = mysqli_query($db_conx, $sql1);
$row = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
$id = $row["id"];
$fname = $row["firstname"];
$lname = $row["lastname"];
$usrlvl = $row["userlevel"];
if ($usrlvl == "b") {
    $isagent = "true";
}
$sql2 = "SELECT * FROM users WHERE username='$log_username' AND activated='1'";
$user_query2 = mysqli_query($db_conx, $sql2);
$row2 = mysqli_fetch_array($user_query2, MYSQLI_ASSOC);
$l_id = $row2["id"];
$ulvl = $row2["userlevel"];
if ($ulvl == "b") {
    $logUserAgent = "true";
}

$check = "SELECT COUNT(id) FROM ratings WHERE agentid='$id' AND userid='$l_id'";
$chkquery = mysqli_query($db_conx, $check);
$chkquery_count = mysqli_fetch_row($chkquery);
$rate_count = $chkquery_count[0];
if ($rate_count < 1) {
    //$rated = '';
    $q = mysqli_query($db_conx, "SELECT AVG(rating) FROM ratings WHERE agentid=$id");
    while ($trow = mysqli_fetch_row($q)) {
        $rate = round($trow[0]);
    }
} else {
    $rated = 'true';
    $q = mysqli_query($db_conx, "SELECT AVG(rating) FROM ratings WHERE agentid=$id");
    while ($trow = mysqli_fetch_row($q)) {
        $rate = round($trow[0]);
    }
}

// Now make sure that user exists in the table
$numrows = mysqli_num_rows($user_query);
if ($numrows < 1) {
    echo "That user does not exist or is not yet activated, press back";
    exit();
}

$name = "$fname $lname";
$you = "";

// Check to see if the viewer is the account owner
$isOwner = "no";
$logStatus = "<font color='red'>offline</font>";
if ($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    $logStatus = "<font color='#00FF7F'>online</font>";
    $profile_pic_btn = '<a href="#" onclick="return false;" onmousedown="toggleElement(\'avatar_form\')">Toggle Avatar Form</a>';
    $avatar_form = '<form id="avatar_form" enctype="multipart/form-data" method="post" action="php_parsers/photo_system.php">';
    $avatar_form .= '<p>Change your avatar</p>';
    $avatar_form .= '<input type="file" name="avatar" required>';
    $avatar_form .= '<p><input type="submit" value="Upload"></p>';
    $avatar_form .= '</form>';
    //$name = "Your";

    $you = "Your";
    if ($isagent == "false") {
//        $bidBox = '';
        $changeBox = '<br /><div class="btn btn-primary">
                            <a style="color: white; padding-right: 6px;" href="change_pass.php?u=' . $log_username . '&id=' . $id . '">Change Password</a>
                            </div><br />'; //&l='.$userlevel.'
    }
    
    } else {
    if ($log_username) {
        $interactionBox = '<br /><div class="interactionLinksDiv btn btn-primary">
           <a href="#" style="text-align: center; color: white;" onclick="return false" onmousedown="javascript:toggleInteractContainers(\'private_message\');">Private Message</a> 
          </div><br />';  // padding-right: 24px; padding-left: 24px;
    }
    $you = $fname . "'s";
}
// Fetch the user row from the query above
while ($row = mysqli_fetch_array($user_query, MYSQLI_ASSOC)) {
    $profile_id = $row["id"];
    $gender = $row["gender"];
    $country = $row["country"];
    $userlevel = $row["userlevel"];
    $avatar = $row["avatar"];
    $signup = $row["signup"];
    $lastlogin = $row["lastlogin"];
    $joindate = strftime("%b %d, %Y", strtotime($signup));
    $lastsession = strftime("%b %d, %Y", strtotime($lastlogin));
    if ($userlevel == "a") {
        $usertype = "Home Owner";
    } else if ($userlevel == "b") {
        $usertype = "Agent";
    } else if ($userlevel == "c") {
        $usertype = "Buyer";
    } else if ($userlevel == "d") {
        $usertype = "Administrator";
    }
}
if ($gender == "f") {
    $sex = "Female";
}
$profile_pic = '<img style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);" class="pure-img" src="user/' . $u . '/' . $avatar . '" alt="' . $u . '">';
if ($avatar == NULL) {
    $profile_pic = '<img style="border: 1px solid #878E63; border-radius: 2px; box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);" class="pure-img" src="images/th_1.jpg" alt="Default Profile Picture">';
}
?><?php
$isFriend = false;
$ownerBlockViewer = false;
$viewerBlockOwner = false;
if ($u != $log_username && $user_ok == true) {
    $friend_check = "SELECT id FROM friends WHERE user1='$log_username' AND user2='$u' AND accepted='1' OR user1='$u' AND user2='$log_username' AND accepted='1' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $friend_check)) > 0) {
        $isFriend = true;
    }
    $block_check1 = "SELECT id FROM blockedusers WHERE blocker='$u' AND blockee='$log_username' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $block_check1)) > 0) {
        $ownerBlockViewer = true;
    }
    $block_check2 = "SELECT id FROM blockedusers WHERE blocker='$log_username' AND blockee='$u' LIMIT 1";
    if (mysqli_num_rows(mysqli_query($db_conx, $block_check2)) > 0) {
        $viewerBlockOwner = true;
    }
}
?>
<?php
$friend_button = ''; //'<button disabled>Request As Friend</button>';
$block_button = ''; //'<button disabled>Block User</button>';
// LOGIC FOR FRIEND BUTTON
if ($isFriend == true) {
    $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'unfriend\',\'' . $u . '\',\'friendBtn\')" style="text-align: center;">Unfriend</button>';
} else if ($user_ok == true && $u != $log_username && $ownerBlockViewer == false) {
    $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'friend\',\'' . $u . '\',\'friendBtn\')" style="text-align: center;">Request As Friend</button>';
}
// LOGIC FOR BLOCK BUTTON
if ($viewerBlockOwner == true) {
    $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'unblock\',\'' . $u . '\',\'blockBtn\')">Unblock User</button>';
} else if ($user_ok == true && $u != $log_username) {
    $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'block\',\'' . $u . '\',\'blockBtn\')" style="padding-left: 10%; text-align: center;">Block User</button>';
}
?>
<?php
$arrFriendList = array();

$friendsHTML = '';
$friends_view_all_link = ''; //<a href="view_friends.php?u=' . $u . '">View all</a>
$sql = "SELECT COUNT(id) FROM friends WHERE user1='$u' AND accepted='1' OR user2='$u' AND accepted='1'";
$query = mysqli_query($db_conx, $sql);
$query_count = mysqli_fetch_row($query);
$friend_count = $query_count[0];
if ($friend_count < 1) {
    $friendsHTML = "<p>" . $u . " has no friends yet</p>";
} else {
    $max = 6;
    $all_friends = array();
    $sql = "SELECT user1 FROM friends WHERE user2='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $un = $row["user1"];

        array_push($all_friends, $row["user1"]);

        $sqla = "SELECT username, avatar FROM users WHERE username='$un'";
        $querya = mysqli_query($db_conx, $sqla);
        $rowrf = mysqli_fetch_array($querya, MYSQLI_ASSOC);
        //$friend_username = $rowrf["username"];
        $friend_avatar = $rowrf["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $un . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
//        array_push($arrFriendList, $un);
        $arrFriendList[] = $rowrf;
//        array_push($arrFriendList, $un, $friend_pic);
    }
    $sql = "SELECT user2 FROM friends WHERE user1='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $un = $row["user2"];

        array_push($all_friends, $row["user2"]);
//        array_push($arrFriendList, $un);
        $sqlb = "SELECT * FROM users WHERE username='$un'";
        $queryv = mysqli_query($db_conx, $sqlb);
        $rowf = mysqli_fetch_array($queryv, MYSQLI_ASSOC);
        $friend_username = $rowf["username"];
        $friend_avatar = $rowf["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $un . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $arrFriendList[] = $rowf;
//        array_push($arrFriendList, $un, $friend_pic);
    }
    $friendArrayCount = count($all_friends);
    if ($friendArrayCount > $max) {
        array_splice($all_friends, $max);
    }
    if ($friend_count > 0) {
        $friends_view_all_link = '<div class="btn btn-primary" style="text-align: center;"><a style="text-align: center; color: white;" href="view_friends.php?u=' . $u . '">View all</a></div>';
    }
    $orLogic = '';
    foreach ($all_friends as $key => $user) {
        $orLogic .= "username='$user' OR ";
    }
    $orLogic = chop($orLogic, "OR ");
    $sql = "SELECT * FROM users WHERE $orLogic";
    $query = mysqli_query($db_conx, $sql);
    while ($rowr = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $friend_id = $rowr["id"];
        $friend_username = $rowr["username"];
        $friend_avatar = $rowr["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $friend_username . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $friend_fname = $rowr["firstname"];
        $friend_lname = $rowr["lastname"];
        $friend_userlevel = $rowr["userlevel"];
        if ($friend_userlevel == "a") {
            $usertype2 = "Home Owner";
        } else if ($friend_userlevel == "b") {
            $usertype2 = "Agent";
        } else if ($friend_userlevel == "c") {
            $usertype2 = "Buyer";
        } else if ($friend_userlevel == "d") {
            $usertype2 = "Administrator";
        }
        $friend_country = $row["country"];

        $friendsHTML .= '<div class="col-sm-3 col-md-4"><div class="thumbnail" style="text-align:center; background:#CCF5FF;">'; //class="thumbnail"  col-md-2 beside sm-2
        $friendsHTML .= '<a href="user.php?u=' . $friend_username . '"><img style="width:80px; height:80px;" class="friendpics" src="' . $friend_pic . '" alt="' . $friend_username . '" title="' . $friend_username . '"></a>';
        $friendsHTML .= '<div class="caption"><p style="text-align: center; font-size: 12px;">' . $friend_fname . ' ' . $friend_lname . '</p><br/><span class="badge" style="margin-left: -8px;">' . $usertype2 . '</span></div></div></div>'; //<br/><p style="text-align: center; padding-left: -1px; font-size: 18px;">(' . $friend_username . ')</p>
    }
}
?>
<?php
$coverpic2 = '<img src="images/scroll-feather2.jpg" alt="pic" class="img-responsive">';
$json1 = array();
$result = mysqli_query($db_conx, "SELECT address, location, id, user, askingPrice, postName FROM posts WHERE location='Mississauga' ORDER BY postdate LIMIT 3");
//$json = mysqli_fetch_all($result, MYSQLI_ASSOC);
while (($brow = mysqli_fetch_row($result))) {
    $json1[] = $brow;
}
$gallery_list = "";
$sql = "SELECT DISTINCT postName, id FROM posts WHERE user='$u'";
$query = mysqli_query($db_conx, $sql);
if (mysqli_num_rows($query) < 1) {
    $gallery_list = "This user has not uploaded any posts yet.";
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $pn = $row["postName"];
        $pid = $row["id"];
        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$pid'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];
        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$pid' ORDER BY RAND() LIMIT 1");
        $filerow = mysqli_fetch_row($filequery);
        $file = $filerow[0];
        $otherquery = mysqli_query($db_conx, "SELECT location FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
        $otherrow = mysqli_fetch_row($otherquery);
        $loc = $otherrow[0];
        $postalquery = mysqli_query($db_conx, "SELECT postal FROM posts WHERE user='$u' AND id='$pid' ORDER BY RAND() LIMIT 1");
        $postalrow = mysqli_fetch_row($postalquery);
        $postal = $postalrow[0];
        $gallery_list .= '<div class="1-box pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-5">';
        $gallery_list .= '<div class="image" style="margin-left: 30px;" onclick="window.location = \'postphotos.php?postn=' . $pn . '&u=' . $u . '&location=' . $loc . '&postal=' . $postal . '&postid=' . $pid . '\'">';
        if ($filerow == NULL) {
            $gallery_list .= '<img src="images/altposts.png" alt="cover photo">';
        } else {
            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:160px; height:120px;">';
        }
        $gallery_list .= '</div>';
        $gallery_list .= '<b style="display: inline-block; width: 180px; white-space:nowrap; margin-left: 28px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' . $pn . ' (' . $count . ')</b>';
        $gallery_list .= '</div>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <script type= "text/javascript">
            $('.avatar').click(function (e) {
                $('.card').toggleClass('active');
            });

// Ripple effect
            var target, ink, d, x, y;
            $(".social").click(function (e) {
                target = $(this);
                //create .ink element if it doesn't exist
                if (target.find(".ink").length === 0)
                    target.prepend("<span class='ink'></span>");

                ink = target.find(".ink");
                //incase of quick double clicks stop the previous animation
                ink.removeClass("animate");

                //set size of .ink
                if (!ink.height() && !ink.width()) {
                    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    d = Math.max(target.outerWidth(), target.outerHeight());
                    ink.css({
                        height: d,
                        width: d
                    });
                }

                //get click coordinates
                //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
                x = e.pageX - target.offset().left - ink.width() / 2;
                y = e.pageY - target.offset().top - ink.height() / 2;

                //set the position and add class .animate
                ink.css({
                    top: y + 'px',
                    left: x + 'px'
                }).addClass("animate");
            });
        </script>
        <style>
            body {
                background-image: url("images/NYC.jpg");
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: 100%;
                //height: 100%;
                /*opacity: 0.6;*/
                //background: #F1F0D1;
                font-family: Verdana, Tahoma, Arial, sans-serif;
                font-size: 18px;
                overflow: auto;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".friend_name").keyup(function () {
                    var str = $(".friend_name").val();
                    var count = 0;
                    $(".fb-friends-list .inner_profile").each(function (index) {
                        if ($(this).attr("id")) {
                            //case insenstive search
                            if (!$(this).attr("id").match(new RegExp(str, "i"))) {
                                $(this).fadeOut("fast");
                            } else {
                                $(this).fadeIn("slow");
                                count++;
                            }
                        }
                    });

                    if (str == '') {
                        $("#result").hide();
                    } else {
                        $("#result").show();
                    }
                    //display no of results found
                    if (count < 1) {
                        $("#result").text("No results for :" + str);
                    } else {
                        $("#result").text("Top " + count + " results for " + str);
                    }
                });
            });
        </script>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <title><?php echo $u; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


        <link rel="stylesheet" href="css/demo.css">
        <link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">

        <!--<![endif]-->

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhPNo9ESa69mJXBUvzKdGMMMDdWx2wwA4&sensor=false&extension=.js"></script>


        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    </head>
    <!-- START BODY -->
    <body class="nomobile">
        <?php include_once("include/template_pageTop.php"); ?>
        <style type="text/css">
            @import url(http://zavoloklom.github.io/material-design-iconic-font/css/docs.md-iconic-font.min.css);
            body {
                background: #64b5f6;
                text-align: center;
            }

            .card {
                height: 800px;
                width: 800px;
                background: #fff;
                font-family: Roboto;
                display: block;
                position: relative;
                margin: 50px auto;
                border-radius: 5px;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
                transition: all 0.2s ease-in-out;
            }

            .card:hover {
                box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            }

            .fab {
                position: absolute;
                width: 60px;
                height: 60px;
                margin-top: 0;
                margin-left: 50px;
                visibility: hidden;
                background-color: #ffd54f;
                border-radius: 50%;
                transform: scale(0);
                box-shadow: 0 2px 3px rgba(0, 0, 0, 0.22), 0 1px 2px rgba(0, 0, 0, 0.24);
                transition: margin-top 0.6s 0.0s ease-in-out, margin-left 0.6s 0.1s ease-in-out, transform 0.6s 0.0s ease-in-out, visibility 0.6s ease-in-out;
            }

            .active .fab {
                margin-top: 50px;
                margin-left: 120px;
                transform: scale(12);
                visibility: visible;
                transition: margin-top 0.5s ease-in-out, margin-left 0.6s ease-in-out, transform 0.4s 0.3s ease-in-out, visibility 0.4s ease-in-out;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            }

            .avatar {
                margin-top: 5px;
                margin-left: -30px;
                width: 60px;
                height: 60px;
                font-size:2em;
                line-height:60px;
                color: #37474f;
                border-radius: 50%;
                background-color: #ffd54f;
                position: absolute;
                transition: 0.6s ease-in-out;
                cursor: pointer;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            }

            .active .avatar {
                transform: scale(2);
                margin-top: 50px;
                margin-left: -30px;
                transition: 0.6s ease-in-out;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            }

            .active .avatar:hover {
                box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            }

            .fabs {
                position: absolute;
                margin-top: -30px;
                margin-left: 260px;
                overflow: hidden;
                width: 80px;
                height: 80px;
                border-radius: 5px;
                transition: 1s ease-in-out;
                border-radius: 50%;
            }

            .active .fabs {
                margin-top: 0px;
                margin-left: 0px;
                width: 300px;
                height: 400px;
                transition: 0.4s ease-in-out;
                border-radius: 0;
            }

            .content {
                position: absolute;
                width: 98%;//280px;
                height: 98%;//380px;
                margin: 10px;
                text-align: center;
                overflow-y: auto;
                transition: 0.5s 0.3s cubic-bezier(.55, 0, .1, 1);
            }

            .active .content {
                transform: scale(0.2);
                opacity: 0;
                transition: 0.2s 0.3s cubic-bezier(.55, 0, .1, 1);
            }

            .post {
                position: relative;
                display: inline-block;
                height: 100px;
                margin: 10px auto 0;
                background: #eceff1;
                border-radius: 3px;
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
                transition: all 0.2s ease-in-out;
            }

            .post:hover{
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
            }

            .counter {
                border-radius: 50%;
                background: #3E50B4;
                cursor: pointer;
            }

            .detail {
                border: 1px solid #607d8b;
            }

            .main {
                width: 99%;
                height: 130px;
            }

            .main>.preview {
                width: 50%;
                height: 100%;
                background: #ef5350;
            }

            .main>.counter {
                width: 40px;
                height: 40px;
                margin-left: 220px;
                margin-top: -120px;
            }

            .main>.detail{
                width:50px;
                margin-top: -10px;
                margin-left: 150px;
            }

            .main>.details{
                border: 1px solid #607d8b;
                width:100px;
                margin-top: 30px;
                margin-left: 150px;
            }

            .sec {
                width: 49%;
            }

            .sec>.preview {
                width: 100%;
                height: 70%;
                background: #42a5f5;
            }

            .sec>.counter {
                width: 20px;
                height: 20px;
                margin-left: 110px;
                margin-top: -10px;
            }

            .sec>.detail{
                width:50px;
                margin-left: 10px;
            }

            .ter {
                width: 32%;
            }

            .ter>.preview {
                width: 100%;
                height: 80%;
                background: #d4e157;
            }

            .ter>.counter {
                width: 15px;
                height: 15px;
                margin-left: 65px;
                margin-top: -7px;
            }

            .ter>.detail{
                width:40px;
                margin-left: 10px;
            }

            .user {
                position: absolute;
                width: 280px;
                height: 200px;
                margin: 150px 10px 0 10px;
                text-align: center;
                visibility: hidden;
                transition: 0.5s cubic-bezier(.55, 0, .1, 1);
            }

            .active .user {
                visibility: visible;
            }

            .socials {
                display: inline-block;
            }

            .social {
                width: 40px;
                height: 40px;
                border-radius: 50%;
                transform: translate(0px, -10px);
                opacity: 0;
                float: left;
                margin: 0 auto;
                overflow: hidden;
                z-index: 2;
                transition: 0.2s cubic-bezier(.55, 0, .1, 1);
                cursor: pointer;
            }

            .social>i {
                line-height: 40px;
                font-size: 2em;
                color: #37474f;
            }

            .active .social {
                transform: translate(0px, 0px);
                opacity: 1;
                transition: 0.3s 0.5s cubic-bezier(.55, 0, .1, 1);
            }

            .profiles {
                display: inline-block;
            }

            .profile {
                width: 50%;
                height: auto;
                transform: translate(0px, -10px);
                opacity: 0;
                float: left;
                margin: 0 auto;
                overflow: hidden;
                z-index: 2;
                transition: 0.2s cubic-bezier(.55, 0, .1, 1);
                color: #37474f;
            }

            .profile>span {
                line-height: 40px;
                font-size: 1.2em;
                font-weight: 600;
                display: block;
                font-style: none;
                color: #37474f;
            }

            .active .profile {
                transform: translate(0px, 0px);
                opacity: 1;
                transition: 0.3s 0.8s cubic-bezier(.55, 0, .1, 1);
            }
            /* Ripple */

            .ink {
                display: block;
                position: absolute;
                background: rgba(38, 50, 56, 0.4);
                border-radius: 100%;
                -moz-transform: scale(0);
                -ms-transform: scale(0);
                webkit-transform: scale(0);
                transform: scale(0);
            }
            /*animation effect*/

            .ink.animate {
                animation: ripple 0.5s ease-in-out;
            }

            @keyframes ripple {
                /*scale the element to 250% to safely cover the entire link and fade it out*/

                100% {
                    opacity: 0;
                    -moz-transform: scale(5);
                    -ms-transform: scale(5);
                    webkit-transform: scale(5);
                    transform: scale(5);
                }
            }
            /*Scrollbar*/

            ::-webkit-scrollbar {
                width: 6px;
            }

            ::-webkit-scrollbar-track {
                border-radius: 0;
            }

            ::-webkit-scrollbar-thumb {
                margin: 2px;
                border-radius: 10px;
                background: rgba(0, 0, 0, 0.2);
            }
        </style>
        <div class="card">
            <div class="content">
                <div class="post main">
                    <div class="preview"></div>
                    <div class="counter"><?php echo $profile_pic; ?></div>
                    <div class="detail"><?php echo $avatar_form; ?></div>
                    <div class="detail"><?php echo $profile_pic_btn; ?></div>
                    <div class="details"></div>
                    <div class="details"></div>
                </div>
                <div class="post sec">
                    <div class="preview"><?php echo $name; ?></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post sec">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post ter">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post ter">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post ter">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post ter">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post ter">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
                <div class="post ter">
                    <div class="preview"></div>
                    <div class="counter"></div>
                    <div class="detail"></div>
                </div>
            </div>
            <div class="fabs">
                <div class="fab"></div>
                <i class="avatar zmdi zmdi-account"></i>
            </div>
            <div class="user">
                <div class="socials">
                    <div class="social"><i class="zmdi zmdi-twitter"></i></div>
                    <div class="social"><i class="zmdi zmdi-github"></i></div>
                    <div class="social"><i class="zmdi zmdi-google-plus"></i></div>
                    <div class="social"><i class="zmdi zmdi-codepen"></i></div>
                </div>
                <div class="profiles">
                    <div class="profile"><span>51</span>Upvoted</div>
                    <div class="profile"><span>9</span>Created</div>
                    <div class="profile"><span>9</span>Showcased</div>
                    <div class="profile"><span>1</span>Collections</div>
                    <div class="profile"><span>2</span>Followers</div>
                    <div class="profile"><span>5</span>Following</div>
                </div>
            </div>
        </div>
    </body>
    <!-- END BODY -->
</html>