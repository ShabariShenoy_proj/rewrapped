<?php
//$views = "";
//$page = "";
include_once ("include/check_login_status.php");
$sql = "SELECT username, avatar FROM users WHERE avatar IS NOT NULL AND userlevel='b' AND activated ='1' ORDER BY RAND() LIMIT 10";
$query = mysqli_query($db_conx, $sql);
$userlist = "";
while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    $u = $row["username"];
    $avatar = $row["avatar"];
    $profile_pic = 'user/' . $u . '/' . $avatar;
    $userlist .= '<div class="thumbnail 1-box pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-5">'; //class="col-sm-2 col-md-2"><div class="thumbnail">
    $userlist .= '<p><a href="user.php?u=' . $u . '" title="' . $u . '"><img src="' . $profile_pic . '" alt="' . $u . '" style="width:120px; height:120px;"></a></p>'; // margin:10px;
    $userlist .= '<div class="caption" style="text-align: center;"><h2>' . $u . '</h2></div>';
    $userlist .= '</div>';
}

//
//Select the user galleries
$gallery_list = "";
$sql = "SELECT DISTINCT id, postName, user FROM posts ORDER BY RAND() LIMIT 12"; //, usr, filename FROM postpictures";
$query = mysqli_query($db_conx, $sql);
if (mysqli_num_rows($query) < 1) {
    $gallery_list = "<div class='pure-u-1'><text style='display: inline-block;'>No posts yet.</text></div>"; //class='pure-u-1'
} else {
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $id = $row["id"];
        $u = $row["user"];
        $pn = $row["postName"];
        $img = "";
//            $img = $row["filename"];
        //$p = $row["pname"];
        //$u = $row["usr"];
//        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND pname='$pn'");
        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$id'");
        $countrow = mysqli_fetch_row($countquery);
        $count = $countrow[0];
//        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND pname='$pn' ORDER BY RAND() LIMIT 1");
        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$id' ORDER BY RAND() LIMIT 1");
        $filerow = mysqli_fetch_row($filequery);
        $file = $filerow[0];
        $gallery_list .= '<div class="1-box pure-u-1 pure-u-sm-1-3 pure-u-md-1-3 pure-u-lg-1-5">';
        $gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
        $gallery_list .= '<div class="image" style="margin-left: 30px;" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
        if ($filerow == NULL) {
            $gallery_list .= '<img src="images/altposts.png" alt="cover photo" style="width:160px; height:120px;">';
        } else {
            $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:160px; height:120px;">';
        }
//        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:120px; height:100px;">';
        $gallery_list .= '</div>';
        $gallery_list .= '<b style="display: inline-block; width: 180px; white-space:nowrap; margin-left: 28px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' . $pn . ' (' . $count . ')</b>';
        $gallery_list .= '</div>';
    }
}

// view counter
mysqli_query($db_conx, "UPDATE viewcounter SET `views` = `views`+'1' WHERE id='1'");
$count = mysqli_query($db_conx, "SELECT * FROM viewcounter WHERE id='1'");
while ($row = mysqli_fetch_array($count)) {
    $id = $row["id"];
    $page = $row["pagename"];
    $views = $row["views"];
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title> Rewrapped - Home </title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <!--<link rel="icon" href="images/altposts.png" type="image/x-icon">-->
        <link rel="icon" href="#" class="glyphicon glyphicon-home" type="image/x-icon"/>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" />
        <!--<meta name="keywords" content="footer, address, phone, icons" />-->
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <link rel="stylesheet" href="css/demo.css">
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css" />
        <!--<![endif]-->
        <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <!-- starting of old code-->
        <link rel="stylesheet" href="style/style.css">
        <style type="text/css">
            .section {
                width: 29%;
                float: left;
                margin: 2% 2%;
                text-align: center;
            }
            body {
                height:100%;
            }
            form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px;}
            div#galleries{}
            div#galleries > div{float:left; margin:20px; text-align:center; cursor:pointer;}
            div#galleries > div > div {height:100px; overflow:hidden;}
            div#galleries > div > div > img{width:150px; cursor:pointer;}
            div#photos{display:none; border:#666 1px solid; padding:20px;}
            div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
            div#photos > div > img{width:125px; cursor:pointer;}
            div#picbox{display:none; padding-top:36px;}
            div#picbox > img{max-width:800px; display:block; margin:0px auto;}
            div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script>
            function emptyElement(x) {
                _(x).innerHTML = "";
            }
            function login() {
                var e = _("email").value;
                var p = _("password").value;
                if (e == "" || p == "") {
                    _("status").innerHTML = "Fill out all of the form data";
                } else {
                    _("loginbtn").style.display = "none";
                    _("status").innerHTML = 'please wait ...';
                    var ajax = ajaxObj("POST", "login.php");
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText == "login_failed") {
                                _("status").innerHTML = "Login unsuccessful, please try again.";
                                _("loginbtn").style.display = "block";
                            } else {
                                window.location = "user.php?u=" + ajax.responseText;
                            }
                        }
                    }
                    ajax.send("e=" + e + "&p=" + p);
                }
            }
        </script>
        <!-- end of old code-->
    </head>
    <body>
        <?php include_once("analyticstracking.php") ?>
        <?php include_once ("include/template_pageTop.php"); ?>
        <!--<a href="mysql_connect.php">Search</a>-->
        <div class="jumbotron">
            <div class="container">
                <h1>Welcome To Rewrapped</h1>
                <p>Rewrapped lets you list your house with ease, avoiding pesky posting and other listing related fees you may incur.  Once posted Real Estate agents may bid on your property based on their commission rate.  There are other cool features such as finding Open Houses near near you! Click below for more.</p>
                <p><!-- <a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p> -->
                <p>There are <?php print $views; ?> Views on the <?php print $page ?> page!</p>
            </div>
        </div>
        
        <div id="myCarousel" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-to-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-to-slide-to="1"></li>
                <li data-target="#myCarousel" data-to-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/Magical.jpg" alt="villa" class="img-responsive" />

                    <div class="container">
                        <!--<div class="carousel-caption">
                            <h1>Example House</h1>
                            <p> Lorem ipsum</p>
                            <p><a class="btn btn-lg btn-primary" href="#" role="button">Button 1</a></p>
                        </div>-->

                    </div>
                </div>

                <div class="item">
                    <img src="/images/Magical.jpg" alt="villa" class="img-responsive"/>

                    <div class="container">
                        <!--<div class="carousel-caption">
                            <h1>Example House 2</h1>
                            <p> Lorem ipsum</p>
                            <p><a class="btn btn-lg btn-primary" href="#" role="button">Button 2</a></p>
                        </div>-->

                    </div>
                </div>

                <div class="item">
                    <img src="/images/npsx.jpg" alt="villa" class="img-responsive"/>

                    <div class="container">
                        <!--<div class="carousel-caption">
                            <h1>Example House 3</h1>
                            <p> Lorem ipsum</p>
                            <p><a class="btn btn-lg btn-primary" href="#" role="button">Button 3</a></p>
                        </div>-->

                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>


        <div class="container">
            <h2 class="content-head is-center">Featured Posts</h2> <!--style="text-aligh: center;"-->
            <div class="pure-g">
                <?php echo $gallery_list; ?>
            </div>
        </div>
        <hr>
        <div class="container">
            <h2>Featured Agents</h2>
            <div class="row">
                <?php echo $userlist; ?>
            </div>
        </div>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-65412649-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!--
        <!-- end of bootstrap -->
        <?php include_once ("include/template_pageBottom.php"); ?>
    </body>
</html>
