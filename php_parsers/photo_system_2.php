<?php

ob_start();
include_once("../include/check_login_status.php");
?><?php

if (isset($_POST["show"]) && $_POST["show"] == "galpics") {
    $picstring = "";
    $pname = $_POST["pname"];
    $gallery = preg_replace('#[^a-z 0-9,]#i', '', $_POST["gallery"]);
    $user = preg_replace('#[^a-z0-9]#i', '', $_POST["user"]);
    $sql = "SELECT * FROM postpictures WHERE usr='$user' AND gallery='$gallery' AND pname='$pname' ORDER BY uploaddate ASC";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $id = $row["id"];
        $filename = $row["filename"];
        //$description = $row["description"];
        $uploaddate = $row["uploaddate"];
        $picstring .= "$id|$filename|Description (add $ to front) |$uploaddate|||";
    }
    mysqli_close($db_conx);
    $picstring = trim($picstring, "|||");
    echo $picstring;
    exit();
}
?><?php

if ($user_ok != true || $log_username == "") {
    exit();
}
?>
<?php

if (isset($_FILES["avatar"]["name"]) && $_FILES["avatar"]["tmp_name"] != "") {
    $fileName = $_FILES["avatar"]["name"];
    $fileTmpLoc = $_FILES["avatar"]["tmp_name"];
    $fileType = $_FILES["avatar"]["type"];
    $fileSize = $_FILES["avatar"]["size"];
    $fileErrorMsg = $_FILES["avatar"]["error"];
    $kaboom = explode(".", $fileName);
    $fileExt = end($kaboom);
    list($width, $height) = getimagesize($fileTmpLoc);
    if ($width < 10 || $height < 10) {
        header("location: ../message.php?msg=ERROR: That image has no dimensions");
        exit();
    }
    $db_file_name = rand(100000000000, 999999999999) . "." . $fileExt;
    if ($fileSize > 1048576) {
        header("location: ../message.php?msg=ERROR: Your image file was larger than 1mb");
        exit();
    } else if (!preg_match("/\.(gif|jpg|png)$/i", $fileName)) {
        header("location: ../message.php?msg=ERROR: Your image file was not jpg, gif or png type");
        exit();
    } else if ($fileErrorMsg == 1) {
        header("location: ../message.php?msg=ERROR: An unknown error occurred");
        exit();
    }
    $sql = "SELECT avatar FROM users WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    $avatar = $row[0];
    if ($avatar != "") {
        $picurl = "../user/$log_username/$avatar";
        if (file_exists($picurl)) {
            unlink($picurl);
        }
    }
    $moveResult = move_uploaded_file($fileTmpLoc, "../user/$db_file_name");
    if ($moveResult != true) {
        header("location: ../message.php?msg=ERROR: File upload failed");
        exit();
    }
    include_once("../include/image_resize.php");
    $target_file = "../user/$log_username/$db_file_name";
    $resized_file = "../user/$log_username/$db_file_name";
    $wmax = 200;
    $hmax = 300;
    img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
    $sql = "UPDATE users SET avatar='$db_file_name' WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    mysqli_close($db_conx);
    header("location: ../user.php?u=$log_username");
    exit();
}
?>

<?php

if (isset($_POST["postname"])) {
    $postn = preg_replace('#[^a-z0-9]#i', '', $_POST["postname"]);
    $desc = preg_replace('#[^a-z0-9-]#i', '', $_POST["description"]);
    $askPrice = preg_replace("/[^0-9\.]/", "", $_POST["askingPrice"]);
    $address = preg_replace('#[^a-z0-9]#i', '', $_POST["address"]);
    $loc = preg_replace('#[^a-zA-Z0-9]#i', '', $_POST["location"]);
    $postal = preg_replace('#[^a-z0-9]#i', '', $_POST["postal"]);
    $bedroom = $_POST["bedroom"];
    $bathroom = $_POST["bathroom"];
    $basement = $_POST["basement"];
    $sqfootage = preg_replace('#[^a-z0-9]#i', '', $_POST["sqfootage"]);
    //$postdate = now();
    $end = "";
    $duration = $_POST["duration"];
    //$postid = "";

    if ($duration == "7") {
        $end = 7;
    } else if ($duration == "10") {
        $end = 10;
    } else if ($duration == "14") {
        $end = 14;
    }

//    $sql = "SELECT COUNT(id) FROM postpictures WHERE usr='$log_username' AND pname='$postn'";
//    $query = mysqli_query($db_conx, $sql);
//    $row = mysqli_fetch_row($query);
//    if ($row[0] > 14) {
//        header("location: ../message.php?msg=The demo system allows only 15 pictures total per post");
//        exit();
//    }



    $sql = "INSERT INTO agentposts(user, description, postName, askingPrice, location, postal, bedroom, bathroom, sqfootage, basement, address, postdate, expiry) VALUES ('$log_username', '$desc', '$postn', '$askPrice', '$loc', '$postal', '$bedroom', '$bathroom', '$sqfootage', '$basement', '$address', now(), now() + INTERVAL '$end' DAY)";
    $query = mysqli_query($db_conx, $sql);

    $postid = mysqli_insert_id($db_conx);
//    $sql1 = "SELECT id FROM posts WHERE user='$log_username' AND postName='$postn' AND askingPrice='$askPrice' AND location='$loc' AND postal='$postal' AND bedroom='$bedroom' AND bathroom='$bathroom' AND sqfootage='$sqfootage' AND basement='$basement' AND address='$address' AND postdate=now()";
//    $query1 = mysqli_query($db_conx, $sql1);
//    $row1 = mysqli_fetch_row($query1);
//    $postid = $row1[0];

    $sql1 = "SELECT * FROM users WHERE username='$log_username'";
    $user_query1 = mysqli_query($db_conx, $sql1);
    $row2 = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
//$id = $row["id"];
    $uname = $row2["firstname"] . ' ' . $row2["lastname"];
//    $lname = $row["lastname"];
//    $location = $row["location"];
    // Check accountOptions table to see if we can email alert them
//            $sqlCheckOptions = mysql_query("SELECT pm_mail_able FROM accountOptions WHERE uid='$to' LIMIT 1");
//            while ($row = mysql_fetch_array($sqlCheckOptions)) {
//                $pm_mail_able = $row["pm_mail_able"];
//                if ($pm_mail_able == "1") {
//                    // email them here
//                    // Get the recipient's email address from DB
    $sqlemailaddy = mysqli_query($db_conx, "SELECT email, firstname, lastname FROM users WHERE location LIKE '%$loc%'");
    while ($row = mysqli_fetch_array($sqlemailaddy, MYSQLI_ASSOC)) {
        $recipient_email = $row["email"];
        $toName = $row["firstname"] . ' ' . $row["lastname"];


        // Send email alert to profile owner telling they have private message
        $eto = "$recipient_email";
        $efrom = "auto_responder@Rewrapped.ca";
        $esubject = "A new home has been posted in your area";
        //Begin Email Message
        $emessage = "Hi $toName,

              This is an automated message to let you know that $uname just created a listing in your area:

              Click here to view the post: http://www.Rewrapped.ca/postphotos.php?postid=$postid&u=$log_username";
        //end of message
        $eheaders = "From: $efrom\r\n";
        $eheaders .= "Content-type: text\r\n";
        mail($eto, $esubject, $emessage, $eheaders);
    }
    // End Get the recipient's email address from DB
    //end of send email to profile owner
//                } // close if
//            } // close while
    // End Check Profile Options table to see if we can email alert them ----------------------------------------------------------	  

    $sql2 = "INSERT INTO viewcounter(pagename, views, user, postid) VALUES ('post', '1', '$log_username', '$postid')";
    $query2 = mysqli_query($db_conx, $sql2);

    mysqli_close($db_conx);
    //header("location: ../photos_1.php?u=$log_username");
    header("location: ../postphotos.php?u=$log_username&location=$loc&postal=$postal&postid=$postid"); //&postn=$postn
//    ob_start();
    exit();
}
?>
<?php

if (isset($_FILES["photo"]["name"]) && isset($_POST["gallery"])) {
    $postN = $_GET["p"];
    $postid = $_GET["pid"];
    $sql = "SELECT COUNT(id) FROM postpictures WHERE usr='$log_username' AND postid='$postid'";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    if ($row[0] > 14) {
        header("location: ../message.php?msg=The demo system allows only 15 pictures total");
        exit();
    }

//        $sql2 = "SELECT id FROM posts WHERE id = '$postN'";
//        $query2 = mysqli_query($db_conx, $sql2);
//        $row2 = mysqli_fetch_array($query2, MYSQLI_ASSOC);
//        $postid = $row2["id"];
//        if (!file_exists("user/$log_username/$postname")) {
//			mkdir("user/$log_username/$postname", 0755);
//        }
//        $desc = $_POST["description"];
    $postal = $_POST["postal"];
    $loc = $_POST["location"];
    $gallery = preg_replace('#[^a-z 0-9,]#i', '', $_POST["gallery"]);
    $fileName = $_FILES["photo"]["name"];
    $fileTmpLoc = $_FILES["photo"]["tmp_name"];
    $fileType = $_FILES["photo"]["type"];
    $fileSize = $_FILES["photo"]["size"];
    $fileErrorMsg = $_FILES["photo"]["error"];
    $kaboom = explode(".", $fileName);
    $fileExt = end($kaboom);
    $db_file_name = date("DMjGisY") . "" . rand(1000, 9999) . "." . $fileExt; // WedFeb272120452013RAND.jpg
    list($width, $height) = getimagesize($fileTmpLoc);
    if ($width < 10 || $height < 10) {
        header("location: ../message.php?msg=ERROR: That image has no dimensions");
        exit();
    }
    if ($fileSize > 2048576) {
        header("location: ../message.php?msg=ERROR: Your image file was larger than 1mb");
        exit();
    } else if (!preg_match("/\.(gif|jpg|png)$/i", $fileName)) {
        header("location: ../message.php?msg=ERROR: Your image file was not jpg, gif or png type");
        exit();
    } else if ($fileErrorMsg == 1) {
        header("location: ../message.php?msg=ERROR: An unknown error occurred");
        exit();
    }
    $moveResult = move_uploaded_file($fileTmpLoc, "../user/$log_username/$db_file_name");
    if ($moveResult != true) {
        header("location: ../message.php?msg=ERROR: File upload failed");
        exit();
    }
    include_once("../include/image_resize.php");
    $wmax = 800;
    $hmax = 600;
    if ($width > $wmax || $height > $hmax) {
        $target_file = "../user/$log_username/$db_file_name";
        $resized_file = "../user/$log_username/$db_file_name";
        img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
    }
    $sql = "INSERT INTO postpictures(usr, gallery, filename, uploaddate, pname, postid) VALUES ('$log_username','$gallery','$db_file_name', now(), '$postN', '$postid')";
    $query = mysqli_query($db_conx, $sql);
    mysqli_close($db_conx);
    header("location: ../postphotos.php?u=$log_username&postn=$postN&location=$loc&postal=$postal&postid=$postid");
    //header("location: ../index.php?u=$log_username");
    exit();
}
?><?php

if (isset($_POST["delete"]) && $_POST["id"] != "") {
    $id = preg_replace('#[^0-9]#', '', $_POST["id"]);
    $query = mysqli_query($db_conx, "SELECT usr, filename FROM postpictures WHERE id='$id' LIMIT 1");
    $row = mysqli_fetch_row($query);
    $user = $row[0];
    $filename = $row[1];
    if ($user == $log_username) {
        $picurl = "../user/$log_username/$filename";
        if (file_exists($picurl)) {
            unlink($picurl);
            $sql = "DELETE FROM postpictures WHERE id='$id' LIMIT 1";
            $query = mysqli_query($db_conx, $sql);
        }
    }
    mysqli_close($db_conx);
    echo "deleted_ok";
    exit();
}
ob_end_flush();
?>