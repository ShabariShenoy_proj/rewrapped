<?php

ob_start();
include_once("../include/check_login_status.php");
?><?php

if (isset($_POST["show"]) && $_POST["show"] == "galpics") {
    $picstring = "";
    $pname = $_POST["pname"];
    $gallery = preg_replace('#[^a-z 0-9,]#i', '', $_POST["gallery"]);
    $user = preg_replace('#[^a-z0-9]#i', '', $_POST["user"]);
    $sql = "SELECT * FROM postpictures WHERE usr='$user' AND gallery='$gallery' AND pname='$pname' ORDER BY uploaddate ASC";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $id = $row["id"];
        $filename = $row["filename"];
        //$description = $row["description"];
        $uploaddate = $row["uploaddate"];
        $picstring .= "$id|$filename|Description (add $ to front) |$uploaddate|||";
    }
    mysqli_close($db_conx);
    $picstring = trim($picstring, "|||");
    echo $picstring;
    exit();
}
?><?php

if ($user_ok != true || $log_username == "") {
    exit();
}
?>
<?php

if (isset($_FILES["avatar"]["name"]) && $_FILES["avatar"]["tmp_name"] != "") {
    $fileName = $_FILES["avatar"]["name"];
    $fileTmpLoc = $_FILES["avatar"]["tmp_name"];
    $fileType = $_FILES["avatar"]["type"];
    $fileSize = $_FILES["avatar"]["size"];
    $fileErrorMsg = $_FILES["avatar"]["error"];
    $kaboom = explode(".", $fileName);
    $fileExt = end($kaboom);
    list($width, $height) = getimagesize($fileTmpLoc);
    if ($width < 10 || $height < 10) {
        header("location: ../message.php?msg=ERROR: That image has no dimensions");
        exit();
    }
    $db_file_name = rand(100000000000, 999999999999) . "." . $fileExt;
    if ($fileSize > 1048576) {
        header("location: ../message.php?msg=ERROR: Your image file was larger than 1mb");
        exit();
    } else if (!preg_match("/\.(gif|jpg|png)$/i", $fileName)) {
        header("location: ../message.php?msg=ERROR: Your image file was not jpg, gif or png type");
        exit();
    } else if ($fileErrorMsg == 1) {
        header("location: ../message.php?msg=ERROR: An unknown error occurred");
        exit();
    }
    $sql = "SELECT avatar FROM users WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    $avatar = $row[0];
    if ($avatar != "") {
        $picurl = "../user/$log_username/$avatar";
        if (file_exists($picurl)) {
            unlink($picurl);
        }
    }
    $moveResult = move_uploaded_file($fileTmpLoc, "../user/$db_file_name");
    if ($moveResult != true) {
        header("location: ../message.php?msg=ERROR: File upload failed");
        exit();
    }
    include_once("../include/image_resize.php");
    $target_file = "../user/$log_username/$db_file_name";
    $resized_file = "../user/$log_username/$db_file_name";
    $wmax = 200;
    $hmax = 300;
    img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
    $sql = "UPDATE users SET avatar='$db_file_name' WHERE username='$log_username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql);
    mysqli_close($db_conx);
    header("location: ../user.php?u=$log_username");
    exit();
}
?>

<?php

if (isset($_POST["postname"])) {
    $postn = $_POST["postname"];
//    $desc = $_POST["description"];
    $askPrice = $_POST["askingPrice"];
    $loc = $_POST["location"];
    $postal = $_POST["postal"];
    $bedroom = $_POST["bedroom"];
    $bathroom = $_POST["bathroom"];
    $basement = $_POST["basement"];
    $sqfootage = $_POST["sqfootage"];
    //$postid = "";

    $sql = "SELECT COUNT(id) FROM postpictures WHERE usr='$log_username' AND pname='$postn'";
    $query = mysqli_query($db_conx, $sql);
    $row = mysqli_fetch_row($query);
    if ($row[0] > 14) {
        header("location: ../message.php?msg=The demo system allows only 15 pictures total per post");
        exit();
    }
    $sql = "INSERT INTO posts(user, postName, askingPrice, location, postal, bedroom, bathroom, sqfootage, basement) VALUES ('$log_username', '$postn', '$askPrice', '$loc', '$postal', '$bedroom', '$bathroom', '$sqfootage', '$basement')";
    $query = mysqli_query($db_conx, $sql);

    $sql1 = "SELECT id FROM posts WHERE user='$log_username' AND postName='$postn'";
    $query1 = mysqli_query($db_conx, $sql1);
    $row1 = mysqli_fetch_row($query1);
    $postid = $row1[0];

    $sql2 = "INSERT INTO viewcounter(pagename, views, user, postid) VALUES ('post', '1', '$log_username', '$postid')";
    $query2 = mysqli_query($db_conx, $sql2);

    mysqli_close($db_conx);
    //header("location: ../photos_1.php?u=$log_username");
    header("location: ../postphotos.php?u=$log_username&postn=$postn&location=$loc&postal=$postal&postid=$postid");
    exit();
}
?>
<?php

if (isset($_POST["com"])) {
    $postN = $_GET["p"];
    $postid = $_GET["pid"];
    $owner = $_GET["u"];
    $ownerid = $_GET["u_id"];
    $agentid = $_GET["a_id"];
    $agent = $_GET["logu"];
    $bid = $_POST["com"];
    $tel = $_POST["desc"];
    $app = "";
    $note = "";
    // use this to control amount of bids
//    $sql = "SELECT COUNT(id) FROM postpictures WHERE usr='$log_username' AND postid='$postid'";
//    $query = mysqli_query($db_conx, $sql);
//    $row = mysqli_fetch_row($query);
//    if ($row[0] > 14) {
//        header("location: ../message.php?msg=The demo system allows only 15 pictures total");
//        exit();
//    }
    //$s
//
    $sql1 = "SELECT * FROM users WHERE username='$log_username' AND id='$agentid'";
    $user_query1 = mysqli_query($db_conx, $sql1);
    $row = mysqli_fetch_array($user_query1, MYSQLI_ASSOC);
    $limit = $row["bidlimit"];

    $reqsql = "SELECT * FROM bids WHERE agent='$log_username' AND owner='$owner' AND postid='$postid' LIMIT 1";
    $requery = mysqli_query($db_conx, $reqsql);
    $rnumrows = mysqli_num_rows($requery);
    $row = mysqli_fetch_array($requery, MYSQLI_ASSOC);
    $bidid = $row["id"];

    if ($rnumrows == 0) {
        if ($limit > 0) {
            $limit = $limit - 1;
        }

        $sqlupdatebids = "UPDATE users SET bidlimit='$limit' WHERE id='$agentid' LIMIT 1";
        $query2 = mysqli_query($db_conx, $sqlupdatebids);

        $sql = "INSERT INTO bids(agent, agentid, owner, ownerid, bid, datemade, postid, description) VALUES ('$log_username', '$agentid','$owner', '$ownerid','$bid', now(), '$postid', '$tel')";
        $query = mysqli_query($db_conx, $sql);

//        $ql = "SELECT * FROM bids WHERE agent='$log_username' AND owner='$owner' AND postid='$postid' AND bid='$bid' LIMIT 1";
//        $uery = mysqli_query($db_conx, $ql);
//        $getRow = mysqli_fetch_array($uery, MYSQLI_ASSOC);
//        $bidid = $getRow["id"];

        $bidid = mysqli_insert_id($db_conx);

        $app = "bid placed";
        $note = '<a href="postphotos.php?u=' . $owner . '&postid=' . $postid . '">Go to post</a>'; // <br/> at the begining
        mysqli_query($db_conx, "INSERT INTO bidnotes(agent, owner, app, note, date_time, postid, bidid, bid) 
		             VALUES('$log_username','$owner','$app','$note',now(), '$postid', '$bidid', '$bid')");
    } else {
        if ($limit > 0) {
            $limit = $limit - 1;
        }

        $sqlupdatebids = "UPDATE users SET bidlimit='$limit' WHERE id='$agentid' LIMIT 1";
        $query2 = mysqli_query($db_conx, $sqlupdatebids);

        $sql = "UPDATE bids set bid='$bid', datemade=now(), description='$tel' WHERE agent='$log_username' AND owner='$owner' AND postid='$postid'";
        $query = mysqli_query($db_conx, $sql);
        $app = "bid placed";
        $note = '<a href="postphotos.php?u=' . $owner . '&postid=' . $postid . '">Go to post</a>'; // <br/> at the begining
        mysqli_query($db_conx, "INSERT INTO bidnotes(agent, owner, app, note, date_time, postid, bidid, bid) 
		             VALUES('$log_username','$owner','$app','$note',now(), '$postid', '$bidid', '$bid')");
    }

    $sqlemailaddy = mysqli_query($db_conx, "SELECT email, firstname, lastname FROM users WHERE id='$ownerid' LIMIT 1");
    $row = mysqli_fetch_array($sqlemailaddy, MYSQLI_ASSOC);
    $recipient_email = $row["email"];
    $toName = $row["firstname"] . ' ' . $row["lastname"];

    $sqlemailaddy2 = mysqli_query($db_conx, "SELECT email, firstname, lastname FROM users WHERE id='$agentid'");
    $agentrow = mysqli_fetch_array($sqlemailaddy2, MYSQLI_ASSOC);
    $fromName = $agentrow["firstname"] . ' ' . $agentrow["lastname"];

    // Send email alert to profile owner telling they have private message
    $eto = "$recipient_email";
    $efrom = "auto_responder@Rewrapped.ca";
    $esubject = "A bid has been placed on your home";
    //Begin Email Message
    $emessage = "Hi $toName,

              This is an automated message to let you know that $fromName just placed a bid on your listing:

              Click here to view the post: http://www.Rewrapped.ca/postphotos.php?postid=$postid&u=$owner";
    //end of message
    $eheaders = "From: $efrom\r\n";
    $eheaders .= "Content-type: text\r\n";
    mail($eto, $esubject, $emessage, $eheaders);
// end mail
// 
    // query the database for all the other agents that bid on the page and email them that someone has placed a bid
//    $bidsql = "SELECT * FROM bids WHERE postid='$postid' AND NOT agentid='$agentid'";
    $bidquery = mysqli_query($db_conx, "SELECT * FROM bids WHERE postid='$postid' AND NOT agentid='$agentid'");
    while ($sqrow = mysqli_fetch_array($bidquery, MYSQLI_ASSOC)) {
        $otherid = $sqrow["agentid"];
        $otheragent = $sqrow["agent"];
        $sqlemailaddy3 = mysqli_query($db_conx, "SELECT * FROM users WHERE id='$otherid' AND username='$otheragent'");
        $agentrow2 = mysqli_fetch_array($sqlemailaddy3, MYSQLI_ASSOC);
        $toName2 = $agentrow2["firstname"] . ' ' . $agentrow2["lastname"];
        $recipient_email2 = $agentrow2["email"];

        // Send email alert to other agents currently bidding on the post
        $eto = "$recipient_email2";
        $efrom = "auto_responder@Rewrapped.ca";
        $esubject = "Another Agent has Placed a bid on the below post";
        //Begin Email Message
        $emessage = "Hi $toName2,

              This is an automated message to let you know that <b><u>$fromName</u></b> has just placed a bid for a listing you are currently bidding on:

              Click here to view the post: http://www.Rewrapped.ca/postphotos.php?postid=$postid&u=$owner";
        //end of message
        $eheaders = "From: $efrom\r\n";
        $eheaders .= "Content-type: text\r\n";
        mail($eto, $esubject, $emessage, $eheaders);
// end mail
    }

    mysqli_close($db_conx);
    header("location: ../postphotos.php?u=$owner&postn=$postN&postid=$postid#bidtable");
    //header("location: ../index.php?u=$log_username");
    exit();
}
?><?php

if (isset($_POST["delete"]) && $_POST["id"] != "") {
    $id = preg_replace('#[^0-9]#', '', $_POST["id"]);
    $query = mysqli_query($db_conx, "SELECT usr, filename FROM postpictures WHERE id='$id' LIMIT 1");
    $row = mysqli_fetch_row($query);
    $user = $row[0];
    $filename = $row[1];
    if ($user == $log_username) {
        $picurl = "../user/$log_username/$filename";
        if (file_exists($picurl)) {
            unlink($picurl);
            $sql = "DELETE FROM postpictures WHERE id='$id' LIMIT 1";
            $query = mysqli_query($db_conx, $sql);
        }
    }
    mysqli_close($db_conx);
    echo "deleted_ok";
    exit();
}
ob_end_flush();
?>