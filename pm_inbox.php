<?php
/* Web Intersect Social Network Template System and CMS v1.34
 * Copyright (c) 2011 Adam Khoury
 * Licensed under the GNU General Public License version 3.0 (GPLv3)
 * http://www.webintersect.com/license.php
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 * See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: February 9, 2010
 * ------------------------------------------------------------------------------------------------ */
ob_start();
// Start_session, check if user is logged in or not, and connect to the database all in one included file
include_once("include/check_login_status.php");
?>
<?php
$my_id = "";
$u = "";
$my_uname = ""; // Put user's first name into a local variable
//

// Make sure the _GET username is set, and sanitize it
if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $my_id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $my_id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$my_id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE id='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $my_id = $ro[0];
//    $name = "$u's ";
} else {
//    header("location: index.php");
//    exit();


    $sql = "SELECT id, username, avatar, firstname FROM users WHERE username='$log_username'";
    $query = mysqli_query($db_conx, $sql);
    $userlist = "";
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $my_id = $row["id"];
        $u = $row["username"];
        $avatar = $row["avatar"];
        $my_uname = $row["firstname"];
    }
//if (!isset($_SESSION['idx'])) {
//echo  '<br /><br /><font color="#FF0000">Your session has timed out</font>
//<p><a href="pm_inbox.php.php">Please Click Here</a></p>';
//exit(); 
//}
//// Decode the Session IDX variable and extract the user's ID from it
//$decryptedID = base64_decode($_SESSION['idx']);
//$id_array = explode("p3h9xfn8sq03hs2234", $decryptedID);
//$my_uname = $_SESSION['username']; // Put user's first name into a local variable
// ------- ESTABLISH THE INTERACTION TOKEN ---------
    $thisRandNum = rand(9999999999999, 999999999999999999);
    $_SESSION['wipit'] = base64_encode($thisRandNum); // Will always overwrite itself each time this script runs
// ------- END ESTABLISH THE INTERACTION TOKEN ---------
}
if (!$log_username) {
    echo "<script type=\"text/javascript\">" .
    "alert('please log in');" .
    "</script>";
    header("location: login.php");
    exit();
} else if ($log_username != $u) {
    echo "<script type=\"text/javascript\">" .
    "alert('please log in');" .
    "</script>";
    header("location: login.php");
    exit();
}
?>
<?php
// Mailbox Parsing for deleting inbox messages
if (isset($_POST['deleteBtn'])) {
    foreach ($_POST as $key => $value) {
        $value = urlencode(stripslashes($value));
        if ($key != "deleteBtn") {
            $sql = mysqli_query($db_conx, "UPDATE private_messages SET recipientDelete='1', opened='1' WHERE id='$value' AND to_id='$my_id' LIMIT 1");
            // Check to see if sender also removed from sent box, then it is safe to remove completely from system
        }
    }
    header("location: pm_inbox.php");
}
ob_end_flush();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Your Private Messages</title>
        <!--<link href="style/main.css" rel="stylesheet" type="text/css" />-->
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>

        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"/>
        <link rel="icon" href="images/altposts.png" type="image/x-icon" />
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css"/>

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <link rel="stylesheet" href="css/main.css"/>
        <link rel="stylesheet" href="style/style.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

        <script language="javascript" type="text/javascript">
            function toggleChecks(field) {
                if (document.myform.toggleAll.checked == true) {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = true;
                    }
                } else {
                    for (i = 0; i < field.length; i++) {
                        field[i].checked = false;
                    }
                }

            }
            $(document).ready(function () {

                $('.interactContainers').hide();
            });
            $(document).ready(function () {
                $(".toggle").click(function () {
                    if ($(this).next().is(":hidden")) {
                        $(".hiddenDiv").hide();
                        $(this).next().slideDown("fast");
                    } else {
                        $(this).next().hide();
                    }
                });
            });
            function markAsRead(msgID) {
                $.post("php_parsers/markAsRead.php", {messageid: msgID, ownerid:<?php echo $my_id; ?>}, function (data) {
                    $('#subj_line_' + msgID).addClass('msgRead');
                    // alert(data); // This line was just for testing returned data from the PHP file, it is not required for marking messages as read
                });
            }
            function toggleReplyBox(subject, sendername, senderid, recName, recID, replyWipit) {
                $("#subjectShow").text(subject);
                $("#recipientShow").text(recName);
                document.replyForm.pmSubject.value = subject;
                document.replyForm.pm_sender_name.value = sendername;
                document.replyForm.pmWipit.value = replyWipit;
                document.replyForm.pm_sender_id.value = senderid;
                document.replyForm.pm_rec_name.value = recName;
                document.replyForm.pm_rec_id.value = recID;
                document.replyForm.replyBtn.value = "Send reply to " + recName;
                if ($('#replyBox').is(":hidden")) {
                    $('#replyBox').fadeIn(1000);
                } else {
                    $('#replyBox').hide();
                }
            }
            function processReply() {

                var pmSubject = $("#pmSubject");
                var pmTextArea = $("#pmTextArea");
                var sendername = $("#pm_sender_name");
                var senderid = $("#pm_sender_id");
                var recName = $("#pm_rec_name");
                var recID = $("#pm_rec_id");
                var pm_wipit = $("#pmWipit");
                var url = "php_parsers/private_msg_parse.php";

                var ajax = ajaxObj("POST", "php_parsers/private_msg_parse.php");
                if (pmTextArea.val() == "") {
                    $("#PMStatus").text("Please type in your message.").show().fadeOut(6000);
                } else {
                    $("#pmFormProcessGif").show();
//                    $.post(url, {subject: pmSubject.val(), message: pmTextArea.val(), senderName: sendername.val(), senderID: senderid.val(), rcpntName: recName.val(), rcpntID: recID.val(), thisWipit: pm_wipit.val()}, function (data) {
//                        document.replyForm.pmTextArea.value = "";
//                        $("#pmFormProcessGif").hide();
//                        $('#replyBox').slideUp("fast");
//                        $("#PMFinal").html("&nbsp; &nbsp;" + data).show().fadeOut(8000);
//                    });
                    ajax.send("pm_sender_id=" + senderid.val() + "&pm_sender_name=" + sendername.val() + "&pm_rec_id=" + recID.val() + "&pm_rec_name=" + recName.val() + "&pmSubject=" + pmSubject.val() + "&pmTextArea=" + pmTextArea.val());
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText == "private_message_sent") {
                                $("#PMFinal").html('<img src="images/round_success.png" alt="Success" width="31" height="30" /> &nbsp;&nbsp;&nbsp;<strong>Message sent successfully</strong>').show().fadeOut(8000);
                                $('#replyBox').slideUp("fast");
                            } else {
                                alert(ajax.responseText);
                                $("#PMFinal").innerHTML = 'Try again later';
                            }
                        }
                    }
                    document.pmForm.pmTextArea.value = '';
                    document.pmForm.pmSubject.value = '';
                    $("#pmFormProcessGif").hide();
                }
            }
        </script>
        <style type="text/css"> 
            .hiddenDiv{display:none}
            #pmFormProcessGif{display:none}
            .msgDefault {font-weight:bold;}
            .msgRead {font-weight:100;color:#666;}
        </style>
    </head>
    <body>
<?php include_once "include/template_pageTop.php"; ?>
        &nbsp;
        <br/>
        <table width="920" style="background-color:#F2F2F2;" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td width="732" valign="top">
                    <h2 style="margin-left:24px;">Your Private Messages</h2>
                    <!-- START THE PM FORM AND DISPLAY LIST -->
                    <form name="myform" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                        <table width="94%" border="0" align="center" cellpadding="4">
                            <tr>
                                <td width="3%" align="right" valign="bottom"><img src="images/crookedArrow.png" width="16" height="17" alt="Develop PHP Private Messages" /></td>
                                <td width="97%" valign="top"><input type="submit" name="deleteBtn" id="deleteBtn" value="Delete" />
                                    <span id="jsbox" style="display:none"></span>
                                </td>
                            </tr>
                        </table>
                        <table width="96%" border="0" align="center" cellpadding="5" style=" background-image:url(style/headerStrip.jpg); background-repeat:repeat-x; border: #999 1px solid;">
                            <tr>
                                <td width="4%" valign="top">
                                    <input name="toggleAll" id="toggleAll" type="checkbox" onclick="toggleChecks(document.myform.cb)" />
                                </td>
                                <td width="20%" valign="top">From</td>
                                <td width="58%" valign="top"><span class="style2">Subject</span></td>
                                <td width="18%" valign="top">Date</td>
                            </tr>
                        </table> 
<?php
///////////End take away///////////////////////
// SQL to gather their entire PM list
$sql = mysqli_query($db_conx, "SELECT * FROM private_messages WHERE to_id='$my_id' AND recipientDelete='0' ORDER BY id DESC LIMIT 100");

while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {

    $date = strftime("%b %d, %Y", strtotime($row['time_sent']));
    if ($row['opened'] == "0") {
        $textWeight = 'msgDefault';
    } else {
        $textWeight = 'msgRead';
    }
    $fr_id = $row['from_id'];
    // SQL - Collect username for sender inside loop
    $ret = mysqli_query($db_conx, "SELECT id, username, firstname FROM users WHERE id='$fr_id' LIMIT 1");
    while ($raw = mysqli_fetch_array($ret, MYSQLI_ASSOC)) {
        $Sid = $raw['id'];
        $Sname = $raw['firstname'];
        $Suname = $raw["username"];
    }
    ?>
                            <table width="96%" border="0" align="center" cellpadding="4">
                                <tr>
                                    <td width="4%" valign="top">
                                        <input type="checkbox" name="cb<?php echo $row['id']; ?>" id="cb" value="<?php echo $row['id']; ?>" />
                                    </td>
                                    <td width="20%" valign="top"><a href="user.php?u=<?php echo $Suname; ?>"><?php echo $Sname; ?></a></td>
                                    <td width="58%" valign="top">
                                        <span class="toggle" style="padding:3px;">
                                            <a class="<?php echo $textWeight; ?>" id="subj_line_<?php echo $row['id']; ?>" style="cursor:pointer;" onclick="markAsRead(<?php echo $row['id']; ?>)"><?php echo stripslashes($row['subject']); ?></a>
                                        </span>
                                        <div class="hiddenDiv"> <br />
    <?php echo stripslashes(wordwrap(nl2br($row['message']), 54, "\n", true)); ?>
                                            <br /><br /><a href="javascript:toggleReplyBox('<?php echo stripslashes($row['subject']); ?>','<?php echo $my_uname; ?>','<?php echo $my_id; ?>','<?php echo $Sname; ?>','<?php echo $fr_id; ?>','<?php echo $thisRandNum; ?>')">REPLY</a><br />
                                        </div>

                                    </td>
                                    <td width="18%" valign="top"><span style="font-size:10px;"><?php echo $date; ?></span></td>
                                </tr>
                            </table>
                            <hr style="margin-left:20px; margin-right:20px;" />
    <?php
}// Close Main while loop
?>
                    </form>
                    <!-- END THE PM FORM AND DISPLAY LIST -->
                    <!-- Start Hidden Container the holds the Reply Form -->            
                    <div id="replyBox" style="display:none; width:680px; height:400px; background-color: #005900; background-repeat:repeat; border: #333 1px solid; top:51px; position:fixed; margin:auto; z-index:50; padding:20px; color:#FFF;">
                        <div align="right"><a href="javascript:toggleReplyBox('close')"><font color="#00CCFF"><strong>CLOSE</strong></font></a></div>
                        <h2>Replying to <span style="color:#ABE3FE;" id="recipientShow"></span></h2>
                        Subject: <strong><span style="color:#ABE3FE;" id="subjectShow"></span></strong> <br>
                            <form action="javascript:processReply();" name="replyForm" id="replyForm" method="post">
                                <textarea class="form-control" id="pmTextArea" rows="8" style="width:98%;"></textarea><br />
                                <input type="hidden" id="pmSubject" />
                                <input type="hidden" id="pm_rec_id" />
                                <input type="hidden" id="pm_rec_name" />
                                <input type="hidden" id="pm_sender_id" />
                                <input type="hidden" id="pm_sender_name" />
                                <input type="hidden" id="pmWipit" />
                                <br />
                                <input class="btn btn-primary" name="replyBtn" type="button" onclick="javascript:processReply()" /> &nbsp;&nbsp;&nbsp; <span id="pmFormProcessGif"><img src="images/loading.gif" width="28" height="10" alt="Loading" /></span>
                                <div id="PMStatus" style="color:#F00; font-size:14px; font-weight:700;">&nbsp;</div>
                            </form>
                    </div>
                    <!-- End Hidden Container the holds the Reply Form -->     
                    <!-- Start PM Reply Final Message box showing user message status when needed -->    
                    <div id="PMFinal" style="display:none; width:652px; background-color:#005900; border:#666 1px solid; top:51px; position:fixed; margin:auto; z-index:50; padding:40px; color:#FFF; font-size:16px;"></div>
                    <!-- End PM Reply Final Message box showing user message status when needed --> 
                </td>
                <td width="188" valign="top"><?php include_once("right_AD_template1.php"); ?><br /><br /><?php include_once "right_AD_template.php"; ?></td>
            </tr>

        </table>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                                    (function (b, o, i, l, e, r) {
                                        b.GoogleAnalyticsObject = l;
                                        b[l] || (b[l] =
                                                function () {
                                                    (b[l].q = b[l].q || []).push(arguments)
                                                });
                                        b[l].l = +new Date;
                                        e = o.createElement(i);
                                        r = o.getElementsByTagName(i)[0];
                                        e.src = '//www.google-analytics.com/analytics.js';
                                        r.parentNode.insertBefore(e, r)
                                    }(window, document, 'script', 'ga'));
                                    ga('create', 'UA-XXXXX-X', 'auto');
                                    ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->
<?php include_once "include/template_pageBottom.php"; ?>
    </body>
</html>