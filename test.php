<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title> Rewrapped - Home </title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!--<link rel="stylesheet" href="style/style.css">-->
        <script type="text/javascript">
            var elem = document.querySelector('.grid');
            var msnry = new Masonry(elem, {
                // options
                itemSelector: '.grid-item',
                columnWidth: 200
            });

// element argument can be a selector string
//   for an individual element
            var msnry = new Masonry('.grid', {
                // options
            });
        </script>
        <script src="/js/mason.js"></script>
    </head>
    <body>
        <div class="grid">
            <div id="div1" class="grid-item">
                <p>Hello world</p>
            </div>
            <div id="div2" class="grid-item">
                <p>Hello world</p>
            </div>
            <div id="div3" class="grid-item">
                <p>Hello world</p>
                <form name=""></form>
            </div>
        </div>
    </body>
</html>
