<?php
include_once ("include/check_login_status.php");
// view counter
//mysqli_query($db_conx, "UPDATE viewcounter SET `views` = `views`+'1' WHERE id='1'");
//$count = mysqli_query($db_conx, "SELECT * FROM viewcounter WHERE id='1'");
//while ($row = mysqli_fetch_array($count)) {
//    $id = $row["id"];
//    $page = $row["pagename"];
//    $views = $row["views"];
//}
//
//Select the user galleries
$u = '';
$id = '';
$usertype = '';
$isFriend = '';
$ownerBlockViewer = '';
$viewerBlockOwner = '';
$friend_button = ''; //'<button disabled>Request As Friend</button>';
$block_button = ''; //'<button disabled>Block User</button>';

if (isset($_GET["u"]) && isset($_GET["id"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    //$name = "$u's ";
} else if (isset($_GET["id"])) {
    $id = preg_replace('#[^a-z0-9]#i', '', $_GET['id']);
    $s = "SELECT username FROM users WHERE id='$id' AND activated='1'";
    $query = mysqli_query($db_conx, $s);
    $row = mysqli_fetch_row($query);
    $u = $row[0];
//    $name = "$u's ";
} else if (isset($_GET["u"])) {
    $u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
    $p = "SELECT id FROM users WHERE id='$u' AND activated='1'";
    $query = mysqli_query($db_conx, $p);
    $ro = mysqli_fetch_row($query);
    $id = $ro[0];
//    $name = "$u's ";
} else {
    header("location: index.php");
    exit();
}


$sqlu = "SELECT * FROM users WHERE username='$u'";
$qv = mysqli_query($db_conx, $sqlu);
$rr = mysqli_fetch_array($qv, MYSQLI_ASSOC);
if ($log_username == $u) {
    $user_name = "your";
} else {
    $user_name = $rr["firstname"]."'s";
}


$arrFriendList = array();

$friendsHTML = '';
$friends_view_all_link = ''; //<a href="view_friends.php?u=' . $u . '">View all</a>
$sql = "SELECT COUNT(id) FROM friends WHERE user1='$u' AND accepted='1' OR user2='$u' AND accepted='1'";
$query = mysqli_query($db_conx, $sql);
$query_count = mysqli_fetch_row($query);
$friend_count = $query_count[0];
if ($friend_count < 1) {
    $friendsHTML = "<p>" . $u . " has no friends yet</p>";
} else {
    $max = 6;
    $all_friends = array();
    $sql = "SELECT user1 FROM friends WHERE user2='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $un = $row["user1"];
        array_push($all_friends, $row["user1"]);

        $sqla = "SELECT * FROM users WHERE username='$un'";
        $querya = mysqli_query($db_conx, $sqla);
        $rowrf = mysqli_fetch_array($querya, MYSQLI_ASSOC);
        //$friend_username = $rowrf["username"];
        $friend_avatar = $rowrf["avatar"];
        $f_userlevel = $rowrf["userlevel"];
        if ($f_userlevel == "a") {
            $usertype2 = "Home Owner";
        } else if ($f_userlevel == "b") {
            $usertype2 = "Agent";
        } else if ($f_userlevel == "c") {
            $usertype2 = "Buyer";
        } else if ($f_userlevel == "d") {
            $usertype2 = "Administrator";
        }
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $un . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
//        array_push($arrFriendList, $un);
        $arrFriendList[] = $rowrf;
//        array_push($arrFriendList, $un, $friend_pic);
    }
    $sql = "SELECT user2 FROM friends WHERE user1='$u' AND accepted='1' ORDER BY RAND() LIMIT $max";
    $query = mysqli_query($db_conx, $sql);
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $un = $row["user2"];

        array_push($all_friends, $row["user2"]);
//        array_push($arrFriendList, $un);
        $sqlb = "SELECT * FROM users WHERE username='$un'";
        $queryv = mysqli_query($db_conx, $sqlb);
        $rowf = mysqli_fetch_array($queryv, MYSQLI_ASSOC);
        $friend_username = $rowf["username"];
        $friend_avatar = $rowf["avatar"];
        $f_userlevel = $rowf["userlevel"];
        if ($f_userlevel == "a") {
            $usertype2 = "Home Owner";
        } else if ($f_userlevel == "b") {
            $usertype2 = "Agent";
        } else if ($f_userlevel == "c") {
            $usertype2 = "Buyer";
        } else if ($f_userlevel == "d") {
            $usertype2 = "Administrator";
        }

        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $un . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $arrFriendList[] = $rowf;
//        array_push($arrFriendList, $un, $friend_pic);
    }
    $friendArrayCount = count($all_friends);
    if ($friendArrayCount > $max) {
        array_splice($all_friends, $max);
    }
    if ($friend_count > 0) {
        $friends_view_all_link = '<div class="btn btn-primary" style="text-align: center;"><a style="text-align: center; color: white;" href="view_friends.php?u=' . $u . '">View all</a></div>';
    }
    $orLogic = '';
    foreach ($all_friends as $key => $user) {
        $orLogic .= "username='$user' OR ";
    }
    $orLogic = chop($orLogic, "OR ");
    $sql = "SELECT * FROM users WHERE $orLogic";
    $query = mysqli_query($db_conx, $sql);
    while ($rowr = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $friend_id = $rowr["id"];
        $friend_username = $rowr["username"];
        $friend_avatar = $rowr["avatar"];
        if ($friend_avatar != "") {
            $friend_pic = 'user/' . $friend_username . '/' . $friend_avatar . '';
        } else {
            $friend_pic = 'images/avatardefault.jpg';
        }
        $friend_fname = $rowr["firstname"];
        $friend_lname = $rowr["lastname"];
        $friend_userlevel = $rowr["userlevel"];
        if ($friend_userlevel == "a") {
            $usertype2 = "Home Owner";
        } else if ($friend_userlevel == "b") {
            $usertype2 = "Agent";
        } else if ($friend_userlevel == "c") {
            $usertype2 = "Buyer";
        } else if ($friend_userlevel == "d") {
            $usertype2 = "Administrator";
        }
        $friend_country = $row["country"];

//        $friendsHTML .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // background-color: #B0E0E6\
//        $friendsHTML .= '<div class=\'pure-u-1  pure-u-sm-1-4 pure-u-md-1-4 pure-u-lg-1-4\' style=\'margin:auto;\'><div class="image" onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // was md-2-5
//        $friendsHTML .= '<img class="pure-img" src="' . $friend_pic . '" alt="cover photo">'; //id="thumbnail"        
//        $friendsHTML .= '</div></div>';
//        //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
//        //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
//        $friendsHTML .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
//                    <div class=\'pure-g\'>
//                        <div class=\'pure-u-1\'><b style="text-align: center;">' . $friend_fname . ' ' . $friend_lname . '</b></div>
//                        <div class=\'pure-u-1\'><text>Username:<span class="badge">' . $friend_username . '</span></text></div>
//                        <div class=\'pure-u-1\'>' . $friend_fname . ' is a(n)<span class="badge">' . $usertype . '</span></div>
//                            <div class=\'pure-u-1\'>Location: <span class="badge">' . $friend_country . '</span></div>
//                                <div class=\'pure-u-1\'>
//                        <p>
//                            <span id="friendBtn">
//                                ' . $friend_button . '</span>
//                        </p></div>
//                        <div class=\'pure-u-1\'><p>
//                            <span id="blockBtn">
//                                ' . $block_button . '
//                            </span>
//                        </p>
//                    </div>
//                    </div>
//                </div>';
////$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
//        $friendsHTML .= '</div><hr>';
        $friendsHTML .= '<div class="col-sm-3 col-md-4"><div class="thumbnail" style="text-align:center; background:#CCF5FF;">'; //class="thumbnail"  col-md-2 beside sm-2
        $friendsHTML .= '<a href="user.php?u=' . $friend_username . '"><img style="width:80px; height:80px;" class="friendpics" src="' . $friend_pic . '" alt="' . $friend_username . '" title="' . $friend_username . '"></a>';
        $friendsHTML .= '<div class="caption"><p style="text-align: center; font-size: 12px;">' . $friend_fname . ' ' . $friend_lname . '</p><br/><span class="badge" style="margin-left: -8px;">' . $usertype2 . '</span></div></div></div>'; //<br/><p style="text-align: center; padding-left: -1px; font-size: 18px;">(' . $friend_username . ')</p>
//        $friendsHTML .= '<div class="caption"></div></div></div>';
    }
}


//if (!$log_username) {
//    echo "<script type=\"text/javascript\">" .
//    "alert('please log in');" .
//    "</script>";
//    header("location: http://www.Rewrapped.ca/login.php");
//    exit();
//}
//$gallery_list = "";
//$friendsHTML = '';
//$friends_view_all_link = '<a href="view_friends.php?u=' . $u . '">View all</a>';
//
//
//$sql = "SELECT COUNT(id) FROM friends WHERE user1='$u' AND accepted='1' OR user2='$u' AND accepted='1'";
//$query = mysqli_query($db_conx, $sql);
//$query_count = mysqli_fetch_row($query);
//$friend_count = $query_count[0];
//if ($friend_count < 1) {
//    $friendsHTML = "<p>" . $u . " has no friends yet</p>";
//} else {
//    $max = 1;
//    $all_friends = array();
//    $sql = "SELECT user1 FROM friends WHERE user2='$u' AND accepted='1' ORDER BY RAND()"; //LIMIT $max
//    $query = mysqli_query($db_conx, $sql);
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        array_push($all_friends, $row["user1"]);
//    }
//    $sql = "SELECT user2 FROM friends WHERE user1='$u' AND accepted='1' ORDER BY RAND()"; // LIMIT $max
//    $query = mysqli_query($db_conx, $sql);
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        array_push($all_friends, $row["user2"]);
//    }
//    $friendArrayCount = count($all_friends);
////    if ($friendArrayCount > $max) {
////        array_splice($all_friends, $max);
////    }
////    if ($friend_count > $max) {
////        $friends_view_all_link = '<a href="view_friends.php?u=' . $u . '">View all</a>';
////    }
//    $orLogic = '';
//    foreach ($all_friends as $key => $user) {
//        $orLogic .= "username='$user' OR ";
//    }
//    $orLogic = chop($orLogic, "OR ");
//    $sql = "SELECT * FROM users WHERE $orLogic";
//    $query = mysqli_query($db_conx, $sql);
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        $friend_id = $row["id"];
//        $friend_username = $row["username"];
//        $friend_avatar = $row["avatar"];
//        if ($friend_avatar != "") {
//            $friend_pic = 'user/' . $friend_username . '/' . $friend_avatar . '';
//        } else {
//            $friend_pic = 'images/avatardefault.jpg';
//        }
//        $friend_fname = $row["firstname"];
//        $friend_lname = $row["lastname"];
//        $friend_userlevel = $row["userlevel"];
//        if ($friend_userlevel == "a") {
//            $usertype = "Home Owner";
//        } else if ($friend_userlevel == "b") {
//            $usertype = "Agent";
//        } else if ($friend_userlevel == "c") {
//            $usertype = "Buyer";
//        } else if ($friend_userlevel == "d") {
//            $usertype = "Administrator";
//        }
//        $friend_country = $row["country"];
//
//        $isFriend = false;
//        $ownerBlockViewer = false;
//        $viewerBlockOwner = false;
//        if ($friend_username != $log_username && $user_ok == true) {
//            $friend_check = "SELECT id FROM friends WHERE user1='$log_username' AND user2='$friend_username' AND accepted='1' OR user1='$friend_username' AND user2='$log_username' AND accepted='1' LIMIT 1";
//            if (mysqli_num_rows(mysqli_query($db_conx, $friend_check)) > 0) {
//                $isFriend = true;
//            }
//            $block_check1 = "SELECT id FROM blockedusers WHERE blocker='$friend_username' AND blockee='$log_username' LIMIT 1";
//            if (mysqli_num_rows(mysqli_query($db_conx, $block_check1)) > 0) {
//                $ownerBlockViewer = true;
//            }
//            $block_check2 = "SELECT id FROM blockedusers WHERE blocker='$log_username' AND blockee='$friend_username' LIMIT 1";
//            if (mysqli_num_rows(mysqli_query($db_conx, $block_check2)) > 0) {
//                $viewerBlockOwner = true;
//            }
//        }
//        if ($isFriend == true) {
//            $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'unfriend\',\'' . $friend_username . '\',\'friendBtn\')">Unfriend</button>';
//        } else if ($user_ok == true && $friend_username != $log_username && $ownerBlockViewer == false) {
//            $friend_button = '<button class="btn btn-primary" onclick="friendToggle(\'friend\',\'' . $friend_username . '\',\'friendBtn\')" style="">Request As Friend</button>';
//        }
//// LOGIC FOR BLOCK BUTTON
//        if ($viewerBlockOwner == true) {
//            $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'unblock\',\'' . $friend_username . '\',\'blockBtn\')">Unblock User</button>';
//        } else if ($user_ok == true && $friend_username != $log_username) {
//            $block_button = '<button class="btn btn-primary" onClick="blockToggle(\'block\',\'' . $friend_username . '\',\'blockBtn\')">Block User</button>';
//        }
//
//        $gallery_list .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'">'; // background-color: #B0E0E6\
//        $gallery_list .= '<div class=\'pure-u-1  pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'><center><div class="image" onclick="window.location = \'user.php?id=' . $friend_id . '&u=' . $friend_username . '\'"></center>'; // was md-2-5
//        $gallery_list .= '<center><img class="pure-img" src="' . $friend_pic . '" alt="cover photo"></center>'; //id="thumbnail"        
//        $gallery_list .= '</div>';
//        //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
//        //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
//        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-2-5 pure-u-md-2-5 pure-u-lg-2-5\'>
//                    <div class=\'pure-g\' style="padding-left: 10px;">
//                        <div class=\'pure-u-1\'><b style="text-align: center;">' . $friend_fname . ' ' . $friend_lname . '</b></div>
//                        <div class=\'pure-u-1\'><text>Username:<span class="badge">' . $friend_username . '</span></text></div>
//                        <div class=\'pure-u-1\'>' . $friend_fname . ' is a(n)<span class="badge">' . $usertype . '</span></div>
//                            <div class=\'pure-u-1\'>Location: <span class="badge">' . $friend_country . '</span></div>
//                                <div class=\'pure-u-1\'>
//                        <p>
//                            <span id="friendBtn">
//                                ' . $friend_button . '</span>
//                        </p></div>
//                        <div class=\'pure-u-1\'><p>
//                            <span id="blockBtn">
//                                ' . $block_button . '
//                            </span>
//                        </p>
//                    </div>
//                    </div>
//                </div>';
////        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
////                    <div class=\'pure-g\' style="padding-left: 10px;">
////                        
////                    </div>
////                </div>';
////        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:120px; height:100px;">';
//        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-2-5\' style=\'margin:auto;\'><center>'
//                . '<div class=\'pure-u-1\'><center>
//                        <p>
//                            <span id="friendBtn">
//                                ' . $friend_button . '</span>
//                        </p>
//                        <p>
//                            <span id="blockBtn">
//                                ' . $block_button . '
//                            </span>
//                        </p>
//                    </center></div></center></div>';
////$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
//        $gallery_list .= '</div><hr>';
////        $friendsHTML .= '<div class="col-sm-2 col-md-2"><div class="thumbnail" style="text-align:center; background:#CCF5FF;">'; //class="thumbnail"  col-md-2 beside sm-2
////        $friendsHTML .= '<a href="user.php?u=' . $friend_username . '"><img style="width:80px; height:80px;" class="friendpics" src="' . $friend_pic . '" alt="' . $friend_username . '" title="' . $friend_username . '"></a>';
////        $friendsHTML .= '<div class="caption"><p style="text-align: center; font-size: 18px;">' . $friend_username . '</p></div></div></div>';
//    }
//}
//
//$sql = "SELECT * FROM posts WHERE expired='0' ORDER BY postdate DESC"; //, usr, filename FROM postpictures";
//$query = mysqli_query($db_conx, $sql);
//if (mysqli_num_rows($query) < 1) {
//    $gallery_list = "This user has not uploaded any posts yet.";
//} else {
//    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
//        $id = $row["id"];
//        $u = $row["user"];
//        $pn = $row["postName"];
//        $desc = $row["description"];
//        $asking = $row["askingPrice"];
//        $location = $row["location"];
//        $bedroom = $row["bedroom"];
//        $bathroom = $row["bathroom"];
//        $sqfootage = $row["sqfootage"];
//        $basement = $row["basement"];
//        $img = "";
//
//        $countquery = mysqli_query($db_conx, "SELECT COUNT(id) FROM postpictures WHERE usr='$u' AND postid='$id'");
//        $countrow = mysqli_fetch_row($countquery);
//        $count = $countrow[0];
//        $filequery = mysqli_query($db_conx, "SELECT filename FROM postpictures WHERE usr='$u' AND postid='$id' ORDER BY RAND() LIMIT 1");
//        $filerow = mysqli_fetch_row($filequery);
//        $file = $filerow[0];
//
//
//
//        $gallery_list .= '<div class=\'pure-g\' style=\'padding:5px; background-color: #c6e2ff;\' onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '\'">'; // background-color: #B0E0E6\
//        $gallery_list .= '<div class=\'pure-u-1  pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'><center><div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'"></center>'; // was md-2-5
//        if ($filerow == NULL) {
//            $gallery_list .= '<img class="pure-img" src="images/altposts.png" alt="cover photo">'; //id="thumbnail"
//        } else {
//            $gallery_list .= '<img class="pure-img" src="user/' . $u . '/' . $file . '" alt="cover photo">'; //style="width:160px; height:120px;"id="thumbnail"
//        }
//        $gallery_list .= '</div>';
//        //$gallery_list .= '<h3 style="text-align: center;" class="content-subhead">' . $u . '</h3>';
//        //$gallery_list .= '<div class="image" onclick="window.location = \'postphotos.php?postid=' . $id . '&u=' . $u . '&postn=' . $pn . '\'">';
//        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-2-5 pure-u-md-2-5 pure-u-lg-2-5\'>
//                    <div class=\'pure-g\' style="padding-left: 10px;">
//                        <div class=\'pure-u-1\'><b style="text-align: center;">Seller: ' . $u . '</b></div>
//                        <div class=\'pure-u-1\'><text>Price:<span class="badge">$' . number_format($asking) . '</span></text></div>
//                        <div class=\'pure-u-1\'><text>Square Footage: <span class="badge">' . $sqfootage . 'Sq Feet </span></text></div>
//                        <div class=\'pure-u-1\'><text>Description: ' . $desc . '</text></div>
//                    </div>
//                </div>';
//        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\'>
//                    <div class=\'pure-g\' style="padding-left: 10px;">
//                        <div class=\'pure-u-1\'>Bathroom: <span class="badge">' . $bathroom . '</span></b></div>
//                        <div class=\'pure-u-1\'><text>Bedroom:<span class="badge">' . $bedroom . '</span></text></div>
//                        <div class=\'pure-u-1\'><text>Basement:<span class="badge">' . $basement . '</span></text></div>
//                        <div class=\'pure-u-1\'><text>Location:<span class="badge">' . $location . '</span></text></div>
//                    </div>
//                </div>';
////        $gallery_list .= '<img src="user/' . $u . '/' . $file . '" alt="cover photo" style="width:120px; height:100px;">';
//        $gallery_list .= '<div class=\'pure-u-1 pure-u-sm-1-5 pure-u-md-1-5 pure-u-lg-1-5\' style=\'margin:auto;\'><center><i class="fa fa-youtube-square fa-3x"></i></center></div>';
////$gallery_list .= '<b style="margin-left: 28px;">' . $pn . ' (' . $count . ')</b>';
//        $gallery_list .= '</div><hr>';
//    }
//}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title> Rewrapped - <?php echo $user_name ?> Friends </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A layout example that shows off a responsive product landing page.">

        <!--<title>Landing Page &ndash; Layout Examples &ndash; Pure</title>-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">


        <link rel="stylesheet" href="css/fancyInput-master/styles.css">
        <link rel="stylesheet" href="css/fancyInput-master/fancyInput.css">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script id="twitter-wjs" src="http://platform.twitter.com/widgets.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css">
        <!--<![endif]-->
        <!-- end of old code-->

        <style>
            .pure-g > div {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .l-box {
                padding: 1em;
            }
            /*            .pure-g:hover {
                            background: yellow;
                        }*/

            @media screen and (min-width: 35.5em){
                #thumbnail{
                    width:30px; 
                    height:20px;
                }
            }

            @media screen and (min-width: 48em){
                #thumbnail{
                    width:120px; 
                    height:80px;
                }
            }
            @media screen and (min-width: 64em){
                #thumbnail{
                    width:160px; 
                    height:120px;
                }
            }
        </style>

        <style>
            * {
                font-family : tahoma, verdana, arial, sans-serif;
            }
            .outer-container {
                width: 100%;
                border: 1px solid #C4CDE0;
                border-radius: 2px;
                margin: 0 auto;
                height: auto;
                overflow: hidden;
            }
            .fb-search-container {
                background: #F6F7F8;
                border-bottom: 1px solid #C4CDE0;
                height: 40px;
            }
            .heading {
                font-size: 16px;
                font-weight: bold;
                width: 70%;
                color: #333333;
                margin-left: 20px;
            }
            .heading img {
                background-position:left;
                margin-top:10px;
                float:left;
            }
            .float {
                float: left;
                line-height: 44px;
            }
            .textbox {
                margin-top: 5px;
            }
            .textbox input {
                border: 1px solid #C4CDE0;
                outline: medium none;
                padding: 2px;
                width: 190px;
                font-size: 11px;
                height: 22px;
            }
            .fb-friends-list {
                margin: 20px 25px;
            }
            .inner_profile {
                border: 1px solid #CCC;
                width: 100%;
                height: 80px;
                margin-bottom:10px;
                margin-top: 2px;
                margin-right: 10px;
                float:left;
            }
            .name {
                margin-left: 10px;
                margin-top:0px;
            }
            .name a {
                font-size: 12px;
                font-weight: bold;
                color: #3B5998;
                cursor: pointer;
                text-decoration: none;
            }
            .name a:hover {
                text-decoration: underline;
            }

            #result { font-size:12px; font-family:Verdana, Geneva, sans-serif; margin:10px; } 
            .friend_span{
                font-size:20px;
                padding-left:5px;
            }
            @media screen and (min-width: 35.5em){
                #thumbnail{
                    width:30px; 
                    height:20px;
                }
                .inner_profile {
                    border: 1px solid #CCC;
                    width: 100%;
                    height: 80px;
                    margin-bottom:10px;
                    margin-top: 2px;
                    margin-right: 10px;
                    float:left;
                }
            }

            @media screen and (min-width: 48em){
                #thumbnail{
                    width:120px; 
                    height:80px;
                }
                .inner_profile {
                    border: 1px solid #CCC;
                    width: 49%;
                    height: 80px;
                    margin-bottom:10px;
                    margin-top: 2px;
                    margin-right: 10px;
                    float:left;
                }
            }
            @media screen and (min-width: 64em){
                #thumbnail{
                    width:160px; 
                    height:120px;
                }
                .inner_profile {
                    border: 1px solid #CCC;
                    width: 24%;
                    height: 80px;
                    margin-bottom:10px;
                    margin-top: 2px;
                    margin-right: 10px;
                    float:left;
                }
            }

        </style>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".friend_name").keyup(function () {
                    var str = $(".friend_name").val();
                    var count = 0;
                    $(".fb-friends-list .inner_profile").each(function (index) {
                        if ($(this).attr("id")) {
                            //case insenstive search
                            if (!$(this).attr("id").match(new RegExp(str, "i"))) {
                                $(this).fadeOut("fast");
                            } else {
                                $(this).fadeIn("slow");
                                count++;
                            }
                        }
                    });

//                    $('div :input').fancyInput();

                    if (str == '') {
                        $("#result").hide();
                    } else {
                        $("#result").show();
                    }
                    //display no of results found
                    if (count < 1) {
                        $("#result").text("No results for: " + str);
                    } else {
                        $("#result").text("Top " + count + " results for " + str);
                    }
                });
            });
        </script>


    </head>
    <body>
        <?php include_once ("include/template_pageTop.php"); ?>
        <!--<a href="mysql_connect.php">Search</a>-->
<!--        <div class="jumbotron">
            <div class="container">
                <h2>Friends</h2>
                <p>Rewrapped lets you list your house with ease, avoiding pesky posting and other listing related fees you may incur.  Once posted Real Estate agents may bid on your property based on their commission rate.  There are other cool features such as finding Open Houses near near you! Click below for more.</p>
                <p><a class="btn btn-primary btn-lg" href="info.php" role="button">Learn more&raquo;</a></p>
            </div>
        </div>-->

        <div class="container">

            <h2 class="content-head is-center" style="text-aligh: center;"><?php echo $user_name ?> Friends</h2>


<!--            <div class='pure-g' style='padding:5px; background-color: #B0E0E6;'>
                <div class='pure-u-1 pure-u-md-1 pure-u-lg-1-5' style='margin:auto;'><center><i class="fa fa-html5 fa-3x"></center></i></div>
                <div class='pure-u-1 pure-u-md-3-5 pure-u-lg-3-5'>
                    <div class='pure-g'>
                        <div class='pure-u-1'><b>Lorem ipsum dolor sit amet, consectetur adipiscing.</b></div>
                        <div class='pure-u-1'><text>Some text goes here.</text></div>
                        <div class='pure-u-1'><text>And then more text down here.</text></div>
                    </div>
                </div>
                <div class='pure-u-1 pure-u-md-2-5 pure-u-lg-1-5' style='margin:auto;'><center><i class="fa fa-youtube-square fa-3x"></i></center></div>

            </div>-->
            <hr>
            <div class="fb-search-container pure-form">
                <div class="pure-u-1 pure-u-md-1-6">
                    <div class="heading float">
                        <img alt="" src="fb_friends.png" align="absmiddle" />
                        <span class='friend_span'>Friends</span>
                    </div>
                </div>
                <div class="pure-u-1 pure-u-md-2-3">
                    <div class="textbox float">
                        <input class="friend_name pure-input-rounded" type="text" autocomplete="off" name="search" placeholder="Search <?php echo $user_name; ?> friends" value="" />
                    </div>
                </div>
            </div>            
            <div class="outer-container pure-g" style='padding:5px;'>
                <div class="fb-friends-list pure-u-1">
                    <div id="result"></div>
                    <!--<div class='pure-g' style="padding-left: 10px;">-->
                    <div class="pure-u-1">
                        <?php foreach ($arrFriendList as $key => $user) { ?>
                            <div class="inner_profile" id="<?= $user["firstname"] ?> <?= $user["lastname"] ?>" onclick='window.location = "user.php?id=<?= $user["id"] ?>&u=<?= $user["username"] ?>"'>
                                <div class="photo float"><img class="pure-img" alt="" style="width: 60px; height: 77px;" src="user/<?= $user["username"] ?>/<?= $user["avatar"] ?>" />
                                </div>
                                <?php
                                if ($user["userlevel"] == "a") {
                                    $usertype2 = "Home Owner";
                                } else if ($user["userlevel"] == "b") {
                                    $usertype2 = "Agent";
                                } else if ($user["userlevel"] == "c") {
                                    $usertype2 = "Buyer";
                                } else if ($user["userlevel"] == "d") {
                                    $usertype2 = "Administrator";
                                }
                                ?>
                                <div class="name float">
                                    <a href="user.php?id=<?= $user['id'] ?>&u=<?= $user['username'] ?>"><?= $user["firstname"] ?> <?= $user["lastname"] ?></a>
                                    <br/><div><?php echo $usertype2; ?></div>
                                </div><br/>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <hr>            <!--<//?/p/h/p echo $gallery_list; ?>-->
        </div>
        <hr>
        <?php include_once ("include/template_pageBottom.php"); ?>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                                (function (b, o, i, l, e, r) {
                                    b.GoogleAnalyticsObject = l;
                                    b[l] || (b[l] =
                                            function () {
                                                (b[l].q = b[l].q || []).push(arguments)
                                            });
                                    b[l].l = +new Date;
                                    e = o.createElement(i);
                                    r = o.getElementsByTagName(i)[0];
                                    e.src = '//www.google-analytics.com/analytics.js';
                                    r.parentNode.insertBefore(e, r)
                                }(window, document, 'script', 'ga'));
                                ga('create', 'UA-XXXXX-X', 'auto');
                                ga('send', 'pageview');
        </script>
        <!-- end of bootstrap -->


    </body>
</html>
