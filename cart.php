<?php
// This file is www.developphp.com curriculum material
// Written by Adam Khoury January 01, 2011
// http://www.youtube.com/view_play_list?p=442E340A42191003
session_start(); // Start session first thing in script
// Script Error Reporting
error_reporting(E_ALL);
ini_set('display_errors', '1');
// Connect to the MySQL database  
include "include/mysql_connect.php";
//include "include/check_login_status.php";
// Make sure the _GET username is set, and sanitize it
if (isset($_SESSION["userid"])) {
    $userid = $_SESSION["userid"];
} else {
    header("location: index.php");
    exit();
}   

?>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//       Section 1 (if user attempts to add something to the cart from the product page)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$uid = '';
if (isset($_POST['pid'])) {
    $pid = $_POST['pid'];
//    $uid = $_POST['uid'];
    $wasFound = false;
    $i = 0;
    // If the cart session variable is not set or cart array is empty
    if (!isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
        // RUN IF THE CART IS EMPTY OR NOT SET
        $_SESSION["cart_array"] = array(0 => array("item_id" => $pid, "quantity" => 1));
    } else {
        // RUN IF THE CART HAS AT LEAST ONE ITEM IN IT
        foreach ($_SESSION["cart_array"] as $each_item) {
            $i++;
            while (list($key, $value) = each($each_item)) {
                if ($key == "item_id" && $value == $pid) {
                    // can say item is already added into cart cannot add again or notify that it will add 2
                    // That item is in cart already so let's adjust its quantity using array_splice()
                    array_splice($_SESSION["cart_array"], $i - 1, 1, array(array("item_id" => $pid, "quantity" => $each_item['quantity'] + 1)));
                    $wasFound = true;
                } // close if condition
            } // close while loop
        } // close foreach loop
        if ($wasFound == false) {
            // puts a new associative array to the multi-dim array and quantity one (second item)
            array_push($_SESSION["cart_array"], array("item_id" => $pid, "quantity" => 1));
        }
    }
    header("location: cart.php");
    exit();
}
?>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//       Section 2 (if user chooses to empty their shopping cart)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_GET['cmd']) && $_GET['cmd'] == "emptycart") {
    unset($_SESSION["cart_array"]);
}
?>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//       Section 3 (if user chooses to adjust item quantity)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_POST['item_to_adjust']) && $_POST['item_to_adjust'] != "") {
    // execute some code
    $item_to_adjust = $_POST['item_to_adjust'];
    $quantity = $_POST['quantity'];
    $quantity = preg_replace('#[^0-9]#i', '', $quantity); // filter everything but numbers
    if ($quantity >= 100) {
        $quantity = 99;
    }
    if ($quantity < 1) {
        $quantity = 1;
    }
    if ($quantity == "") {
        $quantity = 1;
    }
    $i = 0;
    foreach ($_SESSION["cart_array"] as $each_item) {
        $i++;
        while (list($key, $value) = each($each_item)) {
            if ($key == "item_id" && $value == $item_to_adjust) {
                // That item is in cart already so let's adjust its quantity using array_splice()
                array_splice($_SESSION["cart_array"], $i - 1, 1, array(array("item_id" => $item_to_adjust, "quantity" => $quantity)));
            } // close if condition
        } // close while loop
    } // close foreach loop
}
?>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//       Section 4 (if user wants to remove an item from cart)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_POST['index_to_remove']) && $_POST['index_to_remove'] != "") {
    // Access the array and run code to remove that array index
    $key_to_remove = $_POST['index_to_remove'];
    if (count($_SESSION["cart_array"]) <= 1) {
        unset($_SESSION["cart_array"]);
    } else {
        unset($_SESSION["cart_array"]["$key_to_remove"]);
        sort($_SESSION["cart_array"]);
    }
}
?>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//       Section 5  (render the cart for the user to view on the page)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$cartOutput = "";
$cartTotal = "";
$pp_checkout_btn = '';
$product_id_array = '';
if (!isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
    $cartOutput = "<h2 align='center'>Your shopping cart is empty</h2>";
} else {
    // Start PayPal Checkout Button
//    <form action = "https://www.paypal.com/cgi-bin/webscr" method = "post" target = "_top">
//    <input type = "hidden" name = "cmd" value = "_s-xclick">
//    <input type = "hidden" name = "hosted_button_id" value = "NNY2AAGCH786N">
//    <input type = "image" src = "https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border = "0" name = "submit" alt = "PayPal - The safer, easier way to pay online!">
//    <img alt = "" border = "0" src = "https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width = "1" height = "1">
//    </form >
    // PayPal Sandbox
//    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
//<input type="hidden" name="cmd" value="_s-xclick">
//<input type="hidden" name="hosted_button_id" value="XW7JWGXJX5RDE">
//<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
//<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
//</form>
//shabari.cs-facilitator@gmail.com
//
//
//    $pp_checkout_btn .= '<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    $pp_checkout_btn .= '<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_cart">
    <input type="hidden" name="upload" value="1">
    <input type="hidden" name="business" value="shabari.cs-facilitator@gmail.com">';
//    <input type="hidden" name="business" value="shabari.cs@gmail.com">';
    // Start the For Each loop
    $i = 0;
    foreach ($_SESSION["cart_array"] as $each_item) {
        $item_id = $each_item['item_id'];
        $sql = mysqli_query($db_conx, "SELECT * FROM products WHERE id='$item_id' LIMIT 1");
        while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
            $product_name = $row["product_name"];
            $price = $row["price"];
            $details = $row["details"];
        }
        $pricetotal = $price * $each_item['quantity'];
        $cartTotal = $pricetotal + $cartTotal;
        setlocale(LC_MONETARY, "en_CA");
        $pricetotal = number_format($pricetotal, 2, '.', ''); //printf("%10.2n", $pricetotal);
        // Dynamic Checkout Btn Assembly
        $x = $i + 1;
        $pp_checkout_btn .= '<input type="hidden" name="item_name_' . $x . '" value="' . $product_name . '">
        <input type="hidden" name="amount_' . $x . '" value="' . $price . '">
        <input type="hidden" name="quantity_' . $x . '" value="' . $each_item['quantity'] . '">  ';
        // Create the product array variable
        $product_id_array .= "$item_id-" . $each_item['quantity'] . ",";
        // Dynamic table row assembly
        $cartOutput .= "<tr>";
        $cartOutput .= '<td><a href="product.php?id=' . $item_id . '">' . $product_name . '</a><br /><img src="inventory_images/' . $item_id . '.jpg" alt="' . $product_name . '" width="40" height="52" border="1" /></td>';
        $cartOutput .= '<td>' . $details . '</td>';
        $cartOutput .= '<td>$' . $price . '</td>';
        $cartOutput .= '<td><form action="cart.php" method="post">
		<input name="quantity" type="text" value="' . $each_item['quantity'] . '" size="1" maxlength="2" />
		<input name="adjustBtn' . $item_id . '" type="submit" value="change" />
		<input name="item_to_adjust" type="hidden" value="' . $item_id . '" />
		</form></td>';
        //$cartOutput .= '<td>' . $each_item['quantity'] . '</td>';
        $cartOutput .= '<td>' . $pricetotal . '</td>';
        $cartOutput .= '<td><form action="cart.php" method="post"><input name="deleteBtn' . $item_id . '" type="submit" value="X" /><input name="index_to_remove" type="hidden" value="' . $i . '" /></form></td>';
        $cartOutput .= '</tr>';
        $i++;
    }
    setlocale(LC_MONETARY, "en_CA");
    $cartTotal = number_format($cartTotal, 2, '.', ''); //printf("%10.2n", $cartTotal);
    $cartTotal = "<div style='font-size:18px; margin-top:12px;' align='right'>Cart Total : $" . $cartTotal . " CAD</div>";
    // Finish the Paypal Checkout Btn
    // the ipn and checkout complete... have to be https..
    $pp_checkout_btn .= '<input type="hidden" name="custom" value="'. $product_id_array .' ' . $_SESSION["userid"] .'">
	<input type="hidden" name="notify_url" value="http://www.Rewrapped.ca/include/my_ipn.php">
	<input type="hidden" name="return" value="http://www.Rewrapped.ca/checkout_complete.php">
	<input type="hidden" name="rm" value="2">
	<input type="hidden" name="cbt" value="Return to The Store">
	<input type="hidden" name="cancel_return" value="http://www.yoursite.com/paypal_cancel.php">
	<input type="hidden" name="lc" value="CA">
	<input type="hidden" name="currency_code" value="CAD">
        <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border = "0" name="submit" alt="Make payments with PayPal - its fast, free and secure!">
	<img alt = "" border = "0" src = "https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width = "1" height = "1">
        
	</form>';
    
//    <img alt = "" border = "0" src = "https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width = "1" height = "1">
//    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border = "0" name="submit" alt="Make payments with PayPal - its fast, free and secure!">
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Your Cart</title>
        <link rel="stylesheet" href="style/style.css" type="text/css" media="screen" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
            <link rel="icon" href="images/altposts.png" type="image/x-icon">
                <link rel="stylesheet" href="css/bootstrap.min.css" />
                <link rel="stylesheet" href="css/bootstrap-theme.min.css" />

                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


                <!--[if lte IE 8]>
                  
                    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
                  
                <![endif]-->
                <!--[if gt IE 8]><!-->

                <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

                <!--<![endif]-->



                <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />



                <link rel="stylesheet" href="css/demo.css">
                    <link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">

                        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

                            <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
                                <!--[if lte IE 8]>
                                    <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
                                <![endif]-->
                                <!--[if gt IE 8]><!-->
                                <link rel="stylesheet" href="css/layouts/marketing.css" />
                                <!--<![endif]-->
                                <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
                                <!--        <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
                                <link rel="stylesheet" href="css/main.css"/>
                                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
                                <link rel="icon" href="favicon.ico" type="image/x-icon"/>
                                <!-- starting of old code-->
                                <link rel="stylesheet" href="style/style.css"/>


                                </head>
                                <body>

                                    &nbsp;
                                    <div align="center" id="mainWrapper">
                                        <div id="pageContent">
                                            <div align="left" style="margin-right:32px;">
                                                <a href="allProducts.php">Return to Products Page</a>
                                            </div>
                                            <div style="margin:24px; text-align:left;">
                                                <br />
                                                <table width="100%" border="1" cellspacing="0" cellpadding="6">
                                                    <tr>
                                                        <td width="18%" bgcolor="#C5DFFA"><strong>Product</strong></td>
                                                        <td width="45%" bgcolor="#C5DFFA"><strong>Product Description</strong></td>
                                                        <td width="10%" bgcolor="#C5DFFA"><strong>Unit Price</strong></td>
                                                        <td width="9%" bgcolor="#C5DFFA"><strong>Quantity</strong></td>
                                                        <td width="9%" bgcolor="#C5DFFA"><strong>Total</strong></td>
                                                        <td width="9%" bgcolor="#C5DFFA"><strong>Remove</strong></td>
                                                    </tr>
<?php echo $cartOutput; ?>
                                                   <!-- <tr>
                                                      <td>&nbsp;</td>
                                                      <td>&nbsp;</td>
                                                      <td>&nbsp;</td>
                                                      <td>&nbsp;</td>
                                                      <td>&nbsp;</td>
                                                      <td>&nbsp;</td>
                                                    </tr> -->
                                                </table>
<?php echo $cartTotal; ?>
                                                <br />
                                                <br />
                                                <?php echo $pp_checkout_btn; ?>
                                                <br />
                                                <br />
                                                <a href="cart.php?cmd=emptycart">Click Here to Empty Your Shopping Cart</a>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
<?php include_once("include/template_pageBottom.php"); ?>
                                </body>
                                </html>