<?php
// This file is www.developphp.com curriculum material
// Written by Adam Khoury January 01, 2011
// http://www.youtube.com/view_play_list?p=442E340A42191003
// Script Error Reporting
session_start();    

error_reporting(E_ALL);
ini_set('display_errors', '1');

// Make sure the _GET username is set, and sanitize it
if (!isset($_SESSION["userid"])) {
    header("location: index.php");
    exit();
}


?>
<?php
// Run a select query to get my letest 6 items
// Connect to the MySQL database  
include "include/check_login_status.php";
//include "include/mysql_connect.php";
$dynamicList = "";
$count = 0;
$sql = mysqli_query($db_conx, "SELECT * FROM products ORDER BY date_added DESC LIMIT 6");
$productCount = mysqli_num_rows($sql); // count the output amount
if ($productCount > 0) {
    while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
        $count ++; 
        $id = $row["id"];
        $product_name = $row["product_name"];
        $price = $row["price"];
        $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
        $dynamicList .= '<div id="box'.$count.'" class="box"><table width="100%" border="0" cellspacing="0" cellpadding="6">
        <tr>
          <td width="17%" valign="top"><a href="product.php?id=' . $id . '"><img style="border:#666 1px solid;" src="inventory_images/' . $id . '.jpg" alt="' . $product_name . '" width="77" height="102" border="1" /></a></td>
          <td width="83%" valign="top">' . $product_name . '<br />
            $' . $price . '<br />
            <a href="product.php?id=' . $id . '">View Product Details</a></td>
        </tr>
      </table></div>';
    }
} else {
    $dynamicList = "We have no products listed in our store yet";
}
//mysqli_close($db_conx);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Store Home Page</title>
        <link rel="stylesheet" href="style/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/demo.css">
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css"/>
        
        <!-- Pasted from Index.php-->
         <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" />
        <!--<meta name="keywords" content="footer, address, phone, icons" />-->
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <style type="text/css">
            .box {
  position: static;
  /*background-image:url('http://www.teacherfiles.com/clipart/xbackgrounds/lined_paper_light.jpg');*/
  background-color: white;
  width: 300px;
  height: 200px;
  float: left;
  border: 6px solid #338CC3;
  margin: 0;
  text-align: center;
}

#box2 {
  /* The value of margin-left is the width of the FIRST .box including border attribute. */
  margin-left: 36px;
}

#box3 {
  /* The value of margin-left is the width of the FIRST and SECOND .box including border attribute. */
  margin-left: 52px;
}

.box:hover{
  width: 350px;
  
  /* Add extra 52px to normal height. Then, 1/2 of that is for top and the other half is for bottom. */
  height: 252px;
  
  /* Below we are using -26px ( 1/2 of total "extra' used for height). */
  margin-top: -26px;
  
  /* Make the border color lighter/brighter than non-hover color */
  border: 6px solid #52B5D5;
 
  /* Add drop shadow effect */
  /*   http://www.webestools.com/css3-box-shadow-generator-css-property-easy-shadows-div-html5-drop-shadow-moz-webkit-shadow-maker.html   */
  -moz-box-shadow:0px 0px 50px 0px #000000;
  -webkit-box-shadow:0px 0px 50px 0px #000000;
  box-shadow:0px 0px 50px 0px #000000;
  
  /* Add curved corners via CSS3 Border radius to popped item that mimic original website effect */
  /*   http://border-radius.com/    */
  -webkit-border-top-left-radius: 10px;
  -webkit-border-top-right-radius: 10px;
  -moz-border-radius-topleft: 10px;
  -moz-border-radius-topright: 10px;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  
  /* Give it a higher z-index to it's on top of adjacent items near it. */
  z-index: 10;
}



#footer {
    width:100%;
    position:absolute;
    bottom:0;
    left:0;
}
        </style>

        <link rel="stylesheet" href="css/demo.css">
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css"/>


	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css" />
        <!--<![endif]-->
        <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <!-- starting of old code-->
        <link rel="stylesheet" href="style/style.css">
            <!-- pasted from index.php -->
        
    </head>
    <body>
        <?php include_once("include/template_pageTop.php"); ?>
        &nbsp;<br/>
        <div align="center" id="mainWrapper" class="container">
            
            <div id="pageContent">
                <table width="100%" border="0" cellspacing="0" cellpadding="10">
                    <tr>
                        <!-- <td width="32%" valign="top"><h3>What the Hell?</h3>
                            <p>This website is very temporarily being used as an online live showcase area for an E - Commerce tutorial script set Adam is creating which can be seen on his channel here:<br />
                                <a href="http://www.youtube.com/flashbuilding" target="_blank">http://www.youtube.com/flashbuilding</a> </p>
                            <p>It is not an actual store and it will change directly after the tutorial series. <br />
                                <br />
                                This tutorial series is for educational purposes only. Use the scripts at your own risk.</p></td> -->
                        <td width="35%" valign="top"><h3>Latest Designer Fashions</h3>
                            <p><?php echo $dynamicList; ?><br />
                            </p>
                            <p><br />
                            </p></td>
                        <!-- <td width="33%" valign="top"><h3>Handy Tips</h3>
                            <p>If you operate any store online you should read the documentation provided to you by the online payment gateway you choose for handling the checkout process. You can get much more insight than I can offer on the various details of a gateway, from the gateway providers themselves. They are there to help you with whatever you need since they get a cut of your online business dealings.</p></td> -->
                    </tr>
                </table>

            </div>
        </div>
        <div id="footer">
        <?php include_once("include/template_pageBottom.php"); ?> </div>
    </body>
</html>