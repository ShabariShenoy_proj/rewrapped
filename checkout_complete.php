<?php
include_once ("include/check_login_status.php");
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title> Rewrapped - Home </title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <!--<link rel="icon" href="images/altposts.png" type="image/x-icon">-->
        <link rel="icon" href="#" class="glyphicon glyphicon-home" type="image/x-icon"/>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css" />
        <!--<meta name="keywords" content="footer, address, phone, icons" />-->
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css" />
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <link rel="stylesheet" href="css/demo.css">
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <!--[if lte IE 8]>
          
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
          
        <![endif]-->
        <!--[if gt IE 8]><!-->

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css" />

        <!--<![endif]-->



        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />




        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css" />
        <!--<![endif]-->
        <link rel="stylesheet" href="C:/Users/shshenoy/Desktop/Backup/font-awesome-4.3.0/font-awesome-4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css"/>
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <!-- starting of old code-->
        <link rel="stylesheet" href="style/style.css">
        <style type="text/css">
            .section {
                width: 29%;
                float: left;
                margin: 2% 2%;
                text-align: center;
            }
            body {
                height:100%;
            }
            form#photo_form{background:#F3FDD0; border:#AFD80E 1px solid; padding:20px;}
            div#galleries{}
            div#galleries > div{float:left; margin:20px; text-align:center; cursor:pointer;}
            div#galleries > div > div {height:100px; overflow:hidden;}
            div#galleries > div > div > img{width:150px; cursor:pointer;}
            div#photos{display:none; border:#666 1px solid; padding:20px;}
            div#photos > div{float:left; width:125px; height:80px; overflow:hidden; margin:20px;}
            div#photos > div > img{width:125px; cursor:pointer;}
            div#picbox{display:none; padding-top:36px;}
            div#picbox > img{max-width:800px; display:block; margin:0px auto;}
            div#picbox > button{ display:block; float:right; font-size:36px; padding:3px 16px;}
        </style>
        <script src="js/main.js"></script>
        <script src="js/ajax.js"></script>
        <script>
            function emptyElement(x) {
                _(x).innerHTML = "";
            }
            function login() {
                var e = _("email").value;
                var p = _("password").value;
                if (e == "" || p == "") {
                    _("status").innerHTML = "Fill out all of the form data";
                } else {
                    _("loginbtn").style.display = "none";
                    _("status").innerHTML = 'please wait ...';
                    var ajax = ajaxObj("POST", "login.php");
                    ajax.onreadystatechange = function () {
                        if (ajaxReturn(ajax) == true) {
                            if (ajax.responseText == "login_failed") {
                                _("status").innerHTML = "Login unsuccessful, please try again.";
                                _("loginbtn").style.display = "block";
                            } else {
                                window.location = "user.php?u=" + ajax.responseText;
                            }
                        }
                    }
                    ajax.send("e=" + e + "&p=" + p);
                }
            }
        </script>
        <!-- end of old code-->
    </head>
    <body>
        <?php include_once("analyticstracking.php") ?>
        <?php include_once ("include/template_pageTop.php"); ?>
        <!--<a href="mysql_connect.php">Search</a>-->
        <div class="jumbotron">
            <div class="container">
                <h1>Thank you for your business</h1>
                <p>We look forward to doing more with you!</p>
<!--                <php echo $_SESSION["userid"]; ?>
                <php echo $log_username; ?>
                <php echo $log_id; ?>-->
            </div>
        </div>
        <!-- start from twitter bootstrap-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-65412649-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!--
        <!-- end of bootstrap -->
        <?php include_once ("include/template_pageBottom.php"); ?>
    </body>
</html>
